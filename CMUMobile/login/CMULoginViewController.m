//
//  CMULoginViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/10/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMULoginViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUStringUtils.h"
#import "CMULoginModel.h"
#import "CMUSettings.h"
#define REGIS_MOBILE_TOKEN @"https://mobileapi.cmu.ac.th/api/User/regisMobileTOkenTest?cmuaccount=%@&systemName=edoc&oToken=%@"

@interface CMULoginViewController ()
@property(nonatomic, weak) IBOutlet UITextField *txtUsername;
@property(nonatomic, weak) IBOutlet UITextField *txtPassword;
@property(nonatomic, weak) IBOutlet UIButton *btnLogin;
@property(nonatomic, weak) IBOutlet UIButton *btnCancel;
@property(nonatomic, strong) CMUTicket *ticket;

- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;
@end

@implementation CMULoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
#ifdef DEBUG
//    self.txtUsername.text = @"maysayawan_s";
//    self.txtPassword.text = @"maysayawan";
    
//    self.txtUsername.text = @"nuttanont_p";
//    self.txtPassword.text = @"88888888";
    
//
//    self.txtUsername.text = @"user01_std";
//    self.txtPassword.text = @"user01_std";
//    self.txtUsername.text = @"user02_std";
//    self.txtPassword.text = @"user02_std";
//    self.txtUsername.text = @"user03_std";
//    self.txtPassword.text = @"user03_std";
//    self.txtUsername.text = @"user04_std";
//    self.txtPassword.text = @"user04_std";
//    self.txtUsername.text = @"user05_std";
//    self.txtPassword.text = @"user05_std";
    
//    self.txtUsername.text = @"user01.emp";
//    self.txtPassword.text = @"user01.emp";
//    self.txtUsername.text = @"user02.emp";
//    self.txtPassword.text = @"user02.emp";
//    self.txtUsername.text = @"user03.emp";
//    self.txtPassword.text = @"user03.emp";
//    self.txtUsername.text = @"user04.emp";
//    self.txtPassword.text = @"user04.emp";
//    self.txtUsername.text = @"user05.emp";
//    self.txtPassword.text = @"user05.emp";
    
    
//    self.txtUsername.text = @"cheewin.b";
//    self.txtPassword.text = @"best_Mfec";
#endif
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnLoginClicked:(id)sender
{
    [self login];
}

- (void)btnCancelClicked:(id)sender
{
   if(_delegate && [_delegate respondsToSelector:@selector(loginViewControllerDidCancel:)])
   {
       [_delegate loginViewControllerDidCancel:self];
   }
}

- (void)showAlertView:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
}

- (void)login
{
    NSString *username = self.txtUsername.text;
    NSString *password = self.txtPassword.text;
    
    if([CMUStringUtils isEmpty:username])
    {
        [self showAlertView:@"กรุณาระบุชื่อผู้ใช้งาน"];
        [self.txtUsername becomeFirstResponder];
        return;
    }
    if([CMUStringUtils isEmpty:password])
    {
        [self showAlertView:@"กรุณาระบุรหัสผ่าน"];
        [self.txtUsername becomeFirstResponder];
        return;
    }
    
    [self showHUDLoading];
    //__weak typeof(self) weakSelf = self;
    [[CMUWebServiceManager sharedInstance] login:self userName:username password:password success:^(id result) {
        //ได้ access_token, username, expires, expires_in, issued
        
        CMULoginModel *loginModel = (CMULoginModel *)result;
        if(![loginModel success])
        {
            [self showAlertView:@"ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง"];
            [self dismisHUD];
            return;
        }
        
        [[CMUWebServiceManager sharedInstance] getUserInfo:self success:^(id result) {
            CMUUserInfoModel *userInfoModel = (CMUUserInfoModel *)result;
            [self dismisHUD];
            if(userInfoModel.success)
            {
                if(_delegate && [_delegate respondsToSelector:@selector(loginViewControllerDidLogin:)])
                {
                    [_delegate loginViewControllerDidLogin:self];
                    //check login
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLoginSuccess"];
                    CMUSettings *share = [[CMUSettings sharedInstance]init];
                    [share loadMenuItems];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:self];
                    NSLog(@"LOGIN SUCCESS");
       
                    [self getToken];
                }
                
                [[CMUWebServiceManager sharedInstance] registerPushNotification:self success:^(id result) {
                    
                } failure:^(NSError *error) {
                    
                }];
            }
            else
            {
               [self showAlertView:@"เกิดข้อผิดพลาดในการเข้าสู่ระบบ"];
            }
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
        
    } failure:^(NSError *error) {
        [self dismisHUD];
        [self showAlertView:@"เกิดข้อผิดพลาดในการเข้าสู่ระบบ"];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITextFieldDelegate

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
//- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField;          // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
//- (void)textFieldDidEndEditing:(UITextField *)textField;             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
//
//- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.txtUsername)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else if(textField == self.txtPassword)
    {
        [self.view endEditing:YES];
        [self login];
    }
    return YES;
}
// called when 'return' key pressed. return NO to ignore.
-(void)getToken{
    CMUTicket *ticket = [[CMUWebServiceManager sharedInstance] ticket];
    //Set Access Token String
    NSString *URLString = [NSString stringWithFormat:REGIS_MOBILE_TOKEN,ticket.userName,@""];
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    //Set Header
    //[dataRqst addValue:[userDefault valueForKey:@"access_token"] forHTTPHeaderField: @"token"];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",URLString);
    NSLog(@"[GET] return :%@",json);
    [[NSUserDefaults standardUserDefaults]setValue:[json valueForKey:@"data"] forKey:@"edoc_token"];
    NSLog(@"edoc_token :%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"edoc_token"]);
}
@end
