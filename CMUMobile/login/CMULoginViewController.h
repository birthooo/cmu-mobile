//
//  CMULoginViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/10/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUViewController.h"

@class CMULoginViewController;

@protocol CMULoginViewControllerDelegate <NSObject>
- (void)loginViewControllerDidLogin:(CMULoginViewController *) loginViewController;
- (void)loginViewControllerDidCancel:(CMULoginViewController *) loginViewController;
@end

@interface CMULoginViewController : CMUViewController<UITextFieldDelegate>
@property(nonatomic, unsafe_unretained) id<CMULoginViewControllerDelegate> delegate;
@end
