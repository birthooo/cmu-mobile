//
//  CMUTransitBusVIew.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/5/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import "CMUTransitBusView.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface CMUTransitBusView()
@property(nonatomic, weak) IBOutlet UIImageView *imageView;
@end

@implementation CMUTransitBusView

- (void)customInit
{
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUTransitBusView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:contentView];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setRouteId:(int)routeId transitBusPosition:(CMUTransitBusPosition*) transitBusPosition
{
    UIImage *icon = nil;
    switch (routeId) {
        case 1:
            icon = [UIImage imageNamed:@"bus_green.png"];
            break;
        case 2:
            icon = [UIImage imageNamed:@"bus_orange.png"];
            break;
        case 3:
            icon = [UIImage imageNamed:@"bus_red.png"];
            break;
        case 4:
            icon = [UIImage imageNamed:@"bus_blue.png"];
            break;
        case 5:
            icon = [UIImage imageNamed:@"bus_purple.png"];
            break;
        case 6:
            icon = [UIImage imageNamed:@"bus_six.png"];
            break;
        default:
            break;
    }
    self.imageView.image = icon;
    self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(transitBusPosition.direction));
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
