//
//  TransitWebViewViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 11/1/2561 BE.
//  Copyright © 2561 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
@interface TransitWebViewViewController : CMUNavBarViewController <UIWebViewDelegate>

@end
