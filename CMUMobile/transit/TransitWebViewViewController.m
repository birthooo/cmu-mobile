//
//  TransitWebViewViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 11/1/2561 BE.
//  Copyright © 2561 IBSS. All rights reserved.
//

#import "TransitWebViewViewController.h"

@interface TransitWebViewViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *transitWebView;

@end

@implementation TransitWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.transitWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://cmutransit.bda.co.th/bus/live"]]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
