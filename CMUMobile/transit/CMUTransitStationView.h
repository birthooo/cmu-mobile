//
//  CMUTransitStationView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/5/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUTransit.h"

@interface CMUTransitStationView : UIView 
@property(nonatomic, weak)IBOutlet UIView* viewBackground;
@property(nonatomic, weak)IBOutlet UILabel* lblTitle;
- (void)setBusStation:(CMUTransitBusStation*) busStation color:(UIColor *)color;
@end
