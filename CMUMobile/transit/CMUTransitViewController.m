//transit
//  CMUTransitViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/26/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import "CMUTransitViewController.h"
#import "CMUTransitStationView.h"
#import "CMUTransitBusView.h"
#import "CMUWebServiceManager.h"
#import "CMUUIUtils.h"



#define ZONE1_LAT 18.800605
#define ZONE1_LNG 98.953139

#define DEFAULT_ZONE_ZOOM 15

#define GET_BUS_INTERVAL 10.0f



typedef void (^CompleatedBlock)(id result);

@interface CMUTransitGroupUI : NSObject <GMSMapViewDelegate>
- (id)initWithRouteId:(int)routeId map:(GMSMapView *) mapView routeCoordinate:(NSArray *)coors color:(UIColor *)color;
@property(nonatomic, readonly)GMSMapView *mapView;
@property(nonatomic, strong)GMSMutablePath *path;
@property(nonatomic, unsafe_unretained)BOOL visible;
- (void)pauseBusPositions;
@end

@implementation CMUTransitGroupUI

{
    UIColor *_color;
    int _routeId;
    GMSPolyline *_busRoute;//Polyline
    GMSPolyline *_busRoute2;
    
    GMSPolyline *_busRouteAll1;
    GMSPolyline *_busRouteAll2;
    GMSPolyline *_busRouteAll3;
    GMSPolyline *_busRouteAll4;
    GMSPolyline *_busRouteAll5;
    GMSPolyline *_busRouteAll6;
    
    NSMutableDictionary *_busStations;//String:Marker
    NSMutableDictionary *_busPositions;//String:Marker
    NSMutableDictionary *_AllbusStations;//String:Marker
    
    NSMutableDictionary *_AllbusStations1;//String:Marker
    NSMutableDictionary *_AllbusStations2;//String:Marker
    NSMutableDictionary *_AllbusStations3;//String:Marker
    NSMutableDictionary *_AllbusStations4;//String:Marker
    NSMutableDictionary *_AllbusStations5;//String:Marker
    NSMutableDictionary *_AllbusStations6;//String:Marker
    
    NSTimer *_timmer;
    
    CMUTransitGroupUI *group1;
    CMUTransitGroupUI *group2;
    CMUTransitGroupUI *group3;
    CMUTransitGroupUI *group4;
    CMUTransitGroupUI *group5;
    CMUTransitGroupUI *group6;
    
    
    
    BOOL group1Visible;
    BOOL group2Visible;
    BOOL group3Visible;
    BOOL group4Visible;
    BOOL group5Visible;
    BOOL group6Visible;
    
    BOOL didTap;
}


- (id)initWithRouteId:(int)routeId map:(GMSMapView *) mapView routeCoordinate:(NSArray *)coors color:(UIColor *)color
{
    self = [super init];
    if(self)
    {
        _routeId = routeId;
        _mapView = mapView;
        _color = color;
        _busStations = [[NSMutableDictionary alloc] init];
        _busPositions = [[NSMutableDictionary alloc] init];
        _AllbusStations = [[NSMutableDictionary alloc]init];
        
        //
        _AllbusStations1 = [[NSMutableDictionary alloc]init];
        
        
        self.path = [GMSMutablePath path];
        for(int i = 0; i < coors.count; i++){
            CMUTransitCoordinate *coor = [coors objectAtIndex:i];
            [self.path addCoordinate:coor.coordinate];
        }
        _busRoute2 = [GMSPolyline polylineWithPath:self.path];
        _busRoute = [GMSPolyline polylineWithPath:self.path];
        
        //
        _busRouteAll1 = [GMSPolyline polylineWithPath:self.path];
        
        if(routeId==7){
            _busRoute.strokeColor = [_color colorWithAlphaComponent:1.0];
            _busRouteAll1.strokeColor = [_color colorWithAlphaComponent:1.0];
        }
        else if(routeId==6){
            _busRoute.strokeColor = [_color colorWithAlphaComponent:1];
            _busRoute2.strokeColor = [_color colorWithAlphaComponent:1];
        }
        
        else{
            _busRoute.strokeColor = [_color colorWithAlphaComponent:0.5];
            _busRoute2.strokeColor = [_color colorWithAlphaComponent:1];
        }
        
        _busRoute2.strokeWidth = 10;
        _busRoute2.geodesic = YES;
        
        _busRoute.strokeWidth = 10;
        _busRoute.geodesic = YES;
        
        // _busRouteAll1.strokeWidth = 10;
        // _busRouteAll1.geodesic = YES;
        
        
    }
    return self;
}
-(id)GETRequest:(int)routid{
    
    NSString *url = [NSString stringWithFormat:@"http://www.cmutransit.com/cmtbus/routejson.php?route=%i",routid];
    //Set Access Token String
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] return :%@",json);
    return json;
    
}
- (void)loadBusStations:(CompleatedBlock) compleatedBlock
{
    
    [[CMUWebServiceManager sharedInstance] transitGetStation:self routeId:_routeId success:^(id result) {
        
        
        CMUTransitBusStationModel *model = (CMUTransitBusStationModel *)result;
        
        CMUTransitBusStationModel *modelBackground = [[CMUTransitBusStationModel alloc]initWithBackGroundArray:[[NSUserDefaults standardUserDefaults]valueForKey:@"arr"]];
        
        for(int i = 0; i < modelBackground.AllBusStations.count; i++){
            CMUTransitBusStation *station = [modelBackground.AllBusStations objectAtIndex:i];
            if([_AllbusStations objectForKey:station.no] == nil){
                GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(station.coordinate.latitude, station.coordinate.longitude)];
                CMUTransitStationView *stationView = [[CMUTransitStationView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
                [stationView setBusStation:station color:GROUP_ALL_COLOR];
                marker.icon = [CMUUIUtils imageFromView:stationView];
                marker.groundAnchor = CGPointMake(0.5, 0.5);
                marker.title = station.name;
                [_busStations setObject:marker forKey:station.name];
                
            }
        }
        for(int i = 0; i < model.busStations.count; i++){
            CMUTransitBusStation *station = [model.busStations objectAtIndex:i];
            if([_busStations objectForKey:station.no] == nil){
                GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(station.coordinate.latitude, station.coordinate.longitude)];
                CMUTransitStationView *stationView = [[CMUTransitStationView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
                [stationView setBusStation:station color:_color];
                marker.icon = [CMUUIUtils imageFromView:stationView];
                marker.groundAnchor = CGPointMake(0.5, 0.5);
                marker.title = station.name;
                [_busStations setObject:marker forKey:station.name];
                
            }
        }
        
        _mapView.delegate = self;
        
        
        if(compleatedBlock){
            compleatedBlock(nil);
        }
        
        
    } failure:^(NSError *error) {
        if(compleatedBlock){
            compleatedBlock(nil);
        }
    }];
}

-(BOOL) mapView:(GMSMapView *) mapView didTapMarker:(GMSMarker *)marker
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clearButtonState" object:self];
    [mapView setSelectedMarker:marker];
    NSString *markerTitle = marker.title;
    
    //1
    if([markerTitle isEqual:@"7-ELEVEN ไผ่ล้อม"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        
        
    }
    if([markerTitle isEqual:@"สถานีบริการด้านหน้ามหาวิทยาลัยเชียงใหม่"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"สำนักงานมหาวิทยาลัย"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
        
    }
    if([markerTitle isEqual:@"สำนักบริการเทคโนโลยีสารสนเทศ"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารสำนักหอสมุด"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        
    }
    if([markerTitle isEqual:@"หอพักนักศึกษา หญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
    }
    if([markerTitle isEqual:@"สนามวอลเล่ห์บอล หน้าหอพักนักศึกษาหญิง อาคาร 2"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        
    }
    if([markerTitle isEqual:@"หอพักนักศึกษาหญิง อาคาร 6 (สนามบาสเกตบอล)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        
    }
    if([markerTitle isEqual:@"อาคารอุตสาหกรรม คณะศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        
    }
    if([markerTitle isEqual:@"ลานหน้าอาคาร 1 คณะศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารประตูศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร โรงเรียนสาธิต"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
    }
    if([markerTitle isEqual:@"ระหว่างหอพักนักศึกษาชาย อาคาร 4,3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ลานจอดรถไฟฟ้าหน้าหอพักนักศึกษาหญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"สหกรณ์ร้านค้ามหาวิทยาลัยเชียงใหม่"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาฝั่งตรงข้ามสำนักบริการเทคโนโลยีสารสนเทศ"]){
        //  [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        //  [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
    }
    if([markerTitle isEqual:@"ทางขึ้นศาลาธรรม (ตรงข้ามไปรษณีย์)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"สนามรักบี้ด้านหน้ามหาวิทยาลัย"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
    }
    
    //2
    if([markerTitle isEqual:@"ตลาดร่มสัก (ตลาดฝายหิน)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
    }
    if([markerTitle isEqual:@"ลานจอดรถหอพักนักศึกษาชาย อาคาร 4 (ตรงข้ามคณะวิศวกรรมศาสตร์)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"สระว่ายน้ำรุจิรวงศ์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
    }
    if([markerTitle isEqual:@"วิทยาลัยนานาชาติ"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
    }
    if([markerTitle isEqual:@"หอพักนักศึกษา 40 ปี"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"หอพักนักศึกษาหญิง สีชมพู"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"7-ELEVEN ไผ่ล้อม"]){
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารหน้าหอพักนักศึกษาหญิง อาคาร 2"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ลานจอดรถไฟฟ้าหน้าหอพักนักศึกษาหญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารหน้าศูนย์การเรียนรู้ (ก่อนถึงสามแยกหอพักนักศึกษาชาย อาคาร 1,6)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    //3
    if([markerTitle isEqual:@"ลานจอดรถไฟฟ้าหน้าหอพักนักศึกษาหญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"สหกรณ์ร้านค้ามหาวิทยาลัยเชียงใหม่"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"คณะสังคมศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ตรงข้ามโรงอาหารคณะมนุษย์ศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"อ่างแก้ว"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
    }
    if([markerTitle isEqual:@"ตรงข้ามศูนย์บริการวิชาการมนุษย์ศาสตร์ (ตึก HB 6)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาฝั่งตรงข้ามสำนักบริการเทคโนโลยีสารสนเทศ"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร คณะรัฐศาสตร์และรัฐประศาสนศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารฝั่งตรงข้ามอาคารกิจกรรมนักศึกษา (อ.มช.)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    
    //4
    if([markerTitle isEqual:@"ลานจอดรถไฟฟ้าหน้าหอพักนักศึกษาหญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        
    }
    if([markerTitle isEqual:@"คณะเศรษฐศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"คณะการสื่อสารมวลชน"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"สหกรณ์ร้านค้ามหาวิทยาลัยเชียงใหม่"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"คณะสังคมศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ตรงข้ามโรงอาหารคณะมนุษย์ศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"คณะนิติศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารสำนักหอสมุด"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารหน้าศูนย์การเรียนรู้ (ก่อนถึงสามแยกหอพักนักศึกษาชาย อาคาร 1,6)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"คณะสถาปัตยกรรมศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    if([markerTitle isEqual:@"ลานจอดรถหอพักนักศึกษาชาย อาคาร 4 (ตรงข้ามคณะวิศวกรรมศาสตร์)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    
    if([markerTitle isEqual:@"ระหว่างหอพักนักศึกษาชาย อาคาร 4,3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
    }
    
    //5
    if([markerTitle isEqual:@"ลานจอดรถไฟฟ้าหน้าหอพักนักศึกษาหญิง อาคาร 3"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button4selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"สนามวอลเล่ห์บอล หน้าหอพักนักศึกษาหญิง อาคาร 2"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"หอพักนักศึกษาหญิง อาคาร 6 (สนามบาสเกตบอล)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"อาคารอุตสาหกรรม คณะศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ลานหน้าอาคาร 1 คณะศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"อาคาร 40 ปี คณะศึกษาศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"คณะบริหารธุรกิจ"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ร้านนมเกษตร"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร บัณฑิตวิทยาลัย"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"สนามกิฬากลาง"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"หอพักนักศึกษาหญิง สีชมพู"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"7-ELEVEN ไผ่ล้อม"]){
        //  [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        //   [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"อาคารปฏิบัติการกลาง คณะวิทยาศาสตร์ "]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารหน้าหอพักนักศึกษาหญิง อาคาร 2"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    //6
    if([markerTitle isEqual:@"สถานีบริการด้านหน้ามหาวิทยาลัยเชียงใหม่"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"สำนักงานมหาวิทยาลัย"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร คณะรัฐศาสตร์และรัฐประศาสนศาสตร์"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสารฝั่งตรงข้ามอาคารกิจกรรมนักศึกษา (อ.มช.)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"หอพักนักศึกษา 40 ปี "]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button2selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"7-ELEVEN ไผ่ล้อม"]){
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"button5selected" object:self];
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร อาคารกิจกรรมนักศึกษา (อ.มช.)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button3selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ศาลาที่พักผู้โดยสาร ภาควิชาสถิติ"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    if([markerTitle isEqual:@"ทางขึ้นศาลาธรรม (ตรงข้ามไปรษณีย์)"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button1selected" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"button6selected" object:self];
    }
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:self];
    
    return YES;
}
- (void)cameraToGroups:(NSArray *)groups
{
    if(groups.count > 0){
        CMUTransitGroupUI *groupa = [groups objectAtIndex:0];
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:groupa.path];
        for(CMUTransitGroupUI *group in groups)
        {
            bounds = [bounds includingPath:group.path];
        }
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f ]];
    }
}
- (void)resumeBusPositions
{
    [[CMUWebServiceManager sharedInstance] transitGetBusPosition:self routeId:_routeId success:^(id result) {
        
        CMUTransitBusPositionModel *model = (CMUTransitBusPositionModel *)result;
        NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
        for(int i = 0; i < model.busPositions.count; i++){
            CMUTransitBusPosition *bus = [model.busPositions objectAtIndex:i];
            GMSMarker *marker = [_busPositions objectForKey:bus.no];
            if(marker == nil){
                marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(bus.coordinate.latitude, bus.coordinate.longitude)];
                marker.groundAnchor = CGPointMake(0.5, 0.5);
                marker.title = bus.no;
            }
            CMUTransitBusView *busView = [[CMUTransitBusView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
            [busView setRouteId:_routeId transitBusPosition:bus];
            //            [CATransaction begin];
            //            [CATransaction setAnimationDuration:2.0];
            marker.icon = [CMUUIUtils imageFromView:busView];
            marker.position = CLLocationCoordinate2DMake(bus.coordinate.latitude, bus.coordinate.longitude);
            //            [CATransaction commit];
            [temp setObject:marker forKey:bus.no];
        }
        
        //filter out bus not contains in new data
        NSArray *newBusNoArray = [temp allKeys];
        NSArray *oldBusNoArray = [_busPositions allKeys];
        
        for(NSString *no in oldBusNoArray){
            if(![newBusNoArray containsObject:no]){
                GMSMarker *marker = [_busPositions objectForKey:no];
                marker.map = nil;
                NSLog(@"xxx: remove bus no:%@", no);
            }
        }
        
        
        _busPositions = temp;
        [self hideShowBus];
        
    } failure:^(NSError *error) {
        
    }];
    
    
    if(_timmer)
    {
        [_timmer invalidate];
        _timmer = nil;
    }
    
    if(_visible){
        _timmer = [NSTimer scheduledTimerWithTimeInterval: GET_BUS_INTERVAL
                                                   target: self
                                                 selector:@selector(resumeBusPositions)
                                                 userInfo: nil repeats: NO];
    }
    
    
    //    _timmer = [NSTimer timerWithTimeInterval:GET_BUS_INTERVAL target:self selector:@selector(resumeBusPositions:) userInfo:nil repeats:NO];
}

- (void)pauseBusPositions
{
    if(_timmer)
    {
        [_timmer invalidate];
        _timmer = nil;
    }
}


- (void)setVisible:(BOOL)visible
{
    if( _visible == visible){
        return;
    }
    _visible = visible;
    if(visible)
    {
        //check to load station
        if([_busStations allKeys].count == 0)
        {
            [self loadBusStations:^(id result) {
                [self hideShowStation];
            }];
        }
        else
        {
            [self hideShowStation];
        }
        
        //bus positions
        [self resumeBusPositions];
        [self hideShowRoute];
    }
    else
    {
        [self pauseBusPositions];
        [self hideShowAllView];
    }
}

- (void)hideShowAllView
{
    [self hideShowRoute];
    [self hideShowStation];
    [self hideShowBus];
}

- (void)hideShowRoute{
    _busRoute.map = _visible? self.mapView:nil;
}

- (void)hideShowStation{
    NSArray *keys = [_busStations allKeys];
    for(NSString *key in keys){
        GMSMarker *marker = [_busStations objectForKey:key];
        //NSLog(@"key :%@",key);
        //NSLog(@"busStation :%@",_busStations);
        marker.map = _visible? self.mapView:nil;
    }
    
}

- (void)hideShowBus{
    NSArray *keys = [_busPositions allKeys];
    for(NSString *key in keys){
        GMSMarker *marker = [_busPositions objectForKey:key];
        marker.map = _visible? self.mapView:nil;
    }
}

@end


@interface CMUTransitViewController ()//<GMSMapViewDelegate>
@property(nonatomic, strong)IBOutlet UIView *viewMenuBar;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup1;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup2;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup3;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup4;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup5;
@property(nonatomic, strong)IBOutlet UIButton *btnGroup6;
@property(nonatomic, strong)CMUTransitGroupUI *group1;
@property(nonatomic, strong)CMUTransitGroupUI *group2;
@property(nonatomic, strong)CMUTransitGroupUI *group3;
@property(nonatomic, strong)CMUTransitGroupUI *group4;
@property(nonatomic, strong)CMUTransitGroupUI *group5;
@property(nonatomic, strong)CMUTransitGroupUI *group6;

@property(nonatomic, strong)CMUTransitGroupUI *groupAll1;
@property(nonatomic, strong)CMUTransitGroupUI *groupAll2;
@property(nonatomic, strong)CMUTransitGroupUI *groupAll3;
@property(nonatomic, strong)CMUTransitGroupUI *groupAll4;
@property(nonatomic, strong)CMUTransitGroupUI *groupAll5;
@property(nonatomic, strong)CMUTransitGroupUI *groupAll6;

@property(nonatomic, strong)CMUTransitGroupUI *groupStart;
@property(nonatomic, unsafe_unretained) BOOL group1Visible;
@property(nonatomic, unsafe_unretained) BOOL group2Visible;
@property(nonatomic, unsafe_unretained) BOOL group3Visible;
@property(nonatomic, unsafe_unretained) BOOL group4Visible;
@property(nonatomic, unsafe_unretained) BOOL group5Visible;
@property(nonatomic, unsafe_unretained) BOOL group6Visible;
@property(nonatomic, unsafe_unretained) BOOL groupStartVisible;
@property NSMutableDictionary *stationDictionary;

@end

@implementation CMUTransitViewController{
    GMSMapView *mapView_;
    NSMutableDictionary *_AllbusStations ;
    CLLocationManager *locationManager;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"didtap"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callFunction) name:@"notificationName" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button1Selected) name:@"button1selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button2Selected) name:@"button2selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button3Selected) name:@"button3selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button4Selected) name:@"button4selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button5Selected) name:@"button5selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(button6Selected) name:@"button6selected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearButtonState) name:@"clearButtonState" object:nil];
    
    // mapView_.delegate = self;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:ZONE1_LAT
                                                            longitude:ZONE1_LNG
                                                                 zoom:DEFAULT_ZONE_ZOOM];
    mapView_ = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    mapView_.myLocationEnabled = YES;
    [self.view insertSubview:mapView_ belowSubview:self.viewMenuBar];
    
    [mapView_ setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSLog(@"userDefault.arr :%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"arr"]);
    NSLayoutConstraint *width =[NSLayoutConstraint
                                constraintWithItem:mapView_
                                attribute:NSLayoutAttributeWidth
                                relatedBy:0
                                toItem:self.view
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint
                                 constraintWithItem:mapView_
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:0
                                 toItem:self.view
                                 attribute:NSLayoutAttributeHeight
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:mapView_
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.view
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:mapView_
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    [self.view addConstraint:width];
    [self.view addConstraint:height];
    [self.view addConstraint:top];
    [self.view addConstraint:leading];
    
    self.viewMenuBar.layer.cornerRadius = 5;
    self.viewMenuBar.layer.masksToBounds = YES;
    
    self.group1 = [[CMUTransitGroupUI alloc] initWithRouteId:1 map:mapView_ routeCoordinate:[CMUTransit busGreenLine] color:GROUP1_COLOR];
    self.group2 = [[CMUTransitGroupUI alloc] initWithRouteId:2 map:mapView_ routeCoordinate:[CMUTransit busOrangeLine] color:GROUP2_COLOR];
    self.group3 = [[CMUTransitGroupUI alloc] initWithRouteId:3 map:mapView_ routeCoordinate:[CMUTransit busRedLine] color:GROUP3_COLOR];
    self.group4 = [[CMUTransitGroupUI alloc] initWithRouteId:4 map:mapView_ routeCoordinate:[CMUTransit busBlueLine] color:GROUP4_COLOR];
    self.group5 = [[CMUTransitGroupUI alloc] initWithRouteId:5 map:mapView_ routeCoordinate:[CMUTransit busVioletLine] color:GROUP5_COLOR];
    self.group6 = [[CMUTransitGroupUI alloc] initWithRouteId:6 map:mapView_ routeCoordinate:[CMUTransit busSixLine] color:GROUP6_COLOR];
    self.groupStart = [[CMUTransitGroupUI alloc]initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busAllLine] color:GROUP_ALL_COLOR];
    
    self.groupStart.visible =YES;
    self.groupAll1 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busGreenLine] color:GROUP_ALL_COLOR];
    self.groupAll2 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busOrangeLine] color:GROUP_ALL_COLOR];
    self.groupAll3 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busRedLine] color:GROUP_ALL_COLOR];
    self.groupAll4 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busBlueLine] color:GROUP_ALL_COLOR];
    self.groupAll5 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busVioletLine] color:GROUP_ALL_COLOR];
    self.groupAll6 = [[CMUTransitGroupUI alloc] initWithRouteId:7 map:mapView_ routeCoordinate:[CMUTransit busSixLine] color:GROUP_ALL_COLOR];
    
    self.groupStart.visible = NO;
    self.groupAll6.visible = YES;
    self.groupAll5.visible = YES;
    self.groupAll4.visible = YES;
    self.groupAll3.visible = YES;
    self.groupAll2.visible = YES;
    self.groupAll1.visible = YES;
    
    [self cameraToGroups:[NSArray arrayWithObject:self.groupStart]];
    [self updatePresentState];
    
    //update location
    [self updateLocation];
}
-(void)updateLocation{
    locationManager = [[CLLocationManager alloc] init]; // Ensure we keep a strong reference
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    //subview menu bar
    [super updatePresentState];
    [self.btnGroup1 setBackgroundColor:self.group1Visible? GROUP1_COLOR:BAR_COLOR];
    [self.btnGroup2 setBackgroundColor:self.group2Visible? GROUP2_COLOR:BAR_COLOR];
    [self.btnGroup3 setBackgroundColor:self.group3Visible? GROUP3_COLOR:BAR_COLOR];
    [self.btnGroup4 setBackgroundColor:self.group4Visible? GROUP4_COLOR:BAR_COLOR];
    [self.btnGroup5 setBackgroundColor:self.group5Visible? GROUP5_COLOR:BAR_COLOR];
    [self.btnGroup6 setBackgroundColor:self.group6Visible? GROUP6_COLOR:BAR_COLOR];
    
    //set visible
    
    self.group1.visible = self.group1Visible;
    self.group2.visible = self.group2Visible;
    self.group3.visible = self.group3Visible;
    self.group4.visible = self.group4Visible;
    self.group5.visible = self.group5Visible;
    self.group6.visible = self.group6Visible;
    
}
-(id)GETRequest:(int)routid{
    
    NSString *url = [NSString stringWithFormat:@"http://www.cmutransit.com/cmtbus/routejson.php?route=%i",routid];
    //Set Access Token String
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] return :%@",json);
    return json;
}
-(void)button1Selected{
    self.group1Visible = YES;
    self.btnGroup1.selected = YES;
}
-(void)button2Selected{
    self.group2Visible = YES;
    self.btnGroup2.selected = YES;
}
-(void)button3Selected{
    self.group3Visible =YES;
    self.btnGroup3.selected = YES;
}
-(void)button4Selected{
    self.group4Visible = YES;
    self.btnGroup4.selected = YES;
}
-(void)button5Selected{
    self.btnGroup5.selected = YES;
    self.group5Visible =YES;
}
-(void)button6Selected{
    self.group6Visible = YES;
    self.btnGroup6.selected = YES;
}
-(void)clearButtonState{
    self.group1.visible = NO;
    self.group2.visible = NO;
    self.group3.visible = NO;
    self.group4.visible = NO;
    self.group5.visible = NO;
    self.group6.visible = NO;
    self.btnGroup1.selected = NO;
    self.btnGroup2.selected = NO;
    self.btnGroup3.selected = NO;
    self.btnGroup4.selected = NO;
    self.btnGroup5.selected = NO;
    self.btnGroup6.selected = NO;
    self.group1Visible = NO;
    self.group2Visible = NO;
    self.group3Visible = NO;
    self.group4Visible = NO;
    self.group5Visible = NO;
    self.group6Visible = NO;
    
    [self updatePresentState];
}

-(void)callFunction
{
    //1
    if(self.btnGroup1.selected){
        self.group1Visible = self.group1Visible;
        if(self.group1Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group1]];
    }else{
        
        self.btnGroup1.selected = NO;
    }
    //2
    if(self.btnGroup2.selected){
        self.group2Visible = self.group2Visible;
        if(self.group2Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group2]];
    }else{
        self.btnGroup2.selected = NO;
    }
    //3
    if(self.btnGroup3.selected){
        self.group3Visible = self.group3Visible;
        if(self.group3Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group3]];
    }else{
        self.btnGroup3.selected = NO;
    }
    //4
    if(self.btnGroup4.selected){
        self.group4Visible = self.group4Visible;
        if(self.group4Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group4]];
    }else{
        self.btnGroup4.selected = NO;
        
    }
    //5
    if(self.btnGroup5.selected){
        self.group5Visible = self.group5Visible;
        if(self.group5Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group5]];
    }else{
        self.btnGroup5.selected = NO;
        
    }
    //6
    if(self.btnGroup6.selected){
        self.group6Visible = self.group6Visible;
        if(self.group6Visible)
            [self cameraToGroups:[NSArray arrayWithObject:self.group6]];
    }else{
        self.btnGroup6.selected = NO;
    }
    
    [self updatePresentState];
    
    
}
- (IBAction)btnGroupClicked:(id)sender
{
    if(sender == self.btnGroup1){
        self.group1Visible = !self.group1Visible;
        if(self.group1Visible){
            self.btnGroup1.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group1]];
        }
    }else if(sender == self.btnGroup2){
        self.group2Visible = !self.group2Visible;
        if(self.group2Visible){
            self.btnGroup2.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group2]];
        }
    }else if(sender == self.btnGroup3){
        self.group3Visible = !self.group3Visible;
        if(self.group3Visible){
            self.btnGroup3.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group3]];
        }
    }else if(sender == self.btnGroup4){
        self.group4Visible = !self.group4Visible;
        if(self.group4Visible){
            self.btnGroup4.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group4]];
        }
    }else if(sender == self.btnGroup5){
        self.group5Visible = !self.group5Visible;
        if(self.group5Visible){
            self.btnGroup5.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group5]];
        }
    }else if(sender == self.btnGroup6){
        self.group6Visible = !self.group6Visible;
        if(self.group6Visible){
            self.btnGroup6.selected = YES;
            [self cameraToGroups:[NSArray arrayWithObject:self.group6]];
        }
    }
    
    [self updatePresentState];
}

- (IBAction)currentLocationClicked:(id)sender
{
    [self moveMapToLat:mapView_.myLocation.coordinate.latitude long:mapView_.myLocation.coordinate.longitude];
}

- (void)moveMapToLat:(CGFloat) lat long:(CGFloat) lng
{
    [mapView_ animateToLocation:CLLocationCoordinate2DMake(lat, lng)];
}

- (void)cameraToGroups:(NSArray *)groups
{
    if(groups.count > 0){
        CMUTransitGroupUI *group1 = [groups objectAtIndex:0];
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:group1.path];
        for(CMUTransitGroupUI *group in groups)
        {
            bounds = [bounds includingPath:group.path];
        }
        [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f ]];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.group1 pauseBusPositions];
    [self.group2 pauseBusPositions];
    [self.group3 pauseBusPositions];
    [self.group4 pauseBusPositions];
    [self.group5 pauseBusPositions];
    [self.group6 pauseBusPositions];
}
- (IBAction)ComplainButtonAction:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://mobileapi.cmu.ac.th/Transit/index"];
    [[UIApplication sharedApplication]openURL:url];
}

@end

