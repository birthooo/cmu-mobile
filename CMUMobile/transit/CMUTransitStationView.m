//
//  CMUTransitStationView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/5/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import "CMUTransitStationView.h"
#import <QuartzCore/QuartzCore.h>
#import "CMUTransitViewController.h"
@implementation CMUTransitStationView


- (void)customInit
{
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUTransitStationView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:contentView];
    self.viewBackground.layer.cornerRadius = 4;
    self.viewBackground.layer.masksToBounds = YES;
    self.viewBackground.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f].CGColor;
    self.viewBackground.layer.borderWidth = 2;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setBusStation:(CMUTransitBusStation*) busStation color:(UIColor *)color
{
    self.lblTitle.text = busStation.no;
    self.viewBackground.backgroundColor = color;
    if([color isEqual:GROUP6_COLOR]){
        self.viewBackground.layer.backgroundColor = [color colorWithAlphaComponent:0.9].CGColor;
    }else{
        self.viewBackground.layer.backgroundColor = [color colorWithAlphaComponent:0.5].CGColor;
    }
}

@end
