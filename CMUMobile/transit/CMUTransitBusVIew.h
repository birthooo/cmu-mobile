//
//  CMUTransitBusVIew.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/5/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUTransit.h"

@interface CMUTransitBusView : UIView
- (void)setRouteId:(int)routeId transitBusPosition:(CMUTransitBusPosition*) transitBusPosition;
@end
