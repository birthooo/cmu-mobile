//
//  CMUNewsTypeTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsTypeTableViewCell.h"
#import "UIColor+CMU.h"
#import "global.h"

@interface CMUNewsTypeTableViewCell()
@property(nonatomic, weak) IBOutlet UILabel *lblNewsTypeTitle;;
@end

@implementation CMUNewsTypeTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
    if(IS_IPAD)
    {
        self.lblNewsTypeTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCategory:(CMUNewsCategory *)category
{
    _category =category;
    self.lblNewsTypeTitle.text = self.category.title;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblNewsTypeTitle.text = nil;
}


@end
