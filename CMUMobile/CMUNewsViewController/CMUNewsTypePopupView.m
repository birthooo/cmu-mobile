//
//  CMUNewsTypePopupView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsTypePopupView.h"
#import "CMUNewsCategory.h"
#import "CMUNewsTypeTableViewCell.h"

static NSString *cellIdentifier = @"CMUNewsTypeTableViewCell";

@interface CMUNewsTypePopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *newsTypeList;
@end


@implementation CMUNewsTypePopupView

- (void)customInit
{
    self.newsTypeList= [[NSMutableArray alloc] initWithArray:[CMUNewsCategory allCategories]];
    //[self.newsTypeList removeObjectAtIndex:0];//don't display category All in type selection menu items
    
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUNewsTypePopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUNewsTypeTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.newsTypeList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUNewsTypeTableViewCell *cell = (CMUNewsTypeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.category = [self.newsTypeList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(newsTypePopupView:didSelectedCategory:)])
    {
        CMUNewsCategory *selectedCategory = [self.newsTypeList objectAtIndex:indexPath.row];
        [_delegate newsTypePopupView:self didSelectedCategory:selectedCategory];
    }
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
