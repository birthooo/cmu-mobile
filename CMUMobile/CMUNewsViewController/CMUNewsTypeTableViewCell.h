//
//  CMUNewsTypeTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNewsCategory.h"

@interface CMUNewsTypeTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUNewsCategory* category;
@end
