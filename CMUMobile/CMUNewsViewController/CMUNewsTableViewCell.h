//
//  CMUNewsTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUModel.h"
@class CMUNewsTableViewCell;

@protocol CMUNewsTableViewCellDelegate <NSObject>
- (void)newsTableViewCellDidSelectedRead:(CMUNewsTableViewCell *) cell;
- (void)newsTableViewCellDidSelectedFacebook:(CMUNewsTableViewCell *) cell;
- (void)newsTableViewCellDidSelectedTwitter:(CMUNewsTableViewCell *) cell;
@end

@interface CMUNewsTableViewCell : UITableViewCell
@property(nonatomic, weak) id<CMUNewsTableViewCellDelegate> delegate;
@property(nonatomic, strong) CMUNewsFeed *newsFeed;
@property(nonatomic, weak)IBOutlet UIImageView* newsImageView;
@end
