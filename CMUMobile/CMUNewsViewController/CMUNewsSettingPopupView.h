//
//  NewsSettingView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUNewsSettingPopupView : UIView<UITableViewDataSource, UITableViewDelegate>

@end
