//
//  NewsSettingView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsSettingPopupView.h"
#import "CMUNewsCategory.h"
#import "CMUNewsSettingsTableViewCell.h"
#import "CMUNewsForPost.h"
#import "CMUSettings.h"

static NSString *cellIdentifier = @"CMUNewsSettingsTableViewCell";

@interface CMUNewsSettingPopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSArray *titleList;
@property(nonatomic, strong) NSArray *keyList;
@property(nonatomic, strong) CMUNewsForPost *newsSettings;
@end

@implementation CMUNewsSettingPopupView

- (void)customInit
{
    self.keyList= [NSArray arrayWithObjects:
                   @"hotNews",
                   @"jobNews",
                   @"orderNews",
                   @"MISNews",
                   @"CNOCNews",
                   @"regNews",
                   @"laguageNews",
                   @"libraryNews",
                   @"cooperativeNews",
                   //@"academicNews",
                   @"activityNews",
                   @"activityStudentNews",
                   @"directorNews",
                   @"collegeNews",
                   @"dentNews",
                   @"vithedNews",
                   nil];
    self.titleList = [NSArray arrayWithObjects:
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_HOT].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_JOB].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_ORDER].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_MIS].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_CNOC].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_REG].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_LANGUAGE].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_LIBRARY].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_COOPERATIVE].title,
                      //[CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_ACADEMIC].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_ACTIVITY].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_ACTIVITY_STUDENT].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_DIRECTOR].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_COLLEGE].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_DENT].title,
                      [CMUNewsCategory getByCategoryId:CMU_NEWS_CATEGORY_VITHED].title,
                      nil];
    
    self.newsSettings = [CMUSettings sharedInstance].newsSetting;
    
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUNewsSettingPopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUNewsSettingsTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.keyList.count;//not include type All
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUNewsSettingsTableViewCell *cell = (CMUNewsSettingsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell bindObject:self.newsSettings withKey:[self.keyList objectAtIndex:indexPath.row] displayTitle:[self.titleList objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
