//
//  CMUNewsTypePopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNewsCategory.h"

@class CMUNewsTypePopupView;

@protocol CMUNewsTypePopupViewDelegate<NSObject>
- (void)newsTypePopupView:(CMUNewsTypePopupView *) newsTypePopupView didSelectedCategory:(CMUNewsCategory *) category;
@end

@interface CMUNewsTypePopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUNewsTypePopupViewDelegate> delegate;
@end
