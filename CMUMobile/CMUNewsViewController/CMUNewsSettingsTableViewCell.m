//
//  CMUNewsSettingsTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsSettingsTableViewCell.h"
#import "global.h"

@interface CMUNewsSettingsTableViewCell()
@property(nonatomic, weak) IBOutlet UILabel *lblNewsTypeTitle;
@property(nonatomic, weak) IBOutlet UISwitch *swtNewsEnable;
@property(nonatomic, strong) NSObject *object;
@property(nonatomic, strong) NSString *key;
@property(nonatomic, strong) NSString *title;
@end


@implementation CMUNewsSettingsTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [self.swtNewsEnable addTarget:self action:@selector(swtNewsEnableChanged:) forControlEvents:UIControlEventValueChanged];
    if(IS_IPAD)
    {
        self.lblNewsTypeTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblNewsTypeTitle.text = nil;
    self.swtNewsEnable.on = NO;
}

- (void)bindObject:(NSObject *) object withKey:(NSString *) key displayTitle:(NSString *) title
{
    _object = object;
    _key = key;
    _title = title;
    [self updatePresentState];
}

- (void)updatePresentState
{
    self.lblNewsTypeTitle.text = self.title;
    BOOL on = [[self.object valueForKey:self.key] boolValue];
    self.swtNewsEnable.on = on;
}

- (void)swtNewsEnableChanged:(id)sender
{
    [self.object setValue:[NSNumber numberWithBool:self.swtNewsEnable.on] forKey:self.key];
}


@end
