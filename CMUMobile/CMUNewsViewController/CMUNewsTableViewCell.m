//
//  CMUNewsTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsTableViewCell.h"
#import "CMUUIUtils.h"
#import "UIImageView+WebCache.h"
#import "CMUStringUtils.h"
#import "CMUFormatUtils.h"
#import "UIColor+CMU.h"

@interface CMUNewsTableViewCell()
@property(nonatomic, weak)IBOutlet UIView* border;
@property(nonatomic, weak)IBOutlet UILabel* lblCategory;
@property(nonatomic, weak)IBOutlet UILabel* lblDateTime;
@property(nonatomic, weak)IBOutlet UILabel* lblDetail;

-(IBAction) btnReadClicked:(id) sender;
-(IBAction) btnFacebookClicked:(id) sender;
-(IBAction) btnTwitterClicked:(id) sender;
@end

@implementation CMUNewsTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    //self.backgroundColor  = [UIColor clearColor];
    //self.contentView.backgroundColor = [UIColor clearColor];
    self.border.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    self.lblDetail.textColor = [UIColor cmuBlackColor];
}

- (void)setNewsFeed:(CMUNewsFeed *)newsFeed
{
    _newsFeed = newsFeed;
    self.lblCategory.text = [CMUNewsCategory getByCategoryId:newsFeed.typeId].title;
    self.lblDateTime.text = [CMUFormatUtils formatDateMediumStyle:newsFeed.update locale:[NSLocale currentLocale]];
    if(newsFeed.detail) self.lblDetail.text = newsFeed.detail; else self.lblDetail.text = @"";
    //
//    CGSize size = [CMUUIUtils sizeThatFit:self.lblDetail.frame.size.width withText:newsFeed.topic withFont:self.lblDetail.font];
//    [self.textHeightConstraint setConstant:size.height];
//    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblCategory.text = nil;
    self.lblDateTime.text = nil;
    self.lblDetail.text = nil;
    [self.newsImageView cancelCurrentImageLoad];
    
    self.newsImageView.image = nil;
}

-(void) btnReadClicked:(id) sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(newsTableViewCellDidSelectedRead:)])
    {
        [self.delegate newsTableViewCellDidSelectedRead:self];
    }
}

-(void) btnFacebookClicked:(id) sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(newsTableViewCellDidSelectedFacebook:)])
    {
        [self.delegate newsTableViewCellDidSelectedFacebook:self];
    }
}

-(void) btnTwitterClicked:(id) sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(newsTableViewCellDidSelectedTwitter:)])
    {
        [self.delegate newsTableViewCellDidSelectedTwitter:self];
    }
}
@end
