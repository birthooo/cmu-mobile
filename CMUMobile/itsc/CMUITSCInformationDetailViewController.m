//
//  CMUITSCInformationDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUITSCInformationDetailViewController.h"

@interface CMUITSCInformationDetailViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, weak) IBOutlet UILabel *lblWebpageTitle;
@property(nonatomic, assign)BOOL firstLoad;

- (IBAction)back:(id)sender;
@end

@implementation CMUITSCInformationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _firstLoad = YES;
    self.lblWebpageTitle.text = self.webTitle;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:request];
    //self.webView.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(itscInfomationDetailViewControllerBackButtonClicked:)])
    {
        [_delegate itscInfomationDetailViewControllerBackButtonClicked:self];
    }
}

#pragma mark UIWebviewDelegate
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if(_firstLoad)
    {
        _firstLoad = NO;
        //like android
        //[self showHUDLoading];
    }
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end