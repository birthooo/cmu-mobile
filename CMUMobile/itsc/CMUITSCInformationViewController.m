//
//  CMUITSCInformationViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUITSCInformationViewController.h"

@interface CMUITSCInformationViewController ()
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionTitle;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionAccount;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionEmail;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionElerning;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionJumboPlus;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionLicense;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionVPN;
@property(nonatomic, weak) IBOutlet CMUITSCSectionInfoView *viewSectionMIS;
@end

@implementation CMUITSCInformationViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.viewSectionTitle.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"INFORMATION TECHNOLOGY SERVICE CENTER" imageName:@"info_itsc.png" showBar:YES htmlFile:nil];
    self.viewSectionAccount.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"CMU IT Account" imageName:@"info_itsc_account.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/account/Index"];
    self.viewSectionEmail.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"CMU E mail" imageName:@"info_itsc_email.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/Email/Index"];
    self.viewSectionElerning.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"e-learning CMU online" imageName:@"info_itsc_elerning.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/cmuonline/Index"];
    self.viewSectionJumboPlus.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"JumboPlus" imageName:@"info_itsc_jumboplus.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/JumboPlus/Index"];
    self.viewSectionLicense.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"Software license" imageName:@"info_itsc_license.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/license/Index"];
    self.viewSectionVPN.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"VPN" imageName:@"info_itsc_vpn.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/VPN/Index"];
    self.viewSectionMIS.sectionInfo = [[CMUITSCSectionInfo alloc] initWith:@"MIS" imageName:@"info_itsc_mis.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/MIS/Index"];
    
    
    self.viewSectionTitle.delegate = self;
    self.viewSectionAccount.delegate = self;
    self.viewSectionEmail.delegate = self;
    self.viewSectionElerning.delegate = self;
    self.viewSectionJumboPlus.delegate = self;
    self.viewSectionLicense.delegate = self;
    self.viewSectionVPN.delegate = self;
    self.viewSectionMIS.delegate = self;
    
    self.viewSectionTitle.alpha = 0.0;
    self.viewSectionAccount.alpha = 0.0;
    self.viewSectionEmail.alpha = 0.0;
    self.viewSectionElerning.alpha = 0.0;
    self.viewSectionJumboPlus.alpha = 0.0;
    self.viewSectionLicense.alpha = 0.0;
    self.viewSectionVPN.alpha = 0.0;
    self.viewSectionMIS.alpha = 0.0;
    
    [self showFlipAnimation];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showFlipAnimation
{
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionTitle afterDelay:0.1f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionAccount afterDelay:0.2f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionEmail afterDelay:0.3f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionElerning afterDelay:0.4f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionJumboPlus afterDelay:0.5f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionLicense afterDelay:0.6f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionVPN afterDelay:0.7f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionMIS afterDelay:0.8f];
}

-(void)flipAnimation:(UIView *) view{
    [UIView transitionWithView:view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft//|UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        view.alpha = 1.0;
                    } completion:^(BOOL finished) {
                        
                    }];
}



#pragma  mark -CMUSectionInfoView
- (void)sectionInfoViewDidClicked:(CMUITSCSectionInfoView* )sectionInfoView
{
    //DebugLog(@"%@", sectionInfoView);
    CMUITSCSectionInfo *sectionInfo = sectionInfoView.sectionInfo;
    if(sectionInfo.htmlFile == nil)
    {
        //top section don't have detail
        return;
    }
    
    CMUITSCInformationDetailViewController *detailViewController = [[CMUITSCInformationDetailViewController alloc] init];
    detailViewController.delegate = self;
    detailViewController.webTitle = sectionInfo.title;
    NSURL *url = [NSURL URLWithString: sectionInfo.htmlFile ];
    
    detailViewController.url = url;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - CMUInformationDetailViewControllerDelegate
- (void)itscInfomationDetailViewControllerBackButtonClicked:(CMUITSCInformationDetailViewController *) infomationDetailViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
