//
//  CMUITSCInformationDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@class CMUITSCInformationDetailViewController;
@protocol CMUITSCInformationDetailViewControllerDelegate <NSObject>
- (void)itscInfomationDetailViewControllerBackButtonClicked:(CMUITSCInformationDetailViewController *) infomationDetailViewController;
@end

@interface CMUITSCInformationDetailViewController : CMUNavBarViewController
@property(nonatomic, unsafe_unretained)id<CMUITSCInformationDetailViewControllerDelegate> delegate;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) NSString *webTitle;
@end
