//
//  CMUITSCSectionInfoView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CMUITSCSectionInfo : NSObject
- (id)initWith:(NSString *) title imageName:(NSString *)imageName showBar:(BOOL) isShowBar htmlFile:(NSString*) htmlFile;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *imageName;
@property(nonatomic, unsafe_unretained) BOOL isShowBar;
@property(nonatomic, strong) NSString *htmlFile;
@end

@class CMUITSCSectionInfoView;
@protocol CMUITSCSectionInfoViewDelegate<NSObject>
- (void)sectionInfoViewDidClicked:(CMUITSCSectionInfoView* )sectionInfoView;
@end

@interface CMUITSCSectionInfoView : UIView
@property(nonatomic, unsafe_unretained) id<CMUITSCSectionInfoViewDelegate> delegate;
@property(nonatomic, strong) CMUITSCSectionInfo* sectionInfo;
@end
