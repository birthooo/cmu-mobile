//
//  CMUITSCInformationViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUITSCSectionInfoView.h"
#import "CMUITSCInformationDetailViewController.h"

@interface CMUITSCInformationViewController : CMUNavBarViewController<CMUITSCSectionInfoViewDelegate, CMUITSCInformationDetailViewControllerDelegate>


@end
