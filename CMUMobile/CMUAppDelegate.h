//
//  AppDelegate.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "CMUNavigationController.h"
#import "CMULeftViewController.h"
#import "CMUHomeViewController.h"
#import "CMUNewsViewController.h"
#import "CMUPhoneViewController.h"
#import "CMUInformationViewController.h"
#import "CMUITSCInformationViewController.h"
#import "CMUCurriculumViewController.h"
#import "CMUChannelViewController.h"
#import "CMUEventViewController.h"
#import "CMUMapViewController.h"
#import "CMUNotificationViewController.h"
#import "CMUSISViewController.h"
#import "CMUSISPCheckViewController.h"
#import "FeedbackViewController.h"
#import "PhotoAndVideoViewController.h"

#import "CMUSISSummaryViewController.h"
#import "CMUOnlineMenuViewController.h"
#import "CMUMISEDocViewController.h"
#import "CMUContestViewController.h"
#import "CMUTransitViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CMUSISProfileViewController.h"
#import "CMUSISJobViewController.h"
#import "CMUSISScholarshipViewController.h"
#import "CMUSISAdviserViewController.h"
#import "CMUSettings.h"

//  Edited by Satianpong Yodnin on 9/9/2558 BE.
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "TransitWebViewViewController.h"
@interface CMUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CMUNavigationController *leftNavigationController;
@property (strong, nonatomic) CMUNavigationController *navigationController;
@property (strong, nonatomic) MMDrawerController *drawerController;
@property (strong, nonatomic) CMULeftViewController *leftViewController;
@property (strong, nonatomic) CMUHomeViewController *homeViewController;
@property (strong, nonatomic) UIAlertView *networkAlertView;
@property (strong, nonatomic) NSString *userName;
@property(nonatomic, unsafe_unretained) id<CMULoginViewControllerDelegate> delegate;
- (void)goHomeForCall;
- (void)goLeftView;
- (void)goHome:(BOOL) reloginPending;
- (void)goNews;
- (void)goPhone;
- (void)goInfo;
- (void)goCuriculum;
- (void)goPhotoAndVideo;
- (void)goChannel;
- (void)goEvent;
- (void)goMap;
- (void)goNotification:(NotiTab) initTab;
- (void)goREG;
- (void)goSIS;
- (void)goPCheck;
- (void)goELearning;
- (void)goMIS;
- (void)goCMUEtheses;
- (void)goLICMUKids;
- (void)goLICMUConversation;
- (void)goLICMUGrammar;
- (void)goLICMUDiploma;
- (void)goCMU5Dec;
- (void)goAbout;
- (void)goPhotoContest;
- (void)goITSC;
- (void)goConnectMyFlashCard;
- (void)goConnectMyEcho;
- (void)goCMUTransit;
- (void)goEResearch;
- (void)goFeedback;
-(void)goPhone2;
-(void)contactDetail;
-(void)goCMUEnglish;
-(void)goNewEdoc;

//SIS submenu
-(void)goRegStudent;
-(void)goJobManagement;
-(void)goScholarship;
-(void)goRequest;
-(void)goAdviser;
-(void)goActivity;
-(void)goNewsScholarship:(NSString*)number;
@end

