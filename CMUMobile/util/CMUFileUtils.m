//
//  CMUFileUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUFileUtils.h"

@implementation CMUFileUtils
+ (NSString *)pathForDocumentFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (NSString *)pathForTempFolder
{
    return NSTemporaryDirectory();
}

+ (BOOL)deleteFolder:(NSString *)absoluteFolderPath error:(NSError **) error
{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:absoluteFolderPath error:nil];
    for (NSString *path in directoryContents) {
        NSString *fullPath = [absoluteFolderPath stringByAppendingPathComponent:path];
        BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:error];
        if (!removeSuccess) {
            // Error handling.
        }
    }
    
    return [fileMgr removeItemAtPath:absoluteFolderPath error:error];
}

+ (BOOL)renameFolder:(NSString *)fromPath toPath:(NSString *)toPath error:(NSError **) error
{
    return [[NSFileManager defaultManager] moveItemAtPath:fromPath toPath:toPath error:error];
}

+ (NSString *)_sanitizeFileNameString:(NSString *)fileName {
    NSCharacterSet* illegalFileNameCharacters = [NSCharacterSet characterSetWithCharactersInString:@"/\\?%*|\"<>"];
    return [[fileName componentsSeparatedByCharactersInSet:illegalFileNameCharacters] componentsJoinedByString:@""];
}

+ (NSString *)safeUniqueFileNameWithPrefix:(NSString *) prefix andSuffix:(NSString *) suffix
{
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@.%@", prefix, guid, suffix];
    return [self _sanitizeFileNameString:uniqueFileName];
}

+ (uint64_t)freeDiskspace
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %d", [error domain], [error code]);
    }
    
    return totalFreeSpace;
}

@end
