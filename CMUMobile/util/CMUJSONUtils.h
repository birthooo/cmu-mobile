//
//  CMUJSONUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(CMUJSONUtils)
- (id)objectFromJSONString;
@end

@interface NSDictionary(CMUJSONUtils)
- (NSString *)JSONString;
@end

@interface NSData(CMUJSONUtils)
- (id)objectFromJSONData;
@end
