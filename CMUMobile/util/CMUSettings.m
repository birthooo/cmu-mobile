//
//  CMUSettings.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUSettings.h"
#import "CMUMenuItem.h"
#import "CMUJSONUtils.h"

#define KEY_CMU_MENU_ITEMS @"KEY_CMU_MENU_ITEMS"
#define KEY_CMU_NEWS_SETTINGS @"KEY_CMU_NEWS_SETTINGS"
#define KEY_CMU_EVENT_VIEW_MODE @"KEY_CMU_EVENT_VIEW_MODE"

@interface CMUSettings()
@property(strong, nonatomic)NSMutableArray *menuItems;//CMUMenuItem
@property(strong, nonatomic)CMUNewsForPost *newsSetting;
@end

@implementation CMUSettings
+ (CMUSettings *)sharedInstance
{
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMenuItems) name:@"allMenuItems" object:self];
    static CMUSettings *sharedInstance = nil;
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[CMUSettings alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init
{
   
    
    self = [super init];
    if(self)
    {
        [self loadMenuItems];
        [self loadNewsSettings];
        [self loadEventViewMode];
    }
    return self;
}

- (void)saveAll
{
    [self saveMenuItems];
    [self saveNewsSettings];
}

- (void)loadMenuItems
{
    //default menu items
    NSArray *defaultMenuItems = [CMUMenuItem allMenuItems];
    
    NSArray *savedMenuItemIds = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_CMU_MENU_ITEMS];
    self.menuItems = [[NSMutableArray alloc] init];
    for(int i = 0; i< savedMenuItemIds.count; i++)
    {
        CMUMenuItem *temp = [[CMUMenuItem alloc] initWithMenuId:[[savedMenuItemIds objectAtIndex:i] intValue] title:nil iconImage:nil];
        NSUInteger index = [defaultMenuItems indexOfObject:temp];
        if(index != NSNotFound)
        {
            [self.menuItems addObject:[defaultMenuItems objectAtIndex:index]];
        }
    }
    
    //check for consistency
    for(CMUMenuItem *menuItem in defaultMenuItems)
    {
        if([self.menuItems indexOfObject:menuItem] == NSNotFound)
        {
            [self.menuItems addObject:menuItem];
        }
    }
    NSLog(@"menuitems :%ld",_menuItems.count);
}

- (void)saveMenuItems
{
    NSMutableArray *menuItemIdsToSave = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.menuItems.count; i++)
    {
        CMUMenuItem *menuItem = [self.menuItems objectAtIndex:i];
        [menuItemIdsToSave addObject:[NSNumber numberWithInt:menuItem.menuItemId]];
    }
    [[NSUserDefaults standardUserDefaults] setObject:menuItemIdsToSave forKey:KEY_CMU_MENU_ITEMS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadNewsSettings
{
    NSString *savedNewsSettingsDict = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_CMU_NEWS_SETTINGS];
    if(savedNewsSettingsDict == nil)
    {
        self.newsSetting = [[CMUNewsForPost alloc] init];
        self.newsSetting.hotNews = YES;
        self.newsSetting.jobNews = YES;
        self.newsSetting.orderNews = YES;
        self.newsSetting.MISNews = YES;
        self.newsSetting.CNOCNews = YES;
        self.newsSetting.regNews = YES;
        self.newsSetting.laguageNews = YES;
        self.newsSetting.cooperativeNews = YES;
        self.newsSetting.libraryNews = YES;
        self.newsSetting.academicNews = YES;
        self.newsSetting.activityNews = YES;
        self.newsSetting.activityStudentNews = YES;
        self.newsSetting.directorNews = YES;
    }
    else
    {
        NSDictionary *dict = [savedNewsSettingsDict objectFromJSONString];
        self.newsSetting = [[CMUNewsForPost alloc] initFromDictionary:dict];
    }
}

- (void)saveNewsSettings
{
    [[NSUserDefaults standardUserDefaults] setObject:[[self.newsSetting toDictionary] JSONString] forKey:KEY_CMU_NEWS_SETTINGS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadEventViewMode
{
    self.eventViewMode = CMU_EVENT_VIEW_MODE_LIST;
    
    NSNumber *eventViewMode = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_CMU_EVENT_VIEW_MODE];
    if(eventViewMode)
    {
        self.eventViewMode = eventViewMode.intValue;
    }
    
}

- (void)saveEventViewMode
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:self.eventViewMode ] forKey:KEY_CMU_EVENT_VIEW_MODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
