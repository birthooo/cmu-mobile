//
//  CMUImageSizeCached.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/28/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CMUImageSizeCached : NSObject
+ (CMUImageSizeCached *)sharedInstance;
- (void)setImageSizeCached:(CGSize) size forURL:(NSString *) url;
- (NSValue *)getImageSizeCachedForURL:(NSString *) url;
@end
