//
//  NSString+URLEncoder.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "NSString+URLEncoder.h"

@implementation NSString(URL_ENCODE)
- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (NSString *)urlFixSpace
{
    return [self stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}
@end
