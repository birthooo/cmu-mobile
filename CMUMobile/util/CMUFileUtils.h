//
//  CMUFileUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUFileUtils : NSObject
+ (NSString *)pathForDocumentFolder;
+ (NSString *)pathForTempFolder;
+ (BOOL)deleteFolder:(NSString *)absoluteFolderPath error:(NSError **) error;
+ (BOOL)renameFolder:(NSString *)fromPath toPath:(NSString *)toPath error:(NSError **) error;
+ (NSString *)safeUniqueFileNameWithPrefix:(NSString *) prefix andSuffix:(NSString *) suffix;
+ (uint64_t)freeDiskspace;
@end
