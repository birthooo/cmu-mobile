//
//  CMUImageSizeCached.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/28/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUImageSizeCached.h"
#import "NSString+Hashes.h"

@interface CMUImageSizeCached()
@property(nonatomic, strong)NSMutableDictionary *imageSizeCached;
@end
@implementation CMUImageSizeCached
+ (CMUImageSizeCached *)sharedInstance
{
    static CMUImageSizeCached *sharedInstance = nil;
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[CMUImageSizeCached alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        self.imageSizeCached = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)setImageSizeCached:(CGSize) size forURL:(NSString *) url
{
    //NSLog(@"sha:%@",[url sha1]);
    [self.imageSizeCached setObject:[NSValue valueWithCGSize:size] forKey:[url sha1]];
}

- (NSValue *)getImageSizeCachedForURL:(NSString *) url
{
    return [self.imageSizeCached objectForKey:[url sha1]];
}

@end
