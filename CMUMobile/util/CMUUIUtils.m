//
//  CMUUIUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUUIUtils.h"
#import "UIColor+CMU.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define iOS7_0 @"7.0"

@implementation CMUUIUtils
+ (void)customizeNavigationBar:(UINavigationBar*) navBar
{
    
    [self _customizeNavigationBar:navBar color:[UIColor cmuNavigationBarColor]];
    
}

+ (void)customizeLeftNavigationBar:(UINavigationBar*) navBar
{
    
    [self _customizeNavigationBar:navBar color:[UIColor cmuLeftNavigationBarColor]];  
}

+ (void)_customizeNavigationBar:(UINavigationBar*) navBar color:(UIColor *) color
{
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        navBar.translucent = NO;
        navBar.tintColor = [UIColor whiteColor];
        [navBar setBarTintColor:color];
    }else {
        [navBar setBackgroundColor:color];
        [navBar setShadowImage:[[UIImage alloc] init]];
    }
    [navBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor,
                                               [UIColor blackColor], UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
}

+ (void)dropShadow:(UIView *)view offset:(CGFloat) offset
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(offset, offset);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
    
}
+ (UIViewController*)getViewController:(UIView *) view
{
    for (UIView* next = [view superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController*)nextResponder;
        }
    }
    
    return nil;
}

+ (CGSize) sizeThatFit:(CGFloat)width withText:(NSString *) text withFont:(UIFont *)font
{
    CGSize systemCalculateSize = CGSizeZero;
    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
        //version < 7.0
        
        systemCalculateSize = [text sizeWithFont:font
                            constrainedToSize:CGSizeMake(width, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else /*if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0))*/ {
        //version >= 7.0
        
        //Return the calculated size of the Label
        systemCalculateSize = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : font
                                                        }
                                              context:nil].size;
        
    }
    
    return CGSizeMake(systemCalculateSize.width, systemCalculateSize.height + 5 );//TODO now we add 5 to fix text cut!
}

+ (CGSize)sizeThatFit:(CGFloat)width withAttributedString:(NSAttributedString *) attrStr
{
    return [attrStr boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
}

+ (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
