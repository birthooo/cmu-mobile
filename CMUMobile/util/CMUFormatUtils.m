//
//  CMUFormatUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUFormatUtils.h"

#define DATE_FORMAT_WEB_SERVICE @"yyyy-MM-dd'T'HH:mm:ss"
#define DATE_FORMAT_IOS_8601 @"yyyy-MM-dd'T'HH:mm:ssZ"
#define DATE_FORMAT_NOTIFICATION_FEED @"EEE 'at' HH:mm"
#define DATE_FORMAT_CMU_VIDEO_FEED @"MMM dd yyyy HH:mm:ss"
#define DATE_FORMAT_CMU_MIS_EDOC_FEED @"dd/MM/yyyy"

@implementation CMUFormatUtils

//TODO fix this -> currently web service return +0000 timezone
+(NSDate *)parseWebServiceDate:(NSString *) strDate
{
    //fix millisecond "2014-08-29T14:35:58.647"
    NSRange range = [strDate rangeOfString:@"."];
    if(range.location != NSNotFound)
    {
        strDate = [strDate substringToIndex:range.location];
    }
    
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:DATE_FORMAT_WEB_SERVICE];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    }
    return [dateFormatter dateFromString:strDate];
}


+(NSDate *)parseISO8601Date:(NSString *) strDate
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:DATE_FORMAT_IOS_8601];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    }
    return [dateFormatter dateFromString:strDate];
}

+(NSString *)formatDateShortStyle:(NSDate *) date locale:(NSLocale *)locale
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:date];
}

+(NSString *)formatDateMediumStyle:(NSDate *) date locale:(NSLocale *)locale
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    }
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:date];
}

+(NSString *)formatDateNotificationFeedStyle:(NSDate *) date locale:(NSLocale *)locale
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:DATE_FORMAT_NOTIFICATION_FEED];
    }
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:date];
}

+(NSString *)formatDateVideoFeedStyle:(NSDate *) date locale:(NSLocale *)locale
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:DATE_FORMAT_CMU_VIDEO_FEED];
    }
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:date];
}

+(NSString *)formatDateMISEdocFeedStyle:(NSDate *) date locale:(NSLocale *)locale
{
    static NSDateFormatter *dateFormatter = nil;
    if(!dateFormatter)
    {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:DATE_FORMAT_CMU_MIS_EDOC_FEED];
    }
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:date];
}

+(NSString *)formatNumber:(int) number locale:(NSLocale *)locale
{
    static NSNumberFormatter *numberFormatter = nil;
    if(!numberFormatter)
    {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    }
    numberFormatter.locale = locale;
    return [numberFormatter stringFromNumber:[NSNumber numberWithInt:number]];
}

+(NSString *)formatBadge:(int) number
{
    if(number <= 99)
    {
        return [NSString stringWithFormat:@"%i", number];
    }
    return @"99+";
}
@end
