//
//  CMUFormatUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUFormatUtils : NSObject
+(NSDate *)parseWebServiceDate:(NSString *) strDate;
+(NSDate *)parseISO8601Date:(NSString *) strDate;
+(NSString *)formatDateShortStyle:(NSDate *) date locale:(NSLocale *)locale;
+(NSString *)formatDateMediumStyle:(NSDate *) date locale:(NSLocale *)locale;
+(NSString *)formatDateNotificationFeedStyle:(NSDate *) date locale:(NSLocale *)locale;
+(NSString *)formatDateVideoFeedStyle:(NSDate *) date locale:(NSLocale *)locale;
+(NSString *)formatDateMISEdocFeedStyle:(NSDate *) date locale:(NSLocale *)locale;

+(NSString *)formatNumber:(int) number locale:(NSLocale *)locale;
+(NSString *)formatBadge:(int) number;
@end
