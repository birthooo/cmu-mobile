//
//  CMUStringUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUStringUtils : NSObject
+ (BOOL)isEmpty:(NSString *)str;
+ (NSString *)trim:(NSString *)str;
+ (NSString *)createFacebookURLFromPostId:facebookPostId;
+ (BOOL)isDigit:(NSString *)str;
+ (NSString *)extractYoutubeID:(NSString *)youtubeURL;
+ (BOOL) containsString:(NSString*) string substring:(NSString*) substring;
@end
