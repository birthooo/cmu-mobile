//
//  CMUUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMUModel.h"
#import "CMUStringUtils.h"

@interface CMUUtils : NSObject
+ (NSString *) getVersionName;
+ (void)openNewsFeed:(CMUNewsFeed *) newsFeed;
+ (void)makePhoneCall:(NSString *)phoneNo;
+ (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2;
+ (void)PingMethod;
@end
