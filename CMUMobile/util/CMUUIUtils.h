//
//  CMUUIUtils.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CMUUIUtils : NSObject

+ (void)customizeNavigationBar:(UINavigationBar*) navBar;
+ (void)customizeLeftNavigationBar:(UINavigationBar*) navBar;
+ (void)dropShadow:(UIView *)view offset:(CGFloat) offset;
+ (UIViewController*)getViewController:(UIView *) view;
+ (CGSize)sizeThatFit:(CGFloat)width withText:(NSString *) text withFont:(UIFont *)font;
+ (CGSize)sizeThatFit:(CGFloat)width withAttributedString:(NSAttributedString *) text;
+ (UIImage *)imageFromView:(UIView *) view;
@end
