//
//  CMUUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUUtils.h"
#import "CMUStringUtils.h"
#import <UIKit/UIKit.h>
#import "global.h"
@implementation CMUUtils
+ (NSString *) getVersionName
{
    NSNumber *svnVersion = [[ NSBundle mainBundle ] objectForInfoDictionaryKey: @"BuildNumber" ];
    return [NSString stringWithFormat:@"%@", svnVersion];
}

+ (void)openNewsFeed:(CMUNewsFeed *) newsFeed;
{
    if(newsFeed.isFacebook)
    {
        
//http://redmine.ibssthailand.com/issues/96
        
//        NSString *facebookURLString = [CMUStringUtils createFacebookURLFromPostId:newsFeed.facebookPostId];
//        NSURL *facebookURL = [NSURL URLWithString:facebookURLString];
//        if ([[UIApplication sharedApplication] canOpenURL:facebookURL]){
//            [[UIApplication sharedApplication] openURL:facebookURL];
//        }
//        else {
            if(![CMUStringUtils isEmpty:newsFeed.link])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newsFeed.link]];
            }
//        }
    }
    else
    {
        if(![CMUStringUtils isEmpty:newsFeed.link])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newsFeed.link]];
        }
    }
}

+ (void)makePhoneCall:(NSString *)phoneNo
{
    if([CMUStringUtils isDigit:phoneNo])
    {
        NSString *phoneNumber = [@"tel://" stringByAppendingString:phoneNo];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else
    {
        DebugLog(@"Not digit:%@", phoneNo);
    }
    
}

+ (BOOL)isSameDayWithDate1:(NSDate*)date1 date2:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}
@end
