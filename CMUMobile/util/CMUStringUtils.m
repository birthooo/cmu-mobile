//
//  CMUStringUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUStringUtils.h"

@implementation CMUStringUtils
+ (BOOL)isEmpty:(NSString *)str
{
    return str == nil || [self trim:str].length == 0;
}

+ (NSString *)trim:(NSString *)str;
{
    return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)createFacebookURLFromPostId:facebookPostId;
{
    return [NSString stringWithFormat:@"fb://post/%@", facebookPostId];
}

+ (BOOL)isDigit:(NSString *)str
{
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:str];
    return [alphaNums isSupersetOfSet:inStringSet];
}

+ (NSString *)extractYoutubeID:(NSString *)youtubeURL
{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)" options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:youtubeURL options:0 range:NSMakeRange(0, [youtubeURL length])];
    if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:rangeOfFirstMatch];
        
        return substringForFirstMatch;
    }
    return nil;
}

+ (BOOL) containsString:(NSString*) string substring:(NSString*) substring
{
    NSRange range = [string rangeOfString : substring];
    BOOL found = ( range.location != NSNotFound );
    return found;
}
@end
