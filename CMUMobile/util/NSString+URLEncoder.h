//
//  NSString+URLEncoder.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(URL_ENCODE)
- (NSString *)urlencode;
- (NSString *)urlFixSpace;
@end
