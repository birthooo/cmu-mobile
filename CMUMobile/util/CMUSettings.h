//
//  CMUSettings.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMUNewsForPost.h"

typedef enum
{
    CMU_EVENT_VIEW_MODE_LIST = 0,
    CMU_EVENT_VIEW_MODE_CALENDAR_LIST = 1
} CMU_EVENT_VIEW_MODE;


@interface CMUSettings : NSObject
+ (CMUSettings *)sharedInstance;
@property(nonatomic, readonly)NSMutableArray *menuItems;//CMUMenuItem
@property(nonatomic, readonly)CMUNewsForPost *newsSetting;
@property(nonatomic, unsafe_unretained)CMU_EVENT_VIEW_MODE eventViewMode;
- (void)saveAll;
- (void)saveMenuItems;
- (void)saveNewsSettings;
- (void)saveEventViewMode;
- (void)loadMenuItems;
@end
