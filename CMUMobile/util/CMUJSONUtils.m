//
//  CMUJSONUtils.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUJSONUtils.h"


@implementation NSString(CMUJSONUtils)
- (id)objectFromJSONString
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}
@end

@implementation NSDictionary(CMUJSONUtils)
- (NSString *)JSONString
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return nil;
    } else {
        return  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end

@implementation NSData(CMUJSONUtils)
- (id)objectFromJSONData
{
    NSError *jsonError = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:self options:kNilOptions error:&jsonError];
    return jsonObject;
    
//    if ([jsonObject isKindOfClass:[NSArray class]]) {
//        NSLog(@"its an array!");
//        NSArray *jsonArray = (NSArray *)jsonObject;
//        NSLog(@"jsonArray - %@",jsonArray);
//    }
//    else {
//        NSLog(@"its probably a dictionary");
//        NSDictionary *jsonDictionary = (NSDictionary *)jsonObject;
//        NSLog(@"jsonDictionary - %@",jsonDictionary);
//    }
}
@end
