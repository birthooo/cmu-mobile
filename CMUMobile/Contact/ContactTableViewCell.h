//
//  ContactTableViewCell.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 1/13/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *num;

@end
