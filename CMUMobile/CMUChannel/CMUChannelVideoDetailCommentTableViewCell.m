//
//  CMUChannelVideoDetailCommentTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/20/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUChannelVideoDetailCommentTableViewCell.h"

@interface CMUChannelVideoDetailCommentTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel *lblName;
@property(nonatomic, weak)IBOutlet UILabel *lblComment;
@end

@implementation CMUChannelVideoDetailCommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setChannelCommentFeed:(CMUChannelCommentFeed *)channelCommentFeed
{
    _channelCommentFeed = channelCommentFeed;
    self.lblName.text = _channelCommentFeed.accId;
    self.lblComment.text = _channelCommentFeed.commentText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblName.text = nil;
    self.lblComment.text = nil;
}

@end
