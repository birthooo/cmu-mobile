//
//  CMUChannelTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUChannelTableViewCell.h"

@interface CMUChannelTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel *lblView;
@property(nonatomic, weak)IBOutlet UILabel *lblDuration;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoLike;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoName;

@end

@implementation CMUChannelTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setChannelFeed:(CMUChanelFeed *)channelFeed
{
    _channelFeed = channelFeed;
    self.lblVdoName.text = self.channelFeed.topic;
    self.lblView.text = [NSString stringWithFormat:@"%i",self.channelFeed.numberView];
    self.lblVdoLike.text = [NSString stringWithFormat:@"%i",self.channelFeed.numberLike];
    self.lblDuration.text = self.channelFeed.time;
}

@end
