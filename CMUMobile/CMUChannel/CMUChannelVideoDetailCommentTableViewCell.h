//
//  CMUChannelVideoDetailCommentTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/20/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUChanelFeedModel.h"

@interface CMUChannelVideoDetailCommentTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUChannelCommentFeed *channelCommentFeed;
@end
