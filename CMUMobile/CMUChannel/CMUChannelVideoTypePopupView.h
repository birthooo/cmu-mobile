//
//  CMUOnlineVideoTypePopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUChanelFeedModel.h"

@class CMUChannelVideoTypePopupView;

@protocol CMUChannelVideoTypePopupViewDelegate<NSObject>
- (void)cmuChannelVideoTypePopupView:(CMUChannelVideoTypePopupView *) cmuOnlineVideoTypePopupView didSelectedVideoType:(CMUChannelType *) videoType;
@end

@interface CMUChannelVideoTypePopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUChannelVideoTypePopupViewDelegate> delegate;
@end
