//
//  CMUChannelVideoTypeTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUOnlineModel.h"

@interface CMUChannelVideoTypeTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUOnlineVideoType* videoType;
@end
