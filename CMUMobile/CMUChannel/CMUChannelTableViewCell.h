//
//  CMUChannelTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUChanelFeedModel.h"

@interface CMUChannelTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUChanelFeed *channelFeed;
@property(nonatomic, weak)IBOutlet UIImageView* thumbnailImageView;
@end
