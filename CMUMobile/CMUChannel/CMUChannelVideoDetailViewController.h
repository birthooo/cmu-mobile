//
//  CMUChannelVideoDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/3/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUChanelFeedModel.h"

@class CMUChannelVideoDetailViewController;

@protocol CMUChannelVideoDetailViewControllerDelegate <NSObject>
- (void)cmuChannelVideoDetailViewControllerDidBack:(CMUChannelVideoDetailViewController *) vc;
@end

@interface CMUChannelVideoDetailViewController : CMUNavBarViewController<UITextFieldDelegate>
@property(nonatomic, weak)id<CMUChannelVideoDetailViewControllerDelegate> delegate;
@property(nonatomic, strong)CMUChanelFeed *videoFeed;
@end
