//
//  CMUChannelViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/27/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUChannelVideoTypePopupView.h"
#import "CMUChannelTableViewCell.h"
#import "PopoverView.h"
#import "CMUChanelFeedModel.h"
#import "CMUChannelVideoDetailViewController.h"

@class CMUChannelViewController;

@interface CMUChannelViewController : CMUNavBarViewController<PopoverViewDelegate, CMUChannelVideoTypePopupViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CMUChannelVideoDetailViewControllerDelegate>

@end
