//
//  CMUNotificationFeedTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNotificationFeedTableViewCell.h"
#import "CMUFormatUtils.h"

@interface CMUNotificationFeedTableViewCell()
@property(nonatomic, weak) IBOutlet UIImageView *imvIcon;
@property(nonatomic, weak) IBOutlet UILabel *lblTypeName;
@property(nonatomic, weak) IBOutlet UILabel *lblMessage;
@property(nonatomic, weak) IBOutlet UILabel *lblDate;
@end

@implementation CMUNotificationFeedTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFeedModel:(CMUNotificationFeed *)feedModel
{
    _feedModel = feedModel;
    [self updatePresentState];
}

- (void)updatePresentState
{
    UIImage *icon = nil;
    //NSString *typeName = nil;
    switch (_feedModel.type) {
        case 1:
            icon = [UIImage imageNamed:@"noti_news.png"];
            //typeName = @"News";
            break;
        case 2:
            icon = [UIImage imageNamed:@"noti_activity.png"];
            //typeName = @"Activity";
            break;
        case 3:
            icon = [UIImage imageNamed:@"noti_broadcast.png"];
            //typeName = @"Channel";
            break;
        case 4:
            icon = [UIImage imageNamed:@"noti_edoc.png"];
            //typeName = @"Channel";
            break;
        case 5:
            icon = [UIImage imageNamed:@"noti_channel.png"];
            //typeName = @"Channel";
            break;
            
        default:
            break;
    }
    self.imvIcon.image = icon;
    self.lblTypeName.text = _feedModel.typeName;
    self.lblMessage.text = _feedModel.message;
    self.lblDate.text = [CMUFormatUtils formatDateNotificationFeedStyle:_feedModel.update locale:[NSLocale currentLocale]];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imvIcon.image = nil;
    self.lblTypeName.text = nil;
    self.lblMessage.text = nil;
    self.lblDate.text = nil;
}

@end
