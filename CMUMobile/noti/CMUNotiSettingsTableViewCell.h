//
//  CMUNotiSettingsTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUNotiSettingsTableViewCellModel : NSObject
@property(nonatomic, unsafe_unretained) int settingId;
@property(nonatomic, strong) NSString *settingName;
@property(nonatomic, unsafe_unretained) BOOL settingSelected;
@end

@interface CMUNotiSettingsTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUNotiSettingsTableViewCellModel *model;
@end
