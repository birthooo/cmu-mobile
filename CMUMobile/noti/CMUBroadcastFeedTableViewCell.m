//
//  CMUBroadcastFeedTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 12/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUBroadcastFeedTableViewCell.h"
#import "CMUFormatUtils.h"
#import "UIImageView+WebCache.h"
#import "CMUStringUtils.h"

@interface CMUBroadcastFeedTableViewCell()
@property(nonatomic, weak) IBOutlet UIImageView *imvIcon;
@property(nonatomic, weak) IBOutlet UILabel *lblTypeName;
@property(nonatomic, weak) IBOutlet UILabel *lblMessage;
@property(nonatomic, weak) IBOutlet UILabel *lblDate;
@property(nonatomic, weak) IBOutlet UILabel *lblNotificationNumber;
@end

@implementation CMUBroadcastFeedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.lblNotificationNumber.layer.cornerRadius = 5;
    self.lblNotificationNumber.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFeedModel:(CMUBroadcastFeed *)feedModel
{
    _feedModel = feedModel;
    [self updatePresentState];
}

- (void)updatePresentState
{
    if([CMUStringUtils isEmpty:_feedModel.image])
    {
        self.imvIcon.image = [UIImage imageNamed:@"noti_cmu_logo.png"];
    }
    else
    {
        [self.imvIcon setImageWithURL: [NSURL URLWithString: _feedModel.image]];
    }
    self.lblTypeName.text = _feedModel.name;
    self.lblMessage.text = _feedModel.detail;
    self.lblDate.text = [CMUFormatUtils formatDateNotificationFeedStyle:_feedModel.update locale:[NSLocale currentLocale]];
    
    //badge
    self.lblNotificationNumber.text = [CMUFormatUtils formatBadge:_feedModel.unread];
    self.lblNotificationNumber.hidden = _feedModel.unread <= 0;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.imvIcon sd_cancelCurrentImageLoad];
    self.imvIcon.image = nil;
    self.lblTypeName.text = nil;
    self.lblMessage.text = nil;
    self.lblDate.text = nil;
    self.lblNotificationNumber.text = nil;
}


@end
