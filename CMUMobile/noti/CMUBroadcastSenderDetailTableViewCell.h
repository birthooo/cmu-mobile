//
//  CMUBroadcastSenderDetailTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 12/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNotificationModel.h"

@interface CMUBroadcastSenderDetailTableViewCell : UITableViewCell
@property(nonatomic, strong)NSString *iconURL;
@property(nonatomic, strong)CMUBroadcastSenderFeed *feedModel;
@end
