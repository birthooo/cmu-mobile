//
//  CMUBroadcastSenderDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 12/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"

@class CMUBroadcastSenderDetailViewController;
@protocol CMUBroadcastSenderDetailViewControllerDelegate <NSObject>
- (void)broadcastSenderDetailViewControllerBackButtonClicked:(CMUBroadcastSenderDetailViewController *) broadcastSenderDetailViewController;
@end


@interface CMUBroadcastSenderDetailViewController : CMUNavBarViewController
@property(nonatomic, unsafe_unretained)id<CMUBroadcastSenderDetailViewControllerDelegate> delegate;
@property(nonatomic, unsafe_unretained) int senderId;
@property(nonatomic, strong) NSString *senderName;
@property(nonatomic, strong) NSString *senderImageURL;
@end
