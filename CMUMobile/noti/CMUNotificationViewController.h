//
//  CMUNotificationViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUBroadcastSenderDetailViewController.h"
#import "CMUMISEdocDetailViewController.h"
typedef enum {
    NotiTabNotification,
    NotiTabBroadcast,
    NotiTabSettings,
}NotiTab;
@interface CMUNotificationViewController : CMUNavBarViewController<CMUBroadcastSenderDetailViewControllerDelegate, CMUMISEdocDetailViewControllerDelegate>
@property(nonatomic, assign)NotiTab selectedTab;
@end
