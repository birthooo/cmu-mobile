//
//  CMUNotificationFeedTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNotificationModel.h"
@interface CMUNotificationFeedTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUNotificationFeed *feedModel;
@end
