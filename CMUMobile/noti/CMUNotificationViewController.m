//
//  CMUNotificationViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNotificationViewController.h"
#import "CMUModel.h"
#import "CMUNotificationFeedTableViewCell.h"
#import "CMUBroadcastFeedTableViewCell.h"
#import "CMUNotiSettingsTableViewCell.h"
#import "CMUWebServiceManager.h"
#import "CMUAppDelegate.h"
#import "CMUNewsViewController.h"

static NSString *notiFeedTableViewCell = @"CMUNotificationFeedTableViewCell";
static NSString *broadcastFeedTableViewCell = @"CMUBroadcastFeedTableViewCell";
static NSString *notiSettingsTableViewCell = @"CMUNotiSettingsTableViewCell";

@interface CMUNotificationViewController ()

@property(nonatomic, strong)UITapGestureRecognizer *tapGesture;
//Tab
@property(nonatomic, weak)IBOutlet UISegmentedControl *segmentController;
@property(nonatomic, weak)IBOutlet UIButton *btnSettings;

//View Notification
@property(nonatomic, strong)IBOutlet UIView *clearAllNotificationViewHeader;
@property(nonatomic, weak)IBOutlet UIView *clearAllNotificationView;
@property(nonatomic, weak)IBOutlet UIView *notificationView;
@property(nonatomic, weak)IBOutlet UITableView *notificationTableView;

//View Broadcast
@property(nonatomic, weak)IBOutlet UIView *broadcastView;
@property(nonatomic, weak)IBOutlet UITableView *broadcastTableView;

//View Settings
@property(nonatomic, weak)IBOutlet UIView *settingsView;
@property(nonatomic, weak)IBOutlet UITableView *settingsTableView;
@property(nonatomic, strong)IBOutlet UIView *settingsNewsHeader;
@property(nonatomic, strong)IBOutlet UIView *settingsActivityHeader;
@property(nonatomic, strong)IBOutlet UIView *settingsChannelHeader;
@property(nonatomic, strong)IBOutlet UIImageView *settingsNewsHeaderAccessory;
@property(nonatomic, strong)IBOutlet UIImageView *settingsActivityHeaderAccessory;
@property(nonatomic, strong)IBOutlet UIImageView *settingsChannelHeaderAccessory;
@property(nonatomic, strong)UIImageView *bubbleImageView;

//Model notification
@property(nonatomic, strong)CMUNotificationFeedModel *notificationFeedModel;

//Model broadcast
@property(nonatomic, strong)CMUBroadcastFeedModel *broadcastFeedModel;

//Model settings
@property(nonatomic, strong)CMUNotificationSettingsModel *notificationSettingsModel;
@property(nonatomic, unsafe_unretained)BOOL settingsNewsExpand;
@property(nonatomic, unsafe_unretained)BOOL settingsEventExpand;
@property(nonatomic, unsafe_unretained)BOOL settingsChannelExpand;
@property(nonatomic, strong)NSMutableArray *settingsNewsList;
@property(nonatomic, strong)NSMutableArray *settingsEventList;
@property(nonatomic, strong)NSMutableArray *settingsChannelList;
@property(nonatomic, unsafe_unretained)BOOL settingsNewsChange;
@property(nonatomic, unsafe_unretained)BOOL settingsEventChange;
@property(nonatomic, unsafe_unretained)BOOL settingsChannelChange;
@end


@implementation CMUNotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        _selectedTab = NotiTabNotification;
        
        NSMutableArray *allNewsType = [CMUNewsCategory allCategories];
        NSMutableArray *allEventType = [CMUEventType allEventType];
        NSMutableArray *allChannelType = [CMUChannelType allChannelType];
        
        //remove type all
        [allNewsType removeObjectAtIndex:0];
        [allChannelType removeObjectAtIndex:0];
        
        //create selection model
        self.settingsNewsList = [[NSMutableArray alloc] init];
        self.settingsEventList = [[NSMutableArray alloc] init];
        self.settingsChannelList = [[NSMutableArray alloc] init];
        
        for(CMUNewsCategory *newType in allNewsType)
        {
            CMUNotiSettingsTableViewCellModel *model = [[CMUNotiSettingsTableViewCellModel alloc] init];
            model.settingId = newType.categoryId;
            model.settingName = newType.title;
            model.settingSelected = NO;
            [self.settingsNewsList addObject:model];
        }
        
        for(CMUEventType *eventType in allEventType)
        {
            CMUNotiSettingsTableViewCellModel *model = [[CMUNotiSettingsTableViewCellModel alloc] init];
            model.settingId = eventType.eventTypeId;
            model.settingName = eventType.title;
            model.settingSelected = NO;
            [self.settingsEventList addObject:model];
        }
        
        for(CMUChannelType *channelType in allChannelType)
        {
            CMUNotiSettingsTableViewCellModel *model = [[CMUNotiSettingsTableViewCellModel alloc] init];
            model.settingId = channelType.channelTypeId;
            model.settingName = channelType.title;
            model.settingSelected = NO;
            [self.settingsChannelList addObject:model];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidRecieveRemoteNotificationInForeground:) name:CMU_APPLICATION_DID_RECEIVE_REMOTE_NOTIFICATION_IN_FOREGROUND object:nil];
        
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(IS_IPAD)
    {
        UIFont *font = [UIFont systemFontOfSize:24.0f];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                               forKey:NSFontAttributeName];
        [self.segmentController setTitleTextAttributes:attributes
                                              forState:UIControlStateNormal];
    }
    
    [self.notificationTableView registerNib:[UINib nibWithNibName:@"CMUNotificationFeedTableViewCell" bundle:nil] forCellReuseIdentifier:notiFeedTableViewCell];
    [self.broadcastTableView registerNib:[UINib nibWithNibName:@"CMUBroadcastFeedTableViewCell" bundle:nil] forCellReuseIdentifier:broadcastFeedTableViewCell];
    [self.settingsTableView registerNib:[UINib nibWithNibName:@"CMUNotiSettingsTableViewCell" bundle:nil] forCellReuseIdentifier:notiSettingsTableViewCell];
    
    self.clearAllNotificationView.layer.cornerRadius = 5.0f;
    self.clearAllNotificationView.layer.masksToBounds = YES;

    [self updatePresentState];
    if(_selectedTab == NotiTabNotification || _selectedTab == NotiTabBroadcast)
    {
        [self onSegmentSelect:self.segmentController];
    }
    else if(_selectedTab == NotiTabSettings)
    {
        [self btnSettingsClicked:self.btnSettings];
    }
    
    UIImage *bubble = [UIImage imageNamed:@"noti_bubble.png"];
    self.bubbleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 16, 8)];
    self.bubbleImageView.image = bubble;
    [self.navigationController.navigationBar addSubview:self.bubbleImageView];
    self.bubbleImageView.hidden = YES;
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnNotiClicked:)];
    self.tapGesture.cancelsTouchesInView = NO;
    

}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [self updatePresentState];
//}

- (void)viewDidAppear:(BOOL)animated
{
    //need to layout bubble
    [super viewDidAppear:animated];
    self.bubbleImageView.hidden = NO;
    [ self.navigationController.navigationBar addGestureRecognizer:self.tapGesture];
    [self updatePresentState];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)appWillResignActive:(NSNotification*)note
{
    if(_selectedTab == NotiTabSettings)
    {
        [self checkAndSave];
    }
}

-(void)appDidBecomeActive:(NSNotification*)note
{
    if(_selectedTab == NotiTabNotification)
    {
        [self onSegmentSelect:self.segmentController];
    }
    else if(_selectedTab == NotiTabBroadcast)
    {
        [self onSegmentSelect:self.segmentController];
    }
}

-(void)appDidRecieveRemoteNotificationInForeground:(NSNotification*)note
{
    if(_selectedTab == NotiTabNotification)
    {
        [self onSegmentSelect:self.segmentController];
    }
    else if(_selectedTab == NotiTabBroadcast)
    {
        [self onSegmentSelect:self.segmentController];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updatePresentState
{
    [super updatePresentState];
    
    //tab selection
    self.segmentController.selectedSegmentIndex = -1;
    if(_selectedTab == NotiTabNotification)
        self.segmentController.selectedSegmentIndex = 0;
    if(_selectedTab == NotiTabBroadcast)
        self.segmentController.selectedSegmentIndex = 1;
    
    self.btnSettings.selected = _selectedTab == NotiTabSettings;
    [self.btnSettings setImage:self.btnSettings.selected?[UIImage imageNamed:@"noti_settings_selected.png"]:[UIImage imageNamed:@"noti_settings.png"] forState:UIControlStateNormal];
    
    //view display
    self.notificationView.hidden = _selectedTab != NotiTabNotification;
    self.broadcastView.hidden = _selectedTab != NotiTabBroadcast;
    self.settingsView.hidden = _selectedTab != NotiTabSettings;
    
    //bubble position
    if (self.navigationController.topViewController == self) {//fixed bubble position position when push in broadcast detail
        CGRect notiButtonFrame = self.notiButton.frame;
        notiButtonFrame = [self.notiButton.superview convertRect:notiButtonFrame toView:self.navigationController.navigationBar];
        CGRect bubbleFrame = self.bubbleImageView.frame;
        bubbleFrame.origin.x = notiButtonFrame.origin.x + notiButtonFrame.size.width / 2  - bubbleFrame.size.width / 2;
        bubbleFrame.origin.y = self.navigationController.navigationBar.frame.size.height - bubbleFrame.size.height + 1;
        self.bubbleImageView.frame = bubbleFrame;
    }
    
}

- (IBAction)onSegmentSelect:(id) sender
{
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        _selectedTab = NotiTabNotification;
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getNotificationFeed:self success:^(id result) {
            [self dismisHUD];
            self.notificationFeedModel = (CMUNotificationFeedModel *)result;
            [self.notificationTableView reloadData];
            [self.notificationTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        } failure:^(NSError *error) {
            [self dismisHUD];
            self.notificationFeedModel = nil;
            [self.notificationTableView reloadData];
        }];
    }
    else
    {
        _selectedTab = NotiTabBroadcast;
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getBroadcastFeed:self success:^(id result) {
            [self dismisHUD];
            self.broadcastFeedModel = (CMUBroadcastFeedModel *)result;
            [self.broadcastTableView reloadData];
            [self.broadcastTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        } failure:^(NSError *error) {
            [self dismisHUD];
            self.broadcastFeedModel = nil;
            [self.broadcastTableView reloadData];
        }];
    }
    [self checkAndSave];
    [self updatePresentState];
}

- (IBAction)btnSettingsClicked:(id) sender
{
    _selectedTab = NotiTabSettings;
    self.settingsNewsChange = NO;
    self.settingsEventChange = NO;
    self.settingsChannelChange = NO;
    
    if(self.notificationSettingsModel == nil)
    {
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getNotificationSettings:self success:^(id result) {
            [self dismisHUD];
            self.notificationSettingsModel = (CMUNotificationSettingsModel *)result;
            [self prepareToSettingsTableViewCellModel];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];

    }
    [self updatePresentState];
}

- (void)prepareToSettingsTableViewCellModel
{
    for (NSNumber  *number in self.notificationSettingsModel.newsList)
    {
        for (CMUNotiSettingsTableViewCellModel *model in self.settingsNewsList)
        {
            if(model.settingId == number.intValue)
            {
                model.settingSelected = YES;
                break;
            }
        }
    }
    
    for (NSNumber  *number in self.notificationSettingsModel.activityList)
    {
        for (CMUNotiSettingsTableViewCellModel *model in self.settingsEventList)
        {
            if(model.settingId == number.intValue)
            {
                model.settingSelected = YES;
                break;
            }
        }
    }
    
    for (NSNumber  *number in self.notificationSettingsModel.channelList)
    {
        for (CMUNotiSettingsTableViewCellModel *model in self.settingsChannelList)
        {
            if(model.settingId == number.intValue)
            {
                model.settingSelected = YES;
                break;
            }
        }
    }
}

- (void)checkAndSave
{
    if(self.settingsNewsChange)
    {
        NSString *key = [self cellModelToWebServiceModel:self.settingsNewsList];
        DebugLog(@"%@", key);
        [[CMUWebServiceManager sharedInstance] setNotificationSettings:self key:key mode:@"news" success:^(id result) {
            DebugLog(@"");
        } failure:^(NSError *error) {
            DebugLog(@"");
        }];
    }
    if(self.settingsEventChange)
    {
        NSString *key = [self cellModelToWebServiceModel:self.settingsEventList];
        DebugLog(@"%@", key);
        [[CMUWebServiceManager sharedInstance] setNotificationSettings:self key:key mode:@"event" success:^(id result) {
            DebugLog(@"");
        } failure:^(NSError *error) {
            DebugLog(@"");
        }];
    }
    if(self.settingsChannelChange)
    {
        NSString *key = [self cellModelToWebServiceModel:self.settingsChannelList];
        DebugLog(@"%@", key);
        [[CMUWebServiceManager sharedInstance] setNotificationSettings:self key:key mode:@"channel" success:^(id result) {
            DebugLog(@"");
        } failure:^(NSError *error) {
            DebugLog(@"");
        }];
    }
}

- (NSString *)cellModelToWebServiceModel:(NSArray *)cellModelList
{
    NSMutableArray *selectedParams = [[NSMutableArray alloc] init];
    for(CMUNotiSettingsTableViewCellModel *model in cellModelList)
    {
        if(model.settingSelected)
        {
            [selectedParams addObject:[NSString stringWithFormat:@"%i", model.settingId]];
        }
        
    }
    return [selectedParams componentsJoinedByString:@","];
}

- (IBAction)onClearAllNotificationHeaderTapped:(id)sender
{
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] deleteAllNotification:self success:^(id result) {
        [self dismisHUD];
        [self.notificationFeedModel.notificationFeedList removeAllObjects];
        [self.notificationTableView reloadData];
        
        //we don't known how to web service count the notification number
        [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
        } failure:^(NSError *error) {
        }];
        
//        [[CMUWebServiceManager sharedInstance] notificationNumberModel].notificationNumber = 0;
//        [[NSNotificationCenter defaultCenter] postNotificationName:CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION object:self];
        
    } failure:^(NSError *error) {
        [self dismisHUD];
    }];
}

- (IBAction)onSettingsNewsHeaderTapped:(id)sender
{
    UIView *tappedSettingsHeaderView =  ((UITapGestureRecognizer *)sender).view;
    if(tappedSettingsHeaderView == self.settingsNewsHeader)
    {
        self.settingsNewsExpand = !self.settingsNewsExpand;
        [self.settingsTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if(tappedSettingsHeaderView == self.settingsActivityHeader)
    {
        self.settingsEventExpand = !self.settingsEventExpand;
        [self.settingsTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if(tappedSettingsHeaderView == self.settingsChannelHeader)
    {
        self.settingsChannelExpand = !self.settingsChannelExpand;
        [self.settingsTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark section table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == self.settingsTableView)
    {
        return 2;
    }
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.notificationTableView)
    {
        return self.notificationFeedModel.notificationFeedList.count;
    }
    else if(tableView == self.broadcastTableView)
    {
        return self.broadcastFeedModel.broadcastFeedList.count;
    }
    else if(tableView == self.settingsTableView)
    {
        if(section == 0)
        {
            return self.settingsNewsExpand? self.settingsNewsList.count:0;
        }
        else if(section == 1)
        {
            return self.settingsEventExpand? self.settingsEventList.count:0;
        }
        else if(section == 2)
        {
            return self.settingsChannelExpand? self.settingsChannelList.count:0;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.notificationTableView)
    {
        if(self.notificationFeedModel.notificationFeedList.count > 0)
        {
            if(IS_IPAD)
            {
                return 40;
            }
            return 30;
        }
    }
    else if(tableView == self.settingsTableView)
    {
        if(IS_IPAD)
        {
            return 90;
        }
        return 70;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.notificationTableView)
    {
        return self.clearAllNotificationViewHeader;
    }
    else if(tableView == self.settingsTableView)
    {
        switch (section) {
            case 0:
                self.settingsNewsHeaderAccessory.image = self.settingsNewsExpand?[UIImage imageNamed:@"noti_settings_collapes.png"]:[UIImage imageNamed:@"noti_settings_expand.png"];
                return self.settingsNewsHeader;
                break;
            case 1:
                self.settingsActivityHeaderAccessory.image = self.settingsEventExpand?[UIImage imageNamed:@"noti_settings_collapes.png"]:[UIImage imageNamed:@"noti_settings_expand.png"];
                return self.settingsActivityHeader;
                break;
            case 2:
                self.settingsChannelHeaderAccessory.image = self.settingsChannelExpand?[UIImage imageNamed:@"noti_settings_collapes.png"]:[UIImage imageNamed:@"noti_settings_expand.png"];
                return self.settingsChannelHeader;
                break;
                
            default:
                break;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.notificationTableView)
    {
        if(IS_IPAD)
        {
            return 120;
        }
        return 80;
    }
    else if(tableView == self.broadcastTableView)
    {
        if(IS_IPAD)
        {
            return 120;
        }
        return 80;
    }
    else if(tableView == self.settingsTableView)
    {
        if(IS_IPAD)
        {
            return 75;
        }
        return 50;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.notificationTableView)
    {
        CMUNotificationFeedTableViewCell *cell = (CMUNotificationFeedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:notiFeedTableViewCell forIndexPath:indexPath];
        
        cell.feedModel = [self.notificationFeedModel.notificationFeedList objectAtIndex:indexPath.row];
        return cell;
    }
    else if(tableView == self.broadcastTableView)
    {
        CMUBroadcastFeedTableViewCell *cell = (CMUBroadcastFeedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:broadcastFeedTableViewCell forIndexPath:indexPath];
        
        cell.feedModel = [self.broadcastFeedModel.broadcastFeedList objectAtIndex:indexPath.row];
        return cell;
    }
    else if(tableView == self.settingsTableView)
    {
        CMUNotiSettingsTableViewCell *cell = (CMUNotiSettingsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:notiSettingsTableViewCell forIndexPath:indexPath];
        
        CMUNotiSettingsTableViewCellModel *model = nil;
        switch (indexPath.section) {
            case 0:
                model = [self.settingsNewsList objectAtIndex:indexPath.row];
                break;
            case 1:
                model = [self.settingsEventList objectAtIndex:indexPath.row];
                break;
            case 2:
                model = [self.settingsChannelList objectAtIndex:indexPath.row];
                break;
                
            default:
                break;
        }
        cell.model = model;
        return cell;
    }
    return nil;
}

- (void)hideNotificationBubbleInNavigation
{
    [ self.navigationController.navigationBar removeGestureRecognizer:self.tapGesture];
    self.bubbleImageView.hidden = YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(tableView == self.notificationTableView)
    {
        //open news detail
        CMUNotificationFeed *notiFeed = [self.notificationFeedModel.notificationFeedList objectAtIndex:indexPath.row];
        if(notiFeed.type == 1)//news
        {
            CMUNewsViewController *newsViewController = [[CMUNewsViewController alloc] init];
            newsViewController.newsViewMode = NEWS_VIEW_MODE_DETAIL;
            newsViewController.newsDetailItemId = notiFeed.itemId;
            [self.navigationController pushViewController:newsViewController animated:YES];
            [self hideNotificationBubbleInNavigation];
            
            //delete feed
            [[CMUWebServiceManager sharedInstance] deleteNotification:self feedId:notiFeed.notiId success:^(id result) {
                [self.notificationFeedModel.notificationFeedList removeObject:notiFeed];
                [self.notificationTableView reloadData];
                
                //need to update badge
                [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
                    //Nothing todo here
                } failure:^(NSError *error) {
                    
                }];
                
            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
            }];
        }
        else if(notiFeed.type == 2)//activity
        {
            
            CMUEventViewController *eventViewController = [[CMUEventViewController alloc] init];
            eventViewController.activityViewMode = ACTIVITY_VIEW_MODE_NOTIFICATION;
            [self.navigationController pushViewController:eventViewController animated:YES];
            [self hideNotificationBubbleInNavigation];
            //delete feed
            [[CMUWebServiceManager sharedInstance] deleteNotification:self feedId:notiFeed.notiId success:^(id result) {
                [self.notificationFeedModel.notificationFeedList removeObject:notiFeed];
                [self.notificationTableView reloadData];
                
                //need to update badge
                [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
                    //Nothing todo here
                } failure:^(NSError *error) {
                    
                }];
                
            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
            }];

        }
        else if(notiFeed.type == 3)//broadcast
        {
            CMUBroadcastSenderDetailViewController *detailViewController = [[CMUBroadcastSenderDetailViewController alloc] init];
            detailViewController.senderId = notiFeed.senderId;
            detailViewController.senderName = notiFeed.senderName;
            detailViewController.senderImageURL = notiFeed.senderImage;
            detailViewController.delegate = self;
            [self.navigationController pushViewController:detailViewController animated:YES];
            
            //delete feed
            [[CMUWebServiceManager sharedInstance] deleteNotification:self feedId:notiFeed.notiId success:^(id result) {
                [self.notificationFeedModel.notificationFeedList removeObject:notiFeed];
                [self.notificationTableView reloadData];
                
                //need to update badge
                [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
                    //Nothing todo here
                } failure:^(NSError *error) {
                    
                }];
                
            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
            }];

        }
        else if(notiFeed.type == 4)//edoc
        {
            CMUMISEdocDetailViewController *detailVC = [[CMUMISEdocDetailViewController alloc] init];
            detailVC.edocCategory = CmuMisEdocCategory_RECIEVE;
            detailVC.delegate = self;
            detailVC.docID = notiFeed.itemId;
            detailVC.docSubID = notiFeed.subItemId;
            [self.navigationController pushViewController:detailVC animated:YES];
            
            //delete feed
            [[CMUWebServiceManager sharedInstance] deleteNotification:self feedId:notiFeed.notiId success:^(id result) {
                [self.notificationFeedModel.notificationFeedList removeObject:notiFeed];
                [self.notificationTableView reloadData];
                
                //need to update badge
                [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
                    //Nothing todo here
                } failure:^(NSError *error) {
                    
                }];
                
            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
            }];
            
            //set open flag
            [[CMUWebServiceManager sharedInstance] edocRecieveTabEditOpen:self docSubID:notiFeed.subItemId success:^(id result) {
                DebugLog(@"");
            } failure:^(NSError *error) {
                DebugLog(@"");
            }];

        }
    }
    else if(tableView == self.broadcastTableView)
    {
        CMUBroadcastFeed *broadcastFeed = [self.broadcastFeedModel.broadcastFeedList objectAtIndex:indexPath.row];

        CMUBroadcastSenderDetailViewController *detailViewController = [[CMUBroadcastSenderDetailViewController alloc] init];
        detailViewController.senderId = broadcastFeed.broadcastId;
        detailViewController.senderName = broadcastFeed.name;
        detailViewController.senderImageURL = broadcastFeed.image;
        detailViewController.delegate = self;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    
    else if(tableView == self.settingsTableView)
    {
        CMUNotiSettingsTableViewCellModel *model = nil;
        switch (indexPath.section) {
            case 0:
                self.settingsNewsChange = YES;
                model = [self.settingsNewsList objectAtIndex:indexPath.row];
                break;
            case 1:
                self.settingsEventChange = YES;
                model = [self.settingsEventList objectAtIndex:indexPath.row];
                break;
            case 2:
                self.settingsChannelChange = YES;
                model = [self.settingsChannelList objectAtIndex:indexPath.row];
                break;
                
            default:
                break;
        }
        model.settingSelected = !model.settingSelected;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:model.settingSelected? UITableViewRowAnimationTop: UITableViewRowAnimationBottom];
    }
}

//overide
- (void)btnLeftViewClicked:(id) sender
{
    [self checkAndSave];
    CMUAppDelegate *appDelegate = ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate]);
    [appDelegate.drawerController.centerViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)btnHomeClicked:(id) sender
{
    [self checkAndSave];
    CMUAppDelegate *appDelegate = ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate]);
    [appDelegate.drawerController.centerViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)btnNotiClicked:(id) sender
{
    [self checkAndSave];
    CMUAppDelegate *appDelegate = ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate]);
    [appDelegate.drawerController.centerViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark Login / Logout
- (void)btnLoginClicked:(id) sender
{
    [self checkAndSave];
    CMUAppDelegate *appDelegate = ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate]);
    [appDelegate.drawerController.centerViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark CMUBroadcastSenderDetailViewControllerDelegate
- (void)broadcastSenderDetailViewControllerBackButtonClicked:(CMUBroadcastSenderDetailViewController *) broadcastSenderDetailViewController
{
    [self onSegmentSelect:nil];
    [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:nil failure:nil];
    [self.navigationController popToViewController:self animated:YES];
}

#pragma mark CMUMISEdocDetailViewControllerDelegate
- (void)cmuMisEdocDetailViewControllerBackClicked:(CMUMISEdocDetailViewController *) vc
{
    [self onSegmentSelect:nil];
    [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:nil failure:nil];
    [self.navigationController popToViewController:self animated:YES];
}
@end
