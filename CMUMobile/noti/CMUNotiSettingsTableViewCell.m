//
//  CMUNotiSettingsTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNotiSettingsTableViewCell.h"

@implementation CMUNotiSettingsTableViewCellModel

@end

@interface CMUNotiSettingsTableViewCell()
@property(nonatomic, weak)IBOutlet UIImageView *imvCheckbox;
@property(nonatomic, weak)IBOutlet UILabel* lblTitle;
@end

@implementation CMUNotiSettingsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(CMUNotiSettingsTableViewCellModel *)model
{
    _model = model;
    [self updatePresentState];
}

- (void)updatePresentState
{
    self.imvCheckbox.image = self.model.settingSelected? [UIImage imageNamed:@"noti_settings_check.png"]:[UIImage imageNamed:@"noti_settings_uncheck.png"];
    self.lblTitle.text = self.model.settingName;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imvCheckbox.image = nil;
    self.lblTitle.text = nil;
}

@end
