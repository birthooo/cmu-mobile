//
//  CMUBroadcastFeedTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 12/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNotificationModel.h"

@interface CMUBroadcastFeedTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUBroadcastFeed *feedModel;
@end
