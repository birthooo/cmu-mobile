//
//  CMUBroadcastSenderDetailTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 12/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUBroadcastSenderDetailTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CMUFormatUtils.h"
#import "CMUStringUtils.h"

@interface CMUBroadcastSenderDetailTableViewCell()
@property(nonatomic, weak) IBOutlet UIImageView *imvIcon;
@property(nonatomic, weak) IBOutlet UILabel *lblDate;
@property(nonatomic, weak) IBOutlet UIView *mesageBorderView;
@property(nonatomic, weak) IBOutlet UILabel *lblMessage;
@end

@implementation CMUBroadcastSenderDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.mesageBorderView.layer.cornerRadius = 10;
    self.mesageBorderView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFeedModel:(CMUBroadcastSenderFeed *)feedModel
{
    _feedModel = feedModel;
    [self updatePresentState];
}

- (void)updatePresentState
{
    //TODO remove this?
    if([CMUStringUtils isEmpty:self.iconURL])
    {
        self.imvIcon.image = [UIImage imageNamed:@"noti_cmu_logo.png"];
    }
    else
    {
        [self.imvIcon setImageWithURL: [NSURL URLWithString: self.iconURL]];
    }
    self.lblMessage.text = _feedModel.text;
    self.lblDate.text = [CMUFormatUtils formatDateNotificationFeedStyle:_feedModel.update locale:[NSLocale currentLocale]];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.imvIcon sd_cancelCurrentImageLoad];
    self.imvIcon.image = nil;
    self.lblMessage.text = nil;
    self.lblDate.text = nil;
}

@end
