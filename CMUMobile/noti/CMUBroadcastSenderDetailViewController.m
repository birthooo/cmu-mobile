//
//  CMUBroadcastSenderDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 12/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUBroadcastSenderDetailViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUStringUtils.h"
#import "ODRefreshControl.h"
#import "CMUUIUtils.h"
#import "CMUBroadcastSenderDetailTableViewCell.h"

static NSString *broadcastSenderFeedTableViewCell = @"CMUBroadcastSenderDetailTableViewCell";

@interface CMUBroadcastSenderDetailViewController ()
@property(nonatomic, strong) IBOutlet UIView *headerView;
@property(nonatomic, weak) IBOutlet UILabel *lblHeaderTitle;
@property(nonatomic, strong) IBOutlet ODRefreshControl *refreshControl;
@property(nonatomic, weak)IBOutlet UITableView *tableView;
@property(nonatomic, strong) UIActivityIndicatorView *loadMoreSpinner;

@property(nonatomic, unsafe_unretained)BOOL resetFeedList;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSArray *feedList;
@property(nonatomic, unsafe_unretained) BOOL loadingMore;

- (IBAction)backTapped:(id)sender;
@end
@implementation CMUBroadcastSenderDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidRecieveRemoteNotificationInForeground:) name:CMU_APPLICATION_DID_RECEIVE_REMOTE_NOTIFICATION_IN_FOREGROUND object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lblHeaderTitle.text = self.senderName;
    //pull refresh
    self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUBroadcastSenderDetailTableViewCell" bundle:nil] forCellReuseIdentifier:broadcastSenderFeedTableViewCell];
    
    self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
    
    [self refresh];
    [self updatePresentState];
}

- (void)refresh
{
    self.resetFeedList = YES;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getBroadcastBySenderFeed:self senderId:self.senderId success:^(id result) {
        CMUBroadcastSenderFeedModel *broadcastModel = (CMUBroadcastSenderFeedModel *)result;
        self.refreshURL = [CMUStringUtils isEmpty:broadcastModel.refreshURL]? nil: broadcastModel.refreshURL;
        self.loadMoreURL = [CMUStringUtils isEmpty:broadcastModel.loadMoreURL]? nil: broadcastModel.loadMoreURL;
        [self populateFeedList:result];
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self populateFeedList:nil];
        [self updatePresentState];
        [self dismisHUD];
    }];
}

- (void)updatePresentState
{
    [super updatePresentState];
    
    if(self.loadingMore && self.tableView.tableFooterView == nil)
    {
        self.tableView.tableFooterView = self.loadMoreSpinner;
        [self.loadMoreSpinner startAnimating];
    }
    else
    {
        self.tableView.tableFooterView = nil;
        [self.loadMoreSpinner stopAnimating];
    }
}


- (void)populateFeedList:(CMUBroadcastSenderFeedModel *) broadcastSenderFeedModel
{
    //check to reset feed list
    BOOL scrollToTop = self.resetFeedList;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    if(!self.resetFeedList)
    {
        [tempFeedList addObjectsFromArray:self.feedList];
    }
    self.resetFeedList = NO;
    
    //add new feed to feed list
    for(CMUBroadcastSenderFeed *broadcastSenderFeed in broadcastSenderFeedModel.broadcastSenderFeedList)
    {
        if(![tempFeedList containsObject:broadcastSenderFeed])
        {
            [tempFeedList addObject:broadcastSenderFeed];
        }
    }
    
    //perform sort
    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        CMUBroadcastSenderFeed *first = (CMUBroadcastSenderFeed*)a;
        CMUBroadcastSenderFeed *second = (CMUBroadcastSenderFeed*)b;
        return [second.update compare:first.update];
    }];
    
    self.feedList = sortedFeedList;
    
    [self.tableView reloadData];
    if(scrollToTop)
    {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)appDidBecomeActive:(NSNotification*)note
{
    [self refresh];
}

-(void)appDidRecieveRemoteNotificationInForeground:(NSNotification*)note
{
    [self refresh];
}

- (void)dropViewDidBeginRefreshing:(id) sender
{
    if(self.refreshURL)
    {
        [[CMUWebServiceManager sharedInstance] getBroadcastSenderFeedฺByURL:self url:self.refreshURL success:^(id result) {
            CMUBroadcastSenderFeedModel *broadcastModel = (CMUBroadcastSenderFeedModel *)result;
            if(![CMUStringUtils isEmpty:broadcastModel.refreshURL])
            {
                self.refreshURL = broadcastModel.refreshURL;
            }
            [self populateFeedList:broadcastModel];
            [self.refreshControl endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControl endRefreshing];
        }];
    }
    else
    {
        [self.refreshControl endRefreshing];
    }
}


#pragma mark section table view
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUBroadcastSenderFeed *broadcastFeed =[self.feedList objectAtIndex:indexPath.row];

    CGFloat cellWidth = self.tableView.frame.size.width;
    CGFloat height = 0;
    
    height = 80 - (29);// 29 label nib, 80 view nib
    
    if(IS_IPHONE)
    {
        CGFloat labelWidth = cellWidth - (320 - 242); // 242 label width in nib
        UIFont *font = [UIFont systemFontOfSize:14.0]; //according to iphone nib
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:broadcastFeed.text withFont:font];
        height += size.height;
    }
    else
    {
        CGFloat labelWidth = cellWidth - (320 - 224); // 224 label width in nib
        UIFont *font = [UIFont systemFontOfSize:20.0]; //according to iphone nib
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:broadcastFeed.text withFont:font];
        height += size.height;
    }
    
    return height + 1;//+1 = tableview cell line seperator
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUBroadcastSenderDetailTableViewCell *cell = nil;
    CMUBroadcastSenderFeed *broadcastFeed = [self.feedList objectAtIndex:indexPath.row];

    cell = (CMUBroadcastSenderDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:broadcastSenderFeedTableViewCell forIndexPath:indexPath];
    cell.iconURL = self.senderImageURL;
    cell.feedModel = broadcastFeed;
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if(self.loadMoreURL)
        {
            if(self.loadingMore)
            {
                return;
            }
            self.loadingMore = YES;
            [self updatePresentState];
            [[CMUWebServiceManager sharedInstance] getBroadcastSenderFeedฺByURL:self url:self.loadMoreURL success:^(id result) {
                CMUNewsModel *newsModel = (CMUNewsModel *)result;
                self.loadMoreURL = [CMUStringUtils isEmpty:newsModel.loadMoreURL]? nil: newsModel.loadMoreURL;
                [self populateFeedList:result];
                self.loadingMore = NO;
                [self updatePresentState];
            } failure:^(NSError *error) {
                self.loadingMore = NO;
                [self updatePresentState];
            }];
        }
    }
}

- (void)backTapped:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(broadcastSenderDetailViewControllerBackButtonClicked:)])
    {
        [_delegate broadcastSenderDetailViewControllerBackButtonClicked:self];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
