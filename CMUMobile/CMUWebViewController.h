//
//  CMUWebViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUViewController.h"

@interface CMUWebViewController : CMUViewController<UIWebViewDelegate>
@property(nonatomic, strong) NSString *url;
@property(nonatomic, unsafe_unretained) BOOL hideURLTitle;
@end
