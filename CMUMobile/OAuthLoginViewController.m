//
//  OAuthLoginViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 14/12/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "OAuthLoginViewController.h"
#import "OAuthLoginHeader.h"
#import "CMULoginViewController.h"
#import "global.h"
#import "CMULoginModel.h"
#import "CMUUserInfoModel.h"
#import "CMUSettings.h"

#define KEY_CMU_USER_TICKET @"KEY_CMU_USER_TICKET"
#define KEY_CMU_USER_INFO_MODEL @"KEY_CMU_USER_INFO_MODEL"
#define KEY_CMU_USER_DEVICE_TOKEN @"KEY_CMU_USER_DEVICE_TOKEN"
#define KEY_CMU_IS_DEVICE_REGIS_PUSH @"KEY_CMU_IS_DEVICE_REGIS_PUSH"

@interface OAuthLoginViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webview;


@end

@implementation OAuthLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *str = [NSString stringWithFormat:LOGIN_URL,LOGIN_REDIRECT_URI,LOGIN_CLIENT_ID];
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:LOGIN_URL,LOGIN_REDIRECT_URI,LOGIN_CLIENT_ID]]]];
    
}

- (IBAction)backAction:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = [request mainDocumentURL];
    NSString *str = [NSString stringWithFormat:@"%@",url];
    NSArray *objects = [str componentsSeparatedByString:@":"];
    if([objects[0] isEqual:@"cmumobile"]){
        //
        NSArray *object2 = [str componentsSeparatedByString:@"="];
        
        NSString *urlGetToken = [NSString stringWithFormat:@"code=%@&client_id=%@&client_secret=%@&grant_type=%@&redirect_uri=%@",object2[1],LOGIN_CLIENT_ID,LOGIN_CLIENT_SECRET,LOGIN_GRANT_TYPE,LOGIN_REDIRECT_URI];
                NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
                dictionary = [self POSTRequest:urlGetToken url:LOGIN_GET_TOKEN_URL];
                NSLog(@"dictiona :%@",dictionary);
                [[NSUserDefaults standardUserDefaults]setValue:[dictionary valueForKey:@"access_token"] forKey:@"access_token"];
                [[NSUserDefaults standardUserDefaults]setValue:[dictionary valueForKey:@"refresh_token"] forKey:@"refresh_token"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                NSMutableDictionary *userinfo = [[NSMutableDictionary alloc]init];
                userinfo = [[self GetUserInfo]valueForKey:@"data"];
                [[NSUserDefaults standardUserDefaults]setValue:[userinfo valueForKey:@"itaccount_name"] forKey:@"account_name"];
                NSLog(@"userinfo:%@",userinfo);
        
        
               // NSLog(@"USER INFO :%@",loginModel);
        
        //SUCCESS
                NSMutableDictionary *successDict = [[NSMutableDictionary alloc]init];
                [successDict setObject:[NSNumber numberWithBool:YES] forKey:@"success"];

        
        
        //TICKET
                NSMutableDictionary *ticketDict = [[NSMutableDictionary alloc]init];
                [ticketDict setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
                [ticketDict setObject:[dictionary valueForKey:@"expires_in"] forKey:@"expires_in"];
                [ticketDict setObject:[dictionary valueForKey:@"access_token"] forKey:@"access_token"];
        //EMPLOYEE
        NSMutableDictionary *employeeDict = [[NSMutableDictionary alloc]initWithDictionary:userinfo];
        
        NSMutableDictionary *loginDict = [[NSMutableDictionary alloc]init];
        [loginDict setObject:[NSNumber numberWithBool:YES] forKey:@"success"];
        [loginDict setObject:ticketDict forKey:@"ticket"];
       // [loginDict setObject:userinfo forKey:@"employee"];
        
        NSLog(@"loginDict:%@",loginDict);
        CMULoginModel *loginModel = (CMULoginModel *)loginDict;
        [loginDict setObject:userinfo forKey:@"employee"];
         CMUUserInfoModel *userInfoModel = (CMUUserInfoModel *)loginDict;
                CMUSettings *share = [[CMUSettings sharedInstance]init];
                [share loadMenuItems];
                        //
                        NSMutableDictionary *dictToSave = [[NSMutableDictionary alloc]init];
                        [dictToSave setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
                        [dictToSave setObject:[dictionary valueForKey:@"expires_in"] forKey:@"expires_in"];
        
                        [dictionary setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
                        [dictToSave setObject:userinfo forKey:@"employee"];
                        [dictToSave setObject:dictionary forKey:@"ticket"];
        NSLog(@"dictToSave :%@",dictToSave);
        
       
        [self saveTicket:dictToSave];
        [self saveUserInfo:loginDict];
        
//                //
//                NSMutableDictionary *dictToSave = [[NSMutableDictionary alloc]init];
//        //        [dictToSave setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
//        //        [dictToSave setObject:[dictionary valueForKey:@"expires_in"] forKey:@"expires_in"];
//
//                [dictionary setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
//                [dictToSave setObject:userinfo forKey:@"employee"];
//                [dictToSave setObject:dictionary forKey:@"ticket"];
//
//                CMUUserInfoModel *userInfoModel = [[CMUUserInfoModel alloc] initWithDictionary:dictToSave];
//
//                [self regisMobileToken];
//                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLoginSuccess"];
//                CMUSettings *share = [[CMUSettings sharedInstance]init];
//                [share loadMenuItems];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogin" object:nil];
//                       [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
//            }
//
//        CMULoginModel *loginModel = (CMULoginModel *)result;
//        CMUUserInfoModel *userInfoModel = (CMUUserInfoModel *)result;
//
//        CMUSettings *share = [[CMUSettings sharedInstance]init];
//        [share loadMenuItems];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:self];
//        NSLog(@"LOGIN SUCCESS");
//
        
        
        
        
    }

    return YES;
}

//
-(id)POSTRequest:(NSString*)poststr url:(NSString*)url{

NSData *postData = [poststr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
[request setURL:[NSURL URLWithString:url]];
[request setHTTPMethod:@"POST"];
// [request setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];

[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
[request setHTTPBody:postData];
NSURLResponse *response;
NSError *error;
NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
NSData *objectData = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
NSDictionary *dictionaryData = [NSJSONSerialization JSONObjectWithData:objectData
                                                               options:NSJSONReadingMutableContainers
                                                                 error:&error];
NSLog(@"[POST] URL :%@",url);
NSLog(@"[POST] Str :%@", poststr);
NSLog(@"[POST] return :%@",dictionaryData);
return dictionaryData;
}

-(id)GetUserInfo
{
    NSString *strUrl = [NSString stringWithFormat:@"%@UserInfo.aspx?access_token=%@",END_POINT,[[NSUserDefaults standardUserDefaults]valueForKey:@"access_token"]];
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    //Set Header
    [dataRqst addValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"access_token"] forHTTPHeaderField: @"Authorization"];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",strUrl);
    NSLog(@"[GET] return :%@",json);
    return json;
}
-(void)regisMobileToken{
    NSString *str = [NSString stringWithFormat:REGIS_MOBILE_TOKEN,[[NSUserDefaults standardUserDefaults]valueForKey:@"account_name"],[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]];
    NSDictionary *dict = [self GETRequestToken:str];
    [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"data" ] forKey:@"access_token"];
    if([[dict valueForKey:@"status"]isEqual:@"pass"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToInbox" object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToNoPass" object:nil];
        [[NSUserDefaults standardUserDefaults]setBool:0 forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"access_token"];
        [[NSUserDefaults standardUserDefaults]setValue:@"null" forKey:@"account_name"];
        [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"noti_number"];
    }
}

-(id)GETRequestToken:(NSString*)URLString
{
    //Set Access Token String
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    //Set Header
    [dataRqst addValue:[userDefault valueForKey:@"access_token"] forHTTPHeaderField: @"token"];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",URLString);
    NSLog(@"[GET] return :%@",json);
    return json;
}

- (void)saveTicket:(NSDictionary *)ticketDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:ticketDict];
    [defaults setObject:data forKey:KEY_CMU_USER_TICKET];
    [defaults synchronize];
    NSLog(@"saveTicket :%@",[defaults valueForKey:KEY_CMU_USER_TICKET]);
}


- (void)saveUserInfo:(NSDictionary *)userInfoDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userInfoDict];
    [defaults setObject:data forKey:KEY_CMU_USER_INFO_MODEL];
    [defaults synchronize];
}
@end
