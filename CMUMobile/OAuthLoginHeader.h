//
//  OAuthLoginHeader.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 14/12/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#ifndef OAuthLoginHeader_h
#define OAuthLoginHeader_h


#define LOGIN_CLIENT_ID @"SPt5pNFMUJymMGcS83GTTzszVJ8bWHeCDsxEkzB5"
#define LOGIN_CLIENT_SECRET @"Mr0sx09qt3NRmfksw4kRPE0mD5UvkHFJBMgBKab5"
#define LOGIN_REDIRECT_URI @"cmumobile://oauth"
#define LOGIN_GRANT_TYPE @"authorization_code"


#define END_POINT @"https://oauth.cmu.ac.th/v1/"
#define LOGIN_URL @"https://oauth.cmu.ac.th/v1/Authorize.aspx?redirect_uri=%@&response_type=code&client_id=%@"
#define REGIS_MOBILE_TOKEN @"https://mobileapi.cmu.ac.th/api/User/regisMobileTOkenTest?cmuaccount=%@&systemName=edoc&oToken=%@"

//https://oauth.cmu.ac.th/v0/GetToken.aspx
//#define LOGIN_GET_TOKEN_URL @"https://oauth.cmu.ac.th/v0/GetToken.aspx/code=%@&client_id=%@&client_secret=%@&grant_type=%@&redirect_uri=%@"
#define LOGIN_GET_TOKEN_URL @"https://oauth.cmu.ac.th/v1/GetToken.aspx/"

#endif /* OAuthLoginHeader_h */
