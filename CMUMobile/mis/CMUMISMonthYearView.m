//
//  CMUMISMonthYearView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/26/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISMonthYearView.h"
#import "global.h"

@interface CMUMISMonthYearView()
@property(nonatomic, weak)IBOutlet UIView *viewMonthInner;
@property(nonatomic, weak)IBOutlet UIView *viewYearInner;
@property(nonatomic, weak)IBOutlet UILabel *lblMonth;
@property(nonatomic, weak)IBOutlet UILabel *lblYear;
@property(nonatomic, weak)IBOutlet UIImageView *imvMonthDropdown;
@property(nonatomic, weak)IBOutlet UIImageView *imvYearDropdown;
@property(nonatomic, strong) PopoverView *popoverViewMonth;
@property(nonatomic, strong) CMUMonthPopupView *monthPopupView;
@property(nonatomic, strong) PopoverView *popoverViewYear;
@property(nonatomic, strong) CMUYearPopupView *yearPopupView;
- (IBAction)monthTapped:(id)sender;
- (IBAction)yearTapped:(id)sender;
@end

@implementation CMUMISMonthYearView
- (void)customInit
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMISMonthYearView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    self.viewMonthInner.layer.cornerRadius = 5;
    self.viewMonthInner.layer.masksToBounds = YES;
    self.viewYearInner.layer.cornerRadius = 5;
    self.viewYearInner.layer.masksToBounds = YES;
    
    
    self.monthPopupView = [[CMUMonthPopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 264)];
    self.monthPopupView.delegate = self;
    
    self.yearPopupView = [[CMUYearPopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 132): CGRectMake(0, 0, 280, 132)];
    self.yearPopupView.delegate = self;
    
    
    [self reset];
    [self updatePresentState];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)reset
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    int year = [components year];
    int month = [components month];
    //int day = [components day];
    self.month = month;
    self.year = year;
}

- (void)setMonth:(int)month
{
    _month = month;
    [self updatePresentState];
}

- (void)setAllMonth:(BOOL)allMonth
{
    _allMonth = allMonth;
    self.monthPopupView.allMonth = _allMonth;
}

- (void)setYear:(int)year
{
    _year = year;
    [self updatePresentState];
}

- (void)updatePresentState
{
    int monthToFormat = _month;
    if(_month == 0)
    {
        monthToFormat = 1;
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [components setMonth:monthToFormat];
    [components setYear:_year];
    NSDate *date = [calendar dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"MMMM"];
    self.lblMonth.text = [dateFormatter stringFromDate:date];
    
    [dateFormatter setDateFormat:@"yyyy"];
    self.lblYear.text = [dateFormatter stringFromDate:date];
    
    if(_month == 0)
    {
        self.lblMonth.text = @"All";
    }
    
    
}


- (void)monthTapped:(id)sender
{
    
    CGRect monthImageFrame = [self.pickerParentView convertRect:self.imvMonthDropdown.frame fromView:self.viewMonthInner];
    CGPoint pointToShow = CGPointMake(monthImageFrame.origin.x + monthImageFrame.size.width / 2, monthImageFrame.origin.y + monthImageFrame.size.height - 5);
    self.popoverViewMonth = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverViewMonth.delegate = self;
    [self.popoverViewMonth showAtPoint:pointToShow inView:self.pickerParentView withContentView:self.monthPopupView];
    
}

- (void)yearTapped:(id)sender
{
    CGRect yearImageFrame = [self.pickerParentView convertRect:self.imvYearDropdown.frame fromView:self.viewYearInner];
    CGPoint pointToShow = CGPointMake(yearImageFrame.origin.x + yearImageFrame.size.width / 2, yearImageFrame.origin.y + yearImageFrame.size.height - 5);
    self.popoverViewYear = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverViewYear.delegate = self;
    [self.popoverViewYear showAtPoint:pointToShow inView:self.pickerParentView withContentView:self.yearPopupView];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark CMUMonthPopupViewDelegate
- (void)monthPopupView:(CMUMonthPopupView *) newsTypePopupView didSelectedMonth:(int) month
{
    [self.popoverViewMonth dismiss];
    self.month = month;
    if(_delegate && [_delegate respondsToSelector:@selector(misMonthYearViewDidChanged:)])
    {
        [_delegate misMonthYearViewDidChanged:self];
    }
}

#pragma mark CMUYearPopupViewDelegate
- (void)yearPopupView:(CMUYearPopupView *) newsTypePopupView didSelectedYear:(int)year
{
    [self.popoverViewYear dismiss];
    self.year = year;
    if(_delegate && [_delegate respondsToSelector:@selector(misMonthYearViewDidChanged:)])
    {
        [_delegate misMonthYearViewDidChanged:self];
    }
}

@end
