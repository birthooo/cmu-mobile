//
//  CMUMISEDocViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/25/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUMISTabView.h"
#import "CMUMISEdocRecieveViewController.h"
#import "CMUMISEdocSendViewController.h"
#import "CMUMISEdocFollowViewController.h"
#import "CMUMISEdocPublicViewController.h"
#import "CMUMISEdocFolderViewController.h"
#import "CMUMISEdocDetailViewController.h"
#import "CMUMISEdocFolderSublistViewController.h"

@interface CMUMISEDocViewController : CMUNavBarViewController<CMUMISTabViewDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate, CMUMISEdocBaseRSFViewControllerDelegate, CMUMISEdocDetailViewControllerDelegate, CMUMISEdocFolderControllerDelegate, CMUMISEdocFolderSublistViewControllerDelegate>

@end
