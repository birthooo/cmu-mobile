//
//  CMUMISTabView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISTabView.h"
#import "global.h"
#import "GMGridViewLayoutStrategies.h"
#import "CMUFormatUtils.h"

@implementation CMUMISTabModel
- (id)initWithTitle:(NSString *) title number:(int) number pointerColor:(UIColor *) pointerColor
{
    self = [super init];
    if(self)
    {
        self.title = title;
        self.number = number;
        self.pointerColor = pointerColor;
    }
    return self;
}

@end

@interface CMUMISTabView()
@property(nonatomic, weak) IBOutlet GMGridView *gridView;
@property(nonatomic, unsafe_unretained)BOOL firstLayout;
@end

@implementation CMUMISTabView

- (void)customInit
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMISTabView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    //self.userInteractionEnabled = YES;
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.gridView.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontal];
    self.gridView.minimumPressDuration = 0.5;
    self.gridView.style = GMGridViewStyleSwap;
    self.gridView.itemSpacing = 0;
    self.gridView.minEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.gridView.centerGrid = NO;
    self.gridView.dataSource = self;
    self.gridView.actionDelegate = self;
    self.gridView.mainSuperView = self;
    self.gridView.bounces = NO;
    //self.gridView.pagingEnabled = YES;
    self.gridView.showsHorizontalScrollIndicator = NO;
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setTabModels:(NSArray *)tabModels
{
    _tabModels = tabModels;
    [self.gridView reloadData];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if(!self.firstLayout)
    {
        self.firstLayout = YES;
        [self.gridView reloadData];
    }
}

#pragma mark GMGridViewDataSource
- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return self.tabModels.count;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if(self.fitTab)
    {
        if(IS_IPAD)
        {
            return CGSizeMake(self.frame.size.width / self.tabModels.count, 70);
        }
        return CGSizeMake(self.frame.size.width / self.tabModels.count, 45);
        
    }
    else
    {
        if(IS_IPAD)
        {
            return CGSizeMake(200, 70);
        }
        return CGSizeMake(140, 45);
    }
    
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    CMUMISEDocTabViewCell *cell = (CMUMISEDocTabViewCell*)[gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[CMUMISEDocTabViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    
    CMUMISTabModel *model = [self.tabModels objectAtIndex:index];
    cell.lblTitle.text = model.title;
    cell.pointer.pointerColor = model.pointerColor;
    
    int number = model.number;
    
    cell.lblEDocCount.text = [CMUFormatUtils formatBadge: number];
    cell.lblEDocCount.hidden = number == 0;
    
    
    cell.selected = index == _selectedTab;
    [cell setNeedsLayout];
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO;
}

- (void)setSelectedTab:(int)selectedTab
{
    _selectedTab = selectedTab;
    [_gridView reloadData];//update selected;
    if (self.tabModels.count > 0)//scroll to selected thumbnail
    {
        CGSize gridCellSize = [self GMGridView:_gridView sizeForItemsInInterfaceOrientation:UIInterfaceOrientationPortrait];
        CGRect visibleRect = CGRectMake(0, 0, gridCellSize.width, gridCellSize.height);
        visibleRect.origin.x = _gridView.minEdgeInsets.left + _selectedTab*gridCellSize.width;
        [_gridView scrollRectToVisible:visibleRect animated:YES];
        [_gridView reloadData];//fixed last grid cell not repaint
    }
}

#pragma mark GMGridViewActionDelegate
- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    [self setSelectedTab:position];
    
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMisTabViewDidSelectTab:)])
    {
        [_delegate cmuMisTabViewDidSelectTab:_selectedTab];
    }
    
    
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    
}

@end

