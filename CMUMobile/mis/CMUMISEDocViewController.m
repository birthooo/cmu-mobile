//
//  CMUMISEDocViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/25/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEDocViewController.h"
#import "CMUWebServiceManager.h"
#import "UIColor+CMU.h"
#import "CMUMISEdocBaseProtocol.h"
#import "UIAlertView+Blocks.h"

@interface CMUMISEDocViewController ()
{
    
}

@property(nonatomic, unsafe_unretained) BOOL isEditMode;
@property(nonatomic, strong)UIViewController *currenViewController;
//tab
@property(nonatomic, weak)IBOutlet CMUMISTabView *tabView;

//top action
@property(nonatomic, weak)IBOutlet UISearchBar *searchbar;
@property(nonatomic, weak)IBOutlet UIButton *btnEdit;
@property(nonatomic, weak)IBOutlet UIView *cancelView;


@property(nonatomic, strong) NSArray *tabModels;
@property(nonatomic, weak) IBOutlet UIView *pageControllerContainer;
@property(nonatomic, strong) UIPageViewController *pageController;
@property(nonatomic, strong)IBOutlet UIView *actionView;
@property(nonatomic, strong) NSArray *viewControllers;


@property(nonatomic, strong)IBOutlet NSLayoutConstraint *actionHeight;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *actionViewLeading;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *actionViewTailing;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *actionViewBottom;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *actionViewTop;

- (IBAction)btnEditOrCancelClicked:(id)sender;
@end

@implementation CMUMISEDocViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        self.tabModels = [[NSArray alloc] initWithObjects:
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารเข้า" number:0  pointerColor:[UIColor cmuMISEdocLightPurpleColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารเผยแพร่" number:0 pointerColor:[UIColor cmuMISEdocLightPurpleColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารส่ง" number:0 pointerColor:[UIColor cmuMISEdocLightPurpleColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารติดตาม" number:0 pointerColor:[UIColor cmuMISEdocLightPurpleColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารในแฟ้ม" number:0 pointerColor:[UIColor whiteColor]],
                          nil];
        
        CMUMISEdocRecieveViewController *recieveVC = [[CMUMISEdocRecieveViewController alloc] initWithNibName:@"CMUMISEdocBaseRSFViewController" bundle:nil];
        CMUMISEdocPublicViewController *publicVC = [[CMUMISEdocPublicViewController alloc] initWithNibName:@"CMUMISEdocPublicViewController" bundle:nil];
        CMUMISEdocSendViewController *sendVC = [[CMUMISEdocSendViewController alloc] initWithNibName:@"CMUMISEdocBaseRSFViewController" bundle:nil];
        
        CMUMISEdocFollowViewController *followVC = [[CMUMISEdocFollowViewController alloc] initWithNibName:@"CMUMISEdocBaseRSFViewController" bundle:nil];
        
        CMUMISEdocFolderViewController *folderVC = [[CMUMISEdocFolderViewController alloc] init];
        
        recieveVC.delegate = self;
        publicVC.delegate = self;
        sendVC.delegate = self;
        followVC.delegate = self;
        folderVC.delegate = self;
        
        
        self.viewControllers = [[NSArray alloc] initWithObjects:
                                recieveVC,
                                publicVC,
                                sendVC,
                                followVC,
                                folderVC,
                                nil];

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //top action view
    [self.searchbar setBackgroundImage:[UIImage new]];
    self.searchbar.enablesReturnKeyAutomatically = NO;
    self.btnEdit.layer.cornerRadius = 5;
    self.btnEdit.layer.masksToBounds = YES;
    
    
    self.tabView.tabModels = self.tabModels;
    self.tabView.delegate = self;
    
    //load edoc numbers
    [self reloadEdocNumber];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(!self.pageController)
    {
        self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        self.pageController.dataSource = self;
        self.pageController.delegate = self;
        [[self.pageController view] setFrame:[self.pageControllerContainer bounds]];
        
        self.currenViewController = [self.viewControllers objectAtIndex:0];
        [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self addChildViewController:self.pageController];
        [[self pageControllerContainer] addSubview:[self.pageController view]];
        [self.pageController didMoveToParentViewController:self];
        
        [self updatePresentState];
       
        //load current tab
        [self reloadCurrentPageViewController];
    }
}

- (void) reloadEdocNumber
{
    [[CMUWebServiceManager sharedInstance] getEdocCountNumber:self success:^(id result) {
        CMUMISEdocCountNumber *model = (CMUMISEdocCountNumber *)result;
        ((CMUMISTabModel *)[self.tabModels objectAtIndex:0]).number = model.nRecieve;
        ((CMUMISTabModel *)[self.tabModels objectAtIndex:1]).number = model.nPublic;
        ((CMUMISTabModel *)[self.tabModels objectAtIndex:2]).number = model.nStatusSend;
        ((CMUMISTabModel *)[self.tabModels objectAtIndex:3]).number = model.nStatusFollow;
        
        self.tabView.tabModels = self.tabModels;
    } failure:^(NSError *error) {
        
    }];
}

- (void) reloadCurrentPageViewController
{
    id<CMUMISEdocBaseProtocol> edocBase = [self.pageController.viewControllers firstObject];
    NSString *searchText = self.searchbar.text;
    NSDictionary *reloadOptions = [NSDictionary dictionaryWithObjectsAndKeys:searchText,KEY_SEARCH_WORD,  nil];
    [self showHUDLoading];
    [edocBase reloadData:reloadOptions success:^(id result) {
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self dismisHUD];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    [super updatePresentState];
    NSInteger index = [self.viewControllers indexOfObject:[self.pageController.viewControllers firstObject]];
    
    self.tabView.selectedTab = (NSInteger)index;
    
    //hide/show top actionview
    BOOL showAction = index == 0 || index == 2 ||index == 3;
    if(showAction && ![self.actionView superview])
    {
        [self.view addSubview:self.actionView];
        [self.view addConstraint:self.actionHeight];
        [self.view addConstraint:self.actionViewLeading];
        [self.view addConstraint:self.actionViewTailing];
        [self.view addConstraint:self.actionViewBottom];
        [self.view addConstraint:self.actionViewTop];
        
    }
    else if(!showAction && [self.actionView superview])
    {
        [self.actionView removeFromSuperview];
    }
    [self.view needsUpdateConstraints];
    
    //top actionview visibility
    self.cancelView.hidden = !_isEditMode;
    [self.btnEdit setTitle:_isEditMode?@"CANCEL":@"EDIT" forState:UIControlStateNormal];
   
}

- (void) setIsEditMode:(BOOL)isEditMode
{
    _isEditMode = isEditMode;
    id<CMUMISEdocBaseProtocol> edocBase = [self.pageController.viewControllers firstObject];
    [edocBase setToEditMode:_isEditMode];
}

#pragma mark event
- (void) btnEditOrCancelClicked:(id)sender
{
    [self setIsEditMode:!_isEditMode];
    self.searchbar.text = nil;
    [self.view endEditing:YES];
    [self updatePresentState];
}

- (void)resetAllViewController
{
    //reset parent vc
    self.searchbar.text = nil;
    [self setIsEditMode:NO];
    [self.view endEditing:YES];
    [self updatePresentState];
    
    //reset cuurent page viewcontroller
    for(id<CMUMISEdocBaseProtocol> edocBase in self.viewControllers)
    {
        [edocBase reset];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark CMUMISTabViewDelegate
- (void)cmuMisTabViewDidSelectTab:(int) selectedTabIndex
{
    UIViewController *newViewController = [self.viewControllers objectAtIndex:selectedTabIndex];
    if(self.currenViewController != newViewController)
    {
        self.currenViewController = newViewController;
        [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self resetAllViewController];
        [self reloadCurrentPageViewController];
    }
    
}

//#pragma mark UIPageViewControllerDelegate
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self.viewControllers objectAtIndex: index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self.viewControllers indexOfObject:viewController];
    
    
    index++;
    
    if (index == self.viewControllers.count) {
        return nil;
    }
    
     return [self.viewControllers objectAtIndex: index];
    
}

#pragma mark UISearchbar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self reloadCurrentPageViewController];
}

//This code display Search Button if you have empty string .
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
//{
//    [self.searchbar setShowsCancelButton:YES animated:YES];
//    self.tblView.allowsSelection = NO;
//    self.tblView.scrollEnabled = NO;
//    
//    UITextField *searchBarTextField = nil;
//    for (UIView *subview in self.searchBar.subviews)
//    {
//        if ([subview isKindOfClass:[UITextField class]])
//        {
//            searchBarTextField = (UITextField *)subview;
//            break;
//        }
//    }
//    searchBarTextField.enablesReturnKeyAutomatically = NO;
//}

//-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
//    // The number of items reflected in the page indicator.
//    return 3;
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
//    // The selected item reflected in the page indicator.
//    return 0;
//}

#pragma mark UIPageViewControllerDelegate <NSObject>

// Sent when a gesture-initiated transition begins.
//- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers NS_AVAILABLE_IOS(6_0);

// Sent when a gesture-initiated transition ends. The 'finished' parameter indicates whether the animation finished, while the 'completed' parameter indicates whether the transition completed or bailed out (if the user let go early).
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    DebugLog(@"");
    
    UIViewController *newViewController = [self.pageController.viewControllers firstObject];
    if(self.currenViewController != newViewController)
    {
        self.currenViewController = newViewController;
        [self resetAllViewController];
        [self reloadCurrentPageViewController];
    }
    
    
}

// Delegate may specify a different spine location for after the interface orientation change. Only sent for transition style 'UIPageViewControllerTransitionStylePageCurl'.
// Delegate may set new view controllers or update double-sided state within this method's implementation as well.
//- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation;

//- (NSUInteger)pageViewControllerSupportedInterfaceOrientations:(UIPageViewController *)pageViewController NS_AVAILABLE_IOS(7_0);
//- (UIInterfaceOrientation)pageViewControllerPreferredInterfaceOrientationForPresentation:(UIPageViewController *)pageViewController NS_AVAILABLE_IOS(7_0);

#pragma mark CMUMISEdocBaseRSFViewControllerDelegate
- (void)cmuMISEdocBaseRSFViewControllerDidSelectedEdocFeed:(CMUMISEdocBaseRSFViewController *) vc selectedEdocFeed:(CMUMISEdocFeed *) edocFeed
{
    
    if(self.tabView.selectedTab == 0)
    {
        [[CMUWebServiceManager sharedInstance] edocRecieveTabEditOpen:self docSubID:edocFeed.docSubID success:^(id result) {
            DebugLog(@"");
        } failure:^(NSError *error) {
            DebugLog(@"");
        }];
    }
    
    CMUMISEdocDetailViewController *detailVC = [[CMUMISEdocDetailViewController alloc] init];
    detailVC.delegate = self;
    detailVC.docID = edocFeed.docID;
    detailVC.docSubID = edocFeed.docSubID;
    detailVC.edocCategory = [self getCurrentEdocCateory];
    [self.navigationController pushViewController:detailVC animated:YES];
    
}

- (CmuMisEdocCategory) getCurrentEdocCateory
{
    CmuMisEdocCategory cat = CmuMisEdocCategory_RECIEVE;
    switch (self.tabView.selectedTab) {
        case 0:
            cat = CmuMisEdocCategory_RECIEVE;
            break;
        case 1:
            cat = CmuMisEdocCategory_PUBLIC;
            break;
        case 2:
            cat = CmuMisEdocCategory_SEND;
            break;
        case 3:
            cat = CmuMisEdocCategory_FOLLOW;
            break;
        case 4:
            cat = CmuMisEdocCategory_FILE;
            break;
            
        default:
            break;
    }
    return cat;
}

- (void)cmuMISEdocBaseRSFViewControllerDidScrollEdocFeed:(CMUMISEdocBaseRSFViewController *) vc
{
    [self.view endEditing:YES];
}

- (void)cmuMISEdocBaseRSFViewController:(CMUMISEdocBaseRSFViewController *) vc didAction:(CmuMisEdocAction) action selectedEdocFeedList:(NSArray *) selectedEdocFeedList
{
    __weak CMUMISEDocViewController *weakSelf = self;
    __weak CMUMISEdocBaseRSFViewController *weakVC = vc;
    
    CmuMisEdocCategory docCategory = vc.getEdocCategoty;
    if(docCategory == CmuMisEdocCategory_RECIEVE && action == CmuMisEdocAction_DELETE)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"ยืนยันการลบ"
                                                       delegate:self
                                              cancelButtonTitle:@"CANCEL"
                                              otherButtonTitles:@"OK", nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                [weakSelf showHUDLoading];
                [[CMUWebServiceManager sharedInstance] edocRecieveTabEventDelete:weakSelf docFeedList:selectedEdocFeedList success:^(id result) {
                    DebugLog(@"");
                    //[weakSelf dismisHUD];
                    [weakSelf reloadCurrentPageViewController];
                    [weakVC setToEditMode:false];
                } failure:^(NSError *error) {
                    DebugLog(@"");
                    [weakSelf dismisHUD];
                    [weakSelf alert:@"ไม่สามารถลบข้อมูลได้"];
                }];
            }
        };
        [alert show];
    }
    else if(docCategory == CmuMisEdocCategory_SEND && action == CmuMisEdocAction_DELETE)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"ยืนยันการลบ"
                                                       delegate:self
                                              cancelButtonTitle:@"CANCEL"
                                              otherButtonTitles:@"OK", nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                [weakSelf showHUDLoading];
                [[CMUWebServiceManager sharedInstance] edocSendTabEventDelete:weakSelf docFeedList:selectedEdocFeedList success:^(id result) {
                    DebugLog(@"");
                    //[weakSelf dismisHUD];
                    [weakSelf reloadCurrentPageViewController];
                    [ weakVC setToEditMode:false];
                } failure:^(NSError *error) {
                    DebugLog(@"");
                    [weakSelf dismisHUD];
                    [weakSelf alert:@"ไม่สามารถลบข้อมูลได้"];
                }];
            }
        };
        [alert show];
    }
    else if(docCategory == CmuMisEdocCategory_SEND && action == CmuMisEdocAction_PICK_BACK)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"ยืนยันการดึงกลับ"
                                                       delegate:self
                                              cancelButtonTitle:@"CANCEL"
                                              otherButtonTitles:@"OK", nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                [weakSelf showHUDLoading];
                [[CMUWebServiceManager sharedInstance] edocSendTabEventCallback:weakSelf docFeedList:selectedEdocFeedList success:^(id result) {
                    DebugLog(@"");
                    //[weakSelf dismisHUD];
                    [weakSelf reloadCurrentPageViewController];
                    [ weakVC setToEditMode:false];
                } failure:^(NSError *error) {
                    DebugLog(@"");
                    [weakSelf dismisHUD];
                    [weakSelf alert:@"ไม่สามารถดึงกลับได้"];
                }];
            }
        };
        [alert show];
    }
    else if(docCategory == CmuMisEdocCategory_FOLLOW && action == CmuMisEdocAction_UNFOLLOW)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"ยืนยันการยกเลิกเอกสาร"
                                                       delegate:self
                                              cancelButtonTitle:@"CANCEL"
                                              otherButtonTitles:@"OK", nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                [weakSelf showHUDLoading];
                [[CMUWebServiceManager sharedInstance] edocFollowTabEventUnFollow:weakSelf docFeedList:selectedEdocFeedList success:^(id result) {
                    DebugLog(@"");
                    //[weakSelf dismisHUD];
                    [weakSelf reloadCurrentPageViewController];
                    [ weakVC setToEditMode:false];
                } failure:^(NSError *error) {
                    DebugLog(@"");
                    [weakSelf alert:@"ไม่สามารถการยกเลิกเอกสาร"];
                    [weakSelf dismisHUD];
                }];
            }
        };
        [alert show];
    }
}

- (void)cmuMISEdocPublicViewControllerDidChangeMonthYear:(CMUMISEdocBaseRSFViewController *) vc
{
    [self reloadCurrentPageViewController];
}

#pragma mark CMUMISEdocFolderControllerDelegate
-(void)cmuMISEdocFolderController:(CMUMISEdocFolderViewController*) vc didSelected:(CMUMISEdocFolderFeed *) edocFolderFeed
{
    CMUMISEdocFolderSublistViewController *edocFolderSublistViewController = [[CMUMISEdocFolderSublistViewController alloc] init];
    edocFolderSublistViewController.delegate = self;
    edocFolderSublistViewController.edocFolderFeed = edocFolderFeed;
    [self.navigationController pushViewController:edocFolderSublistViewController animated:YES];
}

- (void)cmuMISEdocFolderViewControllerDidScrollEdocFeed:(CMUMISEdocFolderViewController *) vc
{
    [self.view endEditing:YES];
}


#pragma mark CMUMISEdocDetailViewControllerDelegate
- (void)cmuMisEdocDetailViewControllerBackClicked:(CMUMISEdocDetailViewController *) vc
{
    [self.navigationController popToViewController:self animated:YES];
    [self reloadCurrentPageViewController];
}

#pragma mark CMUMISEdocFolderSublistViewControllerDelegate
- (void)cmuMISEdocFolderSublistViewControllerBackClicked:(CMUMISEdocFolderSublistViewController *) vc
{
    [self.navigationController popToViewController:self animated:YES];
    [self reloadCurrentPageViewController];
}
@end
