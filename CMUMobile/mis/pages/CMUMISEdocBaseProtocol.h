//
//  CMUMISEdocBaseProtocol.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/16/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#define KEY_SEARCH_WORD @"KEY_SEARCH_WORD"
#import "CMUWebServiceManager.h"
#import  "CMUMISUIEnum.h"

@protocol CMUMISEdocBaseProtocol <NSObject>
- (CmuMisEdocCategory)getEdocCategoty;
- (void)reset;
- (void)setToEditMode:(BOOL) isEditMode;
- (void)reloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
@end
