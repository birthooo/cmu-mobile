//
//  CMUEdocFileTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/1/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMISModel.h"

@interface CMUEdocFolderTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUMISEdocFolderFeed *edocFolderFeed;
@end
