//
//  CMUMISEdocFileViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocFolderViewController.h"
#import "CMUEdocFolderTableViewCell.h"
#import "CMUMISEdocFolderSublistViewController.h"

static NSString *cellIdentifier = @"CMUEdocFolderTableViewCell";

@interface CMUMISEdocFolderViewController (){
    BOOL _isEditMode;
}
@property(nonatomic, strong)NSArray *feedList;
@property(nonatomic, weak)IBOutlet UITableView *tableView;
@end

@implementation CMUMISEdocFolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUEdocFolderTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark CMUMISEdocBaseProtocol
- (void)reset
{
    
}

- (void)setToEditMode:(BOOL) isEditMode
{
    _isEditMode = isEditMode;
    //[self.tableView reloadData];
}

- (void)reloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceManager sharedInstance] getEdocFolderListTab:self  success:^(id result) {
        CMUMISEdocFolderFeedModel *docFolderFeedModel = (CMUMISEdocFolderFeedModel *)result;
        self.feedList = docFolderFeedModel.edocFolderFeedList;
        [self.tableView reloadData];        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if(IS_IPHONE)
    {
        height = 80;
    }
    else
    {
        height = 120;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFolderFeed *edocFolderFeed = [self.feedList objectAtIndex:indexPath.row];
    
    CMUEdocFolderTableViewCell *cell = (CMUEdocFolderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.edocFolderFeed = edocFolderFeed;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocFolderController:didSelected:)])
    {
        [_delegate cmuMISEdocFolderController:self didSelected:[self.feedList objectAtIndex:indexPath.row]];
    }
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocFolderViewControllerDidScrollEdocFeed:)])
    {
        [_delegate cmuMISEdocFolderViewControllerDidScrollEdocFeed:self ];
    }
}

@end
