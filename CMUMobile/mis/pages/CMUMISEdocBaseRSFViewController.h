//
//  CMUMISEdocBaseViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/16/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "CMUMISEdocBaseProtocol.h"
#import "CMUMISModel.h"

@class CMUMISEdocBaseRSFViewController;
@protocol CMUMISEdocBaseRSFViewControllerDelegate <NSObject>
- (void)cmuMISEdocBaseRSFViewControllerDidSelectedEdocFeed:(CMUMISEdocBaseRSFViewController *) vc selectedEdocFeed:(CMUMISEdocFeed *) edocFeed;

- (void)cmuMISEdocBaseRSFViewControllerDidScrollEdocFeed:(CMUMISEdocBaseRSFViewController *) vc;

- (void)cmuMISEdocBaseRSFViewController:(CMUMISEdocBaseRSFViewController *) vc didAction:(CmuMisEdocAction) action selectedEdocFeedList:(NSArray *) selectedEdocFeedList;;

//for public doc , using same delegate
- (void)cmuMISEdocPublicViewControllerDidChangeMonthYear:(CMUMISEdocBaseRSFViewController *) vc;
@end

@interface CMUMISEdocBaseRSFViewController : CMUViewController<CMUMISEdocBaseProtocol>
@property(nonatomic, weak)id<CMUMISEdocBaseRSFViewControllerDelegate> delegate;
@property(nonatomic, strong)NSArray *feedList;
@property(nonatomic, weak)IBOutlet UITableView *tableView;

// implement CMUMISEdocBaseProtocol
- (CmuMisEdocCategory)getEdocCategoty;
- (void)reset;
- (void)setToEditMode:(BOOL) isEditMode;
- (void)reloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

//overide in subclass
- (void)baseViewReloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;


@end
