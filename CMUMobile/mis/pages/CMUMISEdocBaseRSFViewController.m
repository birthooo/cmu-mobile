//
//  CMUMISEdocBaseViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/16/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocBaseRSFViewController.h"
#import "CMUMISEdocTableViewCell.h"

static NSString *cellIdentifierIPhone = @"CMUMISEdocTableViewCell";

@interface CMUMISEdocBaseRSFViewController (){
    BOOL _isEditMode;
    CGFloat _originalBottomActionViewHeight;
}
//bottom ActionView
@property(nonatomic, weak)IBOutlet UIView *bottomActionView;
@property(nonatomic, weak)IBOutlet UIView *recieveActionView;
- (IBAction)recieveTabDeleteClicked:(id)sender;
@property(nonatomic, weak)IBOutlet UIView *sendActionView;
- (IBAction)sendTabDeleteClicked:(id)sender;
- (IBAction)sendTabPickbackClicked:(id)sender;
@property(nonatomic, weak)IBOutlet UIView *followActionView;
- (IBAction)followCancelDocClicked:(id)sender;



@property(nonatomic, strong)IBOutlet NSLayoutConstraint *bottomActionViewHeighConstraint;


@end

@implementation CMUMISEdocBaseRSFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //tableview
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUMISEdocTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPhone];
    
    //bottomActionView
    _originalBottomActionViewHeight = self.bottomActionView.frame.size.height;
    
    //hidekeyboard
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                   initWithTarget:self
//                                   action:@selector(dismissKeyboard)];
//    
//    [self.view addGestureRecognizer:tap];
    
    
    [self updatePresentState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)dismissKeyboard {
//    [self.view endEditing:YES];
//}

- (void)updatePresentState
{
    [super updatePresentState];
    //bottom action view
    BOOL shouldShowBottomAction = _isEditMode && [[self getSelectedEdocFeeds] count] > 0;// TODO check select cell
    self.bottomActionViewHeighConstraint.constant = shouldShowBottomAction? _originalBottomActionViewHeight : 0;
    
    CmuMisEdocCategory docCategory = [self getEdocCategoty];
    
    self.recieveActionView.hidden = docCategory != CmuMisEdocCategory_RECIEVE;
    self.sendActionView.hidden = docCategory != CmuMisEdocCategory_SEND;
    self.followActionView.hidden = docCategory != CmuMisEdocCategory_FOLLOW;
    [self.view setNeedsLayout];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSArray *)getSelectedDocs
{
    NSMutableArray *selectedList = [[NSMutableArray alloc] init];
    for(CMUMISEdocFeed *edoc in self.feedList)
    {
        if(edoc.uiSelected)
        {
            [selectedList addObject:edoc];
        }
    }
    return selectedList;
}

- (void)recieveTabDeleteClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewController:didAction:selectedEdocFeedList:)])
    {
        [_delegate cmuMISEdocBaseRSFViewController:self didAction:CmuMisEdocAction_DELETE selectedEdocFeedList:[self getSelectedDocs]];
    }
}

- (void)sendTabDeleteClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewController:didAction:selectedEdocFeedList:)])
    {
        [_delegate cmuMISEdocBaseRSFViewController:self didAction:CmuMisEdocAction_DELETE selectedEdocFeedList:[self getSelectedDocs]];
    }

}

- (void)sendTabPickbackClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewController:didAction:selectedEdocFeedList:)])
    {
        [_delegate cmuMISEdocBaseRSFViewController:self didAction:CmuMisEdocAction_PICK_BACK selectedEdocFeedList:[self getSelectedDocs]];
    }

}

- (void)followCancelDocClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewController:didAction:selectedEdocFeedList:)])
    {
        [_delegate cmuMISEdocBaseRSFViewController:self didAction:CmuMisEdocAction_UNFOLLOW selectedEdocFeedList:[self getSelectedDocs]];
    }

}



#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if(IS_IPHONE)
    {
        height = 95;
    }
    else
    {
        height = 120;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFeed *edocFeed = [self.feedList objectAtIndex:indexPath.row];
    
    CMUMISEdocTableViewCell *cell = (CMUMISEdocTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPhone forIndexPath:indexPath];
    
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.editMode = _isEditMode;
    cell.edocFeed = edocFeed;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFeed *edocFeed = [self.feedList objectAtIndex:indexPath.row];
    if(_isEditMode)
    {
        edocFeed.uiSelected = !edocFeed.uiSelected;
        [tableView reloadRowsAtIndexPaths:@[indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
        [self updatePresentState];
    }
    else
    {
        if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewControllerDidSelectedEdocFeed:selectedEdocFeed:)])
        {
            [_delegate cmuMISEdocBaseRSFViewControllerDidSelectedEdocFeed:self selectedEdocFeed:edocFeed];
        }
    }
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocBaseRSFViewControllerDidScrollEdocFeed:)])
    {
        [_delegate cmuMISEdocBaseRSFViewControllerDidScrollEdocFeed:self ];
    }
}

#pragma mark private
- (void)unselectAllEdocFeeds
{
    for(CMUMISEdocFeed *edocFeed in self.feedList)
    {
        edocFeed.uiSelected = NO;
    }
}

- (NSMutableArray *)getSelectedEdocFeeds
{
    NSMutableArray *selectedEdocFeeds = [[NSMutableArray alloc] init];
    for(CMUMISEdocFeed *edocFeed in self.feedList)
    {
       if(edocFeed.uiSelected)
       {
           [selectedEdocFeeds addObject:edocFeed];
       }
    }
    return selectedEdocFeeds;
}

#pragma mark CMUMISEdocBaseProtocol
- (void)reset
{
    _isEditMode = NO;
    [self unselectAllEdocFeeds];
    [self.tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self updatePresentState];
}

- (void)setToEditMode:(BOOL) isEditMode
{
    _isEditMode = isEditMode;
    [self unselectAllEdocFeeds];
    [self.tableView reloadData];
    [self updatePresentState];
}

- (void)reloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [self baseViewReloadData:params success:^(id result) {
        CMUMISEdocFeedModel *docFeedModel = (CMUMISEdocFeedModel *)result;
        self.feedList = docFeedModel.edocFeedList;
        [self.tableView reloadData];
        success(docFeedModel);
    } failure:^(NSError *error) {
        failure(error);
    }];
    
}

@end
