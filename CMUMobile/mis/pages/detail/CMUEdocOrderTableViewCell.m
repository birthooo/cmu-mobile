//
//  CMUEdocOrderTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUEdocOrderTableViewCell.h"
@interface CMUEdocOrderTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel *lblOrderNo;
@property(nonatomic, weak)IBOutlet UILabel *lblFrom;
@property(nonatomic, weak)IBOutlet UILabel *lblTo;
@property(nonatomic, weak)IBOutlet UILabel *lblObjective;
@property(nonatomic, weak)IBOutlet UILabel *lblText;
@end

@implementation CMUEdocOrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setOrderFeed:(CMUMISEdocOrderFeed *)orderFeed
{
    _orderFeed = orderFeed;
    self.lblOrderNo.text = [NSString stringWithFormat:@"%i", self.orderFeed.order];
    self.lblFrom.attributedText = [[NSAttributedString alloc] initWithData:[orderFeed.htmlFrom dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lblTo.attributedText = [[NSAttributedString alloc] initWithData:[orderFeed.htmlTo dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lblObjective.attributedText = [[NSAttributedString alloc] initWithData:[orderFeed.htmlObjective dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.lblText.attributedText = [[NSAttributedString alloc] initWithData:[orderFeed.htmlText dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblOrderNo.text = nil;
    self.lblFrom.attributedText = nil;
    self.lblTo.attributedText = nil;
    self.lblObjective.attributedText = nil;
    self.lblText.attributedText = nil;
}
@end
