//
//  CMUMISEdocFolderSelectionViewTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/8/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMISModel.h"

@interface CMUMISEdocFolderSelectionViewTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUMISEdocFolderFeed *edocFolderFeed;
@end
