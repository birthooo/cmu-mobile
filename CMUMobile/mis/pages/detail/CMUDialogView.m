//
//  CMUDialogView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUDialogView.h"
#import "CMUStringUtils.h"
@interface CMUDialogView()
@property(nonatomic, weak)IBOutlet UIView *containerView;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)replyClicked:(id)sender;
@end
@implementation CMUDialogView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)customInit
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUDialogView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    
    contentView.frame = self.bounds;
    
    self.backgroundColor = [UIColor clearColor];
    self.containerView.layer.cornerRadius = 10.0f;
    self.containerView.layer.masksToBounds = YES;
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


- (void)cancelClicked:(id)sender
{
    if(_delegate &&[ _delegate respondsToSelector:@selector(cmuDialogViewDidCancel)])
    {
        [_delegate cmuDialogViewDidCancel];
    }
}

- (void)replyClicked:(id)sender
{
    NSString *comment = self.textView.text;
    if(_delegate &&[ _delegate respondsToSelector:@selector(cmuDialogView:didOK: )])
    {
        [_delegate cmuDialogView:self didOK:comment];
    }
    
}

@end
