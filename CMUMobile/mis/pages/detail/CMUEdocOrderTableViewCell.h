//
//  CMUEdocOrderTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMISModel.h"

@interface CMUEdocOrderTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUMISEdocOrderFeed *orderFeed;
@end
