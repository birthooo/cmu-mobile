//
//  CMUMISEdocFolderSelectionView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/8/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocFolderSelectionView.h"
#import "CMUMISEdocFolderSelectionViewTableViewCell.h"
#import "global.h"

static NSString *cellIdentifier = @"CMUMISEdocFolderSelectionViewTableViewCell";

@interface CMUMISEdocFolderSelectionView()
@property(nonatomic, weak)IBOutlet UITableView *tableView;
@property(nonatomic, strong)IBOutlet UIView *tableViewFooter;
- (IBAction)btnCancelClicked:(id)sender;
@end

@implementation CMUMISEdocFolderSelectionView

- (void)customInit
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMISEdocFolderSelectionView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    
    contentView.frame = self.bounds;
    
    self.backgroundColor = [UIColor clearColor];
    self.tableView.layer.cornerRadius = 10.0f;
    self.tableView.layer.masksToBounds = YES;
    
    self.tableView.bounces = NO;
    self.tableViewFooter.backgroundColor = [UIColor colorWithRed:137.0/255 green:117.0/255 blue:206.0/255 alpha:1.0];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUMISEdocFolderSelectionViewTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setFeedList:(NSArray *)feedList
{
    _feedList = feedList;
    [self.tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

- (void)btnCancelClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMisFolderSelectionViewDidCancel)])
    {
        [_delegate cmuMisFolderSelectionViewDidCancel];
    }
}

#pragma mark section table view

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 90;
    }
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return self.tableViewFooter;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if(IS_IPHONE)
    {
        height = 80;
    }
    else
    {
        height = 90;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFolderFeed *edocFolderFeed = [self.feedList objectAtIndex:indexPath.row];
    
    CMUMISEdocFolderSelectionViewTableViewCell *cell = (CMUMISEdocFolderSelectionViewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.edocFolderFeed = edocFolderFeed;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMisFolderSelectionView:didSelected:)])
    {
        [_delegate cmuMisFolderSelectionView:self didSelected:[self.feedList objectAtIndex:indexPath.row]];
    }
}



@end

