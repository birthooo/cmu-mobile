//
//  CMUMISEdocDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUMISModel.h"
#import "CMUMISTabView.h"
#import "CMUMISEdoctDetailTableViewCell.h"
#import "CMUMISEdoctDetailAttachTableViewCell.h"
#import "CMUMISUIEnum.h"
#import "CMUMISEdocFolderSelectionView.h"
#import "CMUEdocOrderTableViewCell.h"
#import "CMUDialogView.h"

@class CMUMISEdocDetailViewController;
@protocol CMUMISEdocDetailViewControllerDelegate <NSObject>
- (void)cmuMisEdocDetailViewControllerBackClicked:(CMUMISEdocDetailViewController *) vc;
@end

@interface CMUMISEdocDetailViewController : CMUNavBarViewController<CMUMISTabViewDelegate, UIDocumentInteractionControllerDelegate, CMUMISEdocFolderSelectionViewDelegate, CMUDialogViewDelegate>
@property(nonatomic, weak) id<CMUMISEdocDetailViewControllerDelegate> delegate;
@property(nonatomic, unsafe_unretained) int docID;
@property(nonatomic, strong) NSString *docSubID;
@property(nonatomic, unsafe_unretained)CmuMisEdocCategory edocCategory;
@end
