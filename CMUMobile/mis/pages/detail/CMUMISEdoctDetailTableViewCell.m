//
//  CMUMISEdoctDetailAttatchTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdoctDetailTableViewCell.h"

@implementation CMUMISEdoctDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblText.text = nil;
}
@end
