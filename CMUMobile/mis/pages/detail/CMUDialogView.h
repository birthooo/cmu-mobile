//
//  CMUDialogView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMUDialogView;
@protocol CMUDialogViewDelegate <NSObject>
@optional
- (void)cmuDialogView:(CMUDialogView *)view didOK:(NSString *) text;
- (void)cmuDialogViewDidCancel;
@end

@interface CMUDialogView : UIView
@property(nonatomic, weak)IBOutlet UITextView *textView;
@property(nonatomic, weak)IBOutlet UIButton *btnOK;
@property(nonatomic, weak)id<CMUDialogViewDelegate> delegate;
@end
