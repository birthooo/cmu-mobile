//
//  CMUMISEdocFolderSelectionView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/8/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMISModel.h"

@class CMUMISEdocFolderSelectionView;
@protocol CMUMISEdocFolderSelectionViewDelegate <NSObject>
@optional
- (void) cmuMisFolderSelectionView:(CMUMISEdocFolderSelectionView *)vc didSelected:(CMUMISEdocFolderFeed *) edocFolderFeed;
- (void) cmuMisFolderSelectionViewDidCancel;
@end

@interface CMUMISEdocFolderSelectionView : UIView
@property(nonatomic, unsafe_unretained)id <UITableViewDataSource, UITableViewDelegate, CMUMISEdocFolderSelectionViewDelegate> delegate;
@property(nonatomic, strong)NSArray *feedList;
@end
