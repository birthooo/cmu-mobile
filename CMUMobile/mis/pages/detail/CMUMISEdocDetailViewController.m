//
//  CMUMISEdocDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocDetailViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUStringUtils.h"
#import "global.h"
#import "CMUUIUtils.h"
#import "CMUMISEdocFolderSelectionView.h"
#import <AFNetworking/AFNetworking.h>
#import "UIAlertView+Blocks.h"

static NSString *detailTableViewCell = @"CMUMISEdoctDetailTableViewCell";
static NSString *attachTableViewCell = @"CMUMISEdoctDetailAttachTableViewCell";
static NSString *orderTableViewCell = @"CMUEdocOrderTableViewCell";

@interface CMUMISEdocDetailViewController ()
@property(nonatomic, weak)IBOutlet UILabel *lblTitle;

@property(nonatomic, weak)IBOutlet UILabel *lblTypeDocumentName;
@property(nonatomic, weak)IBOutlet UILabel *lblPriorityLevel;
@property(nonatomic, weak)IBOutlet UILabel *lblSecurityLevel;

@property(nonatomic, weak)IBOutlet UILabel *lblTitleHead;

//tab
@property(nonatomic, strong) NSArray *tabModels;
@property(nonatomic, weak)IBOutlet CMUMISTabView *tabView;
@property(nonatomic, weak)IBOutlet UIView *tabDetailView;
@property(nonatomic, weak)IBOutlet UIView *tabAttachmentView;
@property(nonatomic, weak)IBOutlet UIView *tabOrderView;

//tab1
@property(nonatomic, weak)IBOutlet UILabel *lblSendDate;
@property(nonatomic, weak)IBOutlet UILabel *lblCreateReceiveDate;
@property(nonatomic, weak)IBOutlet UITableView *detailTableView;
@property(nonatomic, strong) NSArray *topicList;
@property(nonatomic, strong) NSArray *detailList;
//tab2
@property(nonatomic, weak)IBOutlet UITableView *attatchTableView;
@property(nonatomic, strong) NSArray *attachList;

//tab3
@property(nonatomic, weak)IBOutlet UITableView *orderTableView;
@property(nonatomic, strong) NSArray *orderList;

//bottomAction
@property(nonatomic, weak)IBOutlet UIView *bottomActionView;
@property(nonatomic, weak)IBOutlet UIView *recieveActionView;
@property(nonatomic, weak)IBOutlet UIButton *btnAccept;
- (IBAction)recieveTabAcceptClicked:(id)sender;
- (IBAction)recieveTabSaveReplyClicked:(id)sender;
- (IBAction)recieveTabSaveToFolderClicked:(id)sender;
@property(nonatomic, weak)IBOutlet UIView *sendActionView;
- (IBAction)sendTabSaveToFolderClicked:(id)sender;
@property(nonatomic, weak)IBOutlet UIView *folderActionView;
- (IBAction)folderTabExitFolderClicked:(id)sender;
- (IBAction)folderTabChangeFolderClicked:(id)sender;

//folder selection
@property(nonatomic, weak)IBOutlet CMUMISEdocFolderSelectionView *folderSelectionView;

//reply view
@property(nonatomic, weak)IBOutlet CMUDialogView *replyView;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *replyViewBottom;

@property(nonatomic, strong)CMUMISEdocDetail *edocDetail;

@property(nonatomic, strong)UIDocumentInteractionController * documentInteractionController;

@property(nonatomic, strong)NSMutableDictionary *cachedHeight;

- (IBAction)backClicked:(id) sender;
@end

@implementation CMUMISEdocDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        self.tabModels = [[NSArray alloc] initWithObjects:
                          [[CMUMISTabModel alloc] initWithTitle:@"รายละเอียด" number:0  pointerColor:[UIColor whiteColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"เอกสารแนบ" number:0 pointerColor:[UIColor whiteColor]],
                          [[CMUMISTabModel alloc] initWithTitle:@"ลำดับการรับ-ส่ง" number:0 pointerColor:[UIColor whiteColor]],
                          nil];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lblTypeDocumentName.layer.cornerRadius = 5;
    self.lblTypeDocumentName.layer.masksToBounds = YES;
    self.lblPriorityLevel.layer.cornerRadius = 5;
    self.lblPriorityLevel.layer.masksToBounds = YES;
    self.lblSecurityLevel.layer.cornerRadius = 5;
    self.lblSecurityLevel.layer.masksToBounds = YES;
    
    self.tabView.fitTab = YES;
    self.tabView.tabModels = self.tabModels;
    self.tabView.delegate = self;
    
    //tableview
    [self.detailTableView registerNib:[UINib nibWithNibName:@"CMUMISEdoctDetailTableViewCell" bundle:nil] forCellReuseIdentifier:detailTableViewCell];
    [self.attatchTableView registerNib:[UINib nibWithNibName:@"CMUMISEdoctDetailAttachTableViewCell" bundle:nil] forCellReuseIdentifier:attachTableViewCell];
    [self.orderTableView registerNib:[UINib nibWithNibName:@"CMUEdocOrderTableViewCell" bundle:nil] forCellReuseIdentifier:orderTableViewCell];
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getEdocDetail:self docId:self.docID  docSubID:self.docSubID success:^(id result) {
        [self setEdocDetail:(CMUMISEdocDetail *)result];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self dismisHUD];
    }];
    
    BOOL showBottomAction = self.edocCategory == CmuMisEdocCategory_RECIEVE ||
                            self.edocCategory == CmuMisEdocCategory_SEND ||
                            self.edocCategory == CmuMisEdocCategory_FILE;
    if(!showBottomAction)
    {
        [self.bottomActionView removeFromSuperview];
    }
    
    [self setEdocDetail:nil]; //just reset view
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
 
}

- (void)updatePresentState
{
    [super updatePresentState];
    int index = (int)self.tabView.selectedTab;
    
    //tab
    self.tabDetailView.hidden = index != 0;
    self.tabAttachmentView.hidden = index != 1;
    self.tabOrderView.hidden = index != 2;
    
    //action
    
    self.recieveActionView.hidden = self.edocCategory != CmuMisEdocCategory_RECIEVE;
    self.sendActionView.hidden = self.edocCategory != CmuMisEdocCategory_SEND;
    self.folderActionView.hidden = self.edocCategory != CmuMisEdocCategory_FILE;
    
    [self.btnAccept setImage: self.edocDetail.accept?[UIImage imageNamed:@"mis_check.png"]:nil forState:UIControlStateNormal];
    [self.btnAccept setBackgroundColor:self.edocDetail.accept?[UIColor colorWithRed:123/255.0f green:170/255.0f blue:200/255.0f alpha:1.0]:[UIColor clearColor]];
    
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    self.replyViewBottom.constant = 0;
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.replyViewBottom.constant = keyboardSize.height;
}

- (void)setEdocDetail:(CMUMISEdocDetail *)edocDetail
{
    self.cachedHeight = [[NSMutableDictionary alloc] init];
    _edocDetail = edocDetail;
    
    self.lblTitle.text = self.edocDetail.lblDocumentCode;
    
    //doc type
    self.lblTypeDocumentName.text =  [NSString stringWithFormat:@" %@ ",  [CMUStringUtils isEmpty:self.edocDetail.lblTypeDocumentName]?@"": self.edocDetail.lblTypeDocumentName];
    self.lblTypeDocumentName.hidden = [CMUStringUtils isEmpty:self.edocDetail.lblTypeDocumentName];
    self.lblPriorityLevel.text = [NSString stringWithFormat:@" %@ ",  [CMUStringUtils isEmpty:self.edocDetail.lblPriorityLevel]?@"": self.edocDetail.lblPriorityLevel];
    self.lblPriorityLevel.hidden = [CMUStringUtils isEmpty:self.edocDetail.lblPriorityLevel];
    self.lblSecurityLevel.text = [NSString stringWithFormat:@" %@ ",  [CMUStringUtils isEmpty:self.edocDetail.lblSecurityLevel]?@"": self.edocDetail.lblSecurityLevel];
    self.lblSecurityLevel.hidden = [CMUStringUtils isEmpty:self.edocDetail.lblSecurityLevel];

    self.lblTitleHead.text = self.edocDetail.lblTitleHead;
    if([CMUStringUtils isEmpty:self.edocDetail.lblTitleHead])
    {
        self.lblTitleHead.text = @" \n ";//just fix layout blink by default 2 lines text
    }
    
    NSString *lblSendDate = [CMUStringUtils isEmpty:self.edocDetail.lblSendDate]?@"":self.edocDetail.lblSendDate;
    NSString *lblCreateReceiveDate = [CMUStringUtils isEmpty:self.edocDetail.lblCreateReceiveDate]?@"":self.edocDetail.lblCreateReceiveDate;
    self.lblSendDate.text = [NSString stringWithFormat:@"วันที่ส่ง : %@",lblSendDate];
    self.lblCreateReceiveDate.text = [NSString stringWithFormat:@"วันที่รับ : %@",lblCreateReceiveDate];
    
    self.topicList = JSON_GET_OBJECT([self.edocDetail.detailModel objectForKey:@"topicName"]);
    self.detailList =  JSON_GET_OBJECT([self.edocDetail.detailModel objectForKey:@"listdata"]);
    self.attachList =  JSON_GET_OBJECT([self.edocDetail.fileModel objectForKey:@"fileList"]);
    self.orderList =   self.edocDetail.orderModel.edocTabOderDetailList;
    
    [self.detailTableView reloadData];
    [self.attatchTableView reloadData];
    [self.orderTableView reloadData];
    [self updatePresentState];
}

- (void)backClicked:(id) sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMisEdocDetailViewControllerBackClicked:)])
    {
        [_delegate cmuMisEdocDetailViewControllerBackClicked:self];
    }
}

- (void)recieveTabAcceptClicked:(id)sender
{
    if(self.edocDetail.accept){
        return;
    }
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] edocAccept:self  docId:self.docID docSubID:self.edocDetail.docSubID success:^(id result) {
        [weakSelf dismisHUD];
        weakSelf.edocDetail.accept = YES;
        [weakSelf updatePresentState];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [weakSelf dismisHUD];
        [weakSelf alert:CMU_ERROR_SAVE_DATA];
    }];
    
}
- (void)recieveTabSaveReplyClicked:(id)sender
{
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    weakSelf.replyView.delegate = self;
    weakSelf.replyView.textView.text = @"";
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         weakSelf.replyView.alpha = 1.0f;
     }completion:^(BOOL finished){
         [weakSelf.replyView.textView becomeFirstResponder];
     }];
}

- (void)recieveTabSaveToFolderClicked:(id)sender
{
    [self doSelectEdocFolder];
}

- (void)sendTabSaveToFolderClicked:(id)sender
{
    [self doSelectEdocFolder];
}

- (void)folderTabChangeFolderClicked:(id)sender
{
    [self doSelectEdocFolder];
}

- (void)folderTabExitFolderClicked:(id)sender
{
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:@"ยืนยันการออกจากแฟ้ม"
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [weakSelf showHUDLoading];
            [[CMUWebServiceManager sharedInstance] edocCancelFolder:self  docSubID:weakSelf.edocDetail.docSubID success:^(id result) {
                [self dismisHUD];
                [UIView animateWithDuration:0.3 animations:^(void)
                 {
                     weakSelf.folderSelectionView.alpha = 0.0f;
                 }completion:^(BOOL finished){
                     
                 }];            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
                [self dismisHUD];
                [weakSelf alert:@"ไม่สามารถนำออกจากแฟ้มได้"];
            }];
        }
    };
    [alert show];
}

- (void)doSelectEdocFolder
{
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getEdocFolderListTab:self  success:^(id result) {
        [self dismisHUD];
        CMUMISEdocFolderFeedModel *docFolderFeedModel = (CMUMISEdocFolderFeedModel *)result;
        NSArray *feedList = docFolderFeedModel.edocFolderFeedList;
        weakSelf.folderSelectionView.feedList = feedList;
        weakSelf.folderSelectionView.delegate = weakSelf;
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             weakSelf.folderSelectionView.alpha = 1.0f;
         }completion:^(BOOL finished){
             
         }];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
       [self dismisHUD];
    }];
}

#pragma mark -CMUMISEdocFolderSelectionViewDelegate
- (void) cmuMisFolderSelectionView:(CMUMISEdocFolderSelectionView *)vc didSelected:(CMUMISEdocFolderFeed *) edocFolderFeed
{
    NSString *message = self.edocCategory == CmuMisEdocCategory_FILE?@"ยืนยันการย้ายเข้าแฟ้ม":@"ยืนยันการเก็บเข้าแฟ้ม";
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [weakSelf showHUDLoading];
            [[CMUWebServiceManager sharedInstance] edocAddFolder:self  docSubID:weakSelf.edocDetail.docSubID folderID:edocFolderFeed.folderID   success:^(id result) {
                [self dismisHUD];
                [UIView animateWithDuration:0.3 animations:^(void)
                 {
                     weakSelf.folderSelectionView.alpha = 0.0f;
                 }completion:^(BOOL finished){
                     
                 }];
            } failure:^(NSError *error) {
                DebugLog(@"%@", error);
                [self dismisHUD];
                [weakSelf alert:@"ไม่สามารถเก็บข้อมูลเข้าแฟ้มได้"];
            }];
        }
    };
    [alert show];
    
}

- (void) cmuMisFolderSelectionViewDidCancel
{
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         self.folderSelectionView.alpha = 0.0f;
     }completion:^(BOOL finished){
         
     }];
}

#pragma mark -CMUMISEdocReplyViewDelegate
- (void)cmuDialogView:(CMUDialogView *)view didOK:(NSString *) text
{
    if([CMUStringUtils isEmpty:text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"กรุณากรอกข้อความตอบกลับ"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                
            } else if (buttonIndex == alertView.cancelButtonIndex) {
                
            }
        };
        [alert show];
        
        return;
    }
    [self.view endEditing:YES];
    
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] edocReply:self  docId:self.docID docSubID:self.edocDetail.docSubID comment:text success:^(id result) {
        [weakSelf dismisHUD];
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             weakSelf.replyView.alpha = 0.0f;
         }completion:^(BOOL finished){
             
         }];
        self.tabView.selectedTab = 2;
        [self setEdocDetail:result];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [weakSelf dismisHUD];
        [weakSelf alert:CMU_ERROR_SAVE_DATA];
    }];
}

- (void)cmuDialogViewDidCancel
{
    [self.view endEditing:YES];
    __weak CMUMISEdocDetailViewController *weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         weakSelf.replyView.alpha = 0.0f;
     }completion:^(BOOL finished){
         
     }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    if(tableView == self.detailTableView)
    {
        count = self.topicList.count;
    }
    else if(tableView == self.attatchTableView)
    {
        count = self.attachList.count;
    }
    else if(tableView == self.orderTableView)
    {
        count = self.orderList.count;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if(tableView == self.detailTableView)
    {
        CGFloat cellWidth = tableView.frame.size.width;
        CGFloat height = 0;
        CGFloat labelWidth = cellWidth - 16; //8 +8
        NSString *topic = [self.topicList objectAtIndex:indexPath.row];
        NSString *detail = [self.detailList objectAtIndex:indexPath.row];
        NSString *text = [NSString stringWithFormat:@"%@ : %@", [CMUStringUtils isEmpty:topic]?@"":topic, [CMUStringUtils isEmpty:detail]?@"":detail];
        text = [CMUStringUtils trim:text];
        
        UIFont *font = nil;
        if(IS_IPHONE)
        {
            font = [UIFont systemFontOfSize:14.0]; //according to iphone nib
        }
        else
        {
            font = [UIFont systemFontOfSize:20.0]; //according to ipad nib
        }
        
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:text withFont:font];
        height += size.height;
        return height + 10;//+1 = tableview cell line seperator
    }
    else if(tableView == self.attatchTableView)
    {
        height = 70;
        if(IS_IPAD)
        {
            height = 90;
        }
    }
    else if(tableView == self.orderTableView)
    {
        CMUMISEdocOrderFeed *orderFeed = [self.orderList objectAtIndex:indexPath.row];
        NSString *key = [NSString stringWithFormat:@"%i" ,orderFeed.order];
        NSNumber *heightNumber = [self.cachedHeight objectForKey:key];
        if(heightNumber != nil)
        {
            return [heightNumber floatValue];
        }
        
        CGFloat cellWidth = tableView.frame.size.width;
        CGFloat height = 0;
        CGFloat labelWidth = cellWidth - (320 - 269); //260 label width in nib
        //UIFont *font = [UIFont systemFontOfSize:14.0]; //no font size need (use html spec)
        
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[orderFeed.htmlFrom dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withAttributedString:attrStr];
        height += size.height;
        
        attrStr = [[NSAttributedString alloc] initWithData:[orderFeed.htmlTo dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        size = [CMUUIUtils sizeThatFit:labelWidth withAttributedString:attrStr];
        height += size.height;
        
        attrStr = [[NSAttributedString alloc] initWithData:[orderFeed.htmlObjective dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        size = [CMUUIUtils sizeThatFit:labelWidth withAttributedString:attrStr];
        height += size.height;
        
        attrStr = [[NSAttributedString alloc] initWithData:[orderFeed.htmlText dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        size = [CMUUIUtils sizeThatFit:labelWidth withAttributedString:attrStr];
        height += size.height;
        
        height += (8*8);//all vertical space x 8 times
        height += 1;//line
        [self.cachedHeight setValue:[NSNumber numberWithFloat:height ]  forKey:key];
        return height;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.detailTableView)
    {
        CMUMISEdoctDetailTableViewCell *cell = (CMUMISEdoctDetailTableViewCell*)[tableView dequeueReusableCellWithIdentifier:detailTableViewCell forIndexPath:indexPath];
        
        NSString *topic = [self.topicList objectAtIndex:indexPath.row];
        NSString *detail = [self.detailList objectAtIndex:indexPath.row];
        topic = [CMUStringUtils isEmpty:topic]?@"":topic;
        detail = [CMUStringUtils isEmpty:topic]?@"":detail;
        
        NSString *text = [NSString stringWithFormat:@"%@ : %@",topic,detail];
        text = [CMUStringUtils trim:text];
        
        cell.lblText.text = text;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;

    }
    else if(tableView == self.attatchTableView)
    {
        CMUMISEdoctDetailAttachTableViewCell *cell = (CMUMISEdoctDetailAttachTableViewCell*)[tableView dequeueReusableCellWithIdentifier:attachTableViewCell forIndexPath:indexPath];
        NSDictionary *data = [self.attachList objectAtIndex:indexPath.row];
        cell.lblNumber.text = [NSString stringWithFormat:@"%li", indexPath.row + 1];
        cell.lblFilename.text = JSON_GET_OBJECT([data objectForKey:@"name"]);
        return cell;
        
    }
    else if(tableView == self.orderTableView)
    {
        CMUMISEdocOrderFeed *orderFeed = [self.orderList objectAtIndex:indexPath.row];
        CMUEdocOrderTableViewCell *cell = (CMUEdocOrderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:orderTableViewCell forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.orderFeed = orderFeed;
        return cell;
        
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.attatchTableView)
    {
        NSDictionary *data = [self.attachList objectAtIndex:indexPath.row];
        NSString *fileUrlStr = JSON_GET_OBJECT([data objectForKey:@"linkFile"]);
        //NSString *fileUrlStr = @"http://www.analysis.im/uploads/seminar/pdf-sample.pdf";
        //NSString *fileUrlStr = @"https://www.swiftview.com/tech/letterlegal5.doc";
        
        //fix bug path from web service
        
        fileUrlStr = [fileUrlStr stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        fileUrlStr = [fileUrlStr stringByReplacingOccurrencesOfString:@"\"" withString:@"/"];
       
        
        NSString *fileName = [fileUrlStr lastPathComponent];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
        
        [self showHUDLoading];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *op = [manager GET:fileUrlStr
                                       parameters:nil
                                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                              NSLog(@"successful download to %@", path );
                                              [self dismisHUD];
                                              [self openFile:path fileName:fileName];
                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                              NSLog(@"Error: %@", error);
                                              [self dismisHUD];
                                          }];
        op.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
        
    }

}

- (void)openFile:(NSString *) filepath fileName:(NSString *)fileName;
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
    self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
    self.documentInteractionController.name = fileName;
    [self.documentInteractionController setDelegate:self];
    
    // Preview PDF
    [self.documentInteractionController presentPreviewAnimated:YES];
    //[self.documentInteractionController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
    return self.view.frame;
}

#pragma mark CMUMISTabViewDelegate
- (void)cmuMisTabViewDidSelectTab:(int) selectedTabIndex
{
    [self updatePresentState];
}

@end
