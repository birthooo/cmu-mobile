//
//  CMUMISEdoctDetailDetailTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUMISEdoctDetailAttachTableViewCell : UITableViewCell
@property(nonatomic, weak)IBOutlet UILabel *lblNumber;
@property(nonatomic, weak)IBOutlet UILabel *lblFilename;
@end
