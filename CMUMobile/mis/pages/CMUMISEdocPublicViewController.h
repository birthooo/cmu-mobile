//
//  CMUMISEdocPublicViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "CMUMISEdocBaseProtocol.h"
#import "CMUMISEdocBaseRSFViewController.h"
#import "CMUMISMonthYearView.h"

@interface CMUMISEdocPublicViewController : CMUMISEdocBaseRSFViewController<CMUMISEdocBaseProtocol, CMUMISMonthYearViewDelegate>

@end
