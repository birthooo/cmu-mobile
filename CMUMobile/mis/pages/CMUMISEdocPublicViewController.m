//
//  CMUMISEdocPublicViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocPublicViewController.h"


@interface CMUMISEdocPublicViewController (){
    BOOL _isEditMode;
}

@property(nonatomic, weak)IBOutlet CMUMISMonthYearView *monthYearView;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *bottomActionViewHeighConstraint;

@end

@implementation CMUMISEdocPublicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.monthYearView.delegate = self;
    self.monthYearView.pickerParentView = self.view;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark CMUMISMonthYearViewDelegate
- (void)misMonthYearViewDidChanged:(CMUMISMonthYearView *)misMonthYearView
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(cmuMISEdocPublicViewControllerDidChangeMonthYear:)] )
    {
        [self.delegate cmuMISEdocPublicViewControllerDidChangeMonthYear:self];
    }
}

#pragma mark CMUMISEdocBaseProtocol
- (void)reset
{
    [super reset];
    [self.monthYearView reset];
}

- (void)setToEditMode:(BOOL) isEditMode
{
    _isEditMode = isEditMode;
    //[self.tableView reloadData];
}

#pragma mark overide

- (CmuMisEdocCategory)getEdocCategoty
{
    return CmuMisEdocCategory_PUBLIC;
}

- (void)baseViewReloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceManager sharedInstance] getEdocPublicTab:self month:self.monthYearView.month year:self.monthYearView.year success:^(id result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
