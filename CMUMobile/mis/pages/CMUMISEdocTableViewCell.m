//
//  MISEdocTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocTableViewCell.h"
#import "CMUFormatUtils.h"
#import "UIColor+CMU.h"

@interface CMUMISEdocTableViewCell ()
@property(nonatomic, weak) IBOutlet UIImageView *imvCheckbox;
@property(nonatomic, weak) IBOutlet UILabel *lblDocCode;
@property(nonatomic, weak) IBOutlet UILabel *lblTopic;
@property(nonatomic, weak) IBOutlet UILabel *lblTextStaus;
@property(nonatomic, weak) IBOutlet UILabel *lblSendTime;
@property(nonatomic, weak) IBOutlet UIImageView *imvAccessory;

@property(nonatomic, weak) IBOutlet UIView *leadingView;
@property(nonatomic, weak) IBOutlet UIView *tailingView;


@property(nonatomic, unsafe_unretained)CGFloat leadingOriginalWidth;
@property(nonatomic, unsafe_unretained)CGFloat tailingOriginalWidth;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *leadingViewWidthConstraint;
@property(nonatomic, strong)IBOutlet NSLayoutConstraint *tailingViewWidthConstraint;

@end

@implementation CMUMISEdocTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.leadingOriginalWidth = self.leadingView.frame.size.width;
    self.tailingOriginalWidth = self.tailingView.frame.size.width;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:137.0/255 green:117.0/255 blue:206.0/255 alpha:1.0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEdocFeed:(CMUMISEdocFeed *)edocFeed
{
    _edocFeed = edocFeed;
    [self updatePresentState];
}

- (void)updatePresentState
{
    BOOL isOpened = self.edocFeed.IsOpened;
    self.contentView.backgroundColor = isOpened?[UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1.0]:[UIColor whiteColor];
    self.lblDocCode.textColor= isOpened?[UIColor colorWithRed:124/255.0 green:124/255.0 blue:124/255.0 alpha:1.0]:[UIColor colorWithRed:232/255.0 green:171/255.0 blue:28/255.0 alpha:1.0];
    self.lblTopic.textColor= isOpened?[UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0]:[UIColor blackColor];
    self.lblTextStaus.textColor= isOpened?[UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0]:[UIColor colorWithRed:215/255.0 green:59/255.0 blue:48/255.0 alpha:1.0];
    self.lblSendTime.textColor = isOpened?[UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0]:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
    self.imvAccessory.image = isOpened?[UIImage imageNamed:@"mis_cell_accessory_disabled.png"]:[UIImage imageNamed:@"mis_cell_accessory.png"];
    
    self.lblDocCode.text = self.edocFeed.docCode;
    self.lblTopic.text = self.edocFeed.topic;
    self.lblTextStaus.text = self.edocFeed.textStaus;
    self.lblSendTime.text = [CMUFormatUtils formatDateMISEdocFeedStyle:self.edocFeed.sendTime locale:[NSLocale currentLocale]];
    
    if(self.editMode)
    {
        self.leadingViewWidthConstraint.constant = self.leadingOriginalWidth;
        self.tailingViewWidthConstraint.constant = 0;
    }
    else
    {
        self.leadingViewWidthConstraint.constant = 0;
        self.tailingViewWidthConstraint.constant = self.tailingOriginalWidth;
    }
    
    BOOL isCheck = self.edocFeed.uiSelected;
    self.imvCheckbox.image = isCheck?[UIImage imageNamed:@"mis_cell_checked.png"]:[UIImage imageNamed:@"mis_cell_uncheck.png"];
    
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imvCheckbox.image = nil;
    self.lblDocCode.text = nil;
    self.lblTopic.text = nil;
    self.lblTextStaus.text = nil;
    self.lblSendTime.text = nil;
}

@end
