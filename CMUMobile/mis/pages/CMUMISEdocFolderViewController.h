//
//  CMUMISEdocFileViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "CMUMISEdocBaseProtocol.h"
#import "CMUMISModel.h"

@class CMUMISEdocFolderViewController;
@protocol CMUMISEdocFolderControllerDelegate <NSObject>
-(void)cmuMISEdocFolderController:(CMUMISEdocFolderViewController*) vc didSelected:(CMUMISEdocFolderFeed *) edocFolderFeed;
- (void)cmuMISEdocFolderViewControllerDidScrollEdocFeed:(CMUMISEdocFolderViewController *) vc;
@end

@interface CMUMISEdocFolderViewController : CMUViewController<CMUMISEdocBaseProtocol>
@property(nonatomic, weak)id<CMUMISEdocFolderControllerDelegate> delegate;
@end
