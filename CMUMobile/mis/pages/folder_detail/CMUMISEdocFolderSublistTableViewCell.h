//
//  MISEdocTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMISModel.h"

@interface CMUMISEdocFolderSublistTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUMISEdocFeed *edocFeed;
@property(nonatomic, unsafe_unretained)BOOL editMode;
@end
