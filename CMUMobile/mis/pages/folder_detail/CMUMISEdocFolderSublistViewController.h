//
//  CMUMISEdocFolderSublistViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUMISMonthYearView.h"
#import "CMUMISModel.h"
#import "CMUMISEdocDetailViewController.h"

@class CMUMISEdocFolderSublistViewController;
@protocol CMUMISEdocFolderSublistViewControllerDelegate <NSObject>
- (void)cmuMISEdocFolderSublistViewControllerBackClicked:(CMUMISEdocFolderSublistViewController *) vc;
@end

@interface CMUMISEdocFolderSublistViewController : CMUNavBarViewController<CMUMISMonthYearViewDelegate, CMUMISEdocDetailViewControllerDelegate>
@property(nonatomic, unsafe_unretained)id<CMUMISEdocFolderSublistViewControllerDelegate> delegate;
@property(nonatomic, strong) CMUMISEdocFolderFeed * edocFolderFeed;
@end
