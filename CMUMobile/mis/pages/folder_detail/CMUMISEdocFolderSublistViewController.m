//
//  CMUMISEdocFolderSublistViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocFolderSublistViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUMISModel.h"
#import "CMUMISEdocFolderSublistTableViewCell.h"
#import "CMUFormatUtils.h"

static NSString *cellIdentifier = @"CMUMISEdocFolderSublistTableViewCell";

@interface CMUMISEdocFolderSublistViewController ()
@property(nonatomic, weak)IBOutlet UILabel *lblTitle;
@property(nonatomic, weak)IBOutlet CMUMISMonthYearView *monthYearView;
@property(nonatomic, weak)IBOutlet UISegmentedControl *segmentController;
@property(nonatomic, weak)IBOutlet UILabel *lblNSend;
@property(nonatomic, weak)IBOutlet UILabel *lblNOut;
@property(nonatomic, weak)IBOutlet UITableView *tableView;

@property(nonatomic, assign)int selectedTab;
@property(nonatomic, strong)CMUMISEdocFolderSubListFeedModel * edocFolderSubListFeedModel;

- (IBAction)backClicked:(id) sender;
- (IBAction)onSegmentSelect:(id) sender;
@end

@implementation CMUMISEdocFolderSublistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lblTitle.text = self.edocFolderFeed.folderName;
    
    
    self.monthYearView.allMonth = YES;
    self.monthYearView.delegate = self;
    self.monthYearView.pickerParentView = self.view;
    
    self.segmentController.selectedSegmentIndex = 0;
    self.lblNSend.layer.cornerRadius = 5;
    self.lblNSend.layer.masksToBounds = YES;
    self.lblNOut.layer.cornerRadius = 5;
    self.lblNOut.layer.masksToBounds = YES;
    
    if(IS_IPAD)
    {
        UIFont *font = [UIFont systemFontOfSize:24.0f];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                               forKey:NSFontAttributeName];
        [self.segmentController setTitleTextAttributes:attributes
                                              forState:UIControlStateNormal];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUMISEdocFolderSublistTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self refreshData:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backClicked:(id) sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuMISEdocFolderSublistViewControllerBackClicked:)])
    {
        [_delegate cmuMISEdocFolderSublistViewControllerBackClicked:self];
    }
}

- (void)updatePresentState
{
    [super updatePresentState];
    
    self.lblNSend.text = [CMUFormatUtils formatBadge: self.edocFolderSubListFeedModel.nSend];
    self.lblNOut.text = [CMUFormatUtils formatBadge: self.edocFolderSubListFeedModel.nOut];
    self.lblNSend.hidden = self.edocFolderSubListFeedModel.nSend <= 0;
    self.lblNOut.hidden = self.edocFolderSubListFeedModel.nOut <= 0;
}



#pragma mark CMUMISMonthYearViewDelegate
- (void)misMonthYearViewDidChanged:(CMUMISMonthYearView *)misMonthYearView
{
    [self refreshData:YES];
}

#pragma mark CMUMISMonthYearViewDelegate
- (void)onSegmentSelect:(id) sender
{
    [self refreshData:YES];
}

- (void)refreshData:(BOOL) scrollToTop
{
    [self showHUDLoading];
    
    [[CMUWebServiceManager sharedInstance] getEdocFolderSubList:self folderId:self.edocFolderFeed.folderID month:self.monthYearView.month year:self.monthYearView.year success:^(id result) {
        [self dismisHUD];
        self.edocFolderSubListFeedModel = (CMUMISEdocFolderSubListFeedModel *)result;
        [self.tableView reloadData];
        if(scrollToTop)
        {
            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        }
        
        [self updatePresentState];
        
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [self dismisHUD];

    }];
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        return self.edocFolderSubListFeedModel.listSend.count;
    }
    return self.edocFolderSubListFeedModel.listOut.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    if(IS_IPHONE)
    {
        height = 80;
    }
    else
    {
        height = 120;
    }
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFeed *edocFeed = nil;
    
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        edocFeed =  [self.edocFolderSubListFeedModel.listSend objectAtIndex:indexPath.row];
    }
    else
    {
        edocFeed =  [self.edocFolderSubListFeedModel.listOut objectAtIndex:indexPath.row];
    }

    CMUMISEdocFolderSublistTableViewCell *cell = (CMUMISEdocFolderSublistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.edocFeed = edocFeed;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUMISEdocFeed *edocFeed = nil;
    
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        edocFeed =  [self.edocFolderSubListFeedModel.listSend objectAtIndex:indexPath.row];
    }
    else
    {
        edocFeed =  [self.edocFolderSubListFeedModel.listOut objectAtIndex:indexPath.row];
    }
    
    CMUMISEdocDetailViewController *detailVC = [[CMUMISEdocDetailViewController alloc] init];
    detailVC.edocCategory = CmuMisEdocCategory_FILE;
    detailVC.delegate = self;
    detailVC.docID = edocFeed.docID;
    detailVC.docSubID = edocFeed.docSubID;
    [self.navigationController pushViewController:detailVC animated:YES];
}


#pragma mark CMUMISEdocDetailViewControllerDelegate
- (void)cmuMisEdocDetailViewControllerBackClicked:(CMUMISEdocDetailViewController *) vc
{
    [self.navigationController popToViewController:self animated:YES];
    [self refreshData:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
