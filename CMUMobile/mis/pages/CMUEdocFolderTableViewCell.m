//
//  CMUEdocFileTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/1/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUEdocFolderTableViewCell.h"

@interface CMUEdocFolderTableViewCell ()
@property(nonatomic, weak) IBOutlet UILabel *lblTopic;
@property(nonatomic, weak) IBOutlet UIView *tailingView;
@end

@implementation CMUEdocFolderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:137.0/255 green:117.0/255 blue:206.0/255 alpha:1.0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEdocFolderFeed:(CMUMISEdocFolderFeed *)edocFolderFeed
{
    _edocFolderFeed = edocFolderFeed;
    [self updatePresentState];
}

- (void)updatePresentState
{
    self.lblTopic.text = self.edocFolderFeed.folderName;
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblTopic.text = nil;
}

@end
