//
//  CMUMISEdocRecieveViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/13/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEdocRecieveViewController.h"

@interface CMUMISEdocRecieveViewController ()

@end

@implementation CMUMISEdocRecieveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark overide

- (CmuMisEdocCategory)getEdocCategoty
{
    return CmuMisEdocCategory_RECIEVE;
}

- (void)baseViewReloadData:(NSDictionary *) params success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceManager sharedInstance] getEdocRecieveTab:self key:[params objectForKey:KEY_SEARCH_WORD] success:^(id result) {
        success(result);
    } failure:^(NSError *error) {
        failure(error);
    }];
}
@end
