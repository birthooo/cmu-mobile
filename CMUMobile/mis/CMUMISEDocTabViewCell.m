//
//  CMUMISEDocTabViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISEDocTabViewCell.h"
#import "UIColor+CMU.h"
#import "CMUFormatUtils.h"

@interface CMUMISEDocTabViewCell()
@property(nonatomic, strong)IBOutlet UIView *bottomLine;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *minDistanceConstraints;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *counterWidth;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *counterTrialing;
@end


@implementation CMUMISEDocTabViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMISEDocTabViewCell" owner:self options:nil];
        UIView *contentView = [nibObjects objectAtIndex:0];
        self.contentView = contentView;
        
        self.lblEDocCount.layer.cornerRadius = 5;
        self.lblEDocCount.layer.masksToBounds = YES;
        self.lblEDocCount.layer.cornerRadius = 5;
        self.lblEDocCount.layer.masksToBounds = YES;
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self updatePresentState];
}

- (void)updatePresentState
{
    self.pointer.hidden = self.selected? NO: YES;
    self.contentView.backgroundColor = self.selected? [UIColor colorWithRed:137.0/255 green:117.0/255 blue:206.0/255 alpha:1.0]:[UIColor whiteColor];
    self.lblTitle.textColor  = self.selected? [UIColor whiteColor]:[UIColor cmuBlackColor];
    self.bottomLine.hidden = self.selected;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.minDistanceConstraints.constant = self.lblEDocCount.hidden? 0 : 5;
    self.counterWidth.constant = self.lblEDocCount.hidden? 0 : 23;
    self.counterTrialing.constant = self.lblEDocCount.hidden? 0 : 5;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.pointer.hidden = YES;
}

@end