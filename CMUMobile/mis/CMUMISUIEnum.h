//
//  CMUMISUIEnum.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

typedef enum {
    CmuMisEdocCategory_NONE = 0,//for notification
    CmuMisEdocCategory_RECIEVE = 1,
    CmuMisEdocCategory_PUBLIC = 2,
    CmuMisEdocCategory_SEND = 3,
    CmuMisEdocCategory_FOLLOW = 4,
    CmuMisEdocCategory_FILE = 5
} CmuMisEdocCategory;

typedef enum {
    CmuMisEdocAction_DELETE = 1,
    CmuMisEdocAction_PICK_BACK = 2,
    CmuMisEdocAction_UNFOLLOW = 3,
    CmuMisEdocAction_ADD_TO_FOLDER = 4,
    CmuMisEdocAction_CHANGE_TO_FOLDER = 5
} CmuMisEdocAction;