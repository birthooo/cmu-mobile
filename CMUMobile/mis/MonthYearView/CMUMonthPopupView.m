//
//  CMUMonthPopupView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMonthPopupView.h"
#import "CMUPopupTableViewCell.h"

static NSString *cellIdentifier = @"CMUPopupTableViewCell";

@interface CMUMonthPopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *monthList;
@property(nonatomic, strong)NSDateFormatter *dateFormatter;
@end


@implementation CMUMonthPopupView

- (void)customInit
{
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [self.dateFormatter setDateFormat:@"MMMM"];
    
    [self createMonthList];
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMonthPopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUPopupTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setAllMonth:(BOOL)allMonth
{
    _allMonth = allMonth;
    [self createMonthList];
}

- (void)createMonthList
{
    self.monthList= [[NSMutableArray alloc] init];
    
    if(self.allMonth)
    {
        [self.monthList addObject:[NSNumber numberWithInt:0] ];
    }
    for(int i = 1; i <= 12; i++)
    {
        [self.monthList addObject:[NSNumber numberWithInt:i] ];
       
    }
    
}


#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.monthList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUPopupTableViewCell *cell = (CMUPopupTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    int month = [[self.monthList objectAtIndex:indexPath.row] intValue];
    NSString *monthStr = nil;
    if(month == 0)
    {
        monthStr = @"All";
    }
    else
    {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        [components setMonth:month];
        NSDate *date = [calendar dateFromComponents:components];
        monthStr = [self.dateFormatter stringFromDate:date];
    }
    
    
    
    cell.lblTitle.text = monthStr;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(monthPopupView:didSelectedMonth:)])
    {
        int month = [[self.monthList objectAtIndex:indexPath.row] intValue];
        [_delegate monthPopupView:self didSelectedMonth:month];
    }
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
