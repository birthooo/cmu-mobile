//
//  CMUMISYearPopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/26/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CMUYearPopupView;

@protocol CMUYearPopupViewDelegate<NSObject>
- (void)yearPopupView:(CMUYearPopupView *) newsTypePopupView didSelectedYear:(int) year;
@end

@interface CMUYearPopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUYearPopupViewDelegate> delegate;
@end