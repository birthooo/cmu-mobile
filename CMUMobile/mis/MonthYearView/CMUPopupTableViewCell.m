//
//  CMUPopupTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPopupTableViewCell.h"
#import "UIColor+CMU.h"
#import "global.h"

@interface CMUPopupTableViewCell()
@end

@implementation CMUPopupTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
    if(IS_IPAD)
    {
        self.lblTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblTitle.text = nil;
}


@end
