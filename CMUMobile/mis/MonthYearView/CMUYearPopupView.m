//
//  CMUYearPopupView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUYearPopupView.h"
#import "CMUPopupTableViewCell.h"

static NSString *cellIdentifier = @"CMUPopupTableViewCell";

@interface CMUYearPopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *yearList;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@end


#define MAX_YEARS 3

@implementation CMUYearPopupView

- (void)customInit
{
    self.yearList= [[NSMutableArray alloc] init];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    int year = [components year];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [self.dateFormatter setDateFormat:@"yyyy"];
    for(int i = year - MAX_YEARS + 1; i <= year; i++)
    {
        
        [self.yearList addObject: [NSNumber numberWithInt:i ]];
    }
    
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUYearPopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUPopupTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.yearList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUPopupTableViewCell *cell = (CMUPopupTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    [components setYear:[[self.yearList objectAtIndex:indexPath.row] intValue]];
    NSDate *date = [calendar dateFromComponents:components];

    
    cell.lblTitle.text = [self.dateFormatter stringFromDate:date];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(yearPopupView:didSelectedYear:)])
    {
        [_delegate yearPopupView:self didSelectedYear:[[self.yearList objectAtIndex:indexPath.row] intValue]];
    }
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
