//
//  CMUMonthPopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/1/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMUMonthPopupView;

@protocol CMUMonthPopupViewDelegate<NSObject>
- (void)monthPopupView:(CMUMonthPopupView *) newsTypePopupView didSelectedMonth:(int) month;
@end

@interface CMUMonthPopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUMonthPopupViewDelegate>
delegate;
@property(nonatomic, unsafe_unretained) BOOL allMonth;
@end
