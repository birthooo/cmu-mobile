//
//  CMUMISMonthYearView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/26/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopoverView.h"
#import "CMUMonthPopupView.h"
#import "CMUYearPopupView.h"

@class CMUMISMonthYearView;

@protocol CMUMISMonthYearViewDelegate <NSObject>
- (void)misMonthYearViewDidChanged:(CMUMISMonthYearView *)misMonthYearView;
@end

@interface CMUMISMonthYearView : UIView<PopoverViewDelegate, CMUMonthPopupViewDelegate, CMUYearPopupViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUMISMonthYearViewDelegate> delegate;

- (void)reset;
@property(nonatomic, strong) UIView *pickerParentView;
@property(nonatomic, unsafe_unretained) int month;
@property(nonatomic, unsafe_unretained) BOOL allMonth;
@property(nonatomic, unsafe_unretained) int year;
@end
