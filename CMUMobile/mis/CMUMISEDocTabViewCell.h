//
//  CMUMISEDocTabViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "GMGridViewCell.h"
#import "CMUTriangleView.h"

@interface CMUMISEDocTabViewCell : GMGridViewCell
@property(nonatomic, strong)IBOutlet UILabel *lblTitle;
@property(nonatomic, unsafe_unretained) BOOL selected;
@property(nonatomic, strong)IBOutlet UILabel *lblEDocCount;
@property(nonatomic, strong)IBOutlet CMUTriangleView *pointer;
@end
