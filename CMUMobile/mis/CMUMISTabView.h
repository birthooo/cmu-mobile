//
//  CMUMISTabView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMGridView.h"
#import "CMUMISEDocTabViewCell.h"

@interface CMUMISTabModel : NSObject
- (id)initWithTitle:(NSString *) title number:(int) number pointerColor:(UIColor *) pointerColor;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, unsafe_unretained) int number;
@property(nonatomic, strong) UIColor *pointerColor;
@end

@class CMUMISTabView;
@protocol CMUMISTabViewDelegate <NSObject>
@optional
- (void) cmuMisTabViewDidSelectTab:(int) selectedTabIndex;
@end

@interface CMUMISTabView : UIView<GMGridViewDataSource, GMGridViewActionDelegate>
@property(nonatomic, unsafe_unretained) id<CMUMISTabViewDelegate> delegate;
@property(nonatomic, unsafe_unretained) BOOL fitTab;
@property(nonatomic, strong)NSArray *tabModels;
@property(nonatomic, unsafe_unretained) int selectedTab;
@end
