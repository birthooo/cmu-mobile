//
//  AppDelegate.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUAppDelegate.h"
#import "CMUNavBarViewController.h"
#import "CMUUIUtils.h"
#import "AFNetworkReachabilityManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CMUWebServiceManager.h"
#import "SVProgressHUD.h"
#import "CMUSISModel.h"
#import "CMURegViewController.h"
#import "NSString+URLEncoder.h"
#import "CMUJSONUtils.h"
#import "CMUAboutViewController.h"
#import "Reachability.h"
#define FIRST_LOAD_APP @"isFirstLoad"
#import "ActivityViewController.h"

#import "NewEdocViewController.h"

#import "Heap.h"

#import "OAuthLoginHeader.h"
#import "global.h"
#import "prod_cfg.h"

#define REDIRECT_OAUTH @"cmumobile://oauth"
#define REDIRECT_TRANSIT @"cmumobile://transit"


#define KEY_CMU_USER_TICKET @"KEY_CMU_USER_TICKET"
#define KEY_CMU_USER_INFO_MODEL @"KEY_CMU_USER_INFO_MODEL"
#define KEY_CMU_USER_DEVICE_TOKEN @"KEY_CMU_USER_DEVICE_TOKEN"
#define KEY_CMU_IS_DEVICE_REGIS_PUSH @"KEY_CMU_IS_DEVICE_REGIS_PUSH"
//#import <Google/Analytics.h>

@interface CMUAppDelegate ()
    

@end

@implementation CMUAppDelegate{
    id services_;
}
            
-(id)GETRequest:(int)routid{
    
    NSString *url = [NSString stringWithFormat:@"http://www.cmutransit.com/cmtbus/routejson.php?route=%i",routid];
    //Set Access Token String
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"APP Delegate call :%@",json);
    return json;
    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"Token :%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"KEY_CMU_USER_DEVICE_TOKEN"]);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goHomeForCall)
                                                 name:@"goHome" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goMIS)
                                                 name:@"goMIS" object:nil];
      //Detect First load app
    if([[NSUserDefaults standardUserDefaults]boolForKey:FIRST_LOAD_APP]==0&&[[Reachability reachabilityForInternetConnection]currentReachabilityStatus]!=NotReachable)
    {
        NSLog(@"FIRST AND REACH");
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:FIRST_LOAD_APP];
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        NSArray *route1 = [self GETRequest:1];
        NSArray *route2 = [self GETRequest:2];
        NSArray *route3 = [self GETRequest:3];
        NSArray *route4 = [self GETRequest:4];
        NSArray *route5 = [self GETRequest:5];
        NSArray *route6 = [self GETRequest:6];
        arr  = [[NSMutableArray alloc]init];
        [arr addObjectsFromArray:route1];
        [arr addObjectsFromArray:route2];
        [arr addObjectsFromArray:route3];
        [arr addObjectsFromArray:route4];
        [arr addObjectsFromArray:route5];
        [arr addObjectsFromArray:route6];
        [[NSUserDefaults standardUserDefaults]setValue:arr forKey:@"arr"];

    }
    else{
        NSLog(@"FIRST AND NOT REACH");
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:FIRST_LOAD_APP];
    }
    
   

    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    /// Configure tracker from GoogleService-Info.plist.
    
    //NSError *configureError;
    //[[GGLContext sharedInstance] configureWithError:&configureError];
    //NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.leftNavigationController = [[CMUNavigationController alloc] init];
    [CMUUIUtils customizeLeftNavigationBar:self.leftNavigationController.navigationBar];
    self.leftViewController = [[CMULeftViewController alloc] init];
    [self.leftNavigationController setViewControllers:[NSArray arrayWithObject:self.leftViewController]];
    
    self.navigationController = [[CMUNavigationController alloc] init];
    [CMUUIUtils customizeNavigationBar:self.navigationController.navigationBar];
    
    self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.navigationController leftDrawerViewController:self.leftNavigationController];
    
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    //[drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    //disable center intraction when hidden
    [self.drawerController setCenterHiddenInteractionMode:MMDrawerOpenCenterInteractionModeNone];
    if(IS_IPAD)
    {
        self.drawerController.maximumLeftDrawerWidth = 400;
    }
    
    
    //check update available
        
    
    
                               
//    self.viewdeckController = [[IIViewDeckController alloc] initWithCenterViewController:self.navigationController leftViewController:self.leftViewController];
    //self.viewdeckController.delegate = self;
    //self.viewdeckController.panningMode = IIViewDeckDelegatePanning;
    //self.viewdectController.leftLedge = 60;
    //self.viewdeckController.centerhiddenInteractivity = IIViewDeckCenterHiddenNotUserInteractiveWithTapToClose;
   
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    
    [self goHome:NO];
    
    self.networkAlertView = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME message:@"No Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    AFNetworkReachabilityManager *shareReachabilityManager = [AFNetworkReachabilityManager sharedManager];
    [shareReachabilityManager startMonitoring];
    [shareReachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        DebugLog(@"%i", (int)status);
        if(status == AFNetworkReachabilityStatusNotReachable)
        {
            [self.networkAlertView show];
        }
        else
        {
            if([[AFNetworkReachabilityManager sharedManager] isReachable])
            {
                [self.networkAlertView dismissWithClickedButtonIndex:0 animated:YES];
            }
            //[self checkForPendingOnlineLogout];
        }
    }];
    
    //Google Map
    [GMSServices provideAPIKey:CMU_GOOGLE_MAP_API_KEY];
    services_ = [GMSServices sharedServices];
    
    //init 
    [CMUWebServiceManager sharedInstance];
    
    // Let the device know we want to receive push notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    } else {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    //App Icon Badge Update
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationNumberUpdatedNotification:)
                                                 name:CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginChangedNotification:)
                                                 name:CMU_LOGIN_CHANGED_NOTIFICATION
                                               object:nil];
    
    
    self.window.rootViewController = self.drawerController;
    [self.window makeKeyAndVisible];
    application.statusBarHidden = YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
       //

//HEAP
    [Heap setAppId:@"3271447366"];
#ifdef DEBUG
    [Heap enableVisualizer];
#endif
//END HEAP
    
    
    return YES;
}
- (void) receiveTestNotification:(NSNotification *) notification
{
       
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    // handler code here
   
    if([url isEqual:REDIRECT_TRANSIT]){
        [self goCMUTransit];
    }
    
    
    
    if([url isEqual:REDIRECT_OAUTH]){
        NSLog(@"OAUTH");
    }
    
    
    return YES;
}
- (void)notificationNumberUpdatedNotification:(NSNotification *) notification
{
   [UIApplication sharedApplication].applicationIconBadgeNumber = [[[CMUWebServiceManager sharedInstance] notificationNumberModel] notificationNumber];
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void) loginChangedNotification:(NSNotification *) notification
{
    if(![[CMUWebServiceManager sharedInstance] isLogin])
    {
        if([[[notification userInfo] objectForKey:@"relogin"] boolValue] == YES)
        {
            [self goHome:YES];
        }
        else
        {
            //goto home if current module is authentication modules
            UIViewController *rootViewController = [self.navigationController.viewControllers firstObject];
            if([rootViewController isKindOfClass:[CMUSISViewController class]] ||
               [rootViewController isKindOfClass:[CMUSISPCheckViewController class]] ||
               [rootViewController isKindOfClass:[CMUSISSummaryViewController class]] ||
               [rootViewController isKindOfClass:[CMUMISEDocViewController class]]||
               [rootViewController isKindOfClass:[CMURegViewController class]] ||
               [rootViewController isKindOfClass:[CMUAboutViewController class]])
            {
                [self goHome:NO];
            }
            
        }
    }
}


//- (void)checkForPendingOnlineLogout
//{
//    DebugLog(@"");
//    if([[CMUWebServiceManager  sharedInstance] is้Pending])
//    {
//        [[CMUWebServiceManager  sharedInstance] logout:self success:^(id result) {
//            DebugLog(@"");
//        } failure:^(NSError *error) {
//            DebugLog(@"");
//        }];
//    }
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActive];
    
    [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
        //Nothing todo here
    } failure:^(NSError *error) {
        
    }];
   
    

}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    NSString *urlstring = [NSString stringWithFormat:@"%@",url];
    
    NSString *str = [url query];
    NSArray *objects = [str componentsSeparatedByString:@"="];
    
    
    if([urlstring isEqual:@"cmumobile://transit"]){
        [self goCMUTransit];
    }
    //NSLog(@"object:%@",objects);
   // if([objects[0] isEqual:@"code"]){
 
//        NSString *urlGetToken = [NSString stringWithFormat:@"code=%@&client_id=%@&client_secret=%@&grant_type=%@&redirect_uri=%@",objects[1],LOGIN_CLIENT_ID,LOGIN_CLIENT_SECRET,LOGIN_GRANT_TYPE,LOGIN_REDIRECT_URI];
//        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
//        dictionary = [self POSTRequest:urlGetToken url:LOGIN_GET_TOKEN_URL];
//        NSLog(@"dictiona :%@",dictionary);
//        [[NSUserDefaults standardUserDefaults]setValue:[dictionary valueForKey:@"access_token"] forKey:@"access_token"];
//        [[NSUserDefaults standardUserDefaults]setValue:[dictionary valueForKey:@"refresh_token"] forKey:@"refresh_token"];
//        [[NSUserDefaults standardUserDefaults]synchronize];
//        NSMutableDictionary *userinfo = [[NSMutableDictionary alloc]init];
//        userinfo = [[self GetUserInfo]valueForKey:@"data"];
//        [[NSUserDefaults standardUserDefaults]setValue:[userinfo valueForKey:@"itaccount_name"] forKey:@"account_name"];
//        NSLog(@"userinfo:%@",userinfo);
//        
//        //
//        NSMutableDictionary *dictToSave = [[NSMutableDictionary alloc]init];
////        [dictToSave setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
////        [dictToSave setObject:[dictionary valueForKey:@"expires_in"] forKey:@"expires_in"];
//        
//        [dictionary setObject:[userinfo valueForKey:@"itaccount_name"] forKey:@"userName"];
//        [dictToSave setObject:userinfo forKey:@"employee"];
//        [dictToSave setObject:dictionary forKey:@"ticket"];
//        [self saveTicket:dictToSave];
//        [self saveUserInfo:dictToSave];
//        CMUUserInfoModel *userInfoModel = [[CMUUserInfoModel alloc] initWithDictionary:dictToSave];
//
//        [self getUserType];
//        [self regisMobileToken];
//        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLoginSuccess"];
//        CMUSettings *share = [[CMUSettings sharedInstance]init];
//        [share loadMenuItems];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogin" object:nil];
//               [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
//    }
    
    
 /*
    BOOL returnValue = NO;
    
    //facebook
    if([FBAppCall handleOpenURL:url sourceApplication:sourceApplication])
    {
        returnValue = YES;
    };
    // Add whatever other url handling code your app requires here
    return returnValue;
  */
    return YES;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    const unsigned *tokenBytes = [deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"My token string is: %@", hexToken);
    
    [[CMUWebServiceManager sharedInstance] saveDeviceToken:hexToken];
    [[CMUWebServiceManager sharedInstance] registerPushNotification:self success:^(id result) {
         NSLog(@"%@", result);
        [[CMUWebServiceManager sharedInstance] getNotificationSettings:self success:^(id result) {
            DebugLog(@"%@", result);
        } failure:^(NSError *error) {
            DebugLog(@"%@", error);
        }];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
    }];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo NS_AVAILABLE_IOS(3_0)
{
    NSLog(@"didReceiveRemoteNotification: %@", userInfo);
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
        [self goNotification:NotiTabNotification];
    }
    else
    {
        //on foreground just update badge update notification list if showing
        [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
            //Nothing todo here
        } failure:^(NSError *error) {
            
        }];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CMU_APPLICATION_DID_RECEIVE_REMOTE_NOTIFICATION_IN_FOREGROUND object:self];
    }
    //application.applicationIconBadgeNumber = 0;
    
    

}

- (UIViewController *)getCurrentDisplayViewController
{
    NSArray *controllers = [_navigationController viewControllers];
    UIViewController *viewControler = nil;
    if(controllers && controllers.count > 0)
    {
        viewControler = [controllers objectAtIndex:0];
    }
    return viewControler;
}

- (void)goLeftView
{
    [self.drawerController  openDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
    }];
}

- (void)goHome:(BOOL) reloginPending
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS Home Screen"];
    NSLog(@"go home go home");
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_goBack_Home"  // Event action (required)
                                                           label:@"ios_Home"          // Event label
                                                           value:nil] build]];
    
    id<GAITracker> tracker2 = [[GAI sharedInstance] defaultTracker];
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"
                                            action:@"ios_home_goBack_Home"
                                             label:@"ios_Home"
                                             value:nil] build];
    [tracker2 send:event];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUHomeViewController class]])
    {
        CMUHomeViewController *homeViewController = [[CMUHomeViewController alloc] init];
        homeViewController.reloginPending = reloginPending;
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:homeViewController]];
    }
    
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
-(void)goHomeForCall{
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUHomeViewController class]])
    {
        CMUHomeViewController *homeViewController = [[CMUHomeViewController alloc] init];
        homeViewController.reloginPending = NO;
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:homeViewController]];
        
    }
    
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];


}

- (void)goNews
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp News"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_News"  // Event action (required)
                                                           label:@"ios_News"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUNewsViewController class]])
    {
        CMUNewsViewController *newsViewController = [[CMUNewsViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:newsViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
//NewsScholarshipViewController
-(void)goNewsScholarship:(NSString*)number{
    
   
        CMUHomeViewController *homeViewController = [[CMUHomeViewController alloc] init];

        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:homeViewController]];
    
    
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
    
    CMUNewsViewController *newsViewController = [[CMUNewsViewController alloc] init];
    [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:newsViewController]];
    newsViewController.num = number;
    
    
    
    
    
    
  
}

-(void)goPhone2{
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUPhoneViewController class]])
    {
        CMUPhoneViewController *phoneViewController = [[CMUPhoneViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:phoneViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
-(void)contactDetail{
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[FeedbackViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Contact" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"contactDetail"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
- (void)goPhone
{
    /*
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Phone"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Phone"  // Event action (required)
                                                           label:@"ios_Phone"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUPhoneViewController class]])
    {
        CMUPhoneViewController *phoneViewController = [[CMUPhoneViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:phoneViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
     */
    

        
        NSLog(@"goRegStudent");
        UIViewController *viewControler = [self getCurrentDisplayViewController];
        if(!viewControler || ![viewControler isKindOfClass:[FeedbackViewController class]])
        {
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Contact" bundle:nil];
            UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"contact"];
            [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
        }
        [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
        }];
    

}

- (void)goInfo
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Info"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Info"  // Event action (required)
                                                           label:@"ios_Info"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUInformationViewController class]])
    {
        CMUInformationViewController *informationViewController = [[CMUInformationViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:informationViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goCuriculum
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Curiculum"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Curiculum"  // Event action (required)
                                                           label:@"ios_Curiculum"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUCurriculumViewController class]])
    {
        CMUCurriculumViewController *curriculumViewController = [[CMUCurriculumViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:curriculumViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goPhotoAndVideo
{
    /*
 MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] init];
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[MWPhotoBrowser class]])
    {
        
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] init];
        
        browser.enableSwipeToDismiss = NO;
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject: browser]];
        
        UINavigationController *nc = [[CMUNavigationController alloc] initWithRootViewController:browser];
        [CMUUIUtils customizeNavigationBar:nc.navigationBar];
        [self.drawerController.centerViewController presentViewController:nc animated:NO completion:nil];
        
        
    }
    //[self.viewdeckController  closeLeftViewAnimated:YES];
    */
    /*
    NSLog(@"photoandvideo");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[PhotoAndVideoViewController class]])
    {
     */
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"photoAndVideo" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"photoandvideo"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    //}
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];

}

- (void)goChannel
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Channel"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Channel"  // Event action (required)
                                                           label:@"ios_Channel"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUChannelViewController class]])
    {
        CMUChannelViewController *channelViewController = [[CMUChannelViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:channelViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goEvent
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Calendar"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Event"  // Event action (required)
                                                           label:@"ios_Event"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUEventViewController class]])
    {
        CMUEventViewController *eventViewController = [[CMUEventViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:eventViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goMap
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Map"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_Map"  // Event action (required)
                                                           label:@"ios_Map"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUMapViewController class]])
    {
        CMUMapViewController *mapViewController = [[CMUMapViewController alloc] init];
        //fixed map display
//        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:mapViewController]];
        [self.navigationController setViewControllers:[NSArray arrayWithObject:mapViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goNotification:(NotiTab) initTab;
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Go Notification"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_detail_Notification"  // Event action (required)
                                                           label:@"ios_Notification"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUNotificationViewController class]])
    {
        if([self.drawerController presentedViewController])
        {
            [self.drawerController dismissViewControllerAnimated:NO completion:^{
                [self doShowNotification: initTab];
            }];
        }
        else
        {
            [self doShowNotification: initTab];
        }
         
    }
}

- (void)doShowNotification:(NotiTab) initTab
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp Show Notification"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_topBar_ShowNotification"  // Event action (required)
                                                           label:@"ios_ShowNotification"          // Event label
                                                           value:nil] build]];
    
    CMUNotificationViewController *noti = [[CMUNotificationViewController alloc] init];
    noti.selectedTab = initTab;
    UINavigationController *nc = [[CMUNavigationController alloc] initWithRootViewController:noti];
    [CMUUIUtils customizeNavigationBar:nc.navigationBar];
    noti.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.drawerController.centerViewController presentViewController:nc animated:NO completion:nil];

}

- (void)goREG
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp REG"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_menu_REG"  // Event action (required)
                                                           label:@"ios_REG"          // Event label
                                                           value:nil] build]];
    
    CMUWebServiceManager *webserviceManager = [CMUWebServiceManager sharedInstance];
    
    CMURegViewController *regWebViewController = [[CMURegViewController alloc] init];
    regWebViewController.url = CMU_REG_URL;
    regWebViewController.headers = [NSDictionary dictionaryWithObjectsAndKeys:
                            webserviceManager.userInfoModel.user.studentId,@"student_id",
                            webserviceManager.ticket.accessToken, @"access_token",
                            nil];
    [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:regWebViewController]];
    
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}


- (void)goSIS
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp SIS"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_menu_SIS"  // Event action (required)
                                                           label:@"ios_SIS"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISViewController class]])
    {
        CMUSISViewController *sisViewController = [[CMUSISViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:sisViewController]];
    }
    else
    {
        CMUSISViewController *sisViewController = (CMUSISViewController *)viewControler;
        [sisViewController refreshData:YES];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goPCheck
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp PCheck"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_menu_PCheck"  // Event action (required)
                                                           label:@"ios_PCheck"          // Event label
                                                           value:nil] build]];
    
    [SVProgressHUD showWithStatus:CMU_HUD_LOADING maskType:SVProgressHUDMaskTypeClear];
    [[CMUWebServiceManager sharedInstance] getPCheckQuestion:self success:^(id result) {
        [SVProgressHUD dismiss];
        CMUSISPCheckModel *pCheckModel = (CMUSISPCheckModel *)result;
        if(pCheckModel.status)
        {
            CMUSISPCheckViewController *sisViewController = [[CMUSISPCheckViewController alloc] init];
            sisViewController.pCheckModel = pCheckModel;
            [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:sisViewController]];
            [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
            }];
        }
        else
        {
            //go web
            CMUSISSummaryViewController *sisSummaryViewController = [[CMUSISSummaryViewController alloc] init];
            sisSummaryViewController.url = pCheckModel.link;
            [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:sisSummaryViewController]];
            [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
            }];

        }
        
    } failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:CMU_ERROR_READ_DATA
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)goELearning
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp ELearning"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_ELearning"  // Event action (required)
                                                           label:@"ios_ELearning"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUOnlineMenuViewController class]])
    {
        CMUOnlineMenuViewController *viewController = [[CMUOnlineMenuViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:viewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goMIS
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp MIS"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_menu_MIS"  // Event action (required)
                                                           label:@"ios_MIS"          // Event label
                                                           value:nil] build]];
    
    CMUMISEDocViewController *sisViewController = [[CMUMISEDocViewController alloc] init];
    [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:sisViewController]];
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goCMUEtheses
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp CMUEtheses"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_CMUEtheses"  // Event action (required)
                                                           label:@"ios_MIS"          // Event label
                                                           value:nil] build]];
    
    NSString *customURL = @"cmu-etheses://?token=123abc";
    
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:customURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
    }
    else
    {
        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id662785682?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}

- (void)goLICMUKids
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp LICMUKids"];
    //[tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_LICMUKids"  // Event action (required)
                                                           label:@"ios_LICMUKids"          // Event label
                                                           value:nil] build]];
    
    [self connectApp:APPSTORE_ID_LICMU_KIDS schemas:APP_SCHEMA_LICMU_KIDS];
}

- (void)goLICMUConversation
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp LICMUConversation"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_LICMUConversation"  // Event action (required)
                                                           label:@"ios_LICMUConversation"          // Event label
                                                           value:nil] build]];
    
    [self connectApp:APPSTORE_ID_LICMU_CONVERSATION schemas:APP_SCHEMA_LICMU_CONVERSATION];
}

- (void)goLICMUGrammar
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp LICMUGrammar"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_LICMUGrammar"  // Event action (required)
                                                           label:@"ios_LICMUGrammar"          // Event label
                                                           value:nil] build]];
    
    [self connectApp:APPSTORE_ID_LICMU_GRAMMA schemas:APP_SCHEMA_LICMU_GRAMMA];
}

- (void)goLICMUDiploma
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp LICMUDiploma"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_LICMUDiploma"  // Event action (required)
                                                           label:@"ios_LICMUDiploma"          // Event label
                                                           value:nil] build]];
    
    [self connectApp:APPSTORE_ID_LICMU_DIPLOMA schemas:APP_SCHEMA_LICMU_DIPLOMA];
}

- (void)goCMU5Dec
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp CMU5Dec"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_CMU5Dec"  // Event action (required)
                                                           label:@"ios_CMU5Dec"          // Event label
                                                           value:nil] build]];
    
    [self connectApp:APPSTORE_ID_LICMU_5DEC schemas:APP_SCHEMA_LICMU_5DEC];
}

//  Edited by Satianpong Yodnin on 9/9/2558 BE
- (void)goConnectMyFlashCard
{
    [self connectApp2:APPSTORE_ID_ENCONCEPT_MYFLASHCARD schemas:APP_SCHEMA_ENCONCEPT_MYFLASHCARD];
}

- (void)connectApp2:(NSString *) appId schemas:(NSString *) schemas
{
    
    NSString *jsonStr = @"{}";
    CMUWebServiceManager *ws = [CMUWebServiceManager sharedInstance];
    if([ws isLogin])
    {
        CMUTicket *ticket = ws.ticket;
        CMUUser *user = ws.userInfoModel.user;
        NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
        [json setObject:ticket.accessToken  forKey:@"token"];
        [json setObject:ticket.userName  forKey:@"userName"];
        [json setObject:user.firstNameEN forKey:@"firstNameEN"];
        [json setObject:user.lastNameEN forKey:@"lastNameEN"];
        [json setObject:user.firstNameTH forKey:@"firstNameTH"];
        [json setObject:user.lastNameTH  forKey:@"lastNameTH"];
        [json setObject:@"CMU" forKey:@"type"];
        
        if([ticket getUserType]==CMU_USER_TYPE_STUDENT)
        {
            [json setObject:user.studentId forKey:@"studentCode"];
            [json setObject:user.facultyCode forKey:@"organizationCode"];
        }
        else
        {
            [json setObject:@"" forKey:@"studentCode"];
            [json setObject:user.organizationCode forKey:@"organizationCode"];
        }
        
        jsonStr = [json JSONString];
    }
    
    NSString *customURL = [NSString stringWithFormat: @"%@://vars=%@", schemas, [jsonStr urlencode] ];
    DebugLog(@"custom url:%@", customURL);
    NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/us/app/apple-store/id%@?mt=8",appId];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"enconcept-myflashcard://"]])
    {
        NSLog(@"Now run MyFlashcard.");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
    }
    else
    {
        NSLog(@"Failed to run MyFlashcard.");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}


- (void)connectApp:(NSString *) appId schemas:(NSString *) schemas
{
    NSString *jsonStr = @"{}";
    CMUWebServiceManager *ws = [CMUWebServiceManager sharedInstance];
    if([ws isLogin])
    {
        CMUTicket *ticket = ws.ticket;
        CMUUser *user = ws.userInfoModel.user;
        NSMutableDictionary *json = [[NSMutableDictionary alloc] init];
        [json setObject:ticket.accessToken  forKey:@"token"];
        [json setObject:ticket.userName  forKey:@"userName"];
        [json setObject:user.firstNameEN forKey:@"firstNameEN"];
        [json setObject:user.lastNameEN forKey:@"lastNameEN"];
        [json setObject:user.firstNameTH forKey:@"firstNameTH"];
        [json setObject:user.lastNameTH  forKey:@"lastNameTH"];
        
        if([ticket getUserType]==CMU_USER_TYPE_STUDENT)
        {
            [json setObject:user.studentId forKey:@"studentCode"];
            [json setObject:user.facultyCode forKey:@"facultyCode"];
        }
        else
        {
            [json setObject:@"" forKey:@"studentCode"];
            [json setObject:user.organizationCode forKey:@"organizationCode"];
        }
        
        jsonStr = [json JSONString];
        
    }
    
    NSString *customURL = [NSString stringWithFormat: @"%@://?%@", schemas, [jsonStr urlencode] ];
    DebugLog(@"custom url:%@", customURL);
    NSString *iTunesLink = [NSString stringWithFormat:@"itms://itunes.apple.com/us/app/apple-store/id%@?mt=8",appId];
    
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:customURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    
}

- (void)goAbout
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp About"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_About"  // Event action (required)
                                                           label:@"ios_About"          // Event label
                                                           value:nil] build]];
    
    CMUAboutViewController *aboutViewController = [[CMUAboutViewController alloc] init];
    [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:aboutViewController]];
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goPhotoContest
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp PhotoContest"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_PhotoContest"  // Event action (required)
                                                           label:@"ios_PhotoContest"          // Event label
                                                           value:nil] build]];
    
    CMUContestViewController *viewController = [[CMUContestViewController alloc] init];
    [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:viewController]];
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

- (void)goITSC
{
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"iOS InApp ITSC"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ios_ui_action"     // Event category (required)
                                                          action:@"ios_home_inApp_ITSC"  // Event action (required)
                                                           label:@"ios_ITSC"          // Event label
                                                           value:nil] build]];
    
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUITSCInformationViewController class]])
    {
        CMUITSCInformationViewController *informationViewController = [[CMUITSCInformationViewController alloc] init];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:informationViewController]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

//  Edited by Satianpong Yodnin on 1/10/2558 BE
- (void)goCMUTransit
{
//New transit with WebView
    NSLog(@"transit webview");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[TransitWebViewViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"TransitWebView" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TransitWebView"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
    
    
    
//OLD TRANSIT
//    UIViewController *viewControler = [self getCurrentDisplayViewController];
//    if(!viewControler || ![viewControler isKindOfClass:[CMUITSCInformationViewController class]])
//    {
//        CMUTransitViewController *transitViewController = [[CMUTransitViewController alloc] init];
//        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:transitViewController]];
//    }
//    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
//    }];
    
}

- (void) goConnectMyEcho{
    [self connectApp2:APPSTORE_ID_ENCONCEPT_MYECHO schemas:APP_SCHEMA_ENCONCEPT_MYECHO];
}
-(void)goEResearch{
     NSString *iTunesLink = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",APPSTORE_ID_ERESEARCH];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}
-(void)goJobManagement{
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISJobViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"SISJob" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SISJob"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
-(void)goScholarship{
    NSLog(@"scholarship");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISScholarshipViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"SISScholarship" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SISScholarship"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}

-(void)goRequest{
    NSLog(@"request");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISScholarshipViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"SISScholarship" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SISScholarship"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
-(void)goAdviser{
    NSLog(@"adviser");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISAdviserViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"CMUSISAdviser" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SISAdviser"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
-(void)goActivity{
    
    NSLog(@"activity");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[ActivityViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"CMUSISActivity" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SISActivity"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];

}
-(void)goRegStudent{
    NSLog(@"goRegStudent");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[CMUSISAdviserViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"CMUSISGridMenu" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"gridMenu"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
    
}

- (void)goFeedback
{
    
    CMUTicket *userticket = [[CMUWebServiceManager sharedInstance] ticket];
    if(userticket==NULL){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"btnLoginAction" object:nil];
    }
    else{
       
    NSLog(@"goRegStudent");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[FeedbackViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"feedbackStoryboard" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"feedback"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
    }

    
}
-(void)goCMUEnglish{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cmuonlineenglish://"]])
    {
        NSLog(@"Now run CMUEnglish.");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cmuonlineenglish://"]];
    }
    else
    {
        NSLog(@"Failed to run CMUEnglish.");
        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id1208400438?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}

-(void)goNewEdoc
{
    NSLog(@"goNewEdoc");
    UIViewController *viewControler = [self getCurrentDisplayViewController];
    if(!viewControler || ![viewControler isKindOfClass:[NewEdocViewController class]])
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"NewEdocStoryboard" bundle:nil];
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"newEdoc"];
        [self.navigationController setViewControllersWithFadeAnimation:[NSArray arrayWithObject:controller]];
    }
    [self.drawerController  closeDrawerAnimated:YES completion:^(BOOL finished) {
    }];
}
#pragma mark IIViewDeckControllerDelegate

//- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer
//{
//    CMUViewController *topViewController = (CMUViewController *)self.navigationController.visibleViewController;
//    return [topViewController viewDeckController:viewDeckController shouldPan:panGestureRecognizer];
//}
//
//- (void)viewDeckController:(IIViewDeckController*)viewDeckController willOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
//{
//    if(viewDeckSide == IIViewDeckLeftSide)
//    {
//        [self.leftViewController updatePresentState];
//    }
//}
-(id)POSTRequest:(NSString*)poststr url:(NSString*)url{
    
    NSData *postData = [poststr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    // [request setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSData *objectData = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionaryData = [NSJSONSerialization JSONObjectWithData:objectData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    NSLog(@"[POST] URL :%@",url);
    NSLog(@"[POST] Str :%@", poststr);
    NSLog(@"[POST] return :%@",dictionaryData);
    return dictionaryData;
}
-(id)GetUserInfo
{
    NSString *strUrl = [NSString stringWithFormat:@"%@UserInfo.aspx?access_token=%@",END_POINT,[[NSUserDefaults standardUserDefaults]valueForKey:@"access_token"]];
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    //Set Header
    [dataRqst addValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"access_token"] forHTTPHeaderField: @"Authorization"];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",strUrl);
    NSLog(@"[GET] return :%@",json);
    return json;
}
-(void)regisMobileToken{
    NSString *str = [NSString stringWithFormat:REGIS_MOBILE_TOKEN,[[NSUserDefaults standardUserDefaults]valueForKey:@"account_name"],[[NSUserDefaults standardUserDefaults] valueForKey:@"access_token"]];
    NSDictionary *dict = [self GETRequestToken:str];
    [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"data" ] forKey:@"access_token"];
    if([[dict valueForKey:@"status"]isEqual:@"pass"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToInbox" object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goToNoPass" object:nil];
        [[NSUserDefaults standardUserDefaults]setBool:0 forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"access_token"];
        [[NSUserDefaults standardUserDefaults]setValue:@"null" forKey:@"account_name"];
        [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"noti_number"];
    }
}

-(id)GETRequestToken:(NSString*)URLString
{
    //Set Access Token String
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    
    //Set Header
    [dataRqst addValue:[userDefault valueForKey:@"access_token"] forHTTPHeaderField: @"token"];
    
    NSError* error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",URLString);
    NSLog(@"[GET] return :%@",json);
    return json;
}
- (void)saveTicket:(NSDictionary *)ticketDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:ticketDict];
    [defaults setObject:data forKey:KEY_CMU_USER_TICKET];
    [defaults synchronize];
    NSLog(@"saveTicket :%@",[defaults valueForKey:KEY_CMU_USER_TICKET]);
}
- (CMU_USER_TYPE) getUserType
{
    if([self.userName rangeOfString:@"."].location != NSNotFound)
    {
        return CMU_USER_TYPE_EMPLOYEE;
    }
    return CMU_USER_TYPE_STUDENT;
}

- (void)saveUserInfo:(NSDictionary *)userInfoDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userInfoDict];
    [defaults setObject:data forKey:KEY_CMU_USER_INFO_MODEL];
    [defaults synchronize];
}
@end
