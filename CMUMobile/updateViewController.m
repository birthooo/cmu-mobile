//
//  updateViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 11/29/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "updateViewController.h"

@interface updateViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *popupView;

@end

@implementation updateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://mobileapi.cmu.ac.th/LogUpdate/ios"]];
    [self.webView loadRequest:request];
    self.webView.scrollView.bounces = NO;
    self.popupView.layer.cornerRadius = 10.0;
    
    // Do any additional setup after loading the view.
}
- (IBAction)closeAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
