//
//  CMUInformationViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/11/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUInformationViewController.h"

@interface CMUInformationViewController ()
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionTitle;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionHistory;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionWish;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionDirector;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionAbout;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionVision;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionResponsibility;
@property(nonatomic, weak) IBOutlet CMUSectionInfoView *viewSectionObjective;

@end

@implementation CMUInformationViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewSectionTitle.sectionInfo = [[CMUSectionInfo alloc] initWith:@"Chiang Mai University" imageName:@"info_title.png" showBar:NO htmlFile:nil];
    self.viewSectionHistory.sectionInfo = [[CMUSectionInfo alloc] initWith:@"ประวัติความเป็นมา" imageName:@"info_history.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationHistory/Index"];
    self.viewSectionWish.sectionInfo = [[CMUSectionInfo alloc] initWith:@"ปณิธาน" imageName:@"info_wish.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationCommitment/Index"];
    self.viewSectionDirector.sectionInfo = [[CMUSectionInfo alloc] initWith:@"สารจากอธิการบดี" imageName:@"info_director.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationPresident/Index"];
    self.viewSectionAbout.sectionInfo = [[CMUSectionInfo alloc] initWith:@"เกี่ยวกับ มช." imageName:@"info_about.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationAbout/Index"];
    self.viewSectionVision.sectionInfo = [[CMUSectionInfo alloc] initWith:@"วิสัยทัศน์" imageName:@"info_vision.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationVision/Index"];
    self.viewSectionResponsibility.sectionInfo = [[CMUSectionInfo alloc] initWith:@"พันธกิจ" imageName:@"info_responsibility.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationResolution/Index"];
    self.viewSectionObjective.sectionInfo = [[CMUSectionInfo alloc] initWith:@"วัตถุประสงค์" imageName:@"info_objective.png" showBar:YES htmlFile:@"http://202.28.249.36/cmumobile/WebviewInformationObjective/Index"];
    
    self.viewSectionTitle.delegate = self;
    self.viewSectionHistory.delegate = self;
    self.viewSectionWish.delegate = self;
    self.viewSectionDirector.delegate = self;
    self.viewSectionAbout.delegate = self;
    self.viewSectionVision.delegate = self;
    self.viewSectionResponsibility.delegate = self;
    self.viewSectionObjective.delegate = self;
    
    self.viewSectionTitle.alpha = 0.0;
    self.viewSectionHistory.alpha = 0.0;
    self.viewSectionWish.alpha = 0.0;
    self.viewSectionDirector.alpha = 0.0;
    self.viewSectionAbout.alpha = 0.0;
    self.viewSectionVision.alpha = 0.0;
    self.viewSectionResponsibility.alpha = 0.0;
    self.viewSectionObjective.alpha = 0.0;
    
    [self showFlipAnimation];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showFlipAnimation
{
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionTitle afterDelay:0.1f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionHistory afterDelay:0.2f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionWish afterDelay:0.3f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionDirector afterDelay:0.4f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionAbout afterDelay:0.5f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionVision afterDelay:0.6f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionResponsibility afterDelay:0.7f];
    [self performSelector:@selector(flipAnimation:) withObject:self.viewSectionObjective afterDelay:0.8f];
}

-(void)flipAnimation:(UIView *) view{
    [UIView transitionWithView:view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft//|UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        view.alpha = 1.0;
                    } completion:^(BOOL finished) {
                        
                    }];
}



#pragma  mark -CMUSectionInfoView
- (void)sectionInfoViewDidClicked:(CMUSectionInfoView* )sectionInfoView
{
    //DebugLog(@"%@", sectionInfoView);
    CMUSectionInfo *sectionInfo = sectionInfoView.sectionInfo;
    if(sectionInfo.htmlFile == nil)
    {
        //top section don't have detail
        return;
    }
    
    CMUInformationDetailViewController *detailViewController = [[CMUInformationDetailViewController alloc] init];
    detailViewController.delegate = self;
    detailViewController.webTitle = sectionInfo.title;
    NSURL *url =  [NSURL URLWithString: sectionInfo.htmlFile];
    
    detailViewController.url = url;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - CMUInformationDetailViewControllerDelegate
- (void)infomationDetailViewControllerBackButtonClicked:(CMUInformationDetailViewController *) infomationDetailViewController
{
    [self.navigationController popToViewController:self animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
