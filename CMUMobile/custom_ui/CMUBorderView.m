//
//  CMUBorderView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/2/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUBorderView.h"
#import "CMUUIUtils.h"

@implementation CMUBorderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [CMUUIUtils dropShadow:self offset:0.5];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
