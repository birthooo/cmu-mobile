//
//  UIColor+CMU.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "UIColor+CMU.h"

@implementation UIColor(CMU)
+ (UIColor *)cmuNavigationBarColor
{
    return [UIColor colorWithRed:37.0/255.0f green:27.0/255.0f blue:53.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuLeftNavigationBarColor
{
    return [UIColor colorWithRed:72.0/255.0f green:29.0/255.0f blue:123.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuPurpleColor
{
    return [UIColor colorWithRed:146.0/255.0f green:118.0/255.0f blue:201.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuPurpleLightColor
{
    return [UIColor colorWithRed:170.0/255.0f green:157.0/255.0f blue:213.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuPurpleTrasparentColor
{
    return [UIColor colorWithRed:146.0/255.0f green:118.0/255.0f blue:201.0/255.0f alpha:0.5];
}

+ (UIColor *)cmuDarkTrasparentColor
{
    return [UIColor colorWithRed:0.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:0.5];
}

+ (UIColor *)cmuTableViewCellTextColor
{
    return [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuBlackColor
{
    return [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuMediumBlackColor
{
    return [UIColor colorWithRed:85.0/255.0f green:85.0/255.0f blue:85.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuLightBlackColor
{
    return [UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuYellowColor
{
    return [UIColor colorWithRed:255.0/255.0f green:160.0/255.0f blue:56.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuPhotoAndVideoBackgroundColor
{
    return [UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuEventCalendarCurrentDate
{
    return [UIColor colorWithRed:200.0/255.0f green:50.0/255.0f blue:150.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuEventCalendarPassDate
{
    return [UIColor colorWithRed:193.0/255.0f green:193.0/255.0f blue:193.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuEventCalendarFutureDate
{
    return [UIColor colorWithRed:100.0/255.0f green:68.0/255.0f blue:178.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuEventCalendarBackgroundColor
{
    return [UIColor colorWithRed:223.0/255.0f green:223.0/255.0f blue:223.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuLeftMenuPurpleColor
{
    return [UIColor colorWithRed:148.0/255.0f green:113.0/255.0f blue:210.0/255.0f alpha:1.0];
}

+ (UIColor *)cmuMISEdocLightPurpleColor
{
    return [UIColor colorWithRed:212.0/255.0f green:204.0/255.0f blue:244.0/255.0f alpha:1.0];
}
@end
