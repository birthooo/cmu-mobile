//
//  CMUTriangleView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUTriangleView : UIView
@property(nonatomic, strong)UIColor *pointerColor;
@end
