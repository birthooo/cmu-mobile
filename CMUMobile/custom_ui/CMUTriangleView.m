//
//  CMUTriangleView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUTriangleView.h"

@implementation CMUTriangleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setPointerColor:(UIColor *)pointerColor
{
    _pointerColor = pointerColor;
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint   (ctx, CGRectGetMinX(rect), CGRectGetMaxY(rect));  // bottom left
    CGContextAddLineToPoint(ctx, CGRectGetMidX(rect), CGRectGetMinY(rect));  // top center
    CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMaxY(rect));  // bottom left
    CGContextClosePath(ctx);
    
    CGContextSetFillColorWithColor(ctx, self.pointerColor == nil?[UIColor whiteColor].CGColor:self.pointerColor.CGColor);
    //CGContextSetRGBFillColor(ctx, 1, 1, 0, 1);
    CGContextFillPath(ctx);
}

@end
