//
//  CMUSimpleTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/29/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUSimpleTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UILabel* lblTitle;
@property(nonatomic, weak) IBOutlet UIImageView *imgAccesoryView;
@end
