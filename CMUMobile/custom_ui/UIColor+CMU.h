//
//  UIColor+CMU.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(CMU)
+ (UIColor *)cmuNavigationBarColor;
+ (UIColor *)cmuLeftNavigationBarColor;
+ (UIColor *)cmuPurpleColor;
+ (UIColor *)cmuPurpleLightColor;
+ (UIColor *)cmuPurpleTrasparentColor;
+ (UIColor *)cmuDarkTrasparentColor;
+ (UIColor *)cmuTableViewCellTextColor;
+ (UIColor *)cmuBlackColor;
+ (UIColor *)cmuMediumBlackColor;
+ (UIColor *)cmuLightBlackColor;
+ (UIColor *)cmuYellowColor;
+ (UIColor *)cmuPhotoAndVideoBackgroundColor;

+ (UIColor *)cmuEventCalendarCurrentDate;
+ (UIColor *)cmuEventCalendarPassDate;
+ (UIColor *)cmuEventCalendarFutureDate;
+ (UIColor *)cmuEventCalendarBackgroundColor;

+ (UIColor *)cmuLeftMenuPurpleColor;

+ (UIColor *)cmuMISEdocLightPurpleColor;
@end
