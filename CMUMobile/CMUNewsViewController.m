//
//  CMUNewsViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsViewController.h"
#import "ODRefreshControl.h"
#import "CMUWebServiceManager.h"
#import "CMUSettings.h"
#import "CMUModel.h"
#import "CMUStringUtils.h"
#import <Twitter/Twitter.h>
#import "CMUUtils.h"
#import <Accounts/Accounts.h>
#import "CMUNewsSettingPopupView.h"
#import "PopoverView.h"
#import "CMUNewsTypePopupView.h"
#import "CMUUIUtils.h"
#import "CMUStringUtils.h"
#import "CMUWebViewController.h"
#import "UIImageView+WebCache.h"
#import "CMUImageSizeCached.h"
#import "UIColor+CMU.h"
#import "CMUNewsIPadTableViewCell.h"
#import "CMUNewsIPadImageTableViewCell.h"
#import "CMUSocial.h"

static NSString *cellIdentifierIPhone = @"CMUNewsTableViewCell";
static NSString *cellIdentifierIPadNoImage = @"CMUNewsIPadTableViewCell";
static NSString *cellIdentifierIPadWithImage = @"CMUNewsIPadImageTableViewCell";

typedef enum {
    NEWS_BY_SETTINGS = 0,
    NEWS_BY_TYPE = 1
} NewsFetchType;

@interface CMUNewsViewController ()
@property(nonatomic, weak) IBOutlet UIView *topView;
@property(nonatomic, weak) IBOutlet UILabel *lblSelectedCategory;
@property(nonatomic, weak) IBOutlet UIButton *btnCategory;
@property(nonatomic, weak) IBOutlet UIButton *btnSettings;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) IBOutlet ODRefreshControl *refreshControl;
@property(nonatomic, strong) CMUNewsTypePopupView *newsTypeView;
@property(nonatomic, strong) CMUNewsSettingPopupView *newsSettingsView;
@property(nonatomic, strong) PopoverView *popoverView;

@property(nonatomic, unsafe_unretained) BOOL loadingMore;
@property(nonatomic, strong) UIActivityIndicatorView *loadMoreSpinner;

-(IBAction) lblSelectedCategoryTapped:(id) sender;
-(IBAction) btnCategoryClicked:(id) sender;
-(IBAction) btnSettingsClicked:(id) sender;

//fetch model
@property(nonatomic, unsafe_unretained)BOOL resetFeedList;
@property(nonatomic, unsafe_unretained)NewsFetchType newsFetchType;
@property(nonatomic, unsafe_unretained)CMU_NEWS_CATEGORY newsType;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSArray *feedList;

@property UIRefreshControl *refresh;
@end


@implementation CMUNewsViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.newsViewMode = NEWS_VIEW_MODE_LIST;
        self.newsFetchType = NEWS_BY_SETTINGS;
        self.newsType = -1;
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
    
    NSLog(@"NUMMMM :%@",self.num);
    if([_num isEqual:@"99"]){
        self.lblSelectedCategory.textColor = [UIColor cmuBlackColor];
        self.lblSelectedCategory.text = @"ข่าวทุนการศึกษา&แนะแนวนักศึกษา";
        self.newsTypeView = [[CMUNewsTypePopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];
        self.newsTypeView.delegate = self;
        
        self.newsSettingsView = [[CMUNewsSettingPopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];
        //pull refresh
        self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];

        [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
        
        [self showHUDLoading];
        //[self getNewsBySettings];
        [self getNewsByType:2];
        self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
        NSLog(@"333333333");
        
    }
    else if([_num isEqual:@"98"]){
        
        self.lblSelectedCategory.textColor = [UIColor cmuBlackColor];
        //popover view content
        self.newsTypeView = [[CMUNewsTypePopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];
        self.newsTypeView.delegate = self;
        
//        self.refresh = [[UIRefreshControl alloc]init];
//        [self.tableView addSubview:self.refresh];
//        [self.refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
//        
        self.newsSettingsView = [[CMUNewsSettingPopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];

        //
//        self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
//        [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
        // [self showHUDLoading];
        [self getNewsBySettings];
        // [self getNewsByType:2];
//        self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
        NSLog(@"00000000");
    }
    else{
        [self.topView removeFromSuperview];
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getNewsDetail:self itemId:self.newsDetailItemId success:^(id result) {
            CMUNewsModel *newsModel = (CMUNewsModel *)result;
            if(![CMUStringUtils isEmpty:newsModel.refreshURL])
            {
                self.refreshURL = newsModel.refreshURL;
            }
            [self populateFeedList:newsModel];
            [self dismisHUD];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
        
    }
    /*
    if(_newsViewMode == NEWS_VIEW_MODE_LIST)
    {
        self.lblSelectedCategory.textColor = [UIColor cmuBlackColor];
        //popover view content
        self.newsTypeView = [[CMUNewsTypePopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];
        self.newsTypeView.delegate = self;
        
        self.newsSettingsView = [[CMUNewsSettingPopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 264): CGRectMake(0, 0, 280, 176)];
        //pull refresh
        self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
        [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
        
        [self showHUDLoading];
        [self getNewsBySettings];
        
        self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
        NSLog(@"111111111");
    }
    else
    {
        NSLog(@"222222222");
        [self.topView removeFromSuperview];
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getNewsDetail:self itemId:self.newsDetailItemId success:^(id result) {
            CMUNewsModel *newsModel = (CMUNewsModel *)result;
            if(![CMUStringUtils isEmpty:newsModel.refreshURL])
            {
                self.refreshURL = newsModel.refreshURL;
            }
            [self populateFeedList:newsModel];
            [self dismisHUD];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
    }
    */
    //tableview
    if(IS_IPHONE)
    {
        [self.tableView registerNib:[UINib nibWithNibName:@"CMUNewsTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPhone];
    }
    else
    {
        [self.tableView registerNib:[UINib nibWithNibName:@"CMUNewsIPadTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPadNoImage];
        [self.tableView registerNib:[UINib nibWithNibName:@"CMUNewsIPadImageTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPadWithImage];
    }
    
    [self updatePresentState];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.refresh = [[UIRefreshControl alloc]init];
         [self.tableView addSubview:self.refresh];
            [self.refresh addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}
- (void)refreshTable {
    //TODO: refresh your data
    
    [[CMUWebServiceManager sharedInstance] getNewsByURL:self url:self.refreshURL success:^(id result) {
        CMUNewsModel *newsModel = (CMUNewsModel *)result;
        if(![CMUStringUtils isEmpty:newsModel.refreshURL])
        {
            self.refreshURL = newsModel.refreshURL;
        }
        [self populateFeedList:newsModel];
        
        [self.refresh endRefreshing];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
         [self.refresh endRefreshing];
    }];

    
    //
//    [self.refresh endRefreshing];
//    [self.tableView reloadData];
}
- (void)updatePresentState
{
    if(self.newsViewMode == NEWS_VIEW_MODE_LIST)
    {
        [super updatePresentState];
        
        if(self.loadingMore && self.tableView.tableFooterView == nil)
        {
            self.tableView.tableFooterView = self.loadMoreSpinner;
           // [self.loadMoreSpinner startAnimating];
        }
        else
        {
            self.tableView.tableFooterView = nil;
            [self.loadMoreSpinner stopAnimating];
        }
    }
    else
    {
        self.navigationItem.title = @"News";
        //  just show default back button in news detail mode
    }
}

- (void)getNewsBySettings
{
    //just for animation
    //[self.cachedImageSize removeAllObjects];
    
    self.resetFeedList = YES;
    [[CMUWebServiceManager sharedInstance] getNewsBySettings:self newForPostSettings:[CMUSettings sharedInstance].newsSetting success:^(id result) {
        CMUNewsModel *newsModel = (CMUNewsModel *)result;
        self.refreshURL = [CMUStringUtils isEmpty:newsModel.refreshURL]? nil: newsModel.refreshURL;
        self.loadMoreURL = [CMUStringUtils isEmpty:newsModel.loadMoreURL]? nil: newsModel.loadMoreURL;
        [self populateFeedList:result];
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self populateFeedList:nil];
        [self updatePresentState];
        [self dismisHUD];
    }];
}

- (void)getNewsByType:(CMU_NEWS_CATEGORY) selectedCategory
{
    //just for animation
    //[self.cachedImageSize removeAllObjects];
    
    //TODO check same category?
    self.resetFeedList = YES;
    self.newsType = selectedCategory;
    [[CMUWebServiceManager sharedInstance] getNewsByType:self type:selectedCategory success:^(id result) {
        CMUNewsModel *newsModel = (CMUNewsModel *)result;
        self.refreshURL = [CMUStringUtils isEmpty:newsModel.refreshURL]? nil: newsModel.refreshURL;
        self.loadMoreURL = [CMUStringUtils isEmpty:newsModel.loadMoreURL]? nil: newsModel.loadMoreURL;
        [self populateFeedList:result];
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self populateFeedList:nil];
        [self updatePresentState];
        [self dismisHUD];
    }];
}

- (void)populateFeedList:(CMUNewsModel *) newsModel
{
    //check to reset feed list
    BOOL scrollToTop = self.resetFeedList;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    if(!self.resetFeedList)
    {
        [tempFeedList addObjectsFromArray:self.feedList];
    }
    self.resetFeedList = NO;
    
    //add new feed to feed list
    for(CMUNewsFeed *newFeed in newsModel.newsFeedList)
    {
        if(![tempFeedList containsObject:newFeed])
        {
            [tempFeedList addObject:newFeed];
        }
    }
    
    //perform sort
    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        CMUNewsFeed *first = (CMUNewsFeed*)a;
        CMUNewsFeed *second = (CMUNewsFeed*)b;
        return [second.update compare:first.update];
    }];
    
    self.feedList = sortedFeedList;
    
    [self.tableView reloadData];
    if(scrollToTop)
    {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dropViewDidBeginRefreshing:(id) sender
{
    if(self.refreshURL)
    {
        [[CMUWebServiceManager sharedInstance] getNewsByURL:self url:self.refreshURL success:^(id result) {
            CMUNewsModel *newsModel = (CMUNewsModel *)result;
            if(![CMUStringUtils isEmpty:newsModel.refreshURL])
            {
                self.refreshURL = newsModel.refreshURL;
            }
            [self populateFeedList:newsModel];
            [self.refreshControl endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControl endRefreshing];
        }];
    }
    else
    {
         [self.refreshControl endRefreshing];
    }
}

-(void) lblSelectedCategoryTapped:(id) sender
{
    //[self.refreshControl beginRefreshing];
    //[self showHUDLoading];
    //[self getNewsBySettings];
    [self btnCategoryClicked:sender];
}

-(void) btnCategoryClicked:(id) sender
{
    CGRect newsTypeButtonFrame = [self.view convertRect:self.btnCategory.frame fromView:self.topView];
    CGPoint pointToShow = CGPointMake(newsTypeButtonFrame.origin.x + newsTypeButtonFrame.size.width / 2, newsTypeButtonFrame.origin.y + newsTypeButtonFrame.size.height - 5);
    
//    CGRect topViewFrame = self.topView.frame;
//    CGPoint pointToShow = CGPointMake(topViewFrame.origin.x + topViewFrame.size.width / 2, topViewFrame.origin.y + topViewFrame.size.height - 5);
    self.popoverView = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverView.delegate = self;
    [self.popoverView showAtPoint:pointToShow inView:self.view withContentView:self.newsTypeView];

}

-(void) btnSettingsClicked:(id) sender
{
    CGRect newsSettingsButtonFrame = [self.view convertRect:self.btnSettings.frame fromView:self.topView];
    CGPoint pointToShow = CGPointMake(newsSettingsButtonFrame.origin.x + newsSettingsButtonFrame.size.width / 2, newsSettingsButtonFrame.origin.y + newsSettingsButtonFrame.size.height - 5);
    
//    CGRect topViewFrame = self.topView.frame;
//    CGPoint pointToShow = CGPointMake(topViewFrame.origin.x + topViewFrame.size.width / 2, topViewFrame.origin.y + topViewFrame.size.height - 5);
    self.popoverView = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverView.delegate = self;
    [self.popoverView showAtPoint:pointToShow inView:self.view withContentView:self.newsSettingsView];
}
- (void)openNewsFeed:(CMUNewsFeed *) newsFeed
{
    if(![CMUStringUtils isEmpty:newsFeed.link])
    {
        if(newsFeed.isOpenBrowser)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newsFeed.link]];
        }
        else
        {
            CMUWebViewController *webViewController = [[CMUWebViewController alloc] init];
            webViewController.url = newsFeed.link;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
    }
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUNewsFeed *newsFeed =[self.feedList objectAtIndex:indexPath.row];
    BOOL hasImage =![CMUStringUtils isEmpty:newsFeed.imageLink];
    
    CGFloat cellWidth = self.tableView.frame.size.width;
    CGFloat height = 0;
    
    
    if(IS_IPHONE)
    {
        height = 302 - (17 + 179);// 17 label nib, 179 image view nib
        /* text (duplicate code  in tableviewcell .mm)*/
        //iPhone
        CGFloat labelWidth = cellWidth - (7 + 10 + 10 + 7); // autolayput constraint (7,7 border, content view), (11,10 , label, border)
        UIFont *font = [UIFont systemFontOfSize:14.0]; //according to iphone nib
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:newsFeed.topic withFont:font];
        height += size.height;
        /*image*/
        //iphone
        
        if(hasImage)
        {
            NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:newsFeed.imageLink];
            if(cachedSizeValue)
            {
                CGSize originalSize = [cachedSizeValue CGSizeValue];
                CGFloat imageWidth = cellWidth - (7 + 7); // autolayput constraint (7,7 border, content view)
                CGFloat imageHeight = 0;
                @try {
                    imageHeight = imageWidth * originalSize.height / originalSize.width;
                }
                @catch (NSException *exception) {
                }
                @finally {
                }
                
                height += imageHeight;
            }
        }
    }
    else
    {
        if(hasImage)
        {
            //right section
            CGFloat rightSectionHeight = 45 + 25;//social tab + border top padding
            NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:newsFeed.imageLink];
            
            // create square rectangle for temporary image space
//            if(!cachedSizeValue)
//            {
//                cachedSizeValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
//            }
            
            if(cachedSizeValue)
            {
                CGSize originalSize = [cachedSizeValue CGSizeValue];
                CGFloat imageWidth = (cellWidth - (75 + 75)) / 2; // autolayput constraint (10,10 border, content view)   /2 =  image locate in right section
                CGFloat imageHeight = 0;
                @try {
                    imageHeight = imageWidth * originalSize.height / originalSize.width;
                }
                @catch (NSException *exception) {
                }
                @finally {
                }
                
                rightSectionHeight += imageHeight;
            }
            
            //left section
            CGFloat leftSectionHeight = 75 + 15 + 45 + 25;// y positin of label in nib, 15 = expect bottom space, 45 social, 25 =  border top padding
            CGFloat labelWidth = (cellWidth - (75 + 75)) / 2  - 15 - 15;
            UIFont *font = [UIFont systemFontOfSize:20.0]; //according to iphone nib
            CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:newsFeed.topic withFont:font];
            leftSectionHeight += size.height;
            
            height = rightSectionHeight > leftSectionHeight? rightSectionHeight: leftSectionHeight;
        }
        else
        {
            height = 311 - 150;//refer from nib (content view height - imageview height)
            CGFloat labelWidth = cellWidth - (75 + 75 + 15 + 15);
            UIFont *font = [UIFont systemFontOfSize:20.0]; //according to iphone nib
            CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:newsFeed.topic withFont:font];
            height += size.height;
        }
    }
    return height + 1;//+1 = tableview cell line seperator
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUNewsTableViewCell *cell = nil;
    CMUNewsFeed *newsFeed = [self.feedList objectAtIndex:indexPath.row];
    if(IS_IPHONE)
    {
        cell = (CMUNewsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPhone forIndexPath:indexPath];
    }
    else
    {
        BOOL hasImage =![CMUStringUtils isEmpty:newsFeed.imageLink];
        if(hasImage)
        {
            cell = (CMUNewsIPadImageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPadWithImage forIndexPath:indexPath];
        }
        else
        {
            cell = (CMUNewsIPadTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPadNoImage forIndexPath:indexPath];
        }
    }
    
    cell.newsFeed = newsFeed;
    BOOL hasImage =![CMUStringUtils isEmpty:newsFeed.imageLink];
    if(hasImage)
    {
        //__weak typeof(self) weakSelf = self;
        [cell.newsImageView setImageWithURL:[NSURL URLWithString:newsFeed.imageLink] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if(image && error == nil)
            {
                NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:newsFeed.imageLink];
                
                if(cachedSizeValue == nil)
                {
                    CGSize originalImageSize = image.size;
                    cachedSizeValue = [NSValue valueWithCGSize:originalImageSize];
                    [[CMUImageSizeCached sharedInstance] setImageSizeCached:originalImageSize forURL:newsFeed.imageLink];
                    [tableView beginUpdates];
                    [tableView reloadRowsAtIndexPaths:@[indexPath]
                                     withRowAnimation:UITableViewRowAnimationFade];
                    [tableView endUpdates];
                }
            }
        }];
    }
    
    cell.newsImageView.hidden = !hasImage;
    
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self openNewsFeed:[self.feedList objectAtIndex:indexPath.row]];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if(self.loadMoreURL)
        {
            if(self.loadingMore)
            {
                return;
            }
            self.loadingMore = YES;
            [self updatePresentState];
            [[CMUWebServiceManager sharedInstance] getNewsByURL:self url:self.loadMoreURL success:^(id result) {
                CMUNewsModel *newsModel = (CMUNewsModel *)result;
                self.loadMoreURL = [CMUStringUtils isEmpty:newsModel.loadMoreURL]? nil: newsModel.loadMoreURL;
                [self populateFeedList:result];
                self.loadingMore = NO;
                [self updatePresentState];
            } failure:^(NSError *error) {
                self.loadingMore = NO;
                [self updatePresentState];
            }];
        }
    }
}

#pragma mark -CMUNewsTableViewCellDelegate
- (void)newsTableViewCellDidSelectedRead:(CMUNewsTableViewCell *) cell
{
    [self openNewsFeed:cell.newsFeed];
}

- (void)newsTableViewCellDidSelectedFacebook:(CMUNewsTableViewCell *) cell
{
//    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
//    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
//    //TODO insert facebook appid here
//    [accountStore requestAccessToAccountsWithType:accountType options:@{ ACFacebookAppIdKey: @"-snip-"}
//                                       completion:^(BOOL granted, NSError *error)
//    {
//        if (!granted) {
//            switch (error.code) {
//                case ACErrorAccountNotFound: {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                        [self presentViewController:composeViewController animated:NO completion:^{
//                            //[composeViewController dismissViewControllerAnimated:NO completion:nil];
//                        }];
//                    });
//                    break;
//                }
//                default: {
//                    break;
//                }
//            }
//            return;
//        }
//    }];

    CMUNewsFeed *newsFeed = cell.newsFeed;
#ifdef USE_FACEBOOK_SDK_SHARE
    [CMUSocial shareFacebookWithTitle:newsFeed.topic url:newsFeed.link desc:newsFeed.detail pictureUrl:[CMUStringUtils isEmpty:newsFeed.imageLink]? nil: newsFeed.imageLink];
#else
    //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        
        [controller setInitialText:newsFeed.topic];
        [controller addURL:[NSURL URLWithString:newsFeed.link]];
        //TODO adding icon?
        //[controller addImage:[UIImage imageNamed:@"myImage.png"]];
        [self presentViewController:controller animated:YES completion:nil];
    //}
#endif
    
}

- (void)newsTableViewCellDidSelectedTwitter:(CMUNewsTableViewCell *) cell
{
//    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
//    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
//    //TODO check for twitter options
//    [accountStore requestAccessToAccountsWithType:accountType options: nil /*@{ ACFacebookAppIdKey: @"-snip-"}*/
//                                       completion:^(BOOL granted, NSError *error)
//     {
//         if (!granted) {
//             switch (error.code) {
//                 case ACErrorAccountNotFound: {
//                     dispatch_async(dispatch_get_main_queue(), ^{
//                         SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//                         [self presentViewController:composeViewController animated:NO completion:^{
//                             //[composeViewController dismissViewControllerAnimated:NO completion:nil];
//                         }];
//                     });
//                     break;
//                 }
//                 default: {
//                     break;
//                 }
//             }
//             return;
//         }
//     }];
    
    CMUNewsFeed *newsFeed = cell.newsFeed;
    //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
     
        [controller setInitialText:newsFeed.topic];
        [controller addURL:[NSURL URLWithString:newsFeed.link]];
        //TODO adding icon?
        //[controller addImage:[UIImage imageNamed:@"myImage.png"]];
        [self presentViewController:controller animated:YES completion:Nil];
        
   // }
}


#pragma mark -PopoverViewDelegate
//Delegate receives this call once the popover has begun the dismissal animation
- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    [[CMUSettings sharedInstance] saveNewsSettings];
}

//Delegate receives this call as soon as the item has been selected
- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    
}

#pragma mark -CMUNewsTypePopupViewDelegate
- (void)newsTypePopupView:(CMUNewsTypePopupView *) newsTypePopupView didSelectedCategory:(CMUNewsCategory *) category
{
    [self.popoverView dismiss];
    //[self.refreshControl beginRefreshing];
    [self showHUDLoading];
    
    if(category.categoryId == CMU_NEWS_CATEGORY_TYPE_ALL)
    {
        [self getNewsBySettings];
    }
    else
    {
        [self getNewsByType:category.categoryId];
    }
    
   
        
        self.lblSelectedCategory.text = category.title;
    
    
    
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
       // [activityIndicator stopAnimating];
        [self.refreshControl endRefreshing];
        [self.loadMoreSpinner stopAnimating];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
