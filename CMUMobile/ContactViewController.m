//
//  ContactViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 1/13/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactTableViewCell.h"
#import "CMUAppDelegate.h"
#import "global.h"
#define AppDelegate ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate])

@interface ContactViewController ()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end

@implementation ContactViewController
{
    NSArray *nameEmergency;
    NSArray *numEmergency;
    
    NSArray *name;
    NSArray *num;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    nameEmergency = @[@"สายด่วนฉุกเฉิน",@"แพทย์ฉุกเฉิน",@"แจ้งรถหาย",@"แจ้งเหตุไฟไหม้"];
    numEmergency = @[@"191",@"1669",@"1192",@"199"];
    name = @[@"กองพัฒนานักศึกษา",@"กองพัฒนานักศึกษา (กยศ. / กรอ.)",@"สำนักทะเบียน",@"กองคลัง",@"สำนักงานหอพัก",@"สำนักหอสมุด",@"สำนักบริการเทคโนโลยีสารสนเทศ",@"One Stop Services (ITSC CMU)",@"รปภ. (งานรักษาความปลอดภัย มช.)",@"รถไฟฟ้า มช.",@"สโมสรนักศึกษา มหาวิทยาลัยเชียงใหม่ (อมช.)",@"คลินิกศูนย์สุขภาพมหาวิทาลัยเชียงใหม่ (ไผ่ล้อม)"];
    num = @[@"053-943-039 ",@"053-941-360",@"053-948-901",@"053-943-122-3",@"053-944-743",@"053-944-531",@"053-943-811 ",@"053-943-827",@"053-941-190-1",@"053-943-064",@"053-943-053 ",@"053943-181"];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"]isEqual:@"cellContact"]){
        self.headerLabel.text = @"เบอร์โทรฉุกเฉิน";
    }
    else{
        self.headerLabel.text = @"เบอร์โทรในหน่วยงานสำคัญสำหรับนักศึกษา";
    }
}
- (IBAction)backButtonAction:(id)sender {
    [AppDelegate goPhone];
   // [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)emergencyButtonAction:(id)sender {
        [[NSUserDefaults standardUserDefaults]setValue:@"cellContact" forKey:@"numType"];
    [AppDelegate contactDetail];
}
- (IBAction)studentButtonAction:(id)sender {
    [[NSUserDefaults standardUserDefaults]setValue:@"cellNum" forKey:@"numType"];
      [AppDelegate contactDetail];
}
- (IBAction)allPhoneButtonAction:(id)sender {
     [AppDelegate goPhone2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"]isEqual:@"cellContact"]){
        return nameEmergency.count;
    }
    else{
        return name.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"] forIndexPath:indexPath];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"]isEqual:@"cellContact"]){
        cell.nameLabel.text = [NSString stringWithFormat:@"%@   %@",numEmergency[indexPath.row],nameEmergency[indexPath.row]];
    }
    else{
        cell.name.text = name[indexPath.row];
        cell.num.text = num[indexPath.row];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"]isEqual:@"cellContact"]){
           NSString *phoneNumber = [@"telprompt://" stringByAppendingString:numEmergency[indexPath.row]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else{
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:num[indexPath.row]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }


    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"numType"]isEqual:@"cellContact"]){
        return 48;
    }
    else{
        return 65;
    }
    
}
@end
