//
//  CMUPhoneViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/3/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUPhoneVersionController.h"
#import "CMUPhoneFacultyViewController.h"

//  Edited by Satianpong Yodnin on 9/9/2558 BE.
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface CMUPhoneViewController : CMUNavBarViewController<CMUPhoneVersionControllerDelegate, CMUPhoneFacultyViewControllerDelegate>

@end
