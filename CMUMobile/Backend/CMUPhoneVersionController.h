//
//  CMUPhoneVersionController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMUModel.h"

typedef enum{
    CMUPhoneVersionControllerState_NoProgress,
    CMUPhoneVersionControllerState_ProgressCheckServerVersion,
    CMUPhoneVersionControllerState_ProgressDownloadServerVersion
} CMUPhoneVersionControllerProgressState;


@class CMUPhoneVersionController;
@protocol CMUPhoneVersionControllerDelegate
@required
#pragma mark check version delegate
- (void)phoneVersionControllerDidStartedCheckNewVersion:(CMUPhoneVersionController *)versionController;
- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didFinishedCheckNewVersion:(CMUPhoneVersionModel *) newVersion//nil = no update
                    error:(NSError*) error;// not nil = error occured
#pragma mark download delegate
- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didStartedDownloadVersion:(CMUPhoneVersionModel *) newVersion;
- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didFinishedDownloadVersion:(CMUPhoneVersionModel *) serverVersion
                     phoneFacultyViewModel:(CMUPhoneFacultyViewModel *) phoneModel
                    error:(NSError *) error;
@end
@interface CMUPhoneVersionController : NSObject

- (id)initWithContentPath:(NSString *) contentPath
                    error:(NSError **) error;
@property(nonatomic, unsafe_unretained) id<CMUPhoneVersionControllerDelegate> delegate;
@property(nonatomic, readonly) CMUPhoneVersionModel *currentPhoneVersion;
@property(nonatomic, readonly) CMUPhoneFacultyViewModel *currentPhoneModel;
@property(readonly, unsafe_unretained)CMUPhoneVersionControllerProgressState progressState;
@property(readonly, unsafe_unretained)BOOL onProgress;

//return false if can't start progress
- (BOOL)startCheckVersion;

@end
