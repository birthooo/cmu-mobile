//
//  CMUSocial.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/2/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUSocial : NSObject
+ (void)shareFacebookWithTitle:(NSString *)title url:(NSString *)url desc:(NSString *)desc pictureUrl:(NSString *)pictureUrl;
@end
