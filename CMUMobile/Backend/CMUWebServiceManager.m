//
//  CMUWebServiceManager.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUWebServiceManager.h"
#import "CMUWebServiceCore.h"
#import "CMUModel.h"
#import "CMUStringUtils.h"
#import "global.h"
#import "NSString+URLEncoder.h"
#import "CMUJSONUtils.h"

#define KEY_CMU_USER_TICKET @"KEY_CMU_USER_TICKET"
#define KEY_CMU_USER_INFO_MODEL @"KEY_CMU_USER_INFO_MODEL"
#define KEY_CMU_USER_DEVICE_TOKEN @"KEY_CMU_USER_DEVICE_TOKEN"
#define KEY_CMU_IS_DEVICE_REGIS_PUSH @"KEY_CMU_IS_DEVICE_REGIS_PUSH"

@interface CMUWebServiceManager()
@property(nonatomic, strong) NSString *deviceToken;
@property NSMutableArray *arr;
- (void)loadDeviceToken;
@end
@implementation CMUWebServiceManager
+ (id)sharedInstance
{
    static CMUWebServiceManager *sharedInstance = nil;
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[CMUWebServiceManager alloc] init];
            //
            [[NSNotificationCenter defaultCenter] addObserver:sharedInstance
                                                     selector:@selector(relogindNotification:)
                                                         name:CMU_RELOGIN_NOTIFICATION
                                                       object:nil];
        }
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        [self loadTicket];
        [self loadUserInfo];
        [self loadDeviceToken];
    }
    return self;
}

//overide
- (void)relogindNotification:(NSNotification *) notification
{
    [self logout:nil success:nil failure:nil];
     [[NSNotificationCenter defaultCenter] postNotificationName:CMU_LOGIN_CHANGED_NOTIFICATION object:self userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"relogin"]];
}

- (BOOL)isLogin
{
    return self.userInfoModel != nil;
}

- (BOOL)isLogoutPending
{
    return self.userInfoModel == nil && self.ticket != nil;
}

- (void)loadTicket
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *ticketData = [defaults objectForKey:KEY_CMU_USER_TICKET];
    if(ticketData)
    {
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:ticketData];
        self.ticket  = [[CMUTicket alloc] initWithDictionary:dictionary];
    }
}

- (void)saveTicket:(NSDictionary *)ticketDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:ticketDict];
    [defaults setObject:data forKey:KEY_CMU_USER_TICKET];
    [defaults synchronize];
}

- (void)deleteTicket
{
    self.ticket = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:KEY_CMU_USER_TICKET];
    [defaults synchronize];
}

- (void)loadUserInfo
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userInfoData = [defaults objectForKey:KEY_CMU_USER_INFO_MODEL];
    if(userInfoData)
    {
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
        self.userInfoModel  = [[CMUUserInfoModel alloc] initWithDictionary:dictionary];
    }
}

- (void)saveUserInfo:(NSDictionary *)userInfoDict
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userInfoDict];
    [defaults setObject:data forKey:KEY_CMU_USER_INFO_MODEL];
    [defaults synchronize];
}

- (void)deleteUserInfo
{
    self.userInfoModel = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:KEY_CMU_USER_INFO_MODEL];
    [defaults synchronize];
}

#pragma mark CMU Login
- (void)login:(id)context userName:(NSString *) userName password:(NSString *) password  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_LOGIN_URL ];
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:
                          CMU_APP_ID, @"appId",
                          CMU_APP_SECRET, @"appSecret",
                          userName, @"user",
                          password, @"pw",
                          nil];
    [[CMUWebServiceCore sharedInstance] get:url headers:headers allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMULoginModel *loginModel = [[CMULoginModel alloc] initWithDictionary:responseObject];
        
        if(loginModel.success)
        {
            self.ticket = loginModel.ticket;
            [self saveTicket:[responseObject objectForKey:@"ticket"]];
        }
        success(loginModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU get User Information
/*CMU GET USER INFO*/
- (void)getUserInfo:(id)context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = nil;
    
    if([self.ticket  getUserType] == CMU_USER_TYPE_EMPLOYEE)
    {
        url = [NSString stringWithFormat:CMU_GET_USER_EMPLOYEE_URL];
    }
    else
    {
        url = [NSString stringWithFormat:CMU_GET_USER_STUDENT_URL];
    }
    
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:
                             CMU_APP_ID, @"appId",
                             CMU_APP_SECRET, @"appSecret",
                             self.ticket.userName, @"userName",
                             self.ticket.accessToken, @"access_token",
                             nil];
    [[CMUWebServiceCore sharedInstance] get:url headers:headers allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUUserInfoModel *userInfoModel = [[CMUUserInfoModel alloc] initWithDictionary:responseObject];
        NSLog(@"%@",responseObject);
        if(userInfoModel.success)
        {
            self.userInfoModel = userInfoModel;
            [self saveUserInfo:responseObject];
            
            //TODO remove this if Account Team confirm
            NSDictionary *tickLevel1 = JSON_GET_OBJECT([responseObject objectForKey:@"ticket"]);
            NSDictionary *tickLevel2 = JSON_GET_OBJECT([tickLevel1 objectForKey:@"ticket"]);
            CMUTicket *ticket = [[CMUTicket alloc] initWithDictionary:tickLevel2];
            if(ticket)
            {
                self.ticket = ticket;
                 [self saveTicket:tickLevel2];
            }
        }
        success(userInfoModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark Logout
- (void)logout:(id)context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_LOGOUT_URL];
    
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:
                             CMU_APP_ID, @"appId",
                             CMU_APP_SECRET, @"appSecret",
                             self.ticket.userName, @"userName",
                             self.ticket.accessToken, @"access_token",
                             nil];

    [[CMUWebServiceCore sharedInstance] get:url headers:headers allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        if(success)
        {
            success(nil);
        }
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
        {
            failure(error);
        }
        
    }];
    [self deleteTicket];
    [self deleteUserInfo];
}

#pragma mark CMU HotNews
- (void)getHotNews:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:CMU_HOT_NEWS_URL headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUNewsModel *newsModel = [[CMUNewsModel alloc] initWithDictionary:responseObject];
        success(newsModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU News
- (void)getNewsByType:(id) context type:(CMU_NEWS_CATEGORY) type  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_NEWS_BY_TYPE_URL, type];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUNewsModel *newsModel = [[CMUNewsModel alloc] initWithDictionary:responseObject];
        success(newsModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getNewsBySettings:(id) context newForPostSettings:(CMUNewsForPost *)newForPostSettings  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
{
    NSString *typeParams = [newForPostSettings toWebServiceParams];
    NSString *url = [NSString stringWithFormat:CMU_NEWS_BY_SETTINGS_URL, [typeParams urlencode]];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUNewsModel *newsModel = [[CMUNewsModel alloc] initWithDictionary:responseObject];
        success(newsModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getNewsByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUNewsModel *newsModel = [[CMUNewsModel alloc] initWithDictionary:responseObject];
        success(newsModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Phone
- (void)getPhoneVersion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:CMU_PHONE_VERSION_URL headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUPhoneVersionModel *phoneVersionModel = [[CMUPhoneVersionModel alloc] initWithDictionary:responseObject];
        success(phoneVersionModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getPhone:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);//need return json for persist
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Curriculum
- (void)getCurriculumList:(id) context year:(int)year semester:(int)semester level:(int)level facultyId:(NSString *)facultyId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_CURRICULUM_LIST_URL, year, semester, level, facultyId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUCurriculumMajorModel *curriculumMajorModel = [[CMUCurriculumMajorModel alloc] initWithDictionary:responseObject];
        success(curriculumMajorModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Photo And Video
- (void)getPhotoAndVideo:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:CMU_PHOTO_AND_VIDEO_URL headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUPhotoAndVideoModel *photoAndVideoModel = [[CMUPhotoAndVideoModel alloc] initWithDictionary:responseObject];
        success(photoAndVideoModel);
       
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Channel
- (void)getChannel:(id) context channelType:(CMU_CHANNEL_TYPE)channelType  searchKeyword:(NSString *)searchKeyword success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_CHANNEL_URL, channelType,[CMUStringUtils isEmpty:searchKeyword]?@"null": [[CMUStringUtils trim:searchKeyword] urlencode]];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUChanelFeedModel *chanelFeedModel = [[CMUChanelFeedModel alloc] initWithDictionary:responseObject];
        success(chanelFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getChannelByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUChanelFeedModel *chanelFeedModel = [[CMUChanelFeedModel alloc] initWithDictionary:responseObject];
        success(chanelFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)addChannelVideoView:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_CHANNEL_ADD_VIDEO_VIEW, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)addChannelVideoLike:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_CHANNEL_ADD_VIDEO_LIKE, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getChannelVideoComment:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_CHANNEL_GET_VIDEO_COMMENT, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUChannelCommentModel *cmuChannelCommentModel = [[CMUChannelCommentModel alloc] initWithDictionary:responseObject];
        success(cmuChannelCommentModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getChannelVideoCommentByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUChannelCommentModel *cmuChannelCommentModel = [[CMUChannelCommentModel alloc] initWithDictionary:responseObject];
        success(cmuChannelCommentModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)addChannelVideoComment:(id) context videoId:(int)videoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSDictionary *commentDict = [NSDictionary dictionaryWithObject:comment forKey:@"comment_text"];
        
        NSString *url = [NSString stringWithFormat:CMU_CHANNEL_ADD_VIDEO_COMMENT,
                         [self.ticket.userName urlencode],
                         videoId];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [commentDict JSONString],@"cmudata",
                                  self.ticket.accessToken, @"cmutoken",
                                  nil];
        
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
        
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}


#pragma mark CMU Event
- (void)getEventDetail:(id) context year:(int)year month:(int)month type:(int)type success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_EVENT_URL, year, month, type];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUEventDetailFeedModel *eventdetailFeedModel = [[CMUEventDetailFeedModel alloc] initWithDictionary:responseObject];
        success(eventdetailFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Map
- (void)getMapVersion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:CMU_MAP_VERSION_URL headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUMapVersionModel *mapVersionModel = [[CMUMapVersionModel alloc] initWithDictionary:responseObject];
        success(mapVersionModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getMap:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);//need return json for persist
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getMapBusType:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure{
    NSString *url = [NSString stringWithFormat:CMU_MAP_BUS_TYPE_URL];
    
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *busTypeList = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in responseObject){
            [busTypeList addObject:[[CMUMapFeed alloc] initWithBusTypeDictionary:dict]];
        }
        success(busTypeList);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getMapBusTypeByType:(id) context type:(int)type success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure{
    NSString *url = [NSString stringWithFormat:CMU_MAP_BUS_BY_TYPE_URL, type];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUMapBusTypeModel *model = [[CMUMapBusTypeModel alloc] initWithDictionary:responseObject];
        success(model);//need return json for persist
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getJumboMap:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] getExternalWS:CMU_JUMBO_MAP_URL parameters:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *mapFeedList = [[NSMutableArray alloc] init];
        @try {
            NSArray *mapFeedListJSON = (NSArray *)responseObject;
            for(NSDictionary *dict in mapFeedListJSON)
            {
                CMUMapFeed *mapFeed = [[CMUMapFeed alloc] initWithJumboMapDictionary:dict];
                [mapFeedList addObject:mapFeed];
            }
        }
        @catch (NSException *exception) {
            DebugLog(@"%@", exception);
        }
        success(mapFeedList);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU Push Notification
- (void)saveDeviceToken:(NSString *)deviceToken
{
    self.deviceToken = deviceToken;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken forKey:KEY_CMU_USER_DEVICE_TOKEN];
    [defaults synchronize];
}

- (void)loadDeviceToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.deviceToken = [defaults objectForKey:KEY_CMU_USER_DEVICE_TOKEN];
}

- (void)saveIsDeviceRegisterPush
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:KEY_CMU_IS_DEVICE_REGIS_PUSH];
    [defaults synchronize];
}

- (BOOL)loadIsDeviceRegisterPush
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *deviceFirstRegister = [defaults objectForKey:KEY_CMU_IS_DEVICE_REGIS_PUSH];
    NSLog(@"deviceFirstRegister :%@",deviceFirstRegister);
    return [deviceFirstRegister boolValue];
}

- (void)registerPushNotification:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        BOOL isDeviceRegisterPush = NO;
        isDeviceRegisterPush = [self loadIsDeviceRegisterPush];
        
        NSString *url = [NSString stringWithFormat:CMU_PUSH_NOTIFICATION_REGISTER_URL, [self.deviceToken urlencode], [(self.ticket ? self.ticket.userName : @"guest") urlencode], @"iOS", isDeviceRegisterPush? @"nonew": @"new"];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self saveIsDeviceRegisterPush];
            success(responseObject);
            
            //on foreground just update badge update notification list if showing
            [[CMUWebServiceManager sharedInstance] getNofiticationNumber:self success:^(id result) {
                //Nothing todo here
            } failure:^(NSError *error) {
                
            }];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            //fixed remove this, now web service return string not json, and this block alway call whether success ro fail to register.
            //[self saveIsDeviceRegisterPush];
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)getNotificationSettings:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_NOTIFICATION_SETTINGS_URL, [self.deviceToken urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUNotificationSettingsModel*notificationSettingsModel = [[CMUNotificationSettingsModel alloc] initWithDictionary:responseObject];
            success(notificationSettingsModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)setNotificationSettings:(id) context key:(NSString *)key mode:(NSString *)mode success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        key = [CMUStringUtils isEmpty:key]?@"null":key;
        NSString *url = [NSString stringWithFormat:CMU_SET_NOTIFICATION_SETTINGS_URL, [key urlencode], [self.deviceToken urlencode], [mode urlencode]];
        
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getNotificationFeed:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure

{
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_NOTIFICATION_FEED_URL, [self.deviceToken urlencode]];
        
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUNotificationFeedModel *notificationFeedModel = [[CMUNotificationFeedModel alloc] initWithDictionary:responseObject];
            success(notificationFeedModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)deleteNotification:(id) context feedId:(int)feedId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_DELETE_NOTIFICATION_URL, feedId, [self.deviceToken urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)deleteAllNotification:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_DELETE_ALL_NOTIFICATION_URL, [self.deviceToken urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getNofiticationNumber:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_NOTIFICATION_NUMBER, [self.deviceToken urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            self.notificationNumberModel = [[CMUNotificationNumberModel alloc] initWithDictionary:responseObject];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION object:self];
            if(success)
            {
                success(self.notificationNumberModel);
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            if(failure)
            {
                failure(error);
            }
            
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

//broadcast
- (void)getBroadcastFeed:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_BROADCAST_FEED_URL, [self.deviceToken urlencode]];
        
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUBroadcastFeedModel *broadcastFeedModel = [[CMUBroadcastFeedModel alloc] initWithDictionary:responseObject];
            success(broadcastFeedModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getBroadcastBySenderFeed:(id) context senderId:(int)senderId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_BROADCAST_FEED_BY_SENDER_URL, [self.deviceToken urlencode], senderId];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUBroadcastSenderFeedModel *broadcastSenderFeedModel = [[CMUBroadcastSenderFeedModel alloc] initWithDictionary:responseObject];
            success(broadcastSenderFeedModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getBroadcastSenderFeedฺByURL:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.deviceToken)
    {
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUBroadcastSenderFeedModel *broadcastSenderFeedModel = [[CMUBroadcastSenderFeedModel alloc] initWithDictionary:responseObject];
            success(broadcastSenderFeedModel);

        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}


#pragma mark CMU Push Notification * News
- (void)getNewsDetail:(id) context itemId:(int) itemId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_NEWS_BY_ITEM_ID, itemId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUNewsModel *newsModel = [[CMUNewsModel alloc] initWithDictionary:responseObject];
        success(newsModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

#pragma mark CMU SIS
- (void)getEvaSubject:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_EVA_SUBJECT_URL,[self.userInfoModel.user.studentId urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUSISFeedModel *sisFeedModel = [[CMUSISFeedModel alloc] initWithDictionary:responseObject];
            success(sisFeedModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Access Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getEvaSubjectForm:(id) context
                     year:(NSString *) year
                     term:(NSString *) term
              subjectCode:(NSString *) subjectCode
                sectionId:(int) sectionId
                teacherId:(NSString *) teacherId
                questType:(NSString *) questType
                  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_EVA_SUBJECT_FORM_URL,
                         [self.userInfoModel.user.studentId urlencode],
                         [year urlencode],
                         [term urlencode],
                         [subjectCode urlencode],
                         sectionId,
                         teacherId == nil?@"null":[teacherId urlencode],
                         [questType urlencode]
                         ];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        
        [[CMUWebServiceCore sharedInstance] post:url params:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUSISEvalFormModel *evalFormModel = [[CMUSISEvalFormModel alloc] initWithDictionary:responseObject];
            success(evalFormModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Access Token is null" code:-1 userInfo:nil]);
    }

}

- (void)saveEvaSubjectForm:(id) context
               subjectCode:(NSString *) subjectCode
                 sectionId:(int) sectionId
                 teacherId:(NSString *) teacherId
              evaFormModel:(CMUSISEvalFormModel *) evalFormModel
                   success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_SAVE_EVA_SUBJECT_FORM_URL,
                         [self.userInfoModel.user.studentId urlencode],
                         [subjectCode urlencode],
                         sectionId,
                         teacherId == nil?@"null":[teacherId urlencode]
                         ];

        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                 [evalFormModel.dictionary JSONString],@"cmudata",
                                nil];
       
        
        [[CMUWebServiceCore sharedInstance] post:url params:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //TODO wrong model
            CMUSISPCheckModel *sisPcheckModel = [[CMUSISPCheckModel alloc] initWithDictionary:responseObject];
            success(sisPcheckModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Access Token is null" code:-1 userInfo:nil]);
    }
    
   
//    if(self.ticket.accessToken)
//    {
//        NSString *url = [NSString stringWithFormat:CMU_SAVE_EVA_SUBJECT_FORM_URL,
//                         [self.userInfoModel.user.studentId urlencode],
//                         [subjectCode urlencode],
//                         sectionId,
//                         teacherId == nil?@"null":[teacherId urlencode]
//                         ];
//
//        DebugLog(@"%@", url);
//        DebugLog(@"%@", [evalFormModel.dictionary JSONString]);
//        
//        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
//                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
//        
//        
//        [request setHTTPMethod:@"POST"];
//        [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
//        [request setValue: self.ticket.accessToken forHTTPHeaderField:@"cmutoken"];
//        [request setHTTPBody: [[evalFormModel.dictionary JSONString] dataUsingEncoding:NSUTF8StringEncoding]];
//        
//        AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//        op.responseSerializer = [AFJSONResponseSerializer serializer];
//        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//            
//            NSLog(@"JSON responseObject: %@ ",responseObject);
//            id status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
//            NSString *data = JSON_GET_OBJECT([responseObject objectForKey:@"data"]);
//            if( [status isKindOfClass:[NSString class]] && [status isEqualToString:@"relogin"])
//            {
//                [[NSNotificationCenter defaultCenter] postNotificationName:CMU_RELOGIN_NOTIFICATION object:self];
//            }
//            
////#ifdef DEBUG
//            if(!status || ![status isKindOfClass:[NSString class]] )
//            {
//                DebugLog(@"this web service not support new cmu json structure :%@",url);
//            }
////#endif
//            
//            if( [status isKindOfClass:[NSString class]] && ![status isEqualToString:@"pass"])
//            {
//                failure(nil);
//            }
//            else
//            {
//                if(data)
//                {
//                    success( data);
//                }
//                else
//                {
//                    success(responseObject);
//                }
//                
//            }
//
//            
//            success(responseObject);
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            DebugLog(@"%@", error);
//            failure(error);
//        }];
//        [op start];
//    }
//    else
//    {
//        failure([NSError errorWithDomain:@"Access Token is null" code:-1 userInfo:nil]);
//    }
}

- (void)getPCheckQuestion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
//#ifdef DEV
//        NSString *url = [NSString stringWithFormat:CMU_GET_PCHECK_QUESTION_URL, [FAKE_STUDENT_CODE urlencode]];
//#else
        NSString *url = [NSString stringWithFormat:CMU_GET_PCHECK_QUESTION_URL, [self.userInfoModel.user.studentId urlencode]];
//#endif
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        
        [[CMUWebServiceCore sharedInstance] post:url params:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUSISPCheckModel *sisPcheckModel = [[CMUSISPCheckModel alloc] initWithDictionary:responseObject];
            success(sisPcheckModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Access Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getPCheckPart3Question:(id) context pCheckModel:(CMUSISPCheckModel *)pCheckModel success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
{
    
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_GET_PCHECK_PART3_URL];
        
        DebugLog(@"%@", url);
        DebugLog(@"%@", [pCheckModel.dictionary JSONString]);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
        
        [request setHTTPMethod:@"POST"];
        [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
        //        [request setValue: @"application/json" forHTTPHeaderField:@"acceptableContentTypes"];
        [request setHTTPBody: [[pCheckModel.dictionary JSONString] dataUsingEncoding:NSUTF8StringEncoding]];
        
        AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        op.responseSerializer = [AFJSONResponseSerializer serializer];
        op.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain", nil];
        //[op.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON responseObject: %@ ",responseObject);
            id status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
            NSString *data = JSON_GET_OBJECT([responseObject objectForKey:@"data"]);
            if( [status isKindOfClass:[NSString class]] && [status isEqualToString:@"relogin"])
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:CMU_RELOGIN_NOTIFICATION object:self];
            }
            
#ifdef DEBUG
            if(!status || ![status isKindOfClass:[NSString class]] )
            {
                DebugLog(@"this web service not support new cmu json structure :%@",url);
            }
#endif
            
            if( [status isKindOfClass:[NSString class]] && ![status isEqualToString:@"pass"])
            {
                failure(nil);
            }
            else
            {
                if(data)
                {
                    success( data);
                }
                else
                {
                    success(responseObject);
                }
                
            }
            
            
            success(responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"%@", error);
            //DebugLog(@"%@",op.responseString);
            failure(error);
        }];
        [op start];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}


#pragma mark CMU Online
- (void)getCuteajarn:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_GET_CUTE_AJARN];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineCuteAjarnModel *cuteAjarnModel = [[CMUOnlineCuteAjarnModel alloc] initWithDictionary:responseObject];
        success(cuteAjarnModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getCuteajarnByType:(id) context cuteId:(int)cuteId mode:(int)mode success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_GET_CUTE_AJARN_BY_TYPE, cuteId, mode];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = [[CMUOnlineVideoFeedModel alloc] initWithDictionary:responseObject];
        success(cmuOnlineVideoFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getCuteajarnByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = [[CMUOnlineVideoFeedModel alloc] initWithDictionary:responseObject];
            success(cmuOnlineVideoFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)getSearchCuteajarn:(id) context cuteId:(int)cuteId key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_GET_SEARCH_CUTE_AJARN, cuteId, [key urlencode]];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = [[CMUOnlineVideoFeedModel alloc] initWithDictionary:responseObject];
        success(cmuOnlineVideoFeedModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)addCMUOnlineVideoView:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_ONLINE_ADD_VIDEO_VIEW, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)addCMUOnlineVideoLike:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_ONLINE_ADD_VIDEO_LIKE, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getVideoComment:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_ONLINE_GET_ONLINE_VIDEO_COMMENT, videoId];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineCommentModel *cmuOnlineCommentModel = [[CMUOnlineCommentModel alloc] initWithDictionary:responseObject];
        success(cmuOnlineCommentModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        failure(error);
    }];
}

- (void)getVideoCommentByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUOnlineCommentModel *cmuOnlineCommentModel = [[CMUOnlineCommentModel alloc] initWithDictionary:responseObject];
        success(cmuOnlineCommentModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)addVideoComment:(id) context videoId:(int)videoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSDictionary *commentDict = [NSDictionary dictionaryWithObject:comment forKey:@"comment_text"];
        
        NSString *url = [NSString stringWithFormat:CMU_ONLINE_ADD_VIDEO_COMMENT,
                         [self.ticket.userName urlencode],
                         videoId];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [commentDict JSONString],@"cmudata",
                                  self.ticket.accessToken, @"cmutoken",
                                  nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

#pragma CMU MIS
- (void) getEdocCountNumber:(id)context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock)failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_COUNT_NUMBER,
                         [self.ticket.userName urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            CMUMISEdocCountNumber *model = [[CMUMISEdocCountNumber alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }

}

- (void)getEdocRecieveTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure

{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_RECIEVE_TAB,
                         [self.ticket.userName urlencode],
                         [CMUStringUtils isEmpty:key]?@"null": [[CMUStringUtils trim:key] urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUMISEdocFeedModel *model = [[CMUMISEdocFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)getEdocPublicTab:(id) context month:(int) month year:(int)year success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_PUBLIC_TAB,
                         [self.ticket.userName urlencode], month, year];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUMISEdocFeedModel *model = [[CMUMISEdocFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)getEdocSendTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_SEND_TAB,
                         [self.ticket.userName urlencode],
                         [CMUStringUtils isEmpty:key]?@"null": [[CMUStringUtils trim:key] urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            CMUMISEdocFeedModel *model = [[CMUMISEdocFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)getEdocFollowTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_FOLLOW_TAB,
                         [self.ticket.userName urlencode],
                         [CMUStringUtils isEmpty:key]?@"null": [[CMUStringUtils trim:key] urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            CMUMISEdocFeedModel *model = [[CMUMISEdocFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)getEdocFolderListTab:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_FOLDER_TAB,
                         [self.ticket.userName urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            CMUMISEdocFolderFeedModel *model = [[CMUMISEdocFolderFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)getEdocDetail:(id) context docId:(int )docId docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_DETAIL,  docId, [self.ticket.userName urlencode]];
        
        if(docSubID && ![CMUStringUtils isEmpty:docSubID] ){
            url = [NSString stringWithFormat:@"%@%@%@",url ,@"&docSubID=", [docSubID urlencode]];
        }
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUMISEdocDetail *model = [[CMUMISEdocDetail alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}


- (void)getEdocFolderSubList:(id) context folderId:(int)folderId month:(int)month year:(int) year success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_GET_FOLDER_LIST,
                         [self.ticket.userName urlencode],
                         folderId,
                         month,
                         year];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUMISEdocFolderSubListFeedModel *model = [[CMUMISEdocFolderSubListFeedModel alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
    
}

- (void)edocRecieveTabEventDelete:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_RECIEVE_TAB_EVENT_DELETE,
                         [self.ticket.userName urlencode]];
        
        
        NSMutableArray *docSubIdList = [[NSMutableArray alloc] init];
        for(CMUMISEdocFeed *feed in docFeedList)
        {
            [docSubIdList addObject: [NSNumber numberWithInt:feed.docSubID ] ] ;
        }
        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:docSubIdList, @"data", nil];
        
        NSString *dataStr = [data JSONString];
        DebugLog(@"%@", dataStr)
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                dataStr, @"cmudata",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocRecieveTabEditOpen:(id) context docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_RECIEVE_TAB_EDIT_IS_OPEN,
                         [self.ticket.userName urlencode], docSubID];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocSendTabEventCallback:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_SEND_TAB_EVENT_CALLBACK,
                         [self.ticket.userName urlencode]];
        
        
        NSMutableArray *docSubIdList = [[NSMutableArray alloc] init];
        for(CMUMISEdocFeed *feed in docFeedList)
        {
            [docSubIdList addObject: [NSNumber numberWithInt:feed.docSubID ] ] ;
        }
        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:docSubIdList, @"data", nil];
        
        NSString *dataStr = [data JSONString];
        DebugLog(@"%@", dataStr)
        
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                dataStr, @"cmudata",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocSendTabEventDelete:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_SEND_TAB_EVENT_DELETE,
                         [self.ticket.userName urlencode]];
        
        
        NSMutableArray *docSubIdList = [[NSMutableArray alloc] init];
        for(CMUMISEdocFeed *feed in docFeedList)
        {
            [docSubIdList addObject: [NSNumber numberWithInt:feed.docID ] ] ;
        }
        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:docSubIdList, @"data", nil];
        
        NSString *dataStr = [data JSONString];
        DebugLog(@"%@", dataStr)
        
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                dataStr, @"cmudata",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocFollowTabEventUnFollow:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_FOLLOW_TAB_EVENT_UNFOLLOW,
                         [self.ticket.userName urlencode]];
        
        
        NSMutableArray *docSubIdList = [[NSMutableArray alloc] init];
        for(CMUMISEdocFeed *feed in docFeedList)
        {
            [docSubIdList addObject: [NSNumber numberWithInt:feed.docID ] ] ;
        }
        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:docSubIdList, @"data", nil];
        
        NSString *dataStr = [data JSONString];
        DebugLog(@"%@", dataStr)
        
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                dataStr, @"cmudata",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}


- (void)edocAccept:(id) context docId:(int)docId docSubID:(NSString *)docSubID  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_ACCEPT,
                         docId, docSubID, [self.ticket.userName urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocReply:(id) context docId:(int)docId docSubID:(NSString *)docSubID  comment:(NSString *)comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_REPLY,
                         docId, docSubID, [self.ticket.userName urlencode]];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                comment, @"comment",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            CMUMISEdocDetail *model = [[CMUMISEdocDetail alloc] initWithDictionary:responseObject];
            success(model);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocAddFolder:(id) context docSubID:(NSString *)docSubID folderID:(int)folderID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_ADD_FOLDER,
                         [self.ticket.userName urlencode],
                         docSubID,
                         folderID];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)edocCancelFolder:(id) context docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_MIS_EDOC_CANCEL_FOLDER,
                         [self.ticket.userName urlencode],
                         docSubID];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

/*CMU PHOTO CONTEST*/
- (void)photoContestGetPhoto:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_GET_PHOTO_URL];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUPhotoContestModel *photoContestModel = [[CMUPhotoContestModel alloc] initWithDictionary:responseObject];
        success(photoContestModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)photoContestGetPhotoByURL:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUPhotoContestModel *photoContestModel = [[CMUPhotoContestModel alloc] initWithDictionary:responseObject];
        success(photoContestModel);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}


- (void)photoContestSave:(id) context image:(UIImage *) image  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_SAVE_URL];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                nil];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
        [manager POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //[formData appendPartWithFormData:imageData name:@"files"];
            [formData appendPartWithFileData:imageData
                                        name:@"files"
                                    fileName:@"image.jpg" mimeType:@"image/jpeg"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
             success(responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestInsertPhoto:(id) context filename:(NSString *) filename comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_INSERT_PHOTO_URL];
        
        
        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       [NSString stringWithFormat:@"%@@cmu.ac.th", self.ticket.userName], @"Account",
                                       comment, @"Ment",
                                       [NSString stringWithFormat:@"%@ %@", self.userInfoModel.user.firstNameEN, self.userInfoModel.user.lastNameEN], @"Name",
                                       filename, @"Pic",
                                       nil];
        
        NSString *dataStr = [data JSONString];
        DebugLog(@"%@", dataStr)
        
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                self.ticket.accessToken, @"cmutoken",
                                dataStr, @"cmudata",
                                nil];
        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestGetPhotoDetail:(id) context photoID:(int) photoID  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_GET_PHOTO_DETAIL_BY_ID, photoID ];
    [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
        CMUPhotoContestFeedDetail *photoContestFeedDetail = [[CMUPhotoContestFeedDetail alloc] initWithDictionary:responseObject];
        success(photoContestFeedDetail);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)photoContestGetHome:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_GET_HOME, [self.ticket.userName urlencode] ];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSMutableDictionary *adaptor = [[NSMutableDictionary alloc] init];
            [adaptor setObject:responseObject forKey:@"PhotocontestViewList"];
            
            CMUPhotoContestModel *photoContestModel = [[CMUPhotoContestModel alloc] initWithDictionary:adaptor];
            success(photoContestModel);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestIsLike:(id) context photoID:(int) photoID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_IS_LIKE, photoID, [self.ticket.userName urlencode] ];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestAddLike:(id) context photoID:(int) photoID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_ADD_LIKE, photoID, [self.ticket.userName urlencode] ];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestReport:(id) context photoId:(int) photoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_REPORT,[self.ticket.userName urlencode], photoId, [comment urlencode]];
        
//        
//        NSMutableDictionary *data =   [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//                                       [NSString stringWithFormat:@"%@@cmu.ac.th", self.ticket.userName], @"Account",
//                                       comment, @"Ment",
//                                       [NSString stringWithFormat:@"%@ %@", self.userInfoModel.user.firstNameEN, self.userInfoModel.user.lastNameEN], @"Name",
//                                       filename, @"Pic",
//                                       nil];
//        
//        NSString *dataStr = [data JSONString];
//        DebugLog(@"%@", dataStr)
//        
//        
//        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//                                self.ticket.accessToken, @"cmutoken",
//                                dataStr, @"cmudata",
//                                nil];
//        [[CMUWebServiceCore sharedInstance] post:url params:params  success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            success(responseObject);
//        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            failure(error);
//        }];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestDelete:(id) context photoId:(int) photoId  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_DELETE,photoId, [self.ticket.userName urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)photoContestCanUpload:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure{
    
    if(self.ticket.accessToken)
    {
        NSString *url = [NSString stringWithFormat:CMU_PHOTO_CONTEST_ISFIVE, [self.ticket.userName urlencode]];
        [[CMUWebServiceCore sharedInstance] get:url headers:nil allowCache:NO success:^(AFHTTPRequestOperation *operation, id responseObject) {
            success(responseObject);
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            failure(error);
        }];
    }
    else
    {
        failure([NSError errorWithDomain:@"Device Token is null" code:-1 userInfo:nil]);
    }
}

- (void)transitGetStation:(id) context routeId:(int) routeId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{


   
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
  //  CMUTransitBusStationModel *model = [[CMUTransitBusStationModel alloc] initWithArray:[userDefault valueForKey:@"arr" ]];
  //  success(model);
    
 
    NSString *url = [NSString stringWithFormat:CMU_TRANSIT_STATION_URL, routeId];
    [[CMUWebServiceCore sharedInstance] getExternalWS:url parameters:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        @try {
            CMUTransitBusStationModel *model = [[CMUTransitBusStationModel alloc] initWithArray:responseObject];
            success(model);
        }
        @catch (NSException *exception) {
            DebugLog(@"%@", exception);
            failure(nil);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
 
}
- (void)transitGetBusPosition:(id) context routeId:(int) routeId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure
{
    NSString *url = [NSString stringWithFormat:CMU_TRANSIT_BUS_POSITION_URL, routeId];
    [[CMUWebServiceCore sharedInstance] getExternalWS:url parameters:nil allowCache:YES success:^(AFHTTPRequestOperation *operation, id responseObject) {
        @try {
            CMUTransitBusPositionModel *model = [[CMUTransitBusPositionModel alloc] initWithArray:responseObject];
            success(model);
        }
        @catch (NSException *exception) {
            DebugLog(@"%@", exception);
            failure(nil);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}


@end
