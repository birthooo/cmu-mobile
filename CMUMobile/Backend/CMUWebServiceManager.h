//
//  CMUWebServiceManager.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMUModel.h"
#import "global.h"
#define KEY_CMU_USER_TICKET @"KEY_CMU_USER_TICKET"
#ifdef DEV
#import "dev_cfg.h"
#else
#import "prod_cfg.h"
#endif

typedef void (^CMUWebServiceManagerSucceededBlock) (id result);
typedef void (^CMUWebServiceManagerFailedBlock) (NSError *error);

@interface CMUWebServiceManager : NSObject
+ (id)sharedInstance;

@property(nonatomic, strong) CMUTicket *ticket;
@property(nonatomic, strong) CMUUserInfoModel *userInfoModel;
@property(nonatomic, strong) CMUNotificationNumberModel *notificationNumberModel;

- (BOOL)isLogin;
- (BOOL)isLogoutPending;
- (void)deleteTicket;
- (void)deleteUserInfo;

/*CMU LOGIN*/
- (void)login:(id)context userName:(NSString *) userName password:(NSString *) password  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU GET USER INFO*/
- (void)getUserInfo:(id)context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU LOGOUT*/
- (void)logout:(id)context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU HOTNEWS*/
- (void)getHotNews:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU NEWS*/
- (void)getNewsByType:(id) context type:(CMU_NEWS_CATEGORY) type  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getNewsBySettings:(id) context newForPostSettings:(CMUNewsForPost *)newForPostSettings  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getNewsByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU PHONE*/
- (void)getPhoneVersion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getPhone:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU CURRICULUM*/
- (void)getCurriculumList:(id) context year:(int)year semester:(int)semester level:(int)level facultyId:(NSString *)facultyId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU PHOTO AND VIDEO*/
- (void)getPhotoAndVideo:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU CHANNEL*/
- (void)getChannel:(id) context channelType:(CMU_CHANNEL_TYPE)channelType  searchKeyword:(NSString *)searchKeyword success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getChannelByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addChannelVideoView:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addChannelVideoLike:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getChannelVideoComment:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getChannelVideoCommentByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addChannelVideoComment:(id) context videoId:(int)videoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU EVENT*/
- (void)getEventDetail:(id) context year:(int)year month:(int)month type:(int)type success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU MAP*/
- (void)getMapVersion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getMap:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getMapBusType:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getMapBusTypeByType:(id) context type:(int)type success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getJumboMap:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU Push Notification*/
- (void)saveDeviceToken:(NSString *)deviceToken;
- (void)registerPushNotification:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getNotificationSettings:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)setNotificationSettings:(id) context key:(NSString *)key mode:(NSString *)mode success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getNotificationFeed:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)deleteNotification:(id) context feedId:(int)feedId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)deleteAllNotification:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getNofiticationNumber:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
//broadcast
- (void)getBroadcastFeed:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getBroadcastBySenderFeed:(id) context senderId:(int)senderId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getBroadcastSenderFeedฺByURL:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/* CMU Push Notification + News*/
- (void)getNewsDetail:(id) context itemId:(int) itemId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/* CMU SIS*/
- (void)getEvaSubject:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)getEvaSubjectForm:(id) context
                     year:(NSString *) year
                     term:(NSString *) term
              subjectCode:(NSString *) subjectCode
                sectionId:(int) sectionId
                teacherId:(NSString *) teacherId
                questType:(NSString *) questType
                  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)saveEvaSubjectForm:(id) context
               subjectCode:(NSString *) subjectCode
                 sectionId:(int) sectionId
                 teacherId:(NSString *) teacherId
              evaFormModel:(CMUSISEvalFormModel *) evalFormModel
                  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;


- (void)getPCheckQuestion:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getPCheckPart3Question:(id) context pCheckModel:(CMUSISPCheckModel *)pCheckModel success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU Online*/
- (void)getCuteajarn:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getCuteajarnByType:(id) context cuteId:(int)cuteId mode:(int)mode success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getCuteajarnByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getSearchCuteajarn:(id) context cuteId:(int)cuteId key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addCMUOnlineVideoView:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addCMUOnlineVideoLike:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getVideoComment:(id) context videoId:(int)videoId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getVideoCommentByURL:(id) context url:(NSString *) url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)addVideoComment:(id) context videoId:(int)videoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU MIS*/
- (void)getEdocCountNumber:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocRecieveTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocPublicTab:(id) context month:(int) month year:(int)year success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocSendTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocFollowTab:(id) context key:(NSString *) key success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocFolderListTab:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocDetail:(id) context docId:(int )docId docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

- (void)getEdocFolderSubList:(id) context folderId:(int)folderId month:(int)month year:(int) year success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

//mis doc event
//tab recieve
- (void)edocRecieveTabEventDelete:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)edocRecieveTabEditOpen:(id) context docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

//tab send
- (void)edocSendTabEventCallback:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)edocSendTabEventDelete:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

//tab follow
- (void)edocFollowTabEventUnFollow:(id) context docFeedList:(NSArray *)docFeedList success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

//detail
- (void)edocAccept:(id) context docId:(int)docId docSubID:(NSString *)docSubID  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)edocReply:(id) context docId:(int)docId docSubID:(NSString *)docSubID  comment:(NSString *)comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)edocAddFolder:(id) context docSubID:(NSString *)docSubID folderID:(int)folderID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)edocCancelFolder:(id) context docSubID:(NSString *)docSubID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*CMU PHOTO CONTEST*/
- (void)photoContestGetPhoto:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestGetPhotoByURL:(id) context url:(NSString *)url success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestSave:(id) context image:(UIImage *) image success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestInsertPhoto:(id) context filename:(NSString *) filename comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestGetPhotoDetail:(id) context photoID:(int) photoID  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestGetHome:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestIsLike:(id) context photoID:(int) photoID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestAddLike:(id) context photoID:(int) photoID success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestReport:(id) context photoId:(int) photoId comment:(NSString *) comment success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestDelete:(id) context photoId:(int) photoId  success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)photoContestCanUpload:(id) context success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;

/*TRANSIT*/
- (void)transitGetStation:(id) context routeId:(int) routeId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;
- (void)transitGetBusPosition:(id) context routeId:(int) routeId success:(CMUWebServiceManagerSucceededBlock)success failure:(CMUWebServiceManagerFailedBlock) failure;


@end
