//
//  CMUMapVersionController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CMUModel.h"

typedef enum{
    CMUMapVersionControllerState_NoProgress,
    CMUMapVersionControllerState_ProgressCheckServerVersion,
    CMUMapVersionControllerState_ProgressDownloadServerVersion
} CMUMapVersionControllerProgressState;


@class CMUMapVersionController;
@protocol CMUMapVersionControllerDelegate
@required
#pragma mark check version delegate
- (void)mapVersionControllerDidStartedCheckNewVersion:(CMUMapVersionController *)versionController;
- (void)mapVersionController:(CMUMapVersionController *)versionController didFinishedCheckNewVersion:(CMUMapVersionModel *) newVersion//nil = no update
                         error:(NSError*) error;// not nil = error occured
#pragma mark download delegate
- (void)mapVersionController:(CMUMapVersionController *)versionController didStartedDownloadVersion:(CMUMapVersionModel *) newVersion;
- (void)mapVersionController:(CMUMapVersionController *)versionController didFinishedDownloadVersion:(CMUMapVersionModel *) serverVersion
         mapFacultyViewModel:(CMUMapFeedModel *) mapModel
                         error:(NSError *) error;
@end
@interface CMUMapVersionController : NSObject

- (id)initWithContentPath:(NSString *) contentPath
                    error:(NSError **) error;
@property(nonatomic, unsafe_unretained) id<CMUMapVersionControllerDelegate> delegate;
@property(nonatomic, readonly) CMUMapVersionModel *currentMapVersion;
@property(nonatomic, readonly) CMUMapFeedModel *currentMapModel;
@property(readonly, unsafe_unretained)CMUMapVersionControllerProgressState progressState;
@property(readonly, unsafe_unretained)BOOL onProgress;

//return false if can't start progress
- (BOOL)startCheckVersion;

@end