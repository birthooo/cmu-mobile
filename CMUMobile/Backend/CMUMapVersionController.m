//
//  CMUMapVersionController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMapVersionController.h"
#import "global.h"
#import "CMUWebServiceManager.h"
#import "CMUJSONUtils.h"

#define MAP_VERSION_FILENAME @"map_version.json"
#define MAP_CONTENT_FILENAME @"map.json"

@interface CMUMapVersionController()
@property(nonatomic, strong) CMUMapVersionModel *currentMapVersion;
@property(nonatomic, strong) CMUMapFeedModel *currentMapModel;
@property(nonatomic, strong) NSString *contentPath;
@property(nonatomic, unsafe_unretained) CMUMapVersionControllerProgressState progressState;
@end

@implementation CMUMapVersionController
- (id)initWithContentPath:(NSString *) contentPath
                    error:(NSError **) error
{
    self = [super init];
    if(self)
    {
        self.contentPath = contentPath;
        
        BOOL contentExists = [[NSFileManager defaultManager] fileExistsAtPath:contentPath];
        if(contentExists)
        {
            DebugLog(@"Content exists...");
            
            //read current version and model
            [self createMapVersionModel];
            [self createMapModel];
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_MAP_VERSION_UPDATE_NOTIFICATION object:self];
        }
        else
        {
            DebugLog(@"Content not found creating...");
            //1. create map content folder
            BOOL createContentFolderSuccess = [[NSFileManager defaultManager] createDirectoryAtPath:contentPath withIntermediateDirectories:NO attributes:nil error:error];
            if(!createContentFolderSuccess)
            {
                DebugLog(@"An error occured while create content directory!.");
                return nil;
            }
            DebugLog(@"1.content folder has been created.");
            
            //2 copy version
            NSString *sourceVersionPath = [[NSBundle mainBundle] pathForResource:@"map_version" ofType:@"json"];
            NSString *targetVersionPath = [self.contentPath stringByAppendingPathComponent:MAP_VERSION_FILENAME];
            [[NSFileManager defaultManager] copyItemAtPath:sourceVersionPath toPath:targetVersionPath error:nil];
            [self createMapVersionModel];
            DebugLog(@"2.version file has been coppied.");
            
            //3 copy content
            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"json"];
            NSString *targetPath = [self.contentPath stringByAppendingPathComponent:MAP_CONTENT_FILENAME];
            [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:nil];
            [self createMapModel];
            DebugLog(@"3.content file has been coppied.");
        }
    }
    return self;
}

#pragma mark read/write version

- (BOOL)startCheckVersion
{
    if([self onProgress])
    {
        //processing not start
        return false;
    }
    
    if(_delegate)
    {
        [_delegate mapVersionControllerDidStartedCheckNewVersion:self];
    }
    
    self.progressState = CMUMapVersionControllerState_ProgressCheckServerVersion;
    [[CMUWebServiceManager sharedInstance] getMapVersion:self success:^(id result) {
        CMUMapVersionModel *mapVersionModel = (CMUMapVersionModel *) result;
        self.progressState = CMUMapVersionControllerState_NoProgress;
        //no update found
        if(self.currentMapVersion.version == mapVersionModel.version)
        {
            if(_delegate)
            {
                [_delegate mapVersionController:self didFinishedCheckNewVersion:nil error:nil];
            }
        }
        else//found new version
        {
            if(_delegate)
            {
                [_delegate mapVersionController:self didFinishedCheckNewVersion:mapVersionModel error:nil];
            }
            [self downloadVersion:mapVersionModel];
        }
    } failure:^(NSError *error) {
        self.progressState = CMUMapVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate mapVersionController:self didFinishedCheckNewVersion:nil error:error];
        }
        
    }];
    return true;
}

#pragma mark private
-(void)downloadVersion:(CMUMapVersionModel *)newVersion
{
    self.progressState = CMUMapVersionControllerState_ProgressDownloadServerVersion;
    if(_delegate)
    {
        [_delegate mapVersionController:self didStartedDownloadVersion:newVersion];
    }
    
    [[CMUWebServiceManager sharedInstance] getMap:self url:newVersion.versionURL success:^(id result) {
        
        //save version model
        NSDictionary *mapVersionDict = [newVersion toDictionary];
        NSString *mapVersionModelString = [mapVersionDict JSONString];
        NSString *targetVersionPath = [self.contentPath stringByAppendingPathComponent:MAP_VERSION_FILENAME];
        [mapVersionModelString writeToFile:targetVersionPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        //save map model
        NSDictionary *mapDict = (NSDictionary *)result;
        NSString *mapModelString = [mapDict JSONString];
        NSString *targetPath = [self.contentPath stringByAppendingPathComponent:MAP_CONTENT_FILENAME];
        [mapModelString writeToFile:targetPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        [self createMapVersionModel];
        [self createMapModel];
        
        self.progressState = CMUMapVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate mapVersionController:self didFinishedDownloadVersion:newVersion mapFacultyViewModel:self.currentMapModel error:nil];
        }
    } failure:^(NSError *error) {
        self.progressState = CMUMapVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate mapVersionController:nil didFinishedDownloadVersion:nil mapFacultyViewModel:nil error:error];
        }
    }];
}

- (void)createMapVersionModel
{
    NSString *pathVersionFile = [self.contentPath stringByAppendingPathComponent:MAP_VERSION_FILENAME];
    NSData *versionData = [NSData dataWithContentsOfFile:pathVersionFile];
    NSDictionary  *dict = [versionData objectFromJSONData];
    self.currentMapVersion = [[CMUMapVersionModel alloc] initWithDictionary:dict];
}

- (void)createMapModel
{
    NSString *pathContentFile = [self.contentPath stringByAppendingPathComponent:MAP_CONTENT_FILENAME];
    NSData *contentData = [NSData dataWithContentsOfFile:pathContentFile];
    NSDictionary  *dict = [contentData objectFromJSONData];
    self.currentMapModel = [[CMUMapFeedModel alloc] initWithDictionary:dict];
}

- (BOOL)onProgress{
    return self.progressState != CMUMapVersionControllerState_NoProgress;
}

@end