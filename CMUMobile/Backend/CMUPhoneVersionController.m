//
//  CMUPhoneController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneVersionController.h"
#import "global.h"
#import "CMUWebServiceManager.h"
#import "CMUJSONUtils.h"

#define PHONE_VERSION_FILENAME @"phone_version.json"
#define PHONE_CONTENT_FILENAME @"phone.json"

@interface CMUPhoneVersionController()
@property(nonatomic, strong) CMUPhoneVersionModel *currentPhoneVersion;
@property(nonatomic, strong) CMUPhoneFacultyViewModel *currentPhoneModel;
@property(nonatomic, strong) NSString *contentPath;
@property(nonatomic, unsafe_unretained) CMUPhoneVersionControllerProgressState progressState;
@end

@implementation CMUPhoneVersionController
- (id)initWithContentPath:(NSString *) contentPath
                    error:(NSError **) error
{
    self = [super init];
    if(self)
    {
        self.contentPath = contentPath;
        
        BOOL contentExists = [[NSFileManager defaultManager] fileExistsAtPath:contentPath];
        if(contentExists)
        {
            DebugLog(@"Content exists...");
            
            //read current version and model
            [self createPhoneVersionModel];
            [self createPhoneModel];
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_PHONE_VERSION_UPDATE_NOTIFICATION object:self];
        }
        else
        {
            DebugLog(@"Content not found creating...");
            //1. create phone content folder
            BOOL createContentFolderSuccess = [[NSFileManager defaultManager] createDirectoryAtPath:contentPath withIntermediateDirectories:NO attributes:nil error:error];
            if(!createContentFolderSuccess)
            {
                DebugLog(@"An error occured while create content directory!.");
                return nil;
            }
            DebugLog(@"1.content folder has been created.");
            
            //2 copy version
            NSString *sourceVersionPath = [[NSBundle mainBundle] pathForResource:@"phone_version" ofType:@"json"];
            NSString *targetVersionPath = [self.contentPath stringByAppendingPathComponent:PHONE_VERSION_FILENAME];
            [[NSFileManager defaultManager] copyItemAtPath:sourceVersionPath toPath:targetVersionPath error:nil];
            [self createPhoneVersionModel];
            DebugLog(@"2.version file has been coppied.");
            
            //3 copy content
            NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"phone" ofType:@"json"];
            NSString *targetPath = [self.contentPath stringByAppendingPathComponent:PHONE_CONTENT_FILENAME];
            [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:targetPath error:nil];
            [self createPhoneModel];
            DebugLog(@"3.content file has been coppied.");
        }
    }
    return self;
}

#pragma mark read/write version

- (BOOL)startCheckVersion
{
    if([self onProgress])
    {
        //processing not start
        return false;
    }
    
    if(_delegate)
    {
        [_delegate phoneVersionControllerDidStartedCheckNewVersion:self];
    }
    
    self.progressState = CMUPhoneVersionControllerState_ProgressCheckServerVersion;
    [[CMUWebServiceManager sharedInstance] getPhoneVersion:self success:^(id result) {
        CMUPhoneVersionModel *phoneVersionModel = (CMUPhoneVersionModel *) result;
        self.progressState = CMUPhoneVersionControllerState_NoProgress;
        //no update found
        if(self.currentPhoneVersion.version == phoneVersionModel.version)
        {
            if(_delegate)
            {
                [_delegate phoneVersionController:self didFinishedCheckNewVersion:nil error:nil];
            }
        }
        else//found new version
        {
            if(_delegate)
            {
                [_delegate phoneVersionController:self didFinishedCheckNewVersion:phoneVersionModel error:nil];
            }
            [self downloadVersion:phoneVersionModel];
        }
    } failure:^(NSError *error) {
        self.progressState = CMUPhoneVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate phoneVersionController:self didFinishedCheckNewVersion:nil error:error];
        }
        
    }];
    return true;
}

#pragma mark private
-(void)downloadVersion:(CMUPhoneVersionModel *)newVersion
{
    self.progressState = CMUPhoneVersionControllerState_ProgressDownloadServerVersion;
    if(_delegate)
    {
        [_delegate phoneVersionController:self didStartedDownloadVersion:newVersion];
    }
    
    [[CMUWebServiceManager sharedInstance] getPhone:self url:newVersion.versionURL success:^(id result) {
        
        //save version model
        NSDictionary *phoneVersionDict = [newVersion toDictionary];
        NSString *phoneVersionModelString = [phoneVersionDict JSONString];
        NSString *targetVersionPath = [self.contentPath stringByAppendingPathComponent:PHONE_VERSION_FILENAME];
        [phoneVersionModelString writeToFile:targetVersionPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        //save phone model
        NSDictionary *phoneDict = (NSDictionary *)result;
        NSString *phoneModelString = [phoneDict JSONString];
        NSString *targetPath = [self.contentPath stringByAppendingPathComponent:PHONE_CONTENT_FILENAME];
        [phoneModelString writeToFile:targetPath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        [self createPhoneVersionModel];
        [self createPhoneModel];
        
        self.progressState = CMUPhoneVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate phoneVersionController:self didFinishedDownloadVersion:newVersion phoneFacultyViewModel:self.currentPhoneModel error:nil];
        }
    } failure:^(NSError *error) {
        self.progressState = CMUPhoneVersionControllerState_NoProgress;
        if(_delegate)
        {
            [_delegate phoneVersionController:nil didFinishedDownloadVersion:nil phoneFacultyViewModel:nil error:error];
        }
    }];
}

- (void)createPhoneVersionModel
{
    NSString *pathVersionFile = [self.contentPath stringByAppendingPathComponent:PHONE_VERSION_FILENAME];
    NSData *versionData = [NSData dataWithContentsOfFile:pathVersionFile];
    NSDictionary  *dict = [versionData objectFromJSONData];
    self.currentPhoneVersion = [[CMUPhoneVersionModel alloc] initWithDictionary:dict];
}

- (void)createPhoneModel
{
    NSString *pathContentFile = [self.contentPath stringByAppendingPathComponent:PHONE_CONTENT_FILENAME];
    NSData *contentData = [NSData dataWithContentsOfFile:pathContentFile];
    NSDictionary  *dict = [contentData objectFromJSONData];
    self.currentPhoneModel = [[CMUPhoneFacultyViewModel alloc] initWithDictionary:dict];
}

- (BOOL)onProgress{
    return self.progressState != CMUPhoneVersionControllerState_NoProgress;
}

@end
