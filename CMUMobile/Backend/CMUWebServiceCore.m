//
//  CMUWebServiceCore.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUWebServiceCore.h"
#import "global.h"
#import "CMUJSONUtils.h"

@implementation CMUWebServiceCore
+ (id)sharedInstance
{
    static CMUWebServiceCore *sharedInstance = nil;
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[CMUWebServiceCore alloc] init];
        }
    }
  
    return sharedInstance;
}

- (void)get:(NSString *)URLString
 headers:(NSDictionary *)headers
allowCache:(BOOL) allowCache
    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure

{
    DebugLog(@"url:%@",URLString);
    AFHTTPRequestOperationManager *manager = nil;
    if(allowCache)
    {
        manager = [CMUHTTPRequestOperationManager manager];
    }
    else
    {
        manager = [AFHTTPRequestOperationManager manager];
    }
    
    NSArray* keys = [headers allKeys];
    for(NSString *key in keys)
    {
        NSString *value = [headers valueForKey: key];
        [manager.requestSerializer setValue:value forHTTPHeaderField:key];
    }
    
    [manager GET:URLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef LOG_RESPONSE
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            DebugLog(@"result:%@",[responseObject JSONString] );
        }
        else
        {
            DebugLog(@"result:%@",responseObject);
        }
        
#endif
        //NSString *status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
        // sometime status is boolean
        id status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
        NSString *data = JSON_GET_OBJECT([responseObject objectForKey:@"data"]);
        if( [status isKindOfClass:[NSString class]] && [status isEqualToString:@"relogin"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_RELOGIN_NOTIFICATION object:self];
        }

//#ifdef DEBUG
        if(!status || ![status isKindOfClass:[NSString class]] )
        {
            DebugLog(@"this web service not support new cmu json structure :%@",URLString);
        }
//#endif
        if( [status isKindOfClass:[NSString class]] && ![status isEqualToString:@"pass"])
        {
            failure(operation, nil);
        }
        else
        {
            if(data)
            {
                success(operation, data);
            }
            else
            {
                //for web service ex. jumbo map
                success(operation, responseObject);
            }

        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

- (void)getExternalWS:(NSString *)URLString
           parameters:(id)parameters
           allowCache:(BOOL) allowCache
              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure

{
    DebugLog(@"url:%@",URLString);
    AFHTTPRequestOperationManager *manager = nil;
    if(allowCache)
    {
        manager = [CMUHTTPRequestOperationManager manager];
    }
    else
    {
        manager = [AFHTTPRequestOperationManager manager];
    }
    
    [manager GET:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef LOG_RESPONSE
        DebugLog(@"result:%@",responseObject);
#endif
        success(operation, responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}


- (void)post:(NSString *)URLString
        data:(NSData *)postData
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:10];
    
    [request setHTTPMethod:@"POST"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue: @"application/json" forHTTPHeaderField:@"acceptableContentTypes"];
    [request setHTTPBody: postData];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    op.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/plain",@"application/json", nil];
    //[op.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON responseObject: %@ ",responseObject);
        id status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
        NSString *data = JSON_GET_OBJECT([responseObject objectForKey:@"data"]);
        if( [status isKindOfClass:[NSString class]] && [status isEqualToString:@"relogin"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_RELOGIN_NOTIFICATION object:self];
        }
        
//#ifdef DEBUG
        if(!status || ![status isKindOfClass:[NSString class]] )
        {
            DebugLog(@"this web service not support new cmu json structure :%@",URLString);
        }
//#endif
        
        if( [status isKindOfClass:[NSString class]] && ![status isEqualToString:@"pass"])
        {
            failure(operation, nil);
        }
        else
        {
            if(data)
            {
                success(operation, data);
            }
            else
            {
                //for web service ex. jumbo map
                success(operation, responseObject);
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        //DebugLog(@"%@",op.responseString);
       failure(operation, error);
    }];
    [op start];
}

- (void)post:(NSString *)URLString
      params:(NSDictionary *)params
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
#ifdef LOG_RESPONSE
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            DebugLog(@"result:%@",[responseObject JSONString] );
        }
        else
        {
            DebugLog(@"result:%@",responseObject);
        }
        
#endif
        id status = JSON_GET_OBJECT([responseObject objectForKey:@"status"]);
        NSString *data = JSON_GET_OBJECT([responseObject objectForKey:@"data"]);
        if( [status isKindOfClass:[NSString class]] && [status isEqualToString:@"relogin"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:CMU_RELOGIN_NOTIFICATION object:self];
        }
        
//#ifdef DEBUG
        if(!status || ![status isKindOfClass:[NSString class]] )
        {
            DebugLog(@"this web service not support new cmu json structure :%@",URLString);
        }
//#endif
        
        if( [status isKindOfClass:[NSString class]] && ![status isEqualToString:@"pass"])
        {
            failure(operation, nil);
        }
        else
        {
            if(data)
            {
                success(operation, data);
            }
            else
            {
                //for web service ex. jumbo map
                success(operation, responseObject);
            }
            
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"%@", error);
        //DebugLog(@"%@",op.responseString);
        failure(operation, error);
    }];
    
}

@end
