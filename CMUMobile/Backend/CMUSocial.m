//
//  CMUSocial.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/2/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUSocial.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation CMUSocial

+ (void)shareFacebookWithTitle:(NSString *)title url:(NSString *)url desc:(NSString *)desc pictureUrl:(NSString *)pictureUrl
{
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:url];
    params.name = title;
    if(desc)
    {
        params.linkDescription = desc;
    }
    if(pictureUrl)
    {
        params.picture = [NSURL URLWithString:pictureUrl];
    }
    
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        
        [FBDialogs presentShareDialogWithLink:params.link
                                         name:params.name
                                      caption:params.caption
                                  description:params.linkDescription
                                      picture:params.picture
                                  clientState:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       title, @"name",
                                       url, @"link",
                                       nil];
        if(desc)
        {
            [params setObject:desc forKey:@"linkDescription"];
        }
        if(pictureUrl)
        {
            [params setObject:pictureUrl forKey:@"picture"];
        }
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }

}

// A function for parsing URL parameters returned by the Feed Dialog.
+ (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}
//+ (void)shareFacebookWithTitle:(NSString *)title url:(NSString *)url
//{
//    [FBSession.activeSession closeAndClearTokenInformation];
//    [FBSession.activeSession close];
//    [FBSession setActiveSession:nil];
//    //first request for read permission
//    
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    [params setObject:title forKey:@"title"];
//    [params setObject:url forKey:@"url"];
//    
//    if ([FBSession.activeSession.permissions
//         indexOfObject:@"public_profile"] == NSNotFound) {
//        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//            if (error)
//            {
//                if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
//                {
//                    
//                    
//                    UIAlertView *alertView2 = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription
//                                                                        delegate:nil
//                                                               cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
//                    [alertView2 show];
//                }
//                
//            }
//            else if (FB_ISSESSIONOPENWITHSTATE(status))
//            {
//                [self doRequestForPublish:params];
//            }
//            
//        }];
//    } else {
//        [self doRequestForPublish:params];
//    }
//
//}
//
//+ (void)doRequestForPublish:(NSDictionary *) params{
//    
//    if ([FBSession.activeSession.permissions
//         indexOfObject:@"publish_actions"] == NSNotFound) {
//        // Permission hasn't been granted, so ask for publish_actions
//        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"]
//                                           defaultAudience:FBSessionDefaultAudienceEveryone
//                                              allowLoginUI:YES
//                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                             if (session.isOpen && !error) {
//                                                 // Publish the story if permission was granted
//                                                 [self doPublishFacebook:params];
//                                             }
//                                             NSLog(@"%@", error);
//                                         }];
//    } else {
//        // If permissions present, publish the story
//        [self doPublishFacebook:params];
//    }
//}
//
//+ (void)doPublishFacebook:(NSDictionary *) params
//{
//    NSURL* url = [NSURL URLWithString:[params objectForKey:@"url"]];
//    if ([FBDialogs canPresentShareDialogWithParams:nil]) {
//        
//        [FBDialogs presentShareDialogWithLink:url name:nil caption:nil description:[params objectForKey:@"title"] picture:nil clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//            if(error) {
//                NSLog(@"Error: %@", error.description);
//            } else {
//                NSLog(@"Success");
//            }
//        }];
//    }
//}

@end
