//
//  dev_cfg.h
//  CMUMobile
//
//  Created by Nikorn lansa on 12/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#ifndef CMUMobile_dev_cfg_h
#define CMUMobile_dev_cfg_h

/*Login*/
#define CMU_LOGIN_URL @"https://account.cmu.ac.th/v3/api/validateUser"

/*Get User Info*/
#define CMU_GET_USER_EMPLOYEE_URL @"https://account.cmu.ac.th/v3/api/Employees"
#define CMU_GET_USER_STUDENT_URL @"https://account.cmu.ac.th/v3/api/Students"

#define CMU_LOGOUT_URL @"https://account.cmu.ac.th/v3/api/logout"

/*News*/
#define CMU_HOT_NEWS_URL @"http://202.28.249.35/cmumobile/api/news/getHotNews?nstart=0"
#define CMU_NEWS_BY_SETTINGS_URL @"http://202.28.249.35/cmumobile/api/news/getNews?key=%@&nstart=0&pstart=0"
#define CMU_NEWS_BY_TYPE_URL @"http://202.28.249.35/cmumobile/api/news/getNewByType?typeId=%i&nstart=0&pstart=0"
/*Phone*/
#define CMU_PHONE_VERSION_URL @"http://202.28.249.35/cmumobile/api/phone/getphone?version=0"

/*Curriculum*/
#define CMU_CURRICULUM_LIST_URL @"http://202.28.249.35/cmumobile/api/Curriculum/getCurriculumList?year=%i&semester=%i&level=%i&FacultyId=%@"
#define CMU_CURRICULUM_DETAIL_URL @"http://202.28.249.35/cmumobile//WebviewCurriculumDetail/Index?CourseID=%@&AcademicYear=%@&AcademicSemester=%i&FacultyId=%@"
#define CMU_CURRICULUM_STRUCTURE_URL @"http://202.28.249.35/cmumobile/WebviewCurriculumStructure/Index?CourseID=%@&AcademicYear=%@&AcademicSemester=%i"
#define CMU_CURRICULUM_PLAN_URL @"http://202.28.249.35/cmumobile/WebviewCurriculumPlan/Index?CurriculumID=%@&AcademicYear=%@&AcademicSemester=%i"

/*Photo and Video*/
//#define CMU_PHOTO_AND_VIDEO_URL @"http://www.ibssthailand.com/PhotoAndVideo.json"
#define CMU_PHOTO_AND_VIDEO_URL @"http://202.28.249.35/cmumobile/api/vdoandphoto/getPhotoVDO"

/*Channel*/
#define CMU_CHANNEL_URL @"http://202.28.249.35/cmumobile/api/CmuChanel/getCMUChanel?typeID=%i&key=%@&nStart=0&pStart=0"
#define CMU_CHANNEL_GET_VIDEO_COMMENT @"http://202.28.249.35/cmumobile/api/CmuChanel/getComment?vdoID=%i&nStart=0&pStart=0"
#define CMU_CHANNEL_ADD_VIDEO_VIEW @"http://202.28.249.35/cmumobile/api/CmuChanel/addview?vdoid=%i"
#define CMU_CHANNEL_ADD_VIDEO_LIKE @"http://202.28.249.35/cmumobile/api/CmuChanel/addlike?vdoid=%i"
#define CMU_CHANNEL_GET_ONLINE_VIDEO_COMMENT @"http://202.28.249.35/cmumobile/api/CmuChanel/getComment?vdoID=%i&nStart=0&pStart=0"
#define CMU_CHANNEL_ADD_VIDEO_COMMENT @"http://202.28.249.35/cmumobile/api/CmuChanel/addComment?Account=%@&vdoID=%i"

/*Event*/
#define CMU_EVENT_URL @"http://202.28.249.35/cmumobile/api/CmuEvent/getCmuEventByType?nYear=%i&nMonth=%i&typeId=%i"

/*Map*/
#define CMU_MAP_VERSION_URL @"http://202.28.249.35/cmumobile/api/map/getmap?version=0"
#define CMU_JUMBO_MAP_URL @"http://jumbomap.cmu.ac.th/json/?type=ap"
#define CMU_MAP_BUS_TYPE_URL @"http://202.28.249.36/cmumobile/api/map/getMapBusType"
#define CMU_MAP_BUS_BY_TYPE_URL @"http://202.28.249.36/cmumobile/api/map/getMapBusByType?typeId=%i"

/*Push notification*/
#define CMU_PUSH_NOTIFICATION_REGISTER_URL @"http://202.28.249.35/cmumobile/api/ManageNofitication/addRegis?regisId=%@&account=%@&type=%@&state=%@"
#define CMU_GET_NOTIFICATION_SETTINGS_URL @"http://202.28.249.35/cmumobile/api/ManageNofitication/getNofiticationSetting?regisId=%@"
#define CMU_SET_NOTIFICATION_SETTINGS_URL @"http://202.28.249.35/cmumobile/api/ManageNofitication/setNofitication?key=%@&regisId=%@&Mode=%@"
#define CMU_GET_NOTIFICATION_FEED_URL @"http://202.28.249.35/cmumobile/api/Notification/getNofitication?regisId=%@"
#define CMU_DELETE_NOTIFICATION_URL @"http://202.28.249.35/cmumobile/api/Notification/deleteFeed?feedid=%i&regisId=%@"
#define CMU_DELETE_ALL_NOTIFICATION_URL @"http://202.28.249.35/cmumobile/api/Notification/deleteFeed?feedid=0&regisId=%@"
#define CMU_GET_NOTIFICATION_NUMBER @"http://202.28.249.35/cmumobile/api/NofiticationNumber/getNofiticationNumber?regisId=%@"
/*Push notification + News*/
#define CMU_NEWS_BY_ITEM_ID @"http://202.28.249.35/cmumobile/api/NewsDetail/getNewsDetail?newId=%i"
/*Push notification + Broadcast*/
#define CMU_GET_BROADCAST_FEED_URL @"http://202.28.249.35/cmumobile/api/Broadcast/getBroadcast?regisID=%@"
#define CMU_GET_BROADCAST_FEED_BY_SENDER_URL @"http://202.28.249.35/cmumobile/api/Broadcast/getBroadcastByPerson?regisID=%@&sendid=%i&nstart=0&pstart=0"

/*SIS*/
#define CMU_GET_EVA_SUBJECT_URL @"http://202.28.249.35/cmumobile/api/Eva/getEvaSubject?studentCode=%@"
#define CMU_GET_EVA_SUBJECT_FORM_URL @"http://202.28.249.35/cmumobile/api/Eva/getEvaSubjectForm?studentCode=%@&year=%@&term=%@&subjectCode=%@&secId=%i&teacherID=%@&quest_type=%@"
#define CMU_SAVE_EVA_SUBJECT_FORM_URL @"http://202.28.249.35/cmumobile/api/Eva/saveEvaSubjectForm?studentcode=%@&subjectCode=%@&secId=%i&TearchID=%@"
#define CMU_GET_PCHECK_QUESTION_URL @"http://202.28.249.35/cmumobile/api/Question/getQuestion?studentCode=%@"
#define CMU_GET_PCHECK_PART3_URL @"http://202.28.249.35/cmumobile/Question/getPart3Save"

/*CMU ONLINE*/
#define CMU_GET_CUTE_AJARN @"http://202.28.249.35/cmumobile/api/Cmuonline/getCuteajarn"
#define CMU_GET_CUTE_AJARN_BY_TYPE @"http://202.28.249.35/cmumobile/api/Cmuonline/getCuteajarnBytype?cuteId=%i&mode=%i&nStart=0&pStart=0"
#define CMU_GET_SEARCH_CUTE_AJARN @"http://202.28.249.35/cmumobile/api/Cmuonline/getSearchCuteajarnBytype?cuteId=%i&key=%@&nStart=0&pStart=0"
#define CMU_ONLINE_ADD_VIDEO_VIEW @"http://202.28.249.35/cmumobile/api/Cmuonline/addview?vdoid=%i"
#define CMU_ONLINE_ADD_VIDEO_LIKE @"http://202.28.249.35/cmumobile/api/Cmuonline/addlike?vdoid=%i"
#define CMU_ONLINE_GET_ONLINE_VIDEO_COMMENT @"http://202.28.249.35/cmumobile/api/Cmuonline/getComment?cuteId=%i&nStart=0&pStart=0" //param cuteId is videoId
#define CMU_ONLINE_ADD_VIDEO_COMMENT @"http://202.28.249.35/cmumobile/api/Cmuonline/addComment?Account=%@&cuteid=%i" //param cuteId is videoId

/*CMU MIS*/
#define CMU_MIS_EDOC_GET_COUNT_NUMBER @"http://202.28.249.35/cmumobile/api/Edoc/getEdocCountNumber?cmuaccount=%@"
#define CMU_MIS_EDOC_GET_RECIEVE_TAB @"http://202.28.249.35/cmumobile/api/Edoc/getEdocRecieveTab?cmuaccount=%@&key=%@"
#define CMU_MIS_EDOC_GET_PUBLIC_TAB @"http://202.28.249.35/cmumobile/api/Edoc/getEdocpublice?cmuaccount=%@&m=%i&year=%i"
#define CMU_MIS_EDOC_GET_SEND_TAB @"http://202.28.249.35/cmumobile/api/Edoc/getSendTab?cmuaccount=%@&key=%@"
#define CMU_MIS_EDOC_GET_FOLLOW_TAB @"http://202.28.249.35/cmumobile/api/Edoc/getFollowTab?cmuaccount=%@&key=%@"
#define CMU_MIS_EDOC_GET_FOLDER_TAB @"http://202.28.249.35/cmumobile/api/Edoc/getEdocFolderList?cmuaccount=%@"
#define CMU_MIS_EDOC_GET_DETAIL @"http://202.28.249.35/cmumobile/api/Edoc/getEdocDetail?docID=%i&cmuaccount=%@"
#define CMU_MIS_EDOC_GET_FOLDER_LIST @"http://202.28.249.35/cmumobile/api/Edoc/getEdocFolderSubList?cmuaccount=%@&folderID=%i&m=%i&year=%i"
#define CMU_MIS_EDOC_ADD_FOLDER @"http://202.28.249.35/cmumobile/api/Edoc/addFolder?cmuaccount=%@&docsubID=%@&folderID=%i"
#define CMU_MIS_EDOC_CANCEL_FOLDER @"http://202.28.249.35/cmumobile/api/Edoc/cancelFolder?cmuaccount=%@&docsubID=%@"
#define CMU_MIS_EDOC_ACCEPT @"https://mobileapi.cmu.ac.th/API/Edoc/updateAcceptStatus?docId=%i&docSubID=%@&cmuaccount=%@"
#define CMU_MIS_EDOC_REPLY @"https://mobileapi.cmu.ac.th/API/Edoc/replyEdoc?docId=%i&docSubID=%@&cmuaccount=%@"

//edoc event
//recieve tab
#define CMU_MIS_EDOC_RECIEVE_TAB_EVENT_DELETE @"http://202.28.249.35/cmumobile/api/Edoc/delteEdocRecive?cmuaccount=%@"
#define CMU_MIS_EDOC_RECIEVE_TAB_EDIT_IS_OPEN @"http://202.28.249.35/cmumobile/api/Edoc/editIsOpen?cmuaccount=%@&docsubID=%@"
#define CMU_MIS_EDOC_SEND_TAB_EVENT_DELETE @"http://202.28.249.35/cmumobile/api/Edoc/delteEdocSend?cmuaccount=%@"

//send tab
#define CMU_MIS_EDOC_SEND_TAB_EVENT_CALLBACK @"http://202.28.249.35/cmumobile/api/Edoc/CallbackEdocRecieve?cmuaccount=%@"

//follow tab
#define CMU_MIS_EDOC_FOLLOW_TAB_EVENT_UNFOLLOW @"http://202.28.249.35/cmumobile/api/Edoc/cancelFollowEdoc?cmuaccount=%@"


/*CMU REG*/
#define CMU_REG_URL @"https://www3.reg.cmu.ac.th/newm/itsc_tunnel.php"


/*CMU PHOTO CONTEST*/
#define CMU_PHOTO_CONTEST_GET_PHOTO_URL @"http://202.28.249.36/cmumobile/api/PhotoContest/getPhoto?nStart=0&pStart=0"
#define CMU_PHOTO_CONTEST_SAVE_URL @"http://202.28.249.36/cmumobile/PhotoContest/save"
#define CMU_PHOTO_CONTEST_INSERT_PHOTO_URL @"http://202.28.249.36/cmumobile/api/PhotoContest/insertPhotoMany"
#define CMU_PHOTO_CONTEST_GET_PHOTO_DETAIL_BY_ID @"http://202.28.249.36/cmumobile/api/PhotoContest/getPhotoByid?nid=%i"
#define CMU_PHOTO_CONTEST_SHARE_FB_URL @"https://mobileapi.cmu.ac.th/PhotoContest/Index?photoId=%i"
#define CMU_PHOTO_CONTEST_GET_HOME @"http://202.28.249.36/cmumobile/api/PhotoContest/getPhotoByAccountList?account=%@"
#define CMU_PHOTO_CONTEST_IS_LIKE @"http://202.28.249.36/cmumobile/api/PhotoContest/islike?photoId=%i&account=%@"
#define CMU_PHOTO_CONTEST_ADD_LIKE @"http://202.28.249.36/cmumobile/api/PhotoContest/addlike?photoId=%i&account=%@"
#define CMU_PHOTO_CONTEST_REPORT @"http://202.28.249.36/cmumobile/api/PhotoContest/report?account=%@&photoID=%i&comment=%@"
#define CMU_PHOTO_CONTEST_DELETE @"http://202.28.249.36/cmumobile/api/PhotoContest/DeletePhoto?photoId=%i&account=%@"
#define CMU_PHOTO_CONTEST_ISFIVE @"http://202.28.249.36/cmumobile/api/PhotoContest/isFive?account=%@"

#define CMU_PHOTO_CONTEST_AGREEMENT @"https://mobileapi.cmu.ac.th/photocontest/agreement"
#define CMU_PHOTO_CONTEST_DETAIL @"https://mobileapi.cmu.ac.th/photocontest/ContestDetail"
#define CMU_PHOTO_PROMOTE_ITSC @"https://mobileapi.cmu.ac.th/photocontest/PromoteITSC"
#define CMU_PHOTO_PROMOTE_CMUMOBILE @"https://mobileapi.cmu.ac.th/photocontest/PromoteCMUMOBILE"

#define CMU_TRANSIT_STATION_URL @"http://www.cmutransit.com/cmtbus/routejson.php?route=%i"
#define CMU_TRANSIT_BUS_POSITION_URL @"http://www.cmutransit.com/cmtbus/busjson.php?route=%i"

#endif
