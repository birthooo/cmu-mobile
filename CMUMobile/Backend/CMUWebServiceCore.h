//
//  CMUWebServiceCore.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "CMUHTTPRequestOperationManager.h"
@interface CMUWebServiceCore : NSObject

+ (id)sharedInstance;

- (void)get:(NSString *)URLString
 headers:(NSDictionary *)headers
 allowCache:(BOOL) allowCache
    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getExternalWS:(NSString *)URLString
 parameters:(id)parameters
 allowCache:(BOOL) allowCache
    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


- (void)post:(NSString *)URLString
     data:(NSData *)postData
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)post:(NSString *)URLString
        params:(NSDictionary *)params
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
