//
//  CMUHTTPRequestOperationManager.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/25/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUHTTPRequestOperationManager.h"

@implementation CMUHTTPRequestOperationManager
#pragma mark - override

- (AFHTTPRequestOperation *)HTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *modifiedRequest = request.mutableCopy;
    
    AFNetworkReachabilityManager *reachability = self.reachabilityManager;
    if (!reachability.isReachable)
    {
        modifiedRequest.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    }
    
    AFHTTPRequestOperation *operation = [super HTTPRequestOperationWithRequest:modifiedRequest
                                                                       success:success
                                                                       failure:failure];
    [operation setCacheResponseBlock:^NSCachedURLResponse *(NSURLConnection *connection, NSCachedURLResponse *cachedResponse) {
        NSURLResponse *response = cachedResponse.response;
        NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
        NSDictionary *headers = HTTPResponse.allHeaderFields;
        
        if (headers[@"Cache-Control"])
        {
            NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
            modifiedHeaders[@"Cache-Control"] = @"max-age=60";
            NSHTTPURLResponse *modifiedHTTPResponse = [[NSHTTPURLResponse alloc]
                                                       initWithURL:HTTPResponse.URL
                                                       statusCode:HTTPResponse.statusCode
                                                       HTTPVersion:@"HTTP/1.1"
                                                       headerFields:modifiedHeaders];
            
            cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:modifiedHTTPResponse
                                                                        data:cachedResponse.data
                                                                    userInfo:cachedResponse.userInfo
                                                               storagePolicy:cachedResponse.storagePolicy];
        }
        return cachedResponse;
    }];
    
    return operation;
}
@end
