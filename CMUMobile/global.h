//
//  global.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#ifndef _global_h
#define _global_h

#define APP_VERSION_NAME @"1.8.2"
#define CMU_APP_NAME @"CMU Mobile Service"

#define LOG_RESPONSE

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define USE_FACEBOOK_SDK_SHARE

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

#define IS_IPHONE UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone

#define AppDelegate ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate])

#define DebugLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define JSON_GET_OBJECT(X) (((X) == [NSNull null])? nil: (X))
#define JSON_PARSE_OBJECT(X) (((X) == nil)? [NSNull null]: (X))

//Login
#define CMU_APP_ID @"A107C293-91F8-4F07-BBDB-A9733501FE7A"
#define CMU_APP_SECRET @"3dckgp6BH1"
#define CMU_LOGIN_CHANGED_NOTIFICATION @"CMU_LOGIN_CHANGED_NOTIFICATION"
#define CMU_RELOGIN_NOTIFICATION @"CMU_RELOGIN_NOTIFICATION"
//Phone
#define CMU_PHONE_VERSION_UPDATE_NOTIFICATION @"CMU_PHONE_VERSION_UPDATE_NOTIFICATION"

//Map
#define CMU_GOOGLE_MAP_API_KEY @"AIzaSyCn4weg0RnKBzZmg09f6FNeD4PgX8lF2lc"
#define CMU_MAP_VERSION_UPDATE_NOTIFICATION @"CMU_MAP_VERSION_UPDATE_NOTIFICATION"

//NOTIFICATION
#define CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION @"CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION"
#define CMU_APPLICATION_DID_RECEIVE_REMOTE_NOTIFICATION_IN_FOREGROUND @"CMU_APPLICATION_DID_RECEIVE_REMOTE_NOTIFICATION_IN_FOREGROUND"

#define CMU_HUD_LOADING @"Loading..."

//CMU CONNECT
#define APPSTORE_ID_LICMU_KIDS @"689621111"
#define APPSTORE_ID_LICMU_CONVERSATION @"702233636"
#define APPSTORE_ID_LICMU_DIPLOMA @"711358114"
#define APPSTORE_ID_LICMU_GRAMMA @"713077890"
#define APPSTORE_ID_LICMU_5DEC @"905637097"
#define APPSTORE_ID_ENCONCEPT_MYFLASHCARD @"661549992"
#define APPSTORE_ID_ENCONCEPT_MYECHO @"1075804602"
#define APPSTORE_ID_ERESEARCH @"669013040"

#define APP_SCHEMA_LICMU_KIDS @"licmu-kids"
#define APP_SCHEMA_LICMU_CONVERSATION @"licmu-conversation"
#define APP_SCHEMA_LICMU_DIPLOMA @"licmu-diploma"
#define APP_SCHEMA_LICMU_GRAMMA @"licmu-grammar"
#define APP_SCHEMA_LICMU_5DEC @"cmu-5decades"
#define APP_SCHEMA_ENCONCEPT_MYFLASHCARD @"enconcept-myflashcard"
#define APP_SCHEMA_ENCONCEPT_MYECHO @"enconcept-myecho"

#define CMU_CONFIRM_SAVE_DATA @"ยืนยันการบันทึกข้อมูล?"
#define CMU_CONFIRM_DELETE_DATA @"ยืนยันการลบข้อมูล?"
#define CMU_ERROR_READ_DATA @"ไม่สามารถอ่านข้อมูลได้"
#define CMU_ERROR_SAVE_DATA @"ไม่สามารถบันทึกข้อมูลได้"
#define CMU_ERROR_DELETE_DATA @"ไม่สามารถลบข้อมูลได้"
#define CMU_SUCCESS_SAVE_DATA @"บันทึกข้อมูลเรียบร้อยแล้ว"
#endif
