//
//  CMUPhotoThumbnailCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhotoThumbnailCell.h"

@interface CMUPhotoThumbnailCell()
@property(nonatomic, strong)IBOutlet UIView *border;
@end

@implementation CMUPhotoThumbnailCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUPhotoThumbnailCell" owner:self options:nil];
        UIView *contentView = [nibObjects objectAtIndex:0];
        self.contentView = contentView;
        
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.border.bounds];
        self.border.layer.masksToBounds = NO;
        self.border.layer.shadowColor = [UIColor clearColor].CGColor;
        self.border.layer.shadowOffset = CGSizeMake(0, 0);
        self.border.layer.shadowOpacity = 1.0f;
        self.border.layer.cornerRadius = 1;
        self.border.layer.shadowPath = shadowPath.CGPath;
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    [self updatePresentState];
}

- (void)updatePresentState
{
    self.border.backgroundColor = self.selected?[UIColor whiteColor]:[UIColor blackColor];
    self.border.layer.shadowColor = self.selected?[UIColor whiteColor].CGColor:[UIColor clearColor].CGColor;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.thumbnailImageView.image = nil;
}

@end
