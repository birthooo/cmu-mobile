//
//  ViewController.m
//  PhotoAndVideo
//
//  Created by Tharadol Chanyutthana on 2/3/2560 BE.
//  Copyright © 2560 Tharadol Chanyutthana. All rights reserved.
//

#import "PhotoAndVideoViewController.h"
#import "UIColor+CMU.h"
#import "UIImageView+WebCache.h"
@interface PhotoAndVideoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *videoview;
@property (weak, nonatomic) IBOutlet UIImageView *photoVdoImage;
@property (weak, nonatomic) IBOutlet UIScrollView *vdoScrollview;

@property (atomic, strong) MPMoviePlayerController *moviePlayer;
@end

@implementation PhotoAndVideoViewController
{
    NSDictionary *dictImage;
    NSDictionary *dictVdo;
    NSArray *arrayVdo;
    NSArray *arrayImage;
    int indexImage;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.photoButton.layer.cornerRadius = 5;
    self.videoButton.layer.cornerRadius = 5;
    indexImage = 0;
    dictImage =  [[self GETRequest:@"http://202.28.249.36/cmumobile/api/vdoandphoto/getPhotoVDO"]valueForKey:@"data"];
    dictVdo = [dictImage valueForKey:@"vdoFeed"];
    arrayImage = [[dictImage valueForKey:@"PhotoFeed"]valueForKey:@"cropPath"];
    arrayVdo = [[dictImage valueForKey:@"vdoFeed"]valueForKey:@"cropPath"];
    
    //
    
    self.scrollview.scrollEnabled = YES;
    
    
    self.vdoScrollview.scrollEnabled = YES;
    
    
    int xOffset = 5;
    //Thumbnail Pic
    for(int index=0; index < [arrayImage count]-1; index++)
    {
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,5,60, 44)];
        //img.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:index]]]];
        
        [img sd_setImageWithURL:[NSURL URLWithString:[arrayImage objectAtIndex:index]]];

        
        //img.image = (UIImage*) [imageArray objectAtIndex:index];
        [img setClipsToBounds:YES];
        [img setTag:index];
        img.layer.cornerRadius = 3;
        img.layer.borderWidth = 1;
        img.layer.borderColor = [[UIColor blackColor]CGColor];
        [self.scrollview addSubview:img];
        //
        img.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
        tapGesture1.numberOfTapsRequired = 1;
        [tapGesture1 setDelegate:self];
        [img addGestureRecognizer:tapGesture1];
 

        //
        xOffset+=75;
    }
    self.scrollview.contentSize = CGSizeMake(100+xOffset,60);;
    xOffset = 5;
    
    for(int index=0; index < [arrayVdo count]; index++)
    {
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,5,60, 44)];
        //img.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayVdo objectAtIndex:index]]]];
        
        [img sd_setImageWithURL:[NSURL URLWithString:[arrayVdo objectAtIndex:index]]];

        
        //img.image = (UIImage*) [imageArray objectAtIndex:index];
        [img setClipsToBounds:YES];
        [img setTag:index];
        img.layer.cornerRadius = 3;
        img.layer.borderWidth = 1;
        img.layer.borderColor = [[UIColor blackColor]CGColor];
        [self.vdoScrollview addSubview:img];
        
        img.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
        tapGesture1.numberOfTapsRequired = 1;
        [tapGesture1 setDelegate:self];
        [img addGestureRecognizer:tapGesture1];
        xOffset+=75;
    }
    
    //self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:0]]]];
    [self.imageview sd_setImageWithURL:[NSURL URLWithString:[arrayImage objectAtIndex:0]]];
    
    self.scrollview.delegate = self;
  
    self.vdoScrollview.contentSize = CGSizeMake(100+xOffset,60);
    
    [self AddSwipe];
   

    [self.videoview setHidden:YES];
    [self.photoButton setSelected:YES];
    [self.vdoScrollview setHidden:YES];
}
- (void) tapGesture: (UITapGestureRecognizer*)sender
{
    UIView *view = sender.view; //cast pointer to the derived class if needed
    NSLog(@"%ld", (long)view.tag);
    indexImage = (int)view.tag;
    [self.imageview.layer addAnimation:[CATransition animation] forKey:kCATransition];
   // self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:view.tag]]]];
    
    [self.imageview sd_setImageWithURL:[NSURL URLWithString:[arrayImage objectAtIndex:view.tag]]];
    
    
}
-(void)playVideo{
    
    MPMoviePlayerController * theMoviPlayer;
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *moviePath = [bundle pathForResource:@"disc" ofType:@"mp4"];
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    
    theMoviPlayer = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    theMoviPlayer.controlStyle = MPMovieControlStyleFullscreen;
    theMoviPlayer.view.transform = CGAffineTransformConcat(theMoviPlayer.view.transform, CGAffineTransformMakeRotation(M_PI_2));
    UIWindow *backgroundWindow = [[UIApplication sharedApplication] keyWindow];
    [theMoviPlayer.view setFrame:backgroundWindow.frame];
    [backgroundWindow addSubview:theMoviPlayer.view];
    [theMoviPlayer play];
}
-(void)AddSwipe{
    [self.imageview setUserInteractionEnabled:YES];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.imageview addGestureRecognizer:swipeLeft];
    [self.imageview addGestureRecognizer:swipeRight];
}

-(id)GETRequest:(NSString*)URLString
{
    NSMutableURLRequest *dataRqst = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    NSHTTPURLResponse *response =[[NSHTTPURLResponse alloc] init];
    NSError* error;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:dataRqst returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
    
    //convert to dictionary
    NSData *data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"[GET] urlString :%@",URLString);
    NSLog(@"[GET] return :%@",json);
    return json;
}
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        indexImage++;
        [self.imageview.layer addAnimation:[CATransition animation] forKey:kCATransition];
        self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:indexImage]]]];
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
        indexImage--;
        [self.imageview.layer addAnimation:[CATransition animation] forKey:kCATransition];
        self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:indexImage]]]];
    }
    
}

- (IBAction)rightButtonAction:(id)sender {
    if(indexImage < arrayImage.count-1){
    indexImage++;
    [self.imageview.layer addAnimation:[CATransition animation] forKey:kCATransition];
    self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:indexImage]]]];
    }
    else{
        NSLog(@"last image");
    }
}
- (IBAction)leftButtonAction:(id)sender {
    if(indexImage >0){
    indexImage--;
    [self.imageview.layer addAnimation:[CATransition animation] forKey:kCATransition];
    self.imageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[arrayImage objectAtIndex:indexImage]]]];
    }
    else{
        NSLog(@"first image");
    }
}
- (IBAction)photoButtonAction:(id)sender {
    [self.videoview setHidden:YES];
    UIColor *purple = [UIColor colorWithRed:128.0/255.0f green:0 blue:255.0/255.0f alpha:1.0];
    [self.vdoScrollview setHidden:YES];
    [self.scrollview setHidden:NO];
    
    
    [self.photoButton setSelected:YES];
    [self.photoButton setBackgroundColor:purple];
    [self.photoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self.videoButton setSelected:NO];
    [self.videoButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.videoButton setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
}
- (IBAction)videoButtonAction:(id)sender {
     UIColor *purple = [UIColor colorWithRed:128.0/255.0f green:0 blue:255.0/255.0f alpha:1.0];
    arrayVdo = [dictVdo valueForKey:@"Link"];
    NSArray *array = [dictVdo valueForKey:@"cropPath"];

    self.photoVdoImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:array[0]]]];
    
    [self.videoButton setSelected:YES];
    [self.vdoScrollview setHidden:NO];
    [self.scrollview setHidden:YES];
    [self.videoButton setBackgroundColor:purple];
    [self.videoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.photoButton setSelected:NO];
    [self.photoButton setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [self.photoButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateSelected];
    [self.videoview setHidden:NO];
    
    //
    }
- (IBAction)playVdoButton:(id)sender {
   
    NSURL *url=[[NSURL alloc] initWithString:arrayVdo[0]];
    self.moviePlayer=[[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDonePressed:) name:MPMoviePlayerDidExitFullscreenNotification object:self.moviePlayer];
    
    self.moviePlayer.controlStyle=MPMovieControlStyleDefault;
    [self.moviePlayer play];
    [self.videoview addSubview:self.moviePlayer.view];
    [self.moviePlayer setFullscreen:YES animated:YES];

}

- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}
- (void) moviePlayBackDonePressed:(NSNotification*)notification
{
    [self.moviePlayer stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.moviePlayer];
    
    
    if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [self.moviePlayer.view removeFromSuperview];
    }
    self.moviePlayer=nil;
}


@end
