//
//  CMUPhotoAndVideoTabView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhotoAndVideoTabView.h"
#import "UIColor+CMU.h"

@interface CMUPhotoAndVideoTabView()
@property(nonatomic, weak)IBOutlet UIButton *btnPhoto;
@property(nonatomic, weak)IBOutlet UIButton *btnVideo;
- (IBAction)btnTabTapped:(id)sender;
@end

@implementation CMUPhotoAndVideoTabView
- (void)customInit
{
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUPhotoAndVideoTabView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self setSelectedTab:kPhotoAndVideoTabPhoto];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setSelectedTab:(CMUPhotoAndVideoTab)selectedTab
{
    _selectedTab = selectedTab;
    [self.btnPhoto setBackgroundImage: _selectedTab == kPhotoAndVideoTabPhoto?[UIImage imageNamed:@"photo_and_video_tab_button.png"]:nil forState:UIControlStateNormal];

    [self.btnPhoto setTitleColor:_selectedTab == kPhotoAndVideoTabPhoto?[UIColor whiteColor]:[UIColor cmuBlackColor] forState:UIControlStateNormal];
    
    [self.btnVideo setBackgroundImage: _selectedTab == kPhotoAndVideoTabVideo?[UIImage imageNamed:@"photo_and_video_tab_button.png"]:nil forState:UIControlStateNormal];

    [self.btnVideo setTitleColor:_selectedTab == kPhotoAndVideoTabVideo?[UIColor whiteColor]:[UIColor cmuBlackColor] forState:UIControlStateNormal];

}

- (IBAction)btnTabTapped:(id)sender
{
    if(sender == self.btnPhoto)
    {
        self.selectedTab = kPhotoAndVideoTabPhoto;
    }
    else if(sender == self.btnVideo)
    {
        self.selectedTab = kPhotoAndVideoTabVideo;
    }
    
    if(self.delegate)
    {
        [self.delegate photoAndVideoTabView:self didSelectedTab:self.selectedTab];
    }
}

@end
