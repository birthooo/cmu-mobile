//
//  PhotoAndVideoViewController.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 1/25/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import <MediaPlayer/MediaPlayer.h>
@interface PhotoAndVideoViewController : CMUNavBarViewController <UIScrollViewDelegate,UIGestureRecognizerDelegate>

@end
