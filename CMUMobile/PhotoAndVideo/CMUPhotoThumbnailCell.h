//
//  CMUPhotoThumbnailCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "GMGridViewCell.h"

@interface CMUPhotoThumbnailCell : GMGridViewCell
@property(nonatomic, strong)IBOutlet UIImageView *thumbnailImageView;
@property(nonatomic, unsafe_unretained) BOOL selected;
@end
