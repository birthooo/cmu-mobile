//
//  CMUPhotoAndVideoTabView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    kPhotoAndVideoTabPhoto,
    kPhotoAndVideoTabVideo
}CMUPhotoAndVideoTab;

@class CMUPhotoAndVideoTabView;

@protocol CMUPhotoAndVideoTabViewDelegate <NSObject>
- (void)photoAndVideoTabView:(CMUPhotoAndVideoTabView *) photoAndVideoTabView didSelectedTab:(CMUPhotoAndVideoTab) photoAndVideoTab;
@end

@interface CMUPhotoAndVideoTabView : UIView
@property(nonatomic, unsafe_unretained) id<CMUPhotoAndVideoTabViewDelegate> delegate;
@property(nonatomic, unsafe_unretained) CMUPhotoAndVideoTab selectedTab;
@end
