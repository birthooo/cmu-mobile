//
//  CMUMapTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUMapFeedModel.h"

@interface CMUMapTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUMapFeed *mapFeed;
@end
