//
//  CMUMapMarkerView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUMapMarkerView : UIView
@property(nonatomic, weak)IBOutlet UIImageView* imvMarker;
@property(nonatomic, weak)IBOutlet UIImageView* imvIcon;
@property(nonatomic, weak)IBOutlet UILabel* lblTitle;
@property(nonatomic, weak)UIColor* borderColor;
@end
