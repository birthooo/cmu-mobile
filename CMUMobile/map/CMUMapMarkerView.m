//
//  CMUMapMarkerView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMapMarkerView.h"
#import <QuartzCore/QuartzCore.h>
#import "CMUUIUtils.h"

@interface CMUMapTitleView : UIView
@property(nonatomic, weak)IBOutlet UIColor* borderColor;
@end

@implementation CMUMapTitleView
- (void)drawRect:(CGRect)rect
{
    CGFloat roundSize = 6;
    [[UIColor whiteColor] setFill];
    UIBezierPath *backgroundPath = [UIBezierPath bezierPath];
    
    [backgroundPath moveToPoint:CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect))];
    [backgroundPath addLineToPoint:CGPointMake(CGRectGetMaxX(rect) - roundSize, CGRectGetMinY(rect))];
    [backgroundPath addQuadCurveToPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect) + roundSize) controlPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))];
    
    [backgroundPath addLineToPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect) - roundSize)];
    
    [backgroundPath addQuadCurveToPoint:CGPointMake(CGRectGetMaxX(rect) - roundSize, CGRectGetMaxY(rect)) controlPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))];
    
    [backgroundPath addLineToPoint:CGPointMake(CGRectGetMinX(rect) - roundSize, CGRectGetMaxY(rect))];
    [backgroundPath closePath];
    [backgroundPath fill];

    
    [self.borderColor setStroke];
    CGFloat lineWidth = 2;
    UIBezierPath *strokePath = [UIBezierPath bezierPath];
    strokePath.lineWidth = lineWidth;
    
    [strokePath moveToPoint:CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect) + lineWidth /2)];
    [strokePath addLineToPoint:CGPointMake(CGRectGetMaxX(rect) - roundSize, CGRectGetMinY(rect) + lineWidth /2)];
    [strokePath addQuadCurveToPoint:CGPointMake(CGRectGetMaxX(rect)  - lineWidth /2, CGRectGetMinY(rect) + roundSize) controlPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))];
    
    [strokePath addLineToPoint:CGPointMake(CGRectGetMaxX(rect)  - lineWidth /2, CGRectGetMaxY(rect) - roundSize)];
    
    [strokePath addQuadCurveToPoint:CGPointMake(CGRectGetMaxX(rect) - roundSize, CGRectGetMaxY(rect)  - lineWidth /2) controlPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))];
    
    [strokePath addLineToPoint:CGPointMake(CGRectGetMinX(rect) - roundSize, CGRectGetMaxY(rect)- lineWidth /2) ];
    [strokePath stroke];
    
    [super drawRect:rect];
}
@end

@interface CMUMapMarkerView()
@property(nonatomic, weak)IBOutlet CMUMapTitleView *titleView;
@end

@implementation CMUMapMarkerView

- (void)customInit
{

    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMapMarkerView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setBorderColor:(UIColor *)borderColor
{
    self.titleView.borderColor = borderColor;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGFloat maxWidth = MAXFLOAT;
    CGSize fitTextSize = [CMUUIUtils sizeThatFit:maxWidth withText:self.lblTitle.text withFont:self.lblTitle.font];
    CGFloat markerWidth = fitTextSize.width + 14 + 12 + 8;//14 = 1/2 markerWidth, 12 = label leading, 8 = label trailing
    return CGSizeMake(markerWidth, 48);
}

- (void)layoutSubviews
{
    //auto layout not working
    [super layoutSubviews];
    CGRect selfFrame = self.frame;
    CGRect viewTitleFrame = self.titleView.frame;
    CGRect titleLabelFrame = self.lblTitle.frame;
    viewTitleFrame.size.width = selfFrame.size.width - 14;
    titleLabelFrame.size.width = viewTitleFrame.size.width - 12 - 8;
    
    self.titleView.frame = viewTitleFrame;
    self.lblTitle.frame = titleLabelFrame;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
