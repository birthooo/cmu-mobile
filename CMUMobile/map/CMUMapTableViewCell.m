//
//  CMUMapTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMapTableViewCell.h"

@interface CMUMapTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel* lblLocationName;
@end

@implementation CMUMapTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setMapFeed:(CMUMapFeed *)mapFeed
{
    _mapFeed = mapFeed;
    self.lblLocationName.text = mapFeed.name;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblLocationName.text = nil;
}
@end
