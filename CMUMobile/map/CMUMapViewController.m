//map
//  CMUMapViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <QuartzCore/QuartzCore.h>
#import "CMUFileUtils.h"
#import "global.h"
#import "CMUMapTableViewCell.h"
#import "CMUMapMarkerView.h"
#import "CMUUIUtils.h"
#import "CMUWebServiceManager.h"

#define ZONE1_LAT 18.800605
#define ZONE1_LNG 98.953139

#define ZONE2_LAT 18.789605
#define ZONE2_LNG 98.974261

#define ZONE3_LAT 18.757788
#define ZONE3_LNG 98.942772

#define DEFAULT_ZONE_ZOOM 15

#define MAPFEED_COORD(x) CLLocationCoordinate2DMake(x.lat, x.lng)

static NSString *cellIdentifierIPhone = @"CMUMapTableViewCell";

@interface CMUMapViewController ()
#define CMU_DEFAULT_MAP_CONTENT_DIR @"map"
@property(nonatomic, strong) CMUMapVersionController *versionController;
@property(nonatomic, strong) CMUMapFeedModel *mapModel;
@property(nonatomic, strong) NSArray *jumboMapFeedList;

//@property(nonatomic, strong) NSMutableArray *makerList;

@property(nonatomic, weak) IBOutlet UIButton *btnMenu;

//bar + menu items
@property(nonatomic, weak) IBOutlet UIView *viewBar;
@property(nonatomic, weak) IBOutlet UIButton *btnCollcapseMenu;
@property(nonatomic, weak) IBOutlet UIButton *btnWifi;
@property(nonatomic, weak) IBOutlet UIButton *btnFood;
@property(nonatomic, weak) IBOutlet UIButton *btnSport;
@property(nonatomic, weak) IBOutlet UIButton *btnDorm;
@property(nonatomic, weak) IBOutlet UIButton *btnATM;
@property(nonatomic, weak) IBOutlet UIButton *btnBuilding;
@property(nonatomic, weak) IBOutlet UIButton *btnCar;

//view menu items option
@property(nonatomic, weak) IBOutlet UIView *viewWifi;
@property(nonatomic, weak) IBOutlet UIView *viewFood;
@property(nonatomic, weak) IBOutlet UIView *viewSport;
@property(nonatomic, weak) IBOutlet UIView *viewDorm;
@property(nonatomic, weak) IBOutlet UIView *viewATM;
@property(nonatomic, weak) IBOutlet UIView *viewBuilding;
@property(nonatomic, weak) IBOutlet UIView *viewCar;

@property(nonatomic, weak) IBOutlet UIButton *btnWifiSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnFoodSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnSportSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnDormSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnATMSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnBuildingSelected;
@property(nonatomic, weak) IBOutlet UIButton *btnCarSelected;

@property(nonatomic, weak) IBOutlet UIButton *btnWifiDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnFoodDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnSportDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnDormDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnATMDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnBuildingDetail;
@property(nonatomic, weak) IBOutlet UIButton *btnCarDetail;

@property(nonatomic, weak) IBOutlet UIView *viewDetail;
@property(nonatomic, weak) IBOutlet UILabel *lblDetailTitle;
@property(nonatomic, weak) IBOutlet UITableView *tableView;


//zone and current location
@property(nonatomic, weak) IBOutlet UIView *viewZone;
@property(nonatomic, weak) IBOutlet UIButton *btnZone;
@property(nonatomic, weak) IBOutlet UIButton *btnCollapseZone;
@property(nonatomic, weak) IBOutlet UIButton *btnZone1;
@property(nonatomic, weak) IBOutlet UIButton *btnZone2;
@property(nonatomic, weak) IBOutlet UIButton *btnZone3;
@property(nonatomic, weak) IBOutlet UIButton *btnCurrentLocation;

//jumbo map
@property(nonatomic, strong)UIImage *jumbomapStateDown;
@property(nonatomic, strong)UIImage *jumbomapStateIdle;
@property(nonatomic, strong)UIImage *jumbomapStateLow;
@property(nonatomic, strong)UIImage *jumbomapStateHigh;
@property(nonatomic, strong)UIImage *jumbomapStateVeryHigh;
@property(nonatomic, strong)UIImage *jumbomapStateVeryMaintenance;


- (IBAction)buttonMenuBarPress:(id)sender;
- (IBAction)buttonZonePress:(id)sender;
@end

typedef enum {
    BAR_STATE_COLLAPSE,
    BAR_STATE_EXPAND,
}BAR_STATE;

typedef enum {
    BAR_ITEM_STATE_NONE,
    BAR_ITEM_STATE_SELECTED,
    //BAR_ITEM_STATE_EXPAND,
}BAR_ITEM_STATE;

typedef enum {
    ZONE_STATE_COLLAPSE,
    ZONE_STATE_EXPAND,
}ZONE_STATE;


@implementation CMUMapViewController{
    GMSMapView *mapView_;
    CLLocationManager *locationManager;
    BAR_STATE barState;
    ZONE_STATE zoneState;
    
    BAR_ITEM_STATE wifiItemState;
    BAR_ITEM_STATE foodItemState;
    BAR_ITEM_STATE sportItemState;
    BAR_ITEM_STATE dormItemState;
    BAR_ITEM_STATE atmItemState;
    BAR_ITEM_STATE buildingItemState;
    BAR_ITEM_STATE carItemState;
    
    CMU_MAP_GROUP mapTypeShowInList;
    CMUMapFeed *selectLocation;
    CMUMapBusTypeModel *selectMapBusType;
}

- (void)commonInit
{
    barState = BAR_STATE_COLLAPSE;
    zoneState = ZONE_STATE_COLLAPSE;
    
    wifiItemState = BAR_ITEM_STATE_NONE;
    foodItemState = BAR_ITEM_STATE_NONE;
    sportItemState = BAR_ITEM_STATE_NONE;
    dormItemState = BAR_ITEM_STATE_NONE;
    atmItemState = BAR_ITEM_STATE_NONE;
    buildingItemState = BAR_ITEM_STATE_NONE;
    carItemState = BAR_ITEM_STATE_NONE;
    
    mapTypeShowInList = CMU_MAP_GROUP_NONE;//no item show in list;
    
    NSString *absoluteContentPath = [[CMUFileUtils pathForDocumentFolder] stringByAppendingPathComponent:CMU_DEFAULT_MAP_CONTENT_DIR];
    
    NSError *error;
    self.versionController = [[CMUMapVersionController alloc] initWithContentPath:absoluteContentPath error:&error];
    self.versionController.delegate = self;
    self.mapModel = self.versionController.currentMapModel;
    
    self.jumbomapStateDown = [UIImage imageNamed:@"jumbo_map_red.png"];
    self.jumbomapStateIdle = [UIImage imageNamed:@"jumbo_map_white.png"];
    self.jumbomapStateLow = [UIImage imageNamed:@"jumbo_map_green.png"];
    self.jumbomapStateHigh = [UIImage imageNamed:@"jumbo_map_yellow.png"];
    self.jumbomapStateVeryHigh = [UIImage imageNamed:@"jumbo_map_orange.png"];
    self.jumbomapStateVeryMaintenance = [UIImage imageNamed:@"jumbo_map_gray.png"];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self commonInit];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    locationManager = [[CLLocationManager alloc] init]; // Ensure we keep a strong reference
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    CGFloat connnerRadius = 5.0f;
    self.viewWifi.layer.cornerRadius = connnerRadius;
    self.viewWifi.layer.masksToBounds = YES;
    self.viewFood.layer.cornerRadius = connnerRadius;
    self.viewFood.layer.masksToBounds = YES;
    self.viewSport.layer.cornerRadius = connnerRadius;
    self.viewSport.layer.masksToBounds = YES;
    self.viewDorm.layer.cornerRadius = connnerRadius;
    self.viewDorm.layer.masksToBounds = YES;
    self.viewATM.layer.cornerRadius = connnerRadius;
    self.viewATM.layer.masksToBounds = YES;
    self.viewBuilding.layer.cornerRadius = connnerRadius;
    self.viewBuilding.layer.masksToBounds = YES;
    self.viewCar.layer.cornerRadius = connnerRadius;
    self.viewCar.layer.masksToBounds = YES;
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:ZONE1_LAT
                                                            longitude:ZONE1_LNG
                                                                 zoom:DEFAULT_ZONE_ZOOM];
    mapView_ = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    //mapView_.settings.compassButton = YES;
    mapView_.myLocationEnabled = YES;
    //self.view = mapView_;
    [self.view insertSubview:mapView_ atIndex:0];
    
    [mapView_ setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
    NSLayoutConstraint *width =[NSLayoutConstraint
                                constraintWithItem:mapView_
                                attribute:NSLayoutAttributeWidth
                                relatedBy:0
                                toItem:self.view
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint
                                 constraintWithItem:mapView_
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:0
                                 toItem:self.view
                                 attribute:NSLayoutAttributeHeight
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:mapView_
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.view
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:mapView_
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    [self.view addConstraint:width];
    [self.view addConstraint:height];
    [self.view addConstraint:top];
    [self.view addConstraint:leading];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUMapTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPhone];
    
    
    [self.versionController startCheckVersion];
    [self updatePresentStateAnimate:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updatePresentStateAnimate:(BOOL) animated
{
    [super updatePresentState];
    BOOL shouldShowBar = barState == BAR_STATE_EXPAND;
    //self.viewBar.hidden = !shouldShowBar;
    [self hideShowView:self.viewBar show:shouldShowBar animated:animated];
    //self.btnMenu.hidden = shouldShowBar;
    [self hideShowView:self.btnMenu show:!shouldShowBar animated:animated];
    
    BOOL shouldShowZoneBar = zoneState == ZONE_STATE_EXPAND;
    //self.viewZone.hidden = !shouldShowZoneBar;
    [self hideShowView:self.viewZone show:shouldShowZoneBar animated:animated];
    //self.btnZone.hidden = shouldShowZoneBar;
    [self hideShowView:self.btnZone show:!shouldShowZoneBar animated:animated];
    
    
    //self.viewWifi.hidden = !(wifiItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewWifi show:(wifiItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewFood.hidden = !(foodItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewFood show:(foodItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewSport.hidden = !(sportItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewSport show:(sportItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewDorm.hidden = !(dormItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewDorm show:(dormItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewATM.hidden = !(atmItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewATM show:(atmItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewBuilding.hidden = !(buildingItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewBuilding show:(buildingItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    //self.viewCar.hidden = !(carItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND);
    [self hideShowView:self.viewCar show:(carItemState == BAR_ITEM_STATE_SELECTED && barState == BAR_STATE_EXPAND) animated:animated];
    
    //location list
    BOOL shouldShowDetail = mapTypeShowInList != CMU_MAP_GROUP_NONE;
    //self.viewDetail.hidden = !shouldShowDetail;
    [self hideShowView:self.viewDetail show:shouldShowDetail animated:animated];
    self.lblDetailTitle.text = [self getGroupTitle:mapTypeShowInList];
    [self.tableView reloadData];
}

-(void)hideShowView:(UIView *)view  show:(BOOL)show animated:(BOOL) animated
{
    CGFloat alpha = show? 1.0:0.0;
    CGFloat duration = 0.0;
    if(animated)
    {
        duration = 0.3;
    }
    
    [UIView animateWithDuration:duration animations:^{
        view.alpha = alpha;
    }];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (void)markerChange
{
    [mapView_ clear];
    
    BOOL focusMapToShowAllMarker = NO;
    //filter
    NSMutableArray *makerList = [[NSMutableArray alloc] init];
    if(selectLocation)
    {
        [makerList addObject:selectLocation];
        focusMapToShowAllMarker = NO;
    }
    else if(selectMapBusType)
    {
        [makerList addObjectsFromArray:selectMapBusType.mapViewList];
        
        if(selectMapBusType.mapBusPointViewList.count > 0){
            GMSMutablePath *path = [GMSMutablePath path];
            for(CMUMapBusPoint *point in  selectMapBusType.mapBusPointViewList){
                [path addCoordinate:CLLocationCoordinate2DMake(point.lat, point.lng)];
            }
            
            //            GMSPolygon *polygon = [GMSPolygon polygonWithPath:path];
            //            polygon.fillColor = [UIColor clearColor];
            //            polygon.strokeColor = [self colorFromHexString:selectMapBusType.colorCode];
            //            polygon.strokeWidth = 5;
            //            polygon.geodesic = YES;
            //            polygon.map = mapView_;
            
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeColor = [self colorFromHexString:selectMapBusType.colorCode];
            polyline.strokeWidth = 5;
            polyline.geodesic = YES;
            polyline.map = mapView_;
        }
        
        
        
    }
    else
    {
        if(wifiItemState == BAR_ITEM_STATE_SELECTED)
        {
            //NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_WIFI].mapFeedList;
            NSArray *locationList = self.jumboMapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        if(foodItemState == BAR_ITEM_STATE_SELECTED)
        {
            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_FOOD_CENTER].mapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        
        if(sportItemState == BAR_ITEM_STATE_SELECTED)
        {
            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_ARENA].mapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        
        if(dormItemState == BAR_ITEM_STATE_SELECTED)
        {
            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_DORM].mapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        
        if(atmItemState == BAR_ITEM_STATE_SELECTED)
        {
            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_ATM].mapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        
        if(buildingItemState == BAR_ITEM_STATE_SELECTED)
        {
            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_BUILDING].mapFeedList;
            [makerList addObjectsFromArray:locationList];
        }
        
        if(carItemState == BAR_ITEM_STATE_SELECTED)
        {
            //for bus it is bus type not marker
            //            NSArray *locationList = [self.mapModel getByMapType:CMU_MAP_GROUP_TERMINAL].mapFeedList;
            //            [makerList addObjectsFromArray:locationList];
        }
        
        focusMapToShowAllMarker = YES;
    }
    
    
    //add marker
    for(CMUMapFeed *mapFeed in  makerList)
    {
        GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(mapFeed.lat, mapFeed.lng)];
        
        BOOL isJumboMapFeed = [self.jumboMapFeedList containsObject:mapFeed];
        
        if(!isJumboMapFeed)
        {
            UIImage *icon = [self markerIconForMapFeed:mapFeed];
            marker.icon = icon;
            CGFloat anchorX = 14.0 / icon.size.width;
            marker.groundAnchor = CGPointMake(anchorX, 1.0);
        }
        else
        {
            switch (mapFeed.status) {
                case CMU_JUMBO_MAP_STATUS_DOWN:
                    marker.icon = self.jumbomapStateDown;
                    break;
                case CMU_JUMBO_MAP_STATUS_IDLE:
                    marker.icon = self.jumbomapStateIdle;
                    break;
                case CMU_JUMBO_MAP_STATUS_LOW:
                    marker.icon = self.jumbomapStateLow;
                    break;
                case CMU_JUMBO_MAP_STATUS_HIGH:
                    marker.icon = self.jumbomapStateHigh;
                    break;
                case CMU_JUMBO_MAP_STATUS_VERY_HIGH:
                    marker.icon = self.jumbomapStateVeryHigh;
                    break;
                case CMU_JUMBO_MAP_STATUS_MAINTENANCE:
                    marker.icon = self.jumbomapStateVeryMaintenance;
                    break;
                default:
                    break;
            }
            marker.title = mapFeed.name;
        }
        marker.map = mapView_;
    }
    
    //
    if(focusMapToShowAllMarker)
    {
        [self focusMapToShowAllMarker:makerList];
    }
    else
    {
        if(makerList.count > 0)
        {
            CMUMapFeed *mapFeed = [makerList objectAtIndex:0];
            [mapView_ animateToLocation:CLLocationCoordinate2DMake(mapFeed.lat, mapFeed.lng)];
            [mapView_ animateToZoom:15.0f];
        }
    }
    
}

- (UIImage *)markerIconForMapFeed:(CMUMapFeed *) mapFeed
{
    UIImage *markerImage = nil;
    UIImage *markerIcon = nil;
    UIColor *borderColor = nil;
    
    if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_WIFI].mapFeedList mapFeed:mapFeed])
    {
        markerImage = [UIImage imageNamed:@"map_marker_wifi"];
        markerIcon = [UIImage imageNamed:@"map_marker_icon_wifi"];
        borderColor = [UIColor colorWithRed:160.0/255 green:211.0/255 blue:118.0/255 alpha:1.0];
    }
    else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_FOOD_CENTER].mapFeedList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_canteen"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_canteen"];
        borderColor = [UIColor colorWithRed:118.0/255 green:177.0/255 blue:211.0/255 alpha:1.0];
    }
    else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_ARENA].mapFeedList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_gym"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_gym"];
        borderColor = [UIColor colorWithRed:211.0/255 green:118.0/255 blue:153.0/255 alpha:1.0];
    }
    else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_DORM].mapFeedList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_dorm"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_dorm"];
        borderColor = [UIColor colorWithRed:211.0/255 green:208.0/255 blue:118.0/255 alpha:1.0];
    }
    else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_ATM].mapFeedList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_atm"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_atm"];
        borderColor = [UIColor colorWithRed:118.0/255 green:211.0/255 blue:175.0/255 alpha:1.0];
    }
    else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_BUILDING].mapFeedList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_faculty"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_faculty"];
        borderColor = [UIColor colorWithRed:146.0/255 green:118.0/255 blue:211.0/255 alpha:1.0];
    }
    //else if([self isMapFeedListContainMapFeed:[self.mapModel getByMapType:CMU_MAP_GROUP_TERMINAL].mapFeedList mapFeed:mapFeed])
    else if([self isMapFeedListContainMapFeed:selectMapBusType.mapViewList mapFeed:mapFeed])
    {
        markerImage =[UIImage imageNamed:@"map_marker_busstop"];
        markerIcon =[UIImage imageNamed:@"map_marker_icon_busstop"];
        borderColor = [UIColor colorWithRed:211.0/255 green:118.0/255 blue:118.0/255 alpha:1.0];
    }
    
    CMUMapMarkerView *markerView = [[CMUMapMarkerView alloc] initWithFrame:CGRectZero];
    markerView.imvMarker.image = markerImage;
    markerView.imvIcon.image = markerIcon;
    markerView.lblTitle.text = mapFeed.name;
    markerView.borderColor = borderColor;
    [markerView sizeToFit];
    [markerView layoutSubviews];
    return [CMUUIUtils imageFromView:markerView];
}

- (BOOL)isMapFeedListContainMapFeed:(NSArray *) mapFeedList mapFeed:(CMUMapFeed *) mapFeed
{
    //use pointer to compare
    for(CMUMapFeed *element in mapFeedList)
    {
        if(element == mapFeed)
        {
            return  YES;
        }
    }
    return NO;
}

- (NSString *)getGroupTitle:(CMU_MAP_GROUP) mapGroup
{
    switch (mapGroup) {
        case CMU_MAP_GROUP_NONE: return @""; break;
        case CMU_MAP_GROUP_WIFI: return @"WIFI"; break;
        case CMU_MAP_GROUP_FOOD_CENTER: return @"ร้านอาหาร"; break;
        case CMU_MAP_GROUP_ARENA: return @"สนามกีฬา"; break;
        case CMU_MAP_GROUP_DORM: return @"หอพัก"; break;
        case CMU_MAP_GROUP_ATM: return @"ATM"; break;
        case CMU_MAP_GROUP_BUILDING: return @"อาคาร"; break;
        case CMU_MAP_GROUP_TERMINAL: return @"จุดโดยสาร"; break;
        default: return @""; break;
    }
}

- (void)focusMapToShowAllMarker:(NSArray *)mapFeedList
{
    if(mapFeedList.count > 0)
    {
        CMUMapFeed *mapFeedFirst = [mapFeedList objectAtIndex:0];
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:MAPFEED_COORD(mapFeedFirst) coordinate:MAPFEED_COORD(mapFeedFirst)];
        for(CMUMapFeed *mapFeed in mapFeedList)
        {
            bounds = [bounds includingCoordinate:MAPFEED_COORD(mapFeed)];
        }
        [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f /*15.0f*/]];
    }
}

- (void)moveMapToLat:(CGFloat) lat long:(CGFloat) lng
{
    [mapView_ animateToLocation:CLLocationCoordinate2DMake(lat, lng)];
}


- (void)buttonMenuBarPress:(id)sender
{
    //keep state
    //BOOL currentListExpand = mapTypeShowInList != CMU_MAP_GROUP_NONE;
    
    //init by hide list
    mapTypeShowInList = CMU_MAP_GROUP_NONE;
    
    //toggle menu should not reset selected location
    selectLocation = (sender == self.btnMenu || sender == self.btnCollcapseMenu)?selectLocation : nil;
    selectMapBusType = (sender == self.btnMenu || sender == self.btnCollcapseMenu)?selectMapBusType : nil;
    
    //main menu item
    if(sender == self.btnMenu)
    {
        barState = BAR_STATE_EXPAND;
    }
    else if(sender == self.btnCollcapseMenu)
    {
        barState = BAR_STATE_COLLAPSE;
    }
    
    //menu bar items normal
    else if(sender == self.btnWifi)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        wifiItemState = BAR_ITEM_STATE_SELECTED;
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getJumboMap:self success:^(id result) {
            [self dismisHUD];
            self.jumboMapFeedList = (NSArray *)result;
            [self markerChange];
        } failure:^(NSError *error) {
            [self dismisHUD];
            [self markerChange];
        }];
    }
    else if(sender == self.btnFood)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        foodItemState = BAR_ITEM_STATE_SELECTED;
        [self markerChange];
    }
    else if(sender == self.btnSport)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        sportItemState = BAR_ITEM_STATE_SELECTED;
        [self markerChange];
    }
    else if(sender == self.btnDorm)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        dormItemState = BAR_ITEM_STATE_SELECTED;
        [self markerChange];
    }
    else if(sender == self.btnATM)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        atmItemState = BAR_ITEM_STATE_SELECTED;
        [self markerChange];
    }
    else if(sender == self.btnBuilding)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        
        buildingItemState = BAR_ITEM_STATE_SELECTED;
        [self markerChange];
    }
    else if(sender == self.btnCar)
    {
        //        if(currentListExpand)
        //            [self resetMenuItemOptionState];
        [self showHUDLoading];
        [[CMUWebServiceManager sharedInstance] getMapBusType:self success:^(id result) {
            [self dismisHUD];
            CMUMapFeed *busType = [[CMUMapFeed alloc] init];
            busType.mapFeedId = CMU_MAP_GROUP_TERMINAL;
            busType.mapFeedList = result;
            
            [self.mapModel.mapFeedList removeObject:busType];//remove old data
            [self.mapModel.mapFeedList addObject:busType];//add new data
            
            //old flow
            [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_TERMINAL];
            [self markerChange];
            
            //use this code if not want to un select
            //            carItemState = BAR_ITEM_STATE_SELECTED;
            //            [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_TERMINAL];
            
            
            //add update present state to new flow
            [self updatePresentStateAnimate:YES];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
        
    }
    
    //menu bar items selected
    else if(sender == self.btnWifiSelected)
    {
        wifiItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnFoodSelected)
    {
        foodItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnSportSelected)
    {
        sportItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnDormSelected)
    {
        dormItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnATMSelected)
    {
        atmItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnBuildingSelected)
    {
        buildingItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    else if(sender == self.btnCarSelected)
    {
        carItemState = BAR_ITEM_STATE_NONE;
        [self markerChange];
    }
    
    //menu bar items selected detail
    else if(sender == self.btnWifiDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_WIFI];
    }
    else if(sender == self.btnFoodDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_FOOD_CENTER];
    }
    else if(sender == self.btnSportDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_ARENA];
    }
    else if(sender == self.btnDormDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_DORM];
    }
    else if(sender == self.btnATMDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_ATM];
    }
    else if(sender == self.btnBuildingDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_BUILDING];
    }
    else if(sender == self.btnCarDetail)
    {
        [self setStateForShowDetailWithMapType:CMU_MAP_GROUP_TERMINAL];
    }
    
    // hide zone option if display group detail
    if(mapTypeShowInList != CMU_MAP_GROUP_NONE)
    {
        zoneState = ZONE_STATE_COLLAPSE;
    }
    
    [self updatePresentStateAnimate:YES];
    
}


- (void)buttonZonePress:(id)sender
{
    //hide list
    mapTypeShowInList = CMU_MAP_GROUP_NONE;
    
    if(sender == self.btnZone)
    {
        zoneState = ZONE_STATE_EXPAND;
    }
    else if(sender == self.btnCollapseZone)
    {
        zoneState = ZONE_STATE_COLLAPSE;
    }
    
    else if(sender == self.btnZone1)
    {
        [self moveMapToLat:ZONE1_LAT long:ZONE1_LNG];
        zoneState = ZONE_STATE_COLLAPSE;
    }
    else if(sender == self.btnZone2)
    {
        [self moveMapToLat:ZONE2_LAT long:ZONE2_LNG];
        zoneState = ZONE_STATE_COLLAPSE;
    }
    else if(sender == self.btnZone3)
    {
        [self moveMapToLat:ZONE3_LAT long:ZONE3_LNG];
        zoneState = ZONE_STATE_COLLAPSE;
    }
    
    //current location
    else if(sender == self.btnCurrentLocation)
    {
        [self moveMapToLat:mapView_.myLocation.coordinate.latitude long:mapView_.myLocation.coordinate.longitude];
    }
    
    [self updatePresentStateAnimate:YES];
}

- (void)setStateForShowDetailWithMapType:(CMU_MAP_GROUP) mapType
{
    mapTypeShowInList = mapType;
    
    //reset another option
    wifiItemState = mapType == CMU_MAP_GROUP_WIFI? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    foodItemState = mapType == CMU_MAP_GROUP_FOOD_CENTER? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    sportItemState = mapType == CMU_MAP_GROUP_ARENA? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    dormItemState = mapType == CMU_MAP_GROUP_DORM? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    atmItemState = mapType == CMU_MAP_GROUP_ATM? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    buildingItemState = mapType == CMU_MAP_GROUP_BUILDING? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
    carItemState = mapType == CMU_MAP_GROUP_TERMINAL? BAR_ITEM_STATE_SELECTED: BAR_ITEM_STATE_NONE;
}

- (void)resetMenuItemOptionState
{
    wifiItemState = BAR_ITEM_STATE_NONE;
    foodItemState = BAR_ITEM_STATE_NONE;
    sportItemState = BAR_ITEM_STATE_NONE;
    dormItemState = BAR_ITEM_STATE_NONE;
    atmItemState = BAR_ITEM_STATE_NONE;
    buildingItemState = BAR_ITEM_STATE_NONE;
    carItemState = BAR_ITEM_STATE_NONE;
}

#pragma mark - CMUMapVersionControllerDelegate
- (void)mapVersionControllerDidStartedCheckNewVersion:(CMUMapVersionController *)versionController
{
    [self showHUDLoading];
    DebugLog(@"");
}

- (void)mapVersionController:(CMUMapVersionController *)versionController didFinishedCheckNewVersion:(CMUPhoneVersionModel *) newVersion//nil = no update
                       error:(NSError*) error
{
    DebugLog(@"");
    if(newVersion == nil || error)
    {
        [self dismisHUD];
    }
}

- (void)mapVersionController:(CMUMapVersionController *)versionController didStartedDownloadVersion:(CMUMapVersionModel *) newVersion
{
    DebugLog(@"");
}

- (void)mapVersionController:(CMUMapVersionController *)versionController didFinishedDownloadVersion:(CMUMapVersionModel *) serverVersion
         mapFacultyViewModel:(CMUMapFeedModel *) mapModel
                       error:(NSError *) error
{
    DebugLog(@"");
    [self dismisHUD];
    self.mapModel = self.versionController.currentMapModel;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CMUMapFeed *selecteGroup = [self.mapModel getByMapType:mapTypeShowInList];
    return selecteGroup.mapFeedList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  32;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUMapFeed *selecteGroup = [self.mapModel getByMapType:mapTypeShowInList];
    CMUMapTableViewCell *cell = nil;
    //    if(IS_IPHONE)
    //    {
    cell = (CMUMapTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPhone forIndexPath:indexPath];
    //    }
    //    else
    //    {
    //        BOOL hasImage =![CMUStringUtils isEmpty:newsFeed.imageLink];
    //        if(hasImage)
    //        {
    //            cell = (CMUNewsIPadImageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPadWithImage forIndexPath:indexPath];
    //        }
    //        else
    //        {
    //            cell = (CMUNewsIPadTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPadNoImage forIndexPath:indexPath];
    //        }
    //    }
    
    
    cell.mapFeed = [selecteGroup.mapFeedList objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(mapTypeShowInList != CMU_MAP_GROUP_TERMINAL){
        CMUMapFeed *selecteGroup = [self.mapModel getByMapType:mapTypeShowInList];
        selectLocation = [selecteGroup.mapFeedList objectAtIndex:indexPath.row];
        [self moveMapToLat:selectLocation.lat long:selectLocation.lng];
        
        ////hide list
        mapTypeShowInList = CMU_MAP_GROUP_NONE;
        [self resetMenuItemOptionState];
        [self updatePresentStateAnimate:YES];
        [self markerChange];
    }else{
        [self showHUDLoading];
        CMUMapFeed *selecteGroup = [self.mapModel getByMapType:mapTypeShowInList];
        CMUMapFeed *selectedBusType = [selecteGroup.mapFeedList objectAtIndex:indexPath.row];
        [[CMUWebServiceManager sharedInstance] getMapBusTypeByType:self type:selectedBusType.busTypeID success:^(id result) {
            [self dismisHUD];
            selectMapBusType = result;
            ////hide list
            mapTypeShowInList = CMU_MAP_GROUP_NONE;
            [self resetMenuItemOptionState];
            [self updatePresentStateAnimate:YES];
            [self markerChange];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
    }
    
}



@end
