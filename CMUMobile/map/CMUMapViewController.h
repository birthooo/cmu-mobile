//
//  CMUMapViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

//
#import "CMUTransitStationView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CMUTransitBusView.h"
#import "CMUWebServiceManager.h"
#import "CMUUIUtils.h"
//
#import "CMUNavBarViewController.h"
#import "CMUMapVersionController.h"
#define BAR_COLOR [UIColor colorWithRed:37/255.0f green:27/255.0f blue:52/255.0f alpha:1.0f]
#define GROUP1_COLOR [UIColor colorWithRed:81/255.0f green:168/255.0f blue:3/255.0f alpha:1.0f]
#define GROUP2_COLOR [UIColor colorWithRed:239/255.0f green:135/255.0f blue:32/255.0f alpha:1.0f]
#define GROUP3_COLOR [UIColor colorWithRed:233/255.0f green:33/255.0f blue:33/255.0f alpha:1.0f]
#define GROUP4_COLOR [UIColor colorWithRed:34/255.0f green:116/255.0f blue:240/255.0f alpha:1.0f]
#define GROUP5_COLOR [UIColor colorWithRed:157/255.0f green:34/255.0f blue:178/255.0f alpha:1.0f]
#define GROUP6_COLOR [UIColor colorWithRed:42/255.0f green:112/255.0f blue:218/255.0f alpha:1.0f]
#define GROUP_ALL_COLOR [UIColor colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0f]
#define GET_BUS_INTERVAL 10.0f
@interface CMUMapViewController : CMUNavBarViewController<CMUMapVersionControllerDelegate,GMSMapViewDelegate>
@property NSMutableArray *busStationArray;

@end
