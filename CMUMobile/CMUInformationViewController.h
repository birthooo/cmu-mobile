//
//  CMUInformationViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/11/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUSectionInfoView.h"
#import "CMUInformationDetailViewController.h"
@interface CMUInformationViewController : CMUNavBarViewController<CMUSectionInfoViewDelegate, CMUInformationDetailViewControllerDelegate>

@end
