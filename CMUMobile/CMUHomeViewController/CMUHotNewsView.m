//
//  CMUHotNewsView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUHotNewsView.h"
#import "global.h"

@interface CMUHotNewsView()
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) IBOutlet UIPageControl *pageControl;

@property(nonatomic, unsafe_unretained)int pageCount;
@property(nonatomic, strong)NSTimer *timmer;
@end

#define NEWSITEM_PER_PAGE 2
#define SLIDE_INTERVAL 5
#define SLIDE_ANIMATION_DURATION 0.75
@implementation CMUHotNewsView

- (void)customInit
{
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUHotNewsView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    self.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = YES;
    [self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    self.scrollView.delegate = self;
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addSubview:contentView];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setHotNewsModel:(CMUNewsModel *)hotNewsModel
{
    _hotNewsModel = hotNewsModel;
    [self initByNewsModel];
    [self updatePresentState];
    
    if(self.pageCount > 1)
    {
        [self resumeSlide];
    }
}

- (void)initByNewsModel
{
    
    CGSize newsPageSize = self.scrollView.frame.size;
    
    //reset view and model
    
    for(UIView *view in self.scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    self.pageCount = self.hotNewsModel.newsFeedList.count / NEWSITEM_PER_PAGE + (self.hotNewsModel.newsFeedList.count % NEWSITEM_PER_PAGE > 0? 1 : 0);
    self.pageCount = self.pageCount == 0? 1 : self.pageCount;
    
    self.scrollView.contentSize = CGSizeMake(self.pageCount * newsPageSize.width, newsPageSize.height);
    
    CGFloat newItemHeight = 48;
    if(IS_IPAD)
    {
        newItemHeight = 80;
    }
    
    for(int i = 0; i < self.hotNewsModel.newsFeedList.count; i++)
    {
        int currentPageIndex = i / NEWSITEM_PER_PAGE;
        CGFloat x = currentPageIndex * newsPageSize.width;
        CGFloat y = (i % NEWSITEM_PER_PAGE) * newItemHeight;
        CMUHotNewsItemView *newsItemView = [[CMUHotNewsItemView alloc] initWithFrame:CGRectMake(x, y, newsPageSize.width, newItemHeight)];
        newsItemView.delegate = self;
        newsItemView.newsFeed = [self.hotNewsModel.newsFeedList objectAtIndex:i];
        [self.scrollView addSubview:newsItemView];
    }
    
    self.pageControl.numberOfPages = self.pageCount;

}

- (void)resumeSlide
{
    if(self.timmer)
    {
        [self.timmer invalidate];
        self.timmer = nil;
    }
    self.timmer = [NSTimer scheduledTimerWithTimeInterval: SLIDE_INTERVAL
                                                  target: self
                                                selector:@selector(onTick:)
                                               userInfo: nil repeats:YES];
}

- (void)stopSlide
{
    [self.timmer invalidate];
    self.timmer = nil;
}

-(void)onTick:(NSTimer *)timer {
    //int currentIndex = [self currentPage];
    if([self canMoveNextPage])
    {
        [self doMoveNextPage:YES];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.scrollView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self doMovePageIndex:0 animate:NO];
            [UIView animateWithDuration:0.3 animations:^{
                self.scrollView.alpha = 1.0;
            } completion:^(BOOL finished) {
            }];
        }];
    }
}

-(void)pageTurn:(UIPageControl *) page
{
    int pageIndex = page.currentPage;
    [self stopSlide];
    [self doMovePageIndex:pageIndex animate:YES];
    [self resumeSlide];
}

- (int)currentPage
{
    CGFloat width = self.scrollView.frame.size.width;
    NSInteger pageIndex = (self.scrollView.contentOffset.x + (0.5f * width)) / width;
    return pageIndex;
}

- (BOOL)canMoveNextPage
{
    return [self currentPage] < (self.pageCount - 1);
}

- (void)doMoveNextPage:(BOOL) animated
{
    [self doMovePageIndex:[self currentPage] + 1 animate:animated];
}

- (void)doMovePageIndex:(int)pageIndex animate:(BOOL) animated
{
    CGPoint contentOffset = CGPointMake(self.scrollView.frame.size.width * pageIndex, 0);
    CGFloat duration = 0.0;
    if(animated)
    {
        duration = SLIDE_ANIMATION_DURATION;
    }
    
    [UIView animateWithDuration:duration animations:^{
        self.scrollView.contentOffset = contentOffset;
    }];
}

- (void)updatePresentState
{
    int currentPage = [self currentPage];
    self.pageControl.currentPage = currentPage;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark CMUHotNewsItemViewDelegate
- (void)hotNewsItemViewDidSelected:(CMUHotNewsItemView *) hotNewsItemView
{
    if(self.delegate)
    {
        [self.delegate hotNewsView:self hotNewsItemViewDidSelected:hotNewsItemView];
    }
}


#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //DebugLog(@"");
    [self updatePresentState];
}
// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self stopSlide];
}

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
    [self resumeSlide];
}
// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //DebugLog(@"");
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    //DebugLog(@"");
}   // called on finger up as we are moving
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //DebugLog(@"");
}      // called when scroll view grinds to a halt

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    //DebugLog(@"");
} // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating



@end
