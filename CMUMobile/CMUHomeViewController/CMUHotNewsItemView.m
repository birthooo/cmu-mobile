//
//  CMUHotNewsItemView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUHotNewsItemView.h"

@interface CMUHotNewsItemView()
@property(nonatomic, strong)IBOutlet UILabel *detailLabel;
@end
@implementation CMUHotNewsItemView

- (void)customInit
{
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUHotNewsItemView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selfTapped:)];
    tapGesture.cancelsTouchesInView = NO;
    self.userInteractionEnabled = YES;
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self addGestureRecognizer:tapGesture];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setNewsFeed:(CMUNewsFeed *)newsFeed
{
    _newsFeed = newsFeed;
    self.detailLabel.text = newsFeed.topic;
}

- (void)selfTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    if(self.delegate)
    {
        [self.delegate hotNewsItemViewDidSelected:self];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
