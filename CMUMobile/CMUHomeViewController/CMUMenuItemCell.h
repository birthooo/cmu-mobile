//
//  CMUMenuItemCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMGridViewCell.h"
#import "CMUMenuItem.h"
@interface CMUMenuItemCell  : GMGridViewCell
@property(nonatomic, strong)CMUMenuItem *menuItem;
@end
