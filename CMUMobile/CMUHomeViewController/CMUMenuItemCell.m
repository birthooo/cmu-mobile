//
//  CMUMenuItemCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMenuItemCell.h"

@interface CMUMenuItemCell()
@property(nonatomic, strong)IBOutlet UIImageView *iconImageView;
@property(nonatomic, strong)IBOutlet UILabel *titleLabel;
@end

@implementation CMUMenuItemCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUMenuItemCell" owner:self options:nil];
        UIView *contentView = [nibObjects objectAtIndex:0];
        self.contentView = contentView;
    }
    return self;
}

-(void)setMenuItem:(CMUMenuItem *)menuItem
{
    _menuItem = menuItem;
    self.iconImageView.image = self.menuItem.iconImage;
    self.titleLabel.text = self.menuItem.title;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.iconImageView.image = nil;
    self.titleLabel.text = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
