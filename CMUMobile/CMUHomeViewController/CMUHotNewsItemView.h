//
//  CMUHotNewsItemView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNewsFeed.h"

@class CMUHotNewsItemView;
@protocol CMUHotNewsItemViewDelegate <NSObject>
- (void)hotNewsItemViewDidSelected:(CMUHotNewsItemView *) hotNewsItemView;
@end
@interface CMUHotNewsItemView : UIView
@property(nonatomic, unsafe_unretained)id<CMUHotNewsItemViewDelegate> delegate;
@property(nonatomic, strong)CMUNewsFeed *newsFeed;
@end
