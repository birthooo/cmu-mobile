//
//  CMUHotNewsView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNewsModel.h"
#import "CMUHotNewsItemView.h"
@class CMUHotNewsView;
@protocol CMUHotNewsViewDelegate <NSObject>
- (void)hotNewsView:(CMUHotNewsView *) hotNewsView hotNewsItemViewDidSelected:(CMUHotNewsItemView *) hotNewsItemView;
@end
@interface CMUHotNewsView : UIView <UIScrollViewDelegate, CMUHotNewsItemViewDelegate>;
@property(nonatomic, unsafe_unretained) id<CMUHotNewsViewDelegate> delegate;
@property(nonatomic, strong) CMUNewsModel *hotNewsModel;
@end
