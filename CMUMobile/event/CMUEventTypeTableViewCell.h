//
//  CMUEventTypeTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUEventDetailFeedModel.h"

@interface CMUEventTypeTableViewCell : UITableViewCell
@property(nonatomic, strong)CMUEventType* eventType;
@end
