//
//  CMUEventViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/10/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUCalendarView.h"
#import "CMUEventTypePopupView.h"
#import "CMUEventTypeTableViewCell.h"
#import "PopoverView.h"
typedef enum {
    ACTIVITY_VIEW_MODE_NORMAL = 0,
    ACTIVITY_VIEW_MODE_NOTIFICATION = 1
} ActivityViewMode;

@interface CMUEventViewController : CMUNavBarViewController<CMUCalendarDelegate, PopoverViewDelegate, CMUEventTypePopupViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property(nonatomic, unsafe_unretained) ActivityViewMode activityViewMode;
@end
