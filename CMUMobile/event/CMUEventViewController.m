//
//  CMUEventViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/10/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUEventViewController.h"
#import "global.h"
#import "UIColor+CMU.h"
#import "CMUWebServiceManager.h"
#import "CMUEventDetailFeedModel.h"
#import "CMUEventTableViewCell.h"
#import "CMUUtils.h"
#import "CMUSettings.h"
#import "CMUWebViewController.h"

static NSString *cellIdentifier = @"CMUEventTableViewCell";

@interface CMUEventViewController ()
@property(nonatomic, weak) IBOutlet UIView *topView;
@property(nonatomic, weak) IBOutlet UILabel *lblEventType;
@property(nonatomic, weak) IBOutlet UIButton *btnEventType;
@property(nonatomic, strong) CMUEventTypePopupView *eventTypeView;
@property(nonatomic, strong) PopoverView *popoverView;

@property(nonatomic, weak) IBOutlet UIView *calBarView;
@property(nonatomic, weak) IBOutlet UIButton *btnPreviousMonth;
@property(nonatomic, weak) IBOutlet UILabel *lblCurrentMonth;
@property(nonatomic, weak) IBOutlet UIButton *btnNextMonth;
@property(nonatomic, weak) IBOutlet UIButton *btnCalendar;

@property(nonatomic, strong) IBOutlet CMUCalendarView *calendarView;
@property(nonatomic, weak) IBOutlet UITableView *tableView;

@property(nonatomic, strong) NSDateFormatter *dateFormatter;

-(IBAction) lblSelectedEventTypeTapped:(id) sender;
-(IBAction) btnEventTypeClicked:(id) sender;
-(IBAction) btnPreviousMonthClicked:(id) sender;
-(IBAction) btnNextMonthClicked:(id) sender;
-(IBAction) btnCalendarClicked:(id) sender;


//@property(nonatomic, unsafe_unretained) CMU_EVENT_VIEW_MODE viewMode;
@property(nonatomic, strong) CMUEventDetailFeedModel *eventModel;
//fetch model
@property(nonatomic, unsafe_unretained)CMU_EVENT_TYPE eventType;
@property(nonatomic, strong) NSArray *feedList;

@end

@implementation CMUEventViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.viewMode = [[CMUSettings sharedInstance] eventViewMode];
        self.eventType = CMU_EVENT_TYPE_HIGHLIGHT;
        //self.eventType = CMU_EVENT_TYPE_HOLIDAY;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    self.dateFormatter.dateFormat = @"LLLL yyyy";
    self.dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    self.lblEventType.textColor = [UIColor cmuBlackColor];
    CMUEventType *eventType = [CMUEventType getByEventId:self.eventType];
    self.lblEventType.text = eventType.title;
    
    self.eventTypeView = [[CMUEventTypePopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 220): CGRectMake(0, 0, 280, 220)];
    self.eventTypeView.delegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUEventTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    self.calendarView = [[CMUCalendarView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)];
    self.calendarView.delegate = self;
    self.calendarView.dateFont = IS_IPAD?[UIFont boldSystemFontOfSize:28]:[UIFont systemFontOfSize:18.0];
    if(IS_IPAD)
    {
        self.calendarView.dateOfWeekFont =  [UIFont boldSystemFontOfSize:28];
    }
    
    self.lblCurrentMonth.text = [self.dateFormatter stringFromDate:self.calendarView.monthShowing];
    
    [self getEventByCriteria];
    [self updatePresentState];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    if(self.activityViewMode == ACTIVITY_VIEW_MODE_NORMAL)
    {
        [super updatePresentState];
    }
    else
    {
        self.navigationItem.title = @"Activity";
        //  just show default back button in news detail mode
    }
}

- (void)getEventByCriteria
{
    NSDate *selectedMonth = [self.calendarView monthShowing];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:selectedMonth];
    
    //int day = [components day];
    int month = (int)[components month];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *yearStr = [dateFormatter stringFromDate:selectedMonth];
    int year = [yearStr intValue];
    
    CMU_EVENT_TYPE eventType = self.eventType;
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance]  getEventDetail:self year:year month:month type:eventType  success:^(id result) {
        [self populateFeedList:result];
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self populateFeedList:nil];
        [self updatePresentState];
        [self dismisHUD];
    }];
}

- (void)populateFeedList:(CMUEventDetailFeedModel *) eventModel
{
    self.eventModel = eventModel;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    NSMutableArray *specialDateList = [[NSMutableArray alloc] init];
    
    for(CMUEventDetailFeed *eventDetail in eventModel.eventdetailFeedList)
    {
        [specialDateList addObject:eventDetail.startDate];
    }
    
    if(!self.calendarView.selectedDate)
    {
        [tempFeedList addObjectsFromArray:eventModel.eventdetailFeedList];
    }
    else
    {
        for(CMUEventDetailFeed *eventDetail in eventModel.eventdetailFeedList)
        {
            BOOL isSameDate = [CMUUtils isSameDayWithDate1:eventDetail.startDate date2:self.calendarView.selectedDate];
            if(isSameDate)
            {
                [tempFeedList addObject:eventDetail];
            }
        }
    }
    
    self.calendarView.specialDateList = specialDateList;
    self.feedList = tempFeedList;
    [self.tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
//    //perform sort
//    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
//        
//        CMUEventFeed *first = (CMUEventFeed*)a;
//        CMUEventFeed *second = (CMUEventFeed*)b;
//        return [second.update compare:first.update];
//    }];
//    
//    self.feedList = sortedFeedList;
//    
//    [self.tableView reloadData];
//    if(scrollToTop)
//    {
//        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
//    }
}

-(void) lblSelectedEventTypeTapped:(id) sender
{
    [self btnEventTypeClicked:sender];
}

-(void) btnEventTypeClicked:(id) sender
{
    
    CGRect newsTypeButtonFrame = [self.view convertRect:self.btnEventType.frame fromView:self.topView];
    CGPoint pointToShow = CGPointMake(newsTypeButtonFrame.origin.x + newsTypeButtonFrame.size.width / 2, newsTypeButtonFrame.origin.y + newsTypeButtonFrame.size.height - 5);
    
    self.popoverView = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverView.delegate = self;
    [self.popoverView showAtPoint:pointToShow inView:self.view withContentView:self.eventTypeView];
    
}

-(void) btnPreviousMonthClicked:(id) sender
{
    DebugLog(@"");
    [self.calendarView  moveCalendarToPreviousMonth];
    [self.calendarView selectDate:nil makeVisible:NO];
}

-(void) btnNextMonthClicked:(id) sender
{
    DebugLog(@"");
    [self.calendarView moveCalendarToNextMonth];
    [self.calendarView selectDate:nil makeVisible:NO];
}

-(void) btnCalendarClicked:(id) sender
{
    DebugLog(@"");
    if([CMUSettings sharedInstance].eventViewMode == CMU_EVENT_VIEW_MODE_LIST)
    {
        [CMUSettings sharedInstance].eventViewMode = CMU_EVENT_VIEW_MODE_CALENDAR_LIST;
    }
    else
    {
        [CMUSettings sharedInstance].eventViewMode = CMU_EVENT_VIEW_MODE_LIST;
    }
    [[CMUSettings sharedInstance] saveEventViewMode];
    [self.tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -CMUEventTypePopupViewDelegate
- (void)eventTypePopupView:(CMUEventTypePopupView *) eventTypePopupView didSelectedEventType:(CMUEventType *) eventType
{
    [self.popoverView dismiss];
    self.eventType = eventType.eventTypeId;
    self.lblEventType.text = eventType.title;
    [self getEventByCriteria];
}

#pragma mark - CMUCalendarViewDelegate
//- (void)calendar:(CMUCalendarView *)calendar configureDateItem:(CMUDateItem *)dateItem forDate:(NSDate *)date;

//- (BOOL)calendar:(CMUCalendarView *)calendar willSelectDate:(NSDate *)date;

- (void)calendar:(CMUCalendarView *)calendar didSelectDate:(NSDate *)date
{
    DebugLog(@"");
    [self populateFeedList:self.eventModel];
}

//- (BOOL)calendar:(CMUCalendarView *)calendar willDeselectDate:(NSDate *)date;

//- (void)calendar:(CMUCalendarView *)calendar didDeselectDate:(NSDate *)date;

//- (BOOL)calendar:(CMUCalendarView *)calendar willChangeToMonth:(NSDate *)date;

- (void)calendar:(CMUCalendarView *)calendar didChangeToMonth:(NSDate *)date
{
    DebugLog(@"");
    self.lblCurrentMonth.text = [self.dateFormatter stringFromDate:date];
    [self getEventByCriteria];
}

- (void)calendar:(CMUCalendarView *)calendar didLayoutInRect:(CGRect)frame
{
    DebugLog(@"%@",NSStringFromCGRect(frame));
    [self.tableView reloadData];
}


#pragma mark section table view

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.calendarView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([CMUSettings sharedInstance].eventViewMode == CMU_EVENT_VIEW_MODE_LIST)
    {
        return 0;
    }
    return self.calendarView.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUEventDetailFeed *eventDetail =[self.feedList objectAtIndex:indexPath.row];
    CMUEventTableViewCell *cell = nil;
    cell = (CMUEventTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CMUEventTableViewCellType currentCellType = CMUEventTableViewCellTypeMiddle;
    int cellCount = (int)self.feedList.count;
    if(cellCount == 1)
    {
        currentCellType = CMUEventTableViewCellTypeSingle;
    }
    else
    {
        if(indexPath.row == 0)
        {
            currentCellType = CMUEventTableViewCellTypeTop;
        }
        else if(indexPath.row == cellCount - 1)
        {
            currentCellType = CMUEventTableViewCellTypeBottom;
        }
    }
    
    [cell setDateEvent:eventDetail cellType:currentCellType];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self openEventDetail:[self.feedList objectAtIndex:indexPath.row]];
}

- (void)openEventDetail:(CMUEventDetailFeed *) eventDetail
{
    if(![CMUStringUtils isEmpty:eventDetail.linkURL])
    {
        CMUWebViewController *webViewController = [[CMUWebViewController alloc] init];
        webViewController.url = eventDetail.linkURL;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

@end
