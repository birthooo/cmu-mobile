//
//  CMUEventTypePopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CMUEventDetailFeedModel.h"

@class CMUEventTypePopupView;

@protocol CMUEventTypePopupViewDelegate<NSObject>
- (void)eventTypePopupView:(CMUEventTypePopupView *) eventTypePopupView didSelectedEventType:(CMUEventType *) eventType;
@end

@interface CMUEventTypePopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUEventTypePopupViewDelegate> delegate;
@end