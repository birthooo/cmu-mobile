//
//  CMUEventTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUEventTableViewCell.h"
#import "UIColor+CMU.h"
#import <QuartzCore/QuartzCore.h>
#import "CMUStringUtils.h"
#import "global.h"

@interface CMUEventTableViewCell()
@property(nonatomic, weak)IBOutlet UIView* border;
@property(nonatomic, weak)IBOutlet UIView* dateView;
@property(nonatomic, weak)IBOutlet UILabel* lblDay;
@property(nonatomic, weak)IBOutlet UILabel* lblDate;
@property(nonatomic, weak)IBOutlet UILabel* lblEventTitle;
@property(nonatomic, weak)IBOutlet UIImageView* imgAccessory;
@property(nonatomic, weak)IBOutlet UIView *line;

@property(nonatomic, strong)CMUEventDetailFeed *eventDetail;
@property(nonatomic, unsafe_unretained)CMUEventTableViewCellType cellType;
@end

@interface CMUEventTableViewCell()
{
    CGFloat _cornerRadius;
    CGFloat _hBorderPading;
    CGFloat _vBorderPading;
}
@end

@implementation CMUEventTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _cornerRadius = 5.0f;
    _hBorderPading = 5.0f;
    _vBorderPading = 5.0f;
    
    self.border.layer.cornerRadius = _cornerRadius;
    self.border.layer.masksToBounds = YES;
    
    self.dateView.layer.cornerRadius = _cornerRadius;
    self.dateView.layer.masksToBounds = YES;
    
    if(IS_IPAD)
    {
        self.lblEventTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDateEvent:(CMUEventDetailFeed *) eventDetail cellType:(CMUEventTableViewCellType) cellType
{
    _eventDetail = eventDetail;
    _cellType = cellType;
    
    self.lblDay.text = [eventDetail.dayOfWeek uppercaseString];
    self.lblDate.text = eventDetail.numOfDay;
    self.lblEventTitle.text = eventDetail.topic;
    
    
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:eventDetail.startDate];
    NSDate* eventDateOnly = [calendar dateFromComponents:components];
    
    components = [calendar components:flags fromDate:[NSDate date]];
    NSDate* currentDateOnly = [calendar dateFromComponents:components];

    if ([eventDateOnly compare:currentDateOnly] == NSOrderedDescending) {
        self.dateView.backgroundColor = [UIColor cmuEventCalendarFutureDate];
    } else if ([eventDateOnly compare:currentDateOnly] == NSOrderedAscending) {
        self.dateView.backgroundColor = [UIColor cmuEventCalendarPassDate];
    } else {
        self.dateView.backgroundColor = [UIColor cmuEventCalendarCurrentDate];
    }
    
    self.imgAccessory.hidden = [CMUStringUtils isEmpty:eventDetail.linkURL];
    [self setNeedsLayout];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblDay.text = nil;
    self.lblDate.text = nil;
    self.lblEventTitle.text = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    CGRect cellFrame = self.contentView.frame;
//    //default for middle cell
//    CGRect borderRect = CGRectMake(_hBorderPading, 0, cellFrame.size.width - (_hBorderPading *2), cellFrame.size.height);
//    
//    self.border.frame = borderRect;
//    self.border.layer.cornerRadius = _cornerRadius;
//    self.border.layer.masksToBounds = YES;
//    //if(self.cellType == )
}

@end
