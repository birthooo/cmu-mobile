//
//  CMUEventTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUEventDetailFeedModel.h"

typedef enum
{
    CMUEventTableViewCellTypeSingle,
    CMUEventTableViewCellTypeTop,
    CMUEventTableViewCellTypeMiddle,
    CMUEventTableViewCellTypeBottom
}CMUEventTableViewCellType;


@interface CMUEventTableViewCell : UITableViewCell
- (void)setDateEvent:(CMUEventDetailFeed *) eventDetail cellType:(CMUEventTableViewCellType) cellType;
@end
