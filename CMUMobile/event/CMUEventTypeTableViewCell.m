//
//  CMUEventTypeTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUEventTypeTableViewCell.h"
#import "UIColor+CMU.h"
#import "global.h"


@interface CMUEventTypeTableViewCell()
@property(nonatomic, weak) IBOutlet UILabel *lblEventTypeTitle;
@end

@implementation CMUEventTypeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
    if(IS_IPAD)
    {
        self.lblEventTypeTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setEventType:(CMUEventType *)eventType
{
    _eventType = eventType;
    self.lblEventTypeTitle.text = self.eventType.title;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblEventTypeTitle.text = nil;
}


@end
