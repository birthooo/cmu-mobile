//
//  CMUEventTypePopupView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/15/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUEventTypePopupView.h"
#import "CMUEventTypeTableViewCell.h"

static NSString *cellIdentifier = @"CMUEventTypeTableViewCell";

@interface CMUEventTypePopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *eventTypeList;
@end

@implementation CMUEventTypePopupView

- (void)customInit
{
    self.eventTypeList= [[NSMutableArray alloc] initWithArray:[CMUEventType allEventType]];
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUEventTypePopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUEventTypeTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.eventTypeList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUEventTypeTableViewCell *cell = (CMUEventTypeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.eventType = [self.eventTypeList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(eventTypePopupView:didSelectedEventType:)])
    {
        CMUEventType *selectedEvent = [self.eventTypeList objectAtIndex:indexPath.row];
        [_delegate eventTypePopupView:self didSelectedEventType:selectedEvent];
    }
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
