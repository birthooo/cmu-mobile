//
//  CMUCalendarView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CMUCalendarDelegate;

@interface CMUDateItem : NSObject
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *selectedBackgroundColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *selectedTextColor;

@end

typedef enum {
    startSunday = 1,
    startMonday = 2,
} CMUCalendarStartDay;

@interface CMUCalendarView : UIView

- (id)initWithStartDay:(CMUCalendarStartDay)firstDay;
- (id)initWithStartDay:(CMUCalendarStartDay)firstDay frame:(CGRect)frame;

@property (nonatomic) CMUCalendarStartDay calendarStartDay;
@property (nonatomic, strong) NSLocale *locale;

@property (nonatomic, readonly) NSArray *datesShowing;

@property (nonatomic) BOOL onlyShowCurrentMonth;
@property (nonatomic) BOOL adaptHeightToNumberOfWeeksInMonth;

@property (nonatomic, weak) id<CMUCalendarDelegate> delegate;

// Theming
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIFont *dateOfWeekFont;
@property (nonatomic, strong) UIColor *dayOfWeekTextColor;
@property (nonatomic, strong) UIFont *dateFont;

@property (nonatomic, readonly) NSDate *monthShowing;
@property (nonatomic, strong) NSArray *specialDateList;

//- (void)setMonthButtonColor:(UIColor *)color;
- (void)setInnerBorderColor:(UIColor *)color;
- (void)setDayOfWeekBottomColor:(UIColor *)bottomColor topColor:(UIColor *)topColor;

- (void)selectDate:(NSDate *)date makeVisible:(BOOL)visible;
@property (nonatomic, readonly) NSDate *selectedDate;
- (void)reloadData;
- (void)reloadDates:(NSArray *)dates;

- (void)moveCalendarToNextMonth;
- (void)moveCalendarToPreviousMonth;


// Helper methods for delegates, etc.
- (BOOL)date:(NSDate *)date1 isSameDayAsDate:(NSDate *)date2;
- (BOOL)dateIsInCurrentMonth:(NSDate *)date;

@end

@protocol CMUCalendarDelegate <NSObject>

@optional
- (void)calendar:(CMUCalendarView *)calendar configureDateItem:(CMUDateItem *)dateItem forDate:(NSDate *)date;
- (BOOL)calendar:(CMUCalendarView *)calendar willSelectDate:(NSDate *)date;
- (void)calendar:(CMUCalendarView *)calendar didSelectDate:(NSDate *)date;
- (BOOL)calendar:(CMUCalendarView *)calendar willDeselectDate:(NSDate *)date;
- (void)calendar:(CMUCalendarView *)calendar didDeselectDate:(NSDate *)date;

- (BOOL)calendar:(CMUCalendarView *)calendar willChangeToMonth:(NSDate *)date;
- (void)calendar:(CMUCalendarView *)calendar didChangeToMonth:(NSDate *)date;

- (void)calendar:(CMUCalendarView *)calendar didLayoutInRect:(CGRect)frame;

@end