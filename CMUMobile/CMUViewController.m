//
//  CMUViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "SVProgressHUD.h"

@interface CMUViewController ()
@property(nonatomic, weak)UITextField *activeTextField;
@property(nonatomic, unsafe_unretained)int keyBoardHeigh;
@end

@implementation CMUViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loginChangedNotification:)
                                                     name:CMU_LOGIN_CHANGED_NOTIFICATION
                                                   object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //fix Status bar and navigation bar overview
//    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//
    
    if(SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.navigationItem.backBarButtonItem =
        [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                         style:UIBarButtonItemStyleBordered
                                        target:nil
                                        action:nil];
    }
    else
    {
        self.navigationItem.backBarButtonItem =
        [[UIBarButtonItem alloc] initWithTitle:@""
                                         style:UIBarButtonItemStyleBordered
                                        target:nil
                                        action:nil];
    }
    
    //keyboard controller
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.scrollViewBase addSubview:self.contentViewBase];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // Touch background and hide keyboard
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollViewBase addGestureRecognizer:tapGesture];
}

- (void) loginChangedNotification:(NSNotification *) notification
{
    [self updatePresentState];
}

- (void)updatePresentState
{
    //DO NOTHING
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollViewBase setContentSize:CGSizeMake(self.scrollViewBase.bounds.size.width, self.scrollViewBase.bounds.size.height)];
    self.contentViewBase.frame = self.scrollViewBase.frame;
    [self.view layoutSubviews];
}

-(IBAction)hideKeyboard
{
    [self.view endEditing:YES];
}


#pragma KeyBoard Observer
- (void) keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollViewBase.contentInset = contentInsets;
    self.scrollViewBase.scrollIndicatorInsets = contentInsets;
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self.scrollViewBase setContentOffset:scrollPoint animated:YES];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _keyBoardHeigh = keyboardSize.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, _keyBoardHeigh, 0.0);
    
    self.scrollViewBase.contentInset = contentInsets;
    self.scrollViewBase.scrollIndicatorInsets = contentInsets;
    
    [self adjustScrollView];
}

-(void)adjustScrollView{
    CGRect aRect = self.view.frame;
    aRect.size.height -= _keyBoardHeigh+30;
    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        
        int x= self.scrollViewBase.frame.size.height-(self.activeTextField.frame.origin.y+self.activeTextField.frame.size.height);
        int y = _keyBoardHeigh - x+30;
        CGPoint scrollPoint = CGPointMake(0.0,y);
        [self.scrollViewBase setContentOffset:scrollPoint animated:YES];
        NSLog(@"%@", NSStringFromCGPoint(scrollPoint));
        NSLog(@"SCROLL %@", NSStringFromCGRect(self.scrollViewBase.frame));
        
    }
    
}

#pragma UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    
    if (nextResponder && nextResponder.canBecomeFirstResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    //    [self adjustScrollView];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeTextField = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -ViewDeck
//- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer
//{
//    return YES;
//}

#pragma mark -Convenient method
- (void)showHUDLoading
{
    [SVProgressHUD showWithStatus:CMU_HUD_LOADING maskType:SVProgressHUDMaskTypeClear];
}

- (void)dismisHUD
{
    [SVProgressHUD dismiss];
}

#pragma alert view
- (void)alert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark -Orientation
//iOS 5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

//iOS6
-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
