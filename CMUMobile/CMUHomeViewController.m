//
//  CMUHomeViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUHomeViewController.h"
#import "CMUSettings.h"
#import "CMUMenuItemCell.h"
#import "CMUModel.h"
#import "CMUHotNewsView.h"
#import "CMUHotNewsItemView.h"
#import "CMUUtils.h"
#import "CMUMenuItem.h"
#import "CMUAppDelegate.h"
#import "CMUWebViewController.h"
#import "CMUUtils.h"
#import "GMGridViewLayoutStrategies.h"
#import "CMUAppDelegate.h"

#define iPadPro12 (UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad && UIScreen.mainScreen.nativeBounds.size.height == 2732)
#define IS_iPHONE_X [[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone && [[UIScreen mainScreen] nativeBounds].size.height==2436

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface CMUHomeViewController ()
@property (nonatomic, strong) NSMutableArray *menuItems;
@property (nonatomic, strong) IBOutlet CMUHotNewsView *hotNewsView;
@property (nonatomic, strong) IBOutlet GMGridView *gridView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControll;
@property (nonatomic, strong) IBOutlet UILabel *lblVersion;

@property (nonatomic, unsafe_unretained)BOOL dragPageIndexChanged;
@property (nonatomic, unsafe_unretained)BOOL cancelViewDeckPand;

- (IBAction)logoTapped:(id)sender;
@end

@implementation CMUHomeViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.menuItems = [[CMUSettings sharedInstance] menuItems];
    
    self.hotNewsView.delegate = self;
    
    NSInteger spacing = 0;
    _gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _gridView.backgroundColor = [UIColor clearColor];
    
    _gridView.minimumPressDuration = 0.5;
    _gridView.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontalPagedLTR];
    _gridView.pagingEnabled = YES;
    _gridView.showsHorizontalScrollIndicator = NO;
    _gridView.showsVerticalScrollIndicator = NO;
    //_gridView.style = GMGridViewStyleSwap;
    _gridView.style = GMGridViewStylePush;
    _gridView.itemSpacing = 0;
    _gridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gridView.centerGrid = NO;
    _gridView.actionDelegate = self;
    _gridView.sortingDelegate = self;
    _gridView.dataSource = self;
    _gridView.mainSuperView = self.view;
    
    //scrollview delegate
    _gridView.delegate = self;

    
    
    self.lblVersion.text = [NSString stringWithFormat:@"svn-r%@", [CMUUtils getVersionName]];
    
   // CGFloat width = self.gridView.frame.size.width;
    if(iPadPro12){
        self.pageControll.numberOfPages = 0;
    }
    else if(IS_IPHONE_5){
        self.pageControll.numberOfPages = 1;
    }
    else if(IS_iPHONE_X){
        self.pageControll.numberOfPages = 2;
    }
    else{
        self.pageControll.numberOfPages = 0;
    }
  //  self.pageControll.numberOfPages = 2;//(int)self.gridView.contentSize.width / width;
    [self updatePageControlIndex];
    [self updatePresentState];
    
    if(self.reloginPending)
    {
        [self btnLoginClicked:nil];
    }
    
    [self checkUpdateLog];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(somethingHappens:) name:@"notificationName" object:nil];
    
}

-(void)somethingHappens:(NSNotification*)notification
{
    [self.gridView reloadData];
    [self viewDidLoad];
    
    
}
-(void)checkUpdateLog{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *previousVersion = [defaults objectForKey:@"appVersion"];
    NSLog(@"currentVersion :%@",currentAppVersion);
    NSLog(@"previousVersion :%@",previousVersion);
    if (!previousVersion) {
        // first launch
        NSLog(@"first launch");
        [defaults setObject:currentAppVersion forKey:@"appVersion"];
        [defaults synchronize];
    } else if ([previousVersion isEqualToString:currentAppVersion]) {
        // same version
        NSLog(@"Current VeNSString	NSString	rsion");
        
    } else {
        // other version

        NSLog(@"Other Version");
        [defaults setObject:currentAppVersion forKey:@"appVersion"];
        [defaults synchronize];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"updateStoryboard" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"update"];
        vc.view.backgroundColor = [UIColor clearColor];
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:vc animated:NO completion:nil];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewWillAppear:(BOOL)animated
{
    //Edited by Satianpong Yodnin on 9/9/2558 BE.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"IOS Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [super viewWillAppear:animated];
    
    [self getHotNews];
    [self.view setNeedsUpdateConstraints];
    NSLog(@"RELOADDD");

    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isLoginSuccess"]){
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isLoginSuccess"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"allMenuItems" object:self];
        
        //app drawer check
        NSLog(@"app drawer check");
        CMUSettings *share = [[CMUSettings sharedInstance]init];
        [share loadMenuItems];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:self];
    }
    
    
    

   // if([[NSUserDefaults standardUserDefaults]boolForKey:@"isLogout"]){
        //[[NSUserDefaults standardUserDefaults]setBool:0 forKey:@"isLogout"];
       // [[NSNotificationCenter defaultCenter] postNotificationName:@"allMenuItems" object:self];
        
        //app drawer check
    /*
        NSLog(@"app drawer check");
        CMUSettings *share = [[CMUSettings sharedInstance]init];
        [share loadMenuItems];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:self];
     */
   // }

   
}

- (void)updatePresentState
{
    [super updatePresentState];
    self.pageControll.hidden = self.pageControll.numberOfPages == 1;
}

- (void)logoTapped:(id)sender
{
    self.lblVersion.hidden = NO;
}

- (void)getHotNews
{
    [[CMUWebServiceManager sharedInstance] getHotNews:self success:^(id result) {
        CMUNewsModel *hotNewsModel = (CMUNewsModel *)result;
        self.hotNewsView.hotNewsModel = hotNewsModel;
    } failure:^(NSError *error) {
        
    }];
}

- (int)currentPageIndex
{
    CGFloat width = self.gridView.frame.size.width;
    return  (int)(self.gridView.contentOffset.x + (0.5f * width)) / width;
}

- (CGPoint)pageOffsetForPageIndex:(int) index
{
    return CGPointMake(self.gridView.frame.size.width * index, 0);
}

- (void)updatePageControlIndex
{
    NSInteger page = [self currentPageIndex];
    self.pageControll.currentPage = page;
}

#pragma mark overide PSBaseViewController
//- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer
//{
//    //return !self.cancelViewDeckPand;
//    return NO;
//}

- (void)gridViewGestureRecognizerShouldBeginStart
{
    DebugLog(@"");
    if(self.gridView.contentOffset.x == 0)
    {
        self.cancelViewDeckPand = NO;
    }
    else
    {
        self.cancelViewDeckPand = YES;
    }
//    self.cancelViewDeckPand = YES;
}

#pragma markCMUHotNewsViewDelegate
- (void)hotNewsView:(CMUHotNewsView *) hotNewsView hotNewsItemViewDidSelected:(CMUHotNewsItemView *) hotNewsItemView
{
    CMUNewsFeed *news = hotNewsItemView.newsFeed;
    if(![CMUStringUtils isEmpty:news.link])
    {
        if(news.isOpenBrowser)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:news.link]];
        }
        else
        {
            CMUWebViewController *webViewController = [[CMUWebViewController alloc] init];
            webViewController.url = news.link;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
        
    }
}


#pragma mark GMGridViewDataSource

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return self.menuItems.count;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    CGFloat cellWidth = gridView.frame.size.width / 4.0;
    //check if iPad Pro

    if (iPadPro12) {
       cellWidth = gridView.frame.size.width / 4.0;
        self.pageControll.numberOfPages = 0;
        return CGSizeMake(185, 170);
    }
    if(IS_IPAD)
    {
        NSLog(@"cellWidth :%f",cellWidth);
        NSLog(@"gridView.frame.size.width :%f",gridView.frame.size.width);
        cellWidth = gridView.frame.size.width / 4.0;
        return CGSizeMake(180, 140);
    }
    if(IS_IPHONE_6){
        NSLog(@"iphone6");
         return CGSizeMake(90, 100);
    }
    if(IS_IPHONE_6P){
         return CGSizeMake(100, 110);
    }
    if(IS_iPHONE_X){
        return CGSizeMake(85, 130);
    }
    if(IS_IPHONE_5){
        return CGSizeMake(75, 100);
    }
  
    return CGSizeMake(cellWidth, 88);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    CMUMenuItemCell *cell = (CMUMenuItemCell*)[gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[CMUMenuItemCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    
    CMUMenuItem *menuItem = [self.menuItems objectAtIndex:index];
    cell.menuItem = menuItem;
    
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark GMGridViewActionDelegate
- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    CMUMenuItem *selectedMenuItem = [self.menuItems objectAtIndex:position];
    switch (selectedMenuItem.menuItemId) {
        case CMU_MENU_ITEM_EDOC:
        {
            //[AppDelegate goMIS];
        
//
//            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cmuedoc://"]])
//            {
//                NSLog(@"Now run CMUEdoc.");
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cmuedoc://"]];
//            }
//            else
//            {
//                NSLog(@"Failed to run CMUEdoc.");
//                NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id1276984354?mt=8";
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
//            }

           // [AppDelegate goNewEdoc];
            [AppDelegate goMIS];
        }
            break;
        case CMU_MENU_ITEM_NEWS:
            {
            [AppDelegate goNewsScholarship:@"98"];
            }
            break;
        case CMU_MENU_ITEM_EVENT:
            [AppDelegate goEvent];
            break;
        case CMU_MENU_ITEM_CMU_CHANNEL:
            [AppDelegate goChannel];
            break;
        case CMU_MENU_ITEM_MAP:
            [AppDelegate goMap];
            break;
        case CMU_MENU_ITEM_CURRICULUM:
            [AppDelegate goCuriculum];
            break;
        case CMU_MENU_ITEM_PHOTO_VIDEO:
         [AppDelegate goPhotoAndVideo];
            break;
        case CMU_MENU_ITEM_INFORMATION:
            [AppDelegate goInfo];
            break;
        case CMU_MENU_ITEM_PHONE:
            [AppDelegate goPhone];
            break;
        case CMU_MENU_ITEM_CMU_ONLINE:
            [AppDelegate goELearning];
            break;
        case CMU_MENU_ITEM_E_RESEARCH:
            [AppDelegate goEResearch];
            break;
        case CMU_MENU_ITEM_E_THESES:
            [AppDelegate goCMUEtheses];
            break;
        case CMU_MENU_ITEM_CMU_ONLINE_ENGLISH:
            [AppDelegate goCMUEnglish];
            break;
        /*case CMU_MENU_ITEM_LI_KIDS:
            [AppDelegate goLICMUKids];
            break;
        case CMU_MENU_ITEM_LI_CONVERSATION:
            [AppDelegate goLICMUConversation];
            break;
        case CMU_MENU_ITEM_LI_GRAMMAR:
            [AppDelegate goLICMUGrammar];
            break;
        case CMU_MENU_ITEM_LI_DIPLOMA:
            [AppDelegate goLICMUDiploma];
            break;
        case CMU_MENU_ITEM_5DEC:
            [AppDelegate goCMU5Dec];
            break;
         */
        case CMU_MENU_ITEM_ITSC:
            [AppDelegate goITSC];
            break;
        case CMU_MENU_ITEM_MY_ECHO:
            [AppDelegate goConnectMyEcho];
        //  Created by Satianpong Yodnin on 9/9/2558 BE.
        case CMU_MENU_ITEM_MYFLASHCARD:
            [AppDelegate goConnectMyFlashCard];
            break;
        case CMU_MENU_ITEM_CMU_TRANSIT:
            [AppDelegate goCMUTransit];
            break;
        /*case CMU_MENU_ITEM_FEEDBACK:
            [AppDelegate goFeedback];
            break;
         */
            //Edoc

             break;
             
        default:
            break;
    }
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    
}

/*
#pragma mark GMGridViewSortingDelegate
- (void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    self.cancelViewDeckPand = YES;
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil
     ];
}

- (void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    self.cancelViewDeckPand = NO;
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:^(BOOL finished) {
                         [[CMUSettings sharedInstance] saveMenuItems];
                         
                         if(self.dragPageIndexChanged)
                         {
                             [self.gridView reloadData];
                             [gridView setContentOffset:[self pageOffsetForPageIndex:[self currentPageIndex]] animated:YES];
                             self.dragPageIndexChanged = NO;
                         }
                         
                     }
     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return NO;
}
*/
//for GMGridViewStylePush we use this style
- (void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    /*
    NSObject *object = [self.menuItems objectAtIndex:oldIndex];
    [self.menuItems removeObject:object];
    [self.menuItems insertObject:object atIndex:newIndex];
    self.dragPageIndexChanged = YES;
     */
}
/*
- (void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
    [self.menuItems exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
    self.dragPageIndexChanged = YES;
}
*/


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updatePageControlIndex];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
    DebugLog(@"");
    self.cancelViewDeckPand = NO;
}
// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DebugLog(@"");
    self.cancelViewDeckPand = NO;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    DebugLog(@"");
    self.cancelViewDeckPand = NO;
}// called on finger up as we are moving
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    DebugLog(@"");
    self.cancelViewDeckPand = NO;
}// called when scroll view grinds to a halt

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    DebugLog(@"");
    self.cancelViewDeckPand = NO;
}// called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating


@end
