//
//  CMUWebViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUWebViewController.h"
#import "SVProgressHUD.h"

@interface CMUWebViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, assign)BOOL firstLoad;
@end

@implementation CMUWebViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(!self.hideURLTitle){
        self.navigationItem.title = self.url;
    }
    
    
    _firstLoad = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismisHUD];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebviewDelegate
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if(_firstLoad)
    {
        _firstLoad = NO;
        [SVProgressHUD showWithStatus:CMU_HUD_LOADING maskType:SVProgressHUDMaskTypeNone];
    }
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
    self.navigationItem.title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
