//
//  ContactViewController.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 1/13/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface ContactViewController : CMUNavBarViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *emergencyButton;
@property (weak, nonatomic) IBOutlet UIButton *studentButton;
@property (weak, nonatomic) IBOutlet UIButton *allPhoneButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
