//
//  CMUAboutViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/24/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUAboutViewController.h"
#import "CMUUtils.h"

@interface CMUAboutViewController ()
@property(nonatomic, weak)IBOutlet UILabel *lblVersion;
@end

@implementation CMUAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    int fontSize = 20;
    if(IS_IPAD)
    {
        fontSize = 28;
    }
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * svnuild = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString *htmlTemplate = @"<html><body style=\"font-size:%ipx; font-family:helvetica;\"><font color=\"#777579\">VERSION</font>&nbsp;<font color=\"#ff8400\">%@</font>&nbsp;<font color=\"#777579\">BUILD</font>&nbsp;<font color=\"#ff8400\">%@</font></body></html>";
    NSString *html = [NSString stringWithFormat:htmlTemplate, fontSize, version, svnuild];
    self.lblVersion.attributedText = [[NSAttributedString alloc] initWithData:[html dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
