//
//  CMURegViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/27/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMURegViewController : CMUNavBarViewController<UIWebViewDelegate>
@property(nonatomic, strong) NSString *url;
@property(nonatomic, strong) NSDictionary *headers;
@end
