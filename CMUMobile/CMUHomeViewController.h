//
//  CMUHomeViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "GMGridView.h"
#import "CMUWebServiceManager.h"
#import "CMUHotNewsView.h"
#import "CMUNewsViewController.h"
#import "MWPhotoBrowser.h"

@interface CMUHomeViewController : CMUNavBarViewController< GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewActionDelegate, CMUHotNewsViewDelegate, UIScrollViewDelegate>
@property(nonatomic, unsafe_unretained)BOOL reloginPending;
@end
