//
//  CMUCurriculumViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/28/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUCurriculumListViewController.h"

@interface CMUCurriculumViewController : CMUNavBarViewController<UITableViewDelegate, UITableViewDataSource, CMUCurriculumListViewControllerDelegate>

@end
