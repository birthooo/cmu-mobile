//
//  CMUCurriculumListViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUCurriculumDetailViewController.h"

@class CMUCurriculumListViewController;

@protocol CMUCurriculumListViewControllerDelegate <NSObject>
- (void)curriculumListViewControllerBackButtonClicked:(CMUCurriculumListViewController *) curriculumListViewController;
@end

@interface CMUCurriculumListViewController : CMUNavBarViewController<CMUCurriculumDetailViewController>
@property(nonatomic, unsafe_unretained) id<CMUCurriculumListViewControllerDelegate> delegate;
@property(nonatomic, strong) NSString *facultyName;
@property(nonatomic, unsafe_unretained) int year;
@property(nonatomic, unsafe_unretained) int semester;
@property(nonatomic, unsafe_unretained) int level;
@property(nonatomic, strong) NSString *facultyId;
@end
