//
//  CMUCurriculumViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/28/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUCurriculumViewController.h"
#import "CMUFaculty.h"
#import "CMUFacultyTableViewCell.h"
#import "global.h"

static NSString *cellIdentifier = @"CMUFacultyTableViewCell";

typedef enum{
    kLevelBachelorLevel = 6,
    kLevelMasterLevel = 7,
    kLevelPhD = 8,
    kLevelMore = 0
} CMUCurriculumLevel;

#define MIN_SEMESTER 1
#define MAX_SEMESTER 2

#define MIN_YEAR 0
#define MAX_YEAR 10000

@interface CMUCurriculumViewController ()
@property(nonatomic, weak) IBOutlet UILabel *lblYear;
@property(nonatomic, weak) IBOutlet UILabel *lblSemester;

@property(nonatomic, weak) IBOutlet UIButton *btnDecreaseYear;
@property(nonatomic, weak) IBOutlet UIButton *btnIncreaseYear;
@property(nonatomic, weak) IBOutlet UIButton *btnDecreaseSemester;
@property(nonatomic, weak) IBOutlet UIButton *btnIncreaseSemester;

@property(nonatomic, weak) IBOutlet UIButton *btnBachelor;
@property(nonatomic, weak) IBOutlet UIButton *btnMaster;
@property(nonatomic, weak) IBOutlet UIButton *btnPhD;
@property(nonatomic, weak) IBOutlet UIButton *btnMore;

@property(nonatomic, weak) IBOutlet UITableView *tableView;

@property(nonatomic, strong)NSArray *facultyList;
@property(nonatomic, unsafe_unretained)NSInteger selectedYear;
@property(nonatomic, unsafe_unretained)NSInteger selectedSemester;
@property(nonatomic, unsafe_unretained)CMUCurriculumLevel selectedLevel;



-(IBAction)btnInCreaseDecreaseClicked:(id) sender;
-(IBAction)btnLevelClicked:(id) sender;

@end

@implementation CMUCurriculumViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.selectedLevel = kLevelBachelorLevel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSDate *currentDate = [NSDate date];
    //bug with region format
//    NSCalendar* calendar = [NSCalendar currentCalendar];
//    calendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//    NSDateComponents* components = [calendar components:NSYearCalendarUnit fromDate:currentDate];
//    self.selectedYear = [components year];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *year = [dateFormatter stringFromDate:currentDate];
    self.selectedYear = [year intValue];
    self.selectedSemester = MIN_SEMESTER;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUFacultyTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self performSelector:@selector(showFaculty) withObject:nil afterDelay:0.3];
    
    [self updatePresentState];
}

- (void)showFaculty
{
    self.facultyList = [CMUFaculty getAllFaculty];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationBottom];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    [super updatePresentState];
    self.lblYear.text = [NSString stringWithFormat:@"%i", (int)self.selectedYear];
    self.lblSemester.text = [NSString stringWithFormat:@"%i", (int)self.selectedSemester];
    
    self.btnBachelor.selected = self.selectedLevel == kLevelBachelorLevel;
    self.btnMaster.selected = self.selectedLevel == kLevelMasterLevel;
    self.btnPhD.selected = self.selectedLevel == kLevelPhD;
    self.btnMore.selected = self.selectedLevel == kLevelMore;
    
    [self.btnBachelor setBackgroundImage:self.selectedLevel == kLevelBachelorLevel?[UIImage imageNamed:@"curriculum_level_button.png"]:nil forState:UIControlStateNormal];
    [self.btnMaster setBackgroundImage:self.selectedLevel == kLevelMasterLevel?[UIImage imageNamed:@"curriculum_level_button.png"]:nil forState:UIControlStateNormal];
    [self.btnPhD setBackgroundImage:self.selectedLevel == kLevelPhD?[UIImage imageNamed:@"curriculum_level_button.png"]:nil forState:UIControlStateNormal];
    [self.btnMore setBackgroundImage:self.selectedLevel == kLevelMore?[UIImage imageNamed:@"curriculum_level_button.png"]:nil forState:UIControlStateNormal];
    
    
}

-(void)btnInCreaseDecreaseClicked:(id) sender
{
    if(sender == self.btnDecreaseYear)
    {
        self.selectedYear --;
    }
    else if(sender == self.btnIncreaseYear)
    {
        self.selectedYear ++;
    }
    else if(sender == self.btnDecreaseSemester)
    {
        self.selectedSemester --;
    }
    else if(sender == self.btnIncreaseSemester)
    {
        self.selectedSemester ++;
    }

    self.selectedYear = self.selectedYear < MIN_YEAR ? MIN_YEAR: self.selectedYear;
    self.selectedYear = self.selectedYear > MAX_YEAR ? MAX_YEAR: self.selectedYear;
    self.selectedSemester = self.selectedSemester < MIN_SEMESTER ? MIN_SEMESTER: self.selectedSemester;
    self.selectedSemester = self.selectedSemester > MAX_SEMESTER ? MAX_SEMESTER: self.selectedSemester;

    
    
    CATransition *animation = [CATransition animation];
    animation.duration = 0.5;
    animation.type = kCATransitionFade;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.lblYear.layer addAnimation:animation forKey:@"changeTextTransition"];
    [self.lblSemester.layer addAnimation:animation forKey:@"changeTextTransition"];
    
    // Change the text
    self.lblYear.text = [NSString stringWithFormat:@"%i", (int)self.selectedYear];
    self.lblSemester.text = [NSString stringWithFormat:@"%i", (int)self.selectedSemester];


}

-(void)btnLevelClicked:(id) sender
{
    if(sender == self.btnBachelor)
    {
        self.selectedLevel = kLevelBachelorLevel;
    }
    else if(sender == self.btnMaster)
    {
        self.selectedLevel = kLevelMasterLevel;
    }
    else if(sender == self.btnPhD)
    {
        self.selectedLevel = kLevelPhD;
    }
    else if(sender == self.btnMore)
    {
        self.selectedLevel = kLevelMore;
    }
    [self updatePresentState];
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.facultyList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUFacultyTableViewCell *cell = nil;
    cell = (CMUFacultyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CMUFaculty *faculty = [self.facultyList objectAtIndex:indexPath.row];
    cell.lblNameThai.text = faculty.nameThai;
    cell.lblNameEng.text = faculty.nameEng;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //like android
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CMUFaculty *faculty = [self.facultyList objectAtIndex:indexPath.row];
    
    CMUCurriculumListViewController *viewController = [[CMUCurriculumListViewController alloc] init];
    viewController.delegate = self;
    viewController.facultyName = faculty.nameEng;
    viewController.year = self.selectedYear;
    viewController.semester = self.selectedSemester;
    viewController.level = self.selectedLevel;
    viewController.facultyId = faculty.facultyId;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - CMUCurriculumListViewControllerDelegate
- (void)curriculumListViewControllerBackButtonClicked:(CMUCurriculumListViewController *) curriculumListViewController;
{
    [self.navigationController popToViewController:self animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
