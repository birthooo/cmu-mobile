//
//  CMUCurriculumDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUCurriculumMajorModel.h"

@class CMUCurriculumDetailViewController;

@protocol CMUCurriculumDetailViewController <NSObject>
- (void)curriculumDetailViewControllerBackButtonClicked:(CMUCurriculumDetailViewController *) curriculumDetailViewController;
@end

@interface CMUCurriculumDetailViewController : CMUNavBarViewController<UIWebViewDelegate>
@property(nonatomic, unsafe_unretained) id<CMUCurriculumDetailViewController> delegate;
@property(nonatomic, strong) NSString *facultyId;
@property(nonatomic, strong) CMUCurriculumMajorView * curriculum;
@end
