//
//  CMUFacultyTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/29/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUFacultyTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UILabel* lblNameThai;
@property(nonatomic, weak) IBOutlet UILabel* lblNameEng;
@property(nonatomic, weak) IBOutlet UIImageView *imgAccesoryView;
@end
