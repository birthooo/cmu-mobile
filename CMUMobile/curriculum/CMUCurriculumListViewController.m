//
//  CMUCurriculumListViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUCurriculumListViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUCurriculumMajorModel.h"
#import "CMUSimpleTableViewCell.h"
#import "CMUCurriculumDetailViewController.h"

@interface CMUCurriculumListViewController ()
@property(nonatomic, weak)IBOutlet UIButton* btnBack;
@property(nonatomic, weak)IBOutlet UILabel *lblFacultyTitle;
@property(nonatomic, weak)IBOutlet UITableView *tableView;

@property(nonatomic, strong) CMUCurriculumMajorModel *curriculumMajorModel;

- (IBAction)btnBackClicked:(id)sender;
@end

static NSString *cellIdentifier = @"CMUSimpleTableViewCell";

@implementation CMUCurriculumListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lblFacultyTitle.text = self.facultyName;
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUSimpleTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getCurriculumList:self year:self.year semester:self.semester level:self.level facultyId:self.facultyId success:^(id result) {
        [self dismisHUD];
        self.curriculumMajorModel = (CMUCurriculumMajorModel *)result;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationBottom];
        [self updatePresentState];
    } failure:^(NSError *error) {
        [self dismisHUD];
        [self updatePresentState];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    [super updatePresentState];
}

- (void)btnBackClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(curriculumListViewControllerBackButtonClicked:)])
    {
        [self.delegate curriculumListViewControllerBackButtonClicked:self];
    }
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.curriculumMajorModel.curriculumList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUSimpleTableViewCell *cell = nil;
    cell = (CMUSimpleTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CMUCurriculumMajorView *curriculum = [self.curriculumMajorModel.curriculumList objectAtIndex:indexPath.row];
    cell.lblTitle.text = curriculum.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //like android
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CMUCurriculumMajorView *curriculum = [self.curriculumMajorModel.curriculumList objectAtIndex:indexPath.row];
    CMUCurriculumDetailViewController *viewController = [[CMUCurriculumDetailViewController alloc] init];
    viewController.facultyId = self.facultyId;
    viewController.curriculum = curriculum;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -CMUCurriculumDetailViewController
- (void)curriculumDetailViewControllerBackButtonClicked:(CMUCurriculumDetailViewController *) curriculumDetailViewController
{
    [self.navigationController popToViewController:self animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
