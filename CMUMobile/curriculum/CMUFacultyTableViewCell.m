//
//  CMUFacultyTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/29/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUFacultyTableViewCell.h"
#import "UIColor+CMU.h"

@implementation CMUFacultyTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self updateUI:selected];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self updateUI:highlighted];
}

- (void)updateUI:(BOOL)selected
{
    self.imgAccesoryView.image = selected?[UIImage imageNamed:@"cell_accessory_selected.png"]:[UIImage imageNamed:@"cell_accessory_normal.png"];
    self.lblNameThai.textColor = selected?[UIColor whiteColor]:[UIColor cmuTableViewCellTextColor];
    self.lblNameEng.textColor = selected?[UIColor whiteColor]:[UIColor cmuTableViewCellTextColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblNameThai.text = nil;
    self.lblNameEng.text = nil;
}
@end
