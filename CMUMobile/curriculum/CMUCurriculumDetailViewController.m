//
//  CMUCurriculumDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUCurriculumDetailViewController.h"
#import "UIColor+CMU.h"
#import "CMUWebServiceManager.h"

typedef enum{
    kTabDetail,
    kTabStructure,
    kTabPlan
} CMUCurriculumTab;

@interface CMUCurriculumDetailViewController ()
@property(nonatomic, weak)IBOutlet UILabel *lblCurriculumTitle;
@property(nonatomic, weak)IBOutlet UIView *viewTabDetail;
@property(nonatomic, weak)IBOutlet UIView *viewTabStructure;
@property(nonatomic, weak)IBOutlet UIView *viewTabPlan;
@property(nonatomic, weak)IBOutlet UILabel *lblTabDetail;
@property(nonatomic, weak)IBOutlet UILabel *lblTabStructure;
@property(nonatomic, weak)IBOutlet UILabel *lblTabPlan;
@property(nonatomic, weak)IBOutlet UIView *separator1;
@property(nonatomic, weak)IBOutlet UIView *separator2;
@property(nonatomic, weak)IBOutlet UIWebView *webDetail;
@property(nonatomic, weak)IBOutlet UIWebView *webStructure;
@property(nonatomic, weak)IBOutlet UIWebView *webPlan;
@property(nonatomic, unsafe_unretained)CMUCurriculumTab selectedTab;

@property(nonatomic, unsafe_unretained)BOOL detailLoaded;
@property(nonatomic, unsafe_unretained)BOOL structureLoaded;
@property(nonatomic, unsafe_unretained)BOOL planLoaded;

- (IBAction)tabPressed:(id)sender;
- (IBAction)btnBackClicked:(id)sender;
@end

@implementation CMUCurriculumDetailViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _selectedTab = kTabDetail;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.webDetail.delegate = self;
    self.webStructure.delegate = self;
    self.webPlan.delegate = self;
    
    self.webDetail.scalesPageToFit = YES;
    self.webStructure.scalesPageToFit = YES;
    self.webPlan.scalesPageToFit = YES;
    
    self.lblCurriculumTitle.text = self.curriculum.name;
    self.viewTabDetail.tag = kTabDetail;
    self.viewTabStructure.tag = kTabStructure;
    self.viewTabPlan.tag = kTabPlan;
    
    [self setSelectedTab:_selectedTab];
    [self updatePresentState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSelectedTab:(CMUCurriculumTab)selectedTab
{
    _selectedTab = selectedTab;
    
    //check to load content from webservice
    if(_selectedTab == kTabDetail)
    {
        if(!self.detailLoaded)
        {
            [self showHUDLoading];
            NSString *url = [NSString stringWithFormat:CMU_CURRICULUM_DETAIL_URL, _curriculum.curriculumId, _curriculum.year, _curriculum.term, self.facultyId];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [self.webDetail loadRequest:request];
        }
        else
        {
            [self.webDetail.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        }
    }
    else if(_selectedTab == kTabStructure)
    {
        if(!self.structureLoaded)
        {
            [self showHUDLoading];
            NSString *url = [NSString stringWithFormat:CMU_CURRICULUM_STRUCTURE_URL, _curriculum.curriculumId, _curriculum.year, _curriculum.term];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [self.webStructure loadRequest:request];
        }
        else
        {
            [self.webStructure.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        }
    }
    else if(_selectedTab == kTabPlan)
    {
        if(!self.planLoaded)
        {
            [self showHUDLoading];
            NSString *url = [NSString stringWithFormat:CMU_CURRICULUM_PLAN_URL, _curriculum.curriculumId, _curriculum.year, _curriculum.term];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [self.webPlan loadRequest:request];
        }
        else
        {
            [self.webPlan.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        }
    }
    
    //animating
    CGFloat detailAlpha = self.selectedTab == kTabDetail? 1.0: 0.0;
    CGFloat structureAlpha = self.selectedTab == kTabStructure? 1.0: 0.0;
    CGFloat planAlpha = self.selectedTab == kTabPlan? 1.0: 0.0;
    [UIView animateWithDuration:0.5 animations:^{
        self.webDetail.alpha = detailAlpha;
        self.webStructure.alpha = structureAlpha;
        self.webPlan.alpha = planAlpha;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)updatePresentState
{
    [super updatePresentState];
    self.viewTabDetail.backgroundColor = self.selectedTab == kTabDetail?[UIColor whiteColor]:[UIColor cmuPurpleLightColor];
    self.viewTabStructure.backgroundColor = self.selectedTab == kTabStructure?[UIColor whiteColor]:[UIColor cmuPurpleLightColor];
    self.viewTabPlan.backgroundColor = self.selectedTab == kTabPlan?[UIColor whiteColor]:[UIColor cmuPurpleLightColor];
    
    self.lblTabDetail.textColor = self.selectedTab == kTabDetail?[UIColor cmuPurpleLightColor]:[UIColor whiteColor];
    self.lblTabStructure.textColor = self.selectedTab == kTabStructure?[UIColor cmuPurpleLightColor]:[UIColor whiteColor];
    self.lblTabPlan.textColor = self.selectedTab == kTabPlan?[UIColor cmuPurpleLightColor]:[UIColor whiteColor];
    
    self.separator1.hidden = self.selectedTab == kTabDetail || self.selectedTab == kTabStructure;
    self.separator2.hidden = self.selectedTab == kTabStructure || self.selectedTab == kTabPlan;
}

- (void)btnBackClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(curriculumDetailViewControllerBackButtonClicked:)])
    {
        [self.delegate curriculumDetailViewControllerBackButtonClicked:self];
    }
}

- (void)tabPressed:(id)sender
{
    self.selectedTab = ((UITapGestureRecognizer *)sender).view.tag;
    [self updatePresentState];
}


#pragma mark UIWebviewDelegate
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
    if(webView == self.webDetail)
    {
        self.detailLoaded = YES;
    }
    else if(webView == self.webStructure)
    {
        self.structureLoaded = YES;
    }
    else if(webView == self.webPlan)
    {
        self.planLoaded = YES;
    }
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
