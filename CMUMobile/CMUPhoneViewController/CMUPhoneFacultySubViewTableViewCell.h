//
//  CMUPhoneFacultySubViewTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUPhoneFacultyView.h"

@interface CMUPhoneFacultySubViewTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUPhoneFacultyView *phoneFacultyView;
@end
