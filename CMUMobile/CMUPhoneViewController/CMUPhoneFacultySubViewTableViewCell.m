//
//  CMUPhoneFacultySubViewTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneFacultySubViewTableViewCell.h"
#import "UIColor+CMU.h"

@interface CMUPhoneFacultySubViewTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel* lblFacultyName;
@property(nonatomic, weak)IBOutlet UIImageView *cmuAccesoryView;
@end

@implementation CMUPhoneFacultySubViewTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor whiteColor];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.cmuAccesoryView.image = selected?[UIImage imageNamed:@"cell_accessory_normal.png"]:[UIImage imageNamed:@"cell_accessory_selected.png"];
    self.lblFacultyName.textColor = selected?[UIColor cmuTableViewCellTextColor]:[UIColor whiteColor];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    self.cmuAccesoryView.image = highlighted?[UIImage imageNamed:@"cell_accessory_normal.png"]:[UIImage imageNamed:@"cell_accessory_selected.png"];
    self.lblFacultyName.textColor = highlighted?[UIColor cmuTableViewCellTextColor]:[UIColor whiteColor];
}

- (void)setPhoneFacultyView:(CMUPhoneFacultyView *)phoneFacultyView
{
    _phoneFacultyView = phoneFacultyView;
    self.lblFacultyName.text = _phoneFacultyView.name;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblFacultyName.text = nil;
}

@end
