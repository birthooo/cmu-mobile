//
//  CMUPhoneFacultyViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUPhoneFacultyView.h"
#import "CMUNavBarViewController.h"
#import <MessageUI/MessageUI.h>

@class CMUPhoneFacultyViewController;
@protocol CMUPhoneFacultyViewControllerDelegate <NSObject>
- (void)phoneFacultyViewControllerBackButtonClicked:(CMUPhoneFacultyViewController *) phoneFacultyViewController;
@end

@interface CMUPhoneFacultyViewController : CMUNavBarViewController<CMUPhoneFacultyViewControllerDelegate, MFMailComposeViewControllerDelegate>
@property(nonatomic, unsafe_unretained)id<CMUPhoneFacultyViewControllerDelegate> delegate;
@property(nonatomic, strong) CMUPhoneFacultyView *phoneFacultyView;
@end
