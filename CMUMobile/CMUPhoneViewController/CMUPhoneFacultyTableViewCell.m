//
//  CMUPhoneFacultyTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneFacultyTableViewCell.h"
#import "UIColor+CMU.h"

@interface CMUPhoneFacultyTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel* lblFacultyName;
@property(nonatomic, weak) IBOutlet UIImageView *cmuAccesoryView;
@end

@implementation CMUPhoneFacultyTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [self updateUI:selected];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self updateUI:highlighted];
}

- (void)updateUI:(BOOL)selected
{
    self.cmuAccesoryView.image = selected?[UIImage imageNamed:@"cell_accessory_selected.png"]:[UIImage imageNamed:@"cell_accessory_normal.png"];
    self.lblFacultyName.textColor = selected?[UIColor whiteColor]:[UIColor cmuTableViewCellTextColor];
}

- (void)setPhoneFacultyView:(CMUPhoneFacultyView *)phoneFacultyView
{
    _phoneFacultyView = phoneFacultyView;
    self.lblFacultyName.text = _phoneFacultyView.name;
}
@end
