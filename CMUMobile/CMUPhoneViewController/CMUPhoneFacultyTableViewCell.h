//
//  CMUPhoneFacultyTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUPhoneFacultyView.h"

@interface CMUPhoneFacultyTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUPhoneFacultyView *phoneFacultyView;
@end
