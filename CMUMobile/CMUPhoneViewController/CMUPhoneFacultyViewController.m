//
//  CMUPhoneFacultyViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/8/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneFacultyViewController.h"
#import "CMUPhoneFacultySubViewTableViewCell.h"
#import "CMUStringUtils.h"
#import "global.h"
#import "CMUUtils.h"
#import "CMUWebViewController.h"
#import "UIAlertView+Blocks.h"

static NSString *cellIdentifier = @"CMUPhoneFacultySubViewTableViewCell";
#define TAG_PHONE_CALL 1

@interface CMUPhoneFacultyViewController ()
@property(nonatomic, weak)IBOutlet UIButton* btnBack;
@property(nonatomic, weak)IBOutlet UILabel *lblFacultyTitle;
@property(nonatomic, weak)IBOutlet UILabel *lblPhone;
@property(nonatomic, weak)IBOutlet UILabel *lblFax;
@property(nonatomic, weak)IBOutlet UILabel *lblEmail;
@property(nonatomic, weak)IBOutlet UILabel *lblWeb;
@property(nonatomic, weak)IBOutlet UILabel *lblFacebook;
@property(nonatomic, weak)IBOutlet UIImageView *imvPhone;
@property(nonatomic, weak)IBOutlet UIImageView *imvFax;
@property(nonatomic, weak)IBOutlet UIImageView *imvEmail;
@property(nonatomic, weak)IBOutlet UIImageView *imvWeb;
@property(nonatomic, weak)IBOutlet UIImageView *imvFacebook;
@property(nonatomic, weak)IBOutlet UITableView *tableView;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)phoneClicked:(id)sender;
- (IBAction)faxClicked:(id)sender;
- (IBAction)emailClicked:(id)sender;
- (IBAction)websiteClicked:(id)sender;
- (IBAction)facebookClicked:(id)sender;
@end

@implementation CMUPhoneFacultyViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUPhoneFacultySubViewTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.lblFacultyTitle.text = [CMUStringUtils isEmpty:self.phoneFacultyView.name]?@"":self.phoneFacultyView.name;
    
    self.lblPhone.text = [CMUStringUtils isEmpty:self.phoneFacultyView.phone]?@"":self.phoneFacultyView.phone;
    self.lblFax.text = [CMUStringUtils isEmpty:self.phoneFacultyView.fax]?@"":self.phoneFacultyView.fax;
    self.lblEmail.text = [CMUStringUtils isEmpty:self.phoneFacultyView.email]?@"":self.phoneFacultyView.email;
    self.lblWeb.text = [CMUStringUtils isEmpty:self.phoneFacultyView.web]?@"":self.phoneFacultyView.web;
    self.lblFacebook.text = [CMUStringUtils isEmpty:self.phoneFacultyView.fanpage]?@"":self.phoneFacultyView.fanpage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

- (void)btnBackClicked:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(phoneFacultyViewControllerBackButtonClicked:)])
    {
        [self.delegate phoneFacultyViewControllerBackButtonClicked:self];
    }
}

- (IBAction)phoneClicked:(id)sender
{
    DebugLog(@"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:[NSString stringWithFormat:@"Call %@", self.phoneFacultyView.phoneNumber]
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [CMUUtils makePhoneCall:self.phoneFacultyView.phoneNumber];
        }
    };
    [alert show];
}

- (IBAction)faxClicked:(id)sender
{
    DebugLog(@"");
}

- (IBAction)emailClicked:(id)sender
{
    DebugLog(@"");
    if(![CMUStringUtils isEmpty:self.phoneFacultyView.email])
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
            [composeViewController setToRecipients:@[self.phoneFacultyView.email]];
            composeViewController.mailComposeDelegate = self;
            //[composeViewController setSubject:@""];
            //[composeViewController setMessageBody:@"" isHTML:NO];
            [self presentViewController:composeViewController animated:YES completion:^{
                
            }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"Device is unable to send email in its current state"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
        
    }
    
}

- (IBAction)websiteClicked:(id)sender
{
    DebugLog(@"");
    if(![CMUStringUtils isEmpty:self.phoneFacultyView.web])
    {
        CMUWebViewController *webViewController = [[CMUWebViewController alloc] init];
        webViewController.url = self.phoneFacultyView.web;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
    
}

- (IBAction)facebookClicked:(id)sender
{
    DebugLog(@"");
    if(![CMUStringUtils isEmpty:self.phoneFacultyView.fanpage])
    {
        CMUWebViewController *webViewController = [[CMUWebViewController alloc] init];
        webViewController.url = self.phoneFacultyView.fanpage;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
}

#pragma mark UIAlertView
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if(alertView.tag == TAG_PHONE_CALL)
//    {
//        if (buttonIndex == 0) {
//            [CMUUtils makePhoneCall:self.phoneFacultyView.phoneNumber];
//        }
//    }
    
}

#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    //feedbackMsg.hidden = NO;
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
        DebugLog(@"Result: Mail sending canceled");
        break;
        case MFMailComposeResultSaved:
        DebugLog(@"Result: Mail saved");
        break;
        case MFMailComposeResultSent:
        DebugLog(@"Result: Mail sent");
        break;
        case MFMailComposeResultFailed:
        DebugLog(@"Result: Mail sending failed");
        break;
        default:
        DebugLog(@"Result: Mail not sent");
        break;
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.phoneFacultyView.phoneSubViewList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //like android
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CMUPhoneFacultySubViewTableViewCell *cell = nil;
    cell = (CMUPhoneFacultySubViewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.phoneFacultyView = [self.phoneFacultyView.phoneSubViewList objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //like android
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CMUPhoneFacultyViewController *viewController = [[CMUPhoneFacultyViewController alloc] init];
    viewController.phoneFacultyView = [self.phoneFacultyView.phoneSubViewList objectAtIndex:indexPath.row];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - CMUPhoneFacultyViewControllerDelegate
- (void)phoneFacultyViewControllerBackButtonClicked:(CMUPhoneFacultyViewController *) phoneFacultyViewController
{
    [self.navigationController popToViewController:self animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
