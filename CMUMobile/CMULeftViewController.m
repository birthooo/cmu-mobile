//
//  CMULeftViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMULeftViewController.h"
#import "CMUModel.h"
#import "CMUWebServiceManager.h"
#import "UIColor+CMU.h"
#import "CMUAppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>

@interface CMULeftViewController ()

@property(nonatomic, weak)IBOutlet UITableView *tableView;
@property(nonatomic, weak)IBOutlet UIView *viewName;
@property(nonatomic, weak)IBOutlet UILabel *lblName;

@property(nonatomic, weak)IBOutlet UIView *viewStudentHeader;

@property(nonatomic, weak)IBOutlet UIView *viewCMURegHeader;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellEvaluate;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellSurvey;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellRegStudent;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellJob;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellScholarship;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellRequest;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellAdviser;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellNewsAlert;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellActivity;


@property(nonatomic, weak)IBOutlet UIView *viewCMUSISHeader;

@property(nonatomic, weak)IBOutlet UIView *viewEmployeeHeader;

@property(nonatomic, weak)IBOutlet UIView *viewCMUMISHeader;
@property(nonatomic, weak)IBOutlet UITableViewCell *cellEDocument;

@property(nonatomic, weak)IBOutlet UIView *viewSettingHeader;
@property(nonatomic, weak)IBOutlet UIView *viewAboutHeader;


//tableview section header items
@property(nonatomic, weak)IBOutlet UIImageView *imvREGLogo;
@property(nonatomic, weak)IBOutlet UILabel *lblREGTitle;
@property(nonatomic, weak)IBOutlet UIImageView *imvREGAccessoryView;

@property(nonatomic, weak)IBOutlet UIImageView *imvSISLogo;
@property(nonatomic, weak)IBOutlet UILabel *lblSISTitle;
@property(nonatomic, weak)IBOutlet UIImageView *imvSISAccessoryView;

@property(nonatomic, weak)IBOutlet UIImageView *imvMISLogo;
@property(nonatomic, weak)IBOutlet UILabel *lblMISTitle;
@property(nonatomic, weak)IBOutlet UIImageView *imvMISAccessoryView;

@property(nonatomic, weak)IBOutlet UIImageView *imvABOUTLogo;
@property(nonatomic, weak)IBOutlet UILabel *lblABOUTTitle;
@property(nonatomic, weak)IBOutlet UIImageView *imvABOUTAccessoryView;

//tableView Model
@property(nonatomic, strong)NSArray *studentSectionViews;
@property(nonatomic, strong)NSArray *employeeSectionViews;

@property(nonatomic, strong)NSArray *regCellChilds;
@property(nonatomic, strong)NSArray *sisCellChilds;
@property(nonatomic, strong)NSArray *misCellChilds;
@property(nonatomic, strong)NSArray *aboutCellChilds;

@property(nonatomic, unsafe_unretained) int selectedSection;

@property (strong, nonatomic) MPMoviePlayerController *streamPlayer;
@end

@implementation CMULeftViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.selectedSection = -1;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loginChangedNotification:)
                                                     name:CMU_LOGIN_CHANGED_NOTIFICATION
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    self.studentSectionViews= [[NSArray alloc] initWithObjects:self.viewStudentHeader, self.viewCMURegHeader, self.viewCMUSISHeader,self.viewSettingHeader, self.viewAboutHeader, nil];
    self.employeeSectionViews= [[NSArray alloc] initWithObjects:self.viewEmployeeHeader, self.viewCMUMISHeader, self.viewSettingHeader,self.viewAboutHeader, nil];
   // self.sisCellChilds = [[NSArray alloc] initWithObjects:self.cellEvaluate, self.cellSurvey, nil];
    
    self.sisCellChilds = [[NSArray alloc] initWithObjects:self.cellRegStudent,self.cellEvaluate, self.cellSurvey,self.cellAdviser,self.cellNewsAlert,self.cellActivity, nil];
    //self.misCellChilds = [[NSArray alloc] initWithObjects:self.cellEDocument, nil];
    [self updatePresentState];
}

- (void) loginChangedNotification:(NSNotification *) notification
{
    [self updatePresentState];
}

- (void)updatePresentState
{
    [super updatePresentState];
    
    //navigation bar
    NSMutableArray *leftMenus = [[NSMutableArray alloc] init];
    [leftMenus addObject:self.cmuButtonItem];
    self.navigationItem.leftBarButtonItems = leftMenus;
    self.navigationItem.rightBarButtonItems = nil;

    
    //user name
    CMUUser *user = [[[CMUWebServiceManager sharedInstance] userInfoModel] user];
    self.lblName.text = [NSString stringWithFormat:@"%@ %@", user.firstNameEN, user.lastNameEN];
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        //self.lblFaculty.text = user.organizationEN;
    }
    else
    {
         //self.lblFaculty.text = user.facultyEN;
    }
    
    //update view by selected color
    
    NSArray *currentSectionViews = nil;
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        currentSectionViews = self.studentSectionViews;
    }
    else
    {
        currentSectionViews = self.employeeSectionViews;
    }
    
    //REG
    BOOL regSelected =  self.selectedSection == [currentSectionViews indexOfObject:self.viewCMURegHeader];
    self.viewCMURegHeader.backgroundColor = regSelected? [UIColor cmuLeftMenuPurpleColor]:[UIColor whiteColor];
    self.imvREGLogo.image = regSelected? [UIImage imageNamed:@"leftmenu_cmu_reg_selected.png"]:[UIImage imageNamed:@"leftmenu_cmu_reg.png"];
    self.lblREGTitle.textColor = regSelected? [UIColor whiteColor]:[UIColor cmuBlackColor];
    self.imvREGAccessoryView.hidden = self.regCellChilds.count == 0;
    self.imvREGAccessoryView.image = regSelected? [UIImage imageNamed:@"leftmenu_collapse.png"]:[UIImage imageNamed:@"leftmenu_expand.png"];
    
    //SIS
    BOOL sisSelected =  self.selectedSection == [currentSectionViews indexOfObject:self.viewCMUSISHeader];
    self.viewCMUSISHeader.backgroundColor = sisSelected? [UIColor cmuLeftMenuPurpleColor]:[UIColor whiteColor];
    self.imvSISLogo.image = sisSelected? [UIImage imageNamed:@"leftmenu_cmu_sis_selected.png"]:[UIImage imageNamed:@"leftmenu_cmu_sis.png"];
    self.lblSISTitle.textColor = sisSelected? [UIColor whiteColor]:[UIColor cmuBlackColor];
    self.imvSISAccessoryView.hidden = self.sisCellChilds.count == 0;
    self.imvSISAccessoryView.image = sisSelected? [UIImage imageNamed:@"leftmenu_collapse.png"]:[UIImage imageNamed:@"leftmenu_expand.png"];

    //MIS
    BOOL misSelected =  self.selectedSection == [currentSectionViews indexOfObject:self.viewCMUMISHeader];
    self.viewCMUMISHeader.backgroundColor = misSelected? [UIColor cmuLeftMenuPurpleColor]:[UIColor whiteColor];
    self.imvMISLogo.image = misSelected? [UIImage imageNamed:@"leftmenu_cmu_mis_selected.png"]:[UIImage imageNamed:@"leftmenu_cmu_mis.png"];
    self.lblMISTitle.textColor = misSelected? [UIColor whiteColor]:[UIColor cmuBlackColor];
    self.imvMISAccessoryView.hidden = self.misCellChilds.count == 0;
    self.imvMISAccessoryView.image = misSelected? [UIImage imageNamed:@"leftmenu_collapse.png"]:[UIImage imageNamed:@"leftmenu_expand.png"];
    
    //ABOUT
    BOOL aboutSelected =  self.selectedSection == [currentSectionViews indexOfObject:self.viewAboutHeader];
    self.viewAboutHeader.backgroundColor = aboutSelected? [UIColor cmuLeftMenuPurpleColor]:[UIColor whiteColor];
    self.imvABOUTLogo.image = aboutSelected? [UIImage imageNamed:@"leftmenu_cmu_about_selected.png"]:[UIImage imageNamed:@"leftmenu_cmu_about.png"];
    self.lblABOUTTitle.textColor = aboutSelected? [UIColor whiteColor]:[UIColor cmuBlackColor];
    self.imvABOUTAccessoryView.hidden = self.aboutCellChilds.count == 0;
    self.imvABOUTAccessoryView.image = aboutSelected? [UIImage imageNamed:@"leftmenu_collapse.png"]:[UIImage imageNamed:@"leftmenu_expand.png"];

//    NSUInteger numOfSection = [self numberOfSectionsInTableView:self.tableView];
//    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
//    for(int i = 0; i < numOfSection ; i++)
//    {
//        [indexSet addIndex:i];
//    }
//    [self.tableView reloadData];
    //[self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    
//    if(self.selectedSection >= 0)
//    {
//        [self.tableView beginUpdates];
//        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:self.selectedSection] withRowAnimation:UITableViewRowAnimationFade];
//        [self.tableView endUpdates];
//    }
//    [self.tableView reloadData];
    
    [UIView transitionWithView: self.tableView
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.tableView reloadData];
     }
                    completion: ^(BOOL isFinished)
     {
         /* TODO: Whatever you want here */
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tabOnSectionHeader:(id)sender
{
    UIView *tappedSectionHeaderView =  ((UITapGestureRecognizer *)sender).view;

    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        if(tappedSectionHeaderView == self.viewCMURegHeader)
        {
            [AppDelegate goREG];
        }
        else if(tappedSectionHeaderView == self.viewCMUSISHeader)
        {
            NSUInteger index = [self.studentSectionViews indexOfObject:self.viewCMUSISHeader];
            if(index == self.selectedSection)
            {
                self.selectedSection = -1;//toggle unselect
            }
            else
            {
                self.selectedSection = (int)index;//become new selection
            }
            
        }
        else if(tappedSectionHeaderView == self.viewAboutHeader)
        {
            [AppDelegate goAbout];
        }
    }
    else
    {
        if(tappedSectionHeaderView == self.viewCMUMISHeader)
        {
            NSUInteger index = [self.employeeSectionViews indexOfObject:self.viewCMUMISHeader];
            if(index == self.selectedSection)
            {
                self.selectedSection = -1;//toggle unselect
            }
            else
            {
                self.selectedSection = (int)index;//become new selection
            }
        }
        else if(tappedSectionHeaderView == self.viewAboutHeader)
        {
            [AppDelegate goAbout];
        }
    }
    
    [self updatePresentState];
    
}

#pragma mark section table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        return self.studentSectionViews.count;
    }
    else
    {
        return self.employeeSectionViews.count;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        if(section == [self.studentSectionViews indexOfObject:self.viewCMUSISHeader])
        {
            return self.selectedSection == section? self.sisCellChilds.count: 0;
        }
        return 0;
    }
    else
    {
        
        switch (section) {
            case 0://chiangmai university
                return 0;
                break;
            case 1://mis
                return self.selectedSection == section? 1: 0;
                break;
            case 2://setting
                return 0;
                break;
            case 3://about
                return 0;
                break;
            default:
                return 0;
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        if(IS_IPAD)
        {
            switch (section) {
                case 0://student
                    return 70;
                    break;
                case 1://reg
                    return 90;
                    break;
                case 2://sis
                    return 90;
                    break;
                case 3://setting
                    return 70;
                    break;
                case 4://about
                    return 90;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
        else
        {
            switch (section) {
                case 0://student
                    return 45;
                    break;
                case 1://reg
                    return 70;
                    break;
                case 2://sis
                    return 70;
                    break;
                case 3://setting
                    return 45;
                    break;
                case 4://about
                    return 70;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
       
    }
    else
    {
        if(IS_IPAD)
        {
            switch (section) {
                case 0://chiangmai university
                    return 70;
                    break;
                case 1://mis
                    return 90;
                    break;
                case 2://setting
                    return 70;
                    break;
                case 3://about
                    return 90;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
        else
        {
            switch (section) {
                case 0://chiangmai university
                    return 45;
                    break;
                case 1://mis
                    return 70;
                    break;
                case 2://setting
                    return 45;
                    break;
                case 3://about
                    return 70;
                    break;
                    
                default:
                    return 0;
                    break;
            }
        }
        
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        return [self.studentSectionViews objectAtIndex: section];
    }
    else
    {
        return [self.employeeSectionViews objectAtIndex: section];
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        if(indexPath.section == [self.studentSectionViews indexOfObject:self.regCellChilds])
        {
            return [self.regCellChilds objectAtIndex:indexPath.row];
        }
        else if(indexPath.section == [self.studentSectionViews indexOfObject:self.viewCMUSISHeader])
        {
            return [self.sisCellChilds objectAtIndex:indexPath.row];
        }
        else if (indexPath.section == [self.studentSectionViews indexOfObject:self.aboutCellChilds])
        {
            return [self.aboutCellChilds objectAtIndex:indexPath.row];
        }
        return nil;
    }
    else
    {
        switch (indexPath.section) {
            case 1://mis
                return [self.misCellChilds objectAtIndex:indexPath.row];
                break;
            case 3://about
                return [self.aboutCellChilds objectAtIndex:indexPath.row];
                break;
            default:
                //DebugLog(@"%i, %i",indexPath.section, indexPath.row);
                return nil;
                break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[[CMUWebServiceManager sharedInstance] ticket] getUserType] == CMU_USER_TYPE_STUDENT )
    {
        
        if(indexPath.section == [self.studentSectionViews indexOfObject:self.viewCMUSISHeader])
        {
            if([self.sisCellChilds objectAtIndex:indexPath.row] == self.cellEvaluate)
            {
                [AppDelegate goSIS];
            }
            else if([self.sisCellChilds objectAtIndex:indexPath.row] == self.cellSurvey)
            {
                [AppDelegate goPCheck];
            }
            else if([self.sisCellChilds objectAtIndex:indexPath.row] == self.cellRegStudent)
            {
                [AppDelegate goRegStudent];
            }
            else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellJob)
            {
                [AppDelegate goJobManagement];
            }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellScholarship)
             {
                 [AppDelegate goScholarship];
             }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellRequest)
             {
                 [AppDelegate goRequest];
             }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellAdviser)
             {
                 [AppDelegate goAdviser];
             }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellActivity)
             {
                 [AppDelegate goActivity];
             }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellNewsAlert)
             {
                 [AppDelegate goNewsScholarship:@"99"];
                 
             }
             else if([self.sisCellChilds objectAtIndex:indexPath.row]==self.cellActivity)
             {
                 [AppDelegate goActivity];
                 
             }
        }
        
    }
    else
    {
        if(indexPath.section == [self.employeeSectionViews indexOfObject:self.viewCMUMISHeader])
        {
            if([self.misCellChilds objectAtIndex:indexPath.row] == self.cellEDocument)
            {
                [AppDelegate goMIS];
            }
        }

    }
}





@end
