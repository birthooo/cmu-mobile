//
//  FeedbackViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 12/1/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "FeedbackViewController.h"
#import <CoreFoundation/CoreFoundation.h>
#import "CMUWebServiceManager.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

#import <SystemConfiguration/CaptiveNetwork.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "CMUAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "dropdownTableViewCell.h"
#define AppDelegate ((CMUAppDelegate *)[[UIApplication sharedApplication] delegate])

@interface FeedbackViewController ()
@property NSArray *pickerData;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property NSString *type;
@property MBProgressHUD *HUD;
@property int count;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UITableView *dropdownTable;
@property NSMutableDictionary *dict;
@property NSMutableArray *wifilog;
@property NSDictionary *finalJsonData;
@property NSDictionary *wifiDict;
@property float timeSec;
@property NSTimer *timer;
@property NSArray *dropdownDataArray;
@end

@implementation FeedbackViewController
{
    CMUTicket *userticket;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self.textfield1 setEnabled:NO];
    [self.subView setHidden:YES];
    self.dropdownDataArray = @[@"เรื่องเกี่ยวกับ WIFI JumboPlus",@"เรื่องเกี่ยวกับ CMU MOBILE ",@"เรื่องเกี่ยวกับ CMU SIS",@"เรื่องเกี่ยวกับ CMU MIS",@"เรื่องเกี่ยวกับ  CMU Account"];
    
    self.dict = [[NSMutableDictionary alloc]init];
    self.wifilog = [[NSMutableArray alloc]init];

    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard)];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolbar.items = [NSArray arrayWithObject:barButton];
    self.textView.inputAccessoryView = toolbar;
    self.textfield1.inputAccessoryView = toolbar;
    self.textfield2.inputAccessoryView = toolbar;
    self.submitButton.layer.cornerRadius = 10.0f;
    
    userticket = [[CMUWebServiceManager sharedInstance] ticket];
    
    

    
}

- (IBAction)buttonAction:(id)sender {
    /*
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Sharing option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"CMUMobile Feedback",
                            @"JumboPlus Feedback",
                            @"SIS Feedback",
                            nil];
    popup.tag = 3;
    [popup showInView:self.view];
     */
    [self.subView setHidden:NO];
    //[popup showFromRect:[self.textfield1 frame] inView:self.view animated:YES];

}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if(sender.tag==1){
    
    
    }
}

- (IBAction)submitAction:(id)sender {
       if([self.textfield1.text isEqualToString:@""]||[self.textfield2.text isEqualToString:@""]||[self.textView.text isEqualToString:@""])
       {
           UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"กรุณากรอกข้อมูลให้ครบถ้วน" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [errorAlert show];
       }
       else{
           [self sendPOSTrequest];

       }

}
-(void)sendPOSTrequest{
        NSString *url = @"https://mobileapi.cmu.ac.th/api/FeedBack/addFeedback";
        [self POST:url account:userticket.userName typeid:self.type comment:self.textView.text title:self.textfield2.text];
    
}

- (void)dismissKeyboard {

    [self.view endEditing:YES];
    
}
-(id)POST:(NSString*)url account:(NSString*)account typeid:(NSString*)typeid comment:(NSString*)comment title:(NSString*)title{
    
    NSLog(@"url :%@",url);
    NSLog(@"account :%@",account);
    NSLog(@"typeid :%@",typeid);
    NSLog(@"comment :%@",comment);
    NSLog(@"title :%@",title);
    NSArray *objects = [[NSArray alloc] initWithObjects:account,title,comment,typeid, nil];
    NSArray *keys = [[NSArray alloc] initWithObjects:@"account",@"title",@"comment",@"typeid", nil];
    NSDictionary *tempJsonData = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    NSDictionary *finalJsonData = [[NSDictionary alloc] initWithObjectsAndKeys:tempJsonData,@"params", nil];
    NSData *temp = [NSJSONSerialization dataWithJSONObject:finalJsonData options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"555" forHTTPHeaderField:@"token"];
    [request setValue:@"application/raw charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:temp];

    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSData *objectData = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionaryData = [NSJSONSerialization JSONObjectWithData:objectData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    if([[dictionaryData valueForKey:@"status"]isEqual:@"pass"]){
     
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"ส่งข้อความสำเร็จ"
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"กลับหน้าหลัก" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [AppDelegate goHome:NO];
                                                                                                                     }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    NSLog(@"dictionaryData :%@",dictionaryData);
    return dictionaryData;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.placeholderLabel.hidden = YES;
    self.subView.hidden = YES;
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        self.placeholderLabel.hidden = NO;
    }
    
    [textView resignFirstResponder];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dropdownDataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    dropdownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dropdownCell" forIndexPath:indexPath];
    cell.textLabel.text = [self.dropdownDataArray objectAtIndex:indexPath.row];
    self.type = [NSString stringWithFormat:@"%d",indexPath.row+1];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
   // dropdownTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
  
    self.textfield1.text = [self.dropdownDataArray objectAtIndex:indexPath.row];
    [self.subView setHidden:YES];
}

@end
