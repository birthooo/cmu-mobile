//
//  CMUPhoneVersion.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneVersionModel.h"
#import "global.h"

@implementation CMUPhoneVersionModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *versionNumber = JSON_GET_OBJECT([dict objectForKey:@"version"]);
        if(versionNumber)
        {
            self.version = [versionNumber intValue];
        }
        self.versionURL = JSON_GET_OBJECT([dict objectForKey:@"versionURL"]);
    }
    return self;
}
- (NSDictionary *)toDictionary
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInt:self.version] forKey:@"version"];
    [dict setObject:self.versionURL forKey:@"versionURL"];
    return dict;
}
@end
