//
//  CMUPhoneFacultyViewFeed.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUPhoneFacultyViewModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
//@property(nonatomic, unsafe_unretained)int version;
@property(nonatomic, strong)NSMutableArray *phoneFacultyViewList;//CMUPhoneFacultyView
@end
