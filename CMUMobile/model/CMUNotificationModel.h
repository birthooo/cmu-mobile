//
//  CMUNotificationModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface CMUNotificationSettingsModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSMutableArray *newsList;
@property(nonatomic, strong)NSMutableArray *activityList;
@property(nonatomic, strong)NSMutableArray *channelList;
@end

//feed
@interface CMUNotificationFeed:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int notiId;
@property(nonatomic, unsafe_unretained)int type;
@property(nonatomic, strong)NSString *typeName;
@property(nonatomic, strong)NSString *message;
@property(nonatomic, strong)NSDate *update;
@property(nonatomic, unsafe_unretained)NSTimeInterval time;
@property(nonatomic, unsafe_unretained)BOOL read;
@property(nonatomic, unsafe_unretained)int itemId;
//broadcast
@property(nonatomic, unsafe_unretained)int senderId;
@property(nonatomic, strong)NSString *senderName;
@property(nonatomic, strong)NSString *senderImage;
//edoc
@property(nonatomic, strong)NSString *subItemId;
@end

@interface CMUNotificationFeedModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int notificationNumber;
@property(nonatomic, strong)NSMutableArray *notificationFeedList;
@end

//broadcast
@interface CMUBroadcastFeed:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int broadcastId;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *detail;
@property(nonatomic, strong)NSString *image;
@property(nonatomic, strong)NSDate *update;
@property(nonatomic, unsafe_unretained)int unread;
@end

@interface CMUBroadcastFeedModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSMutableArray *broadcastFeedList;
@end

@interface CMUBroadcastSenderFeed:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int broadcastSenderId;
@property(nonatomic, unsafe_unretained)int senderId;
@property(nonatomic, strong)NSString *senderName;
@property(nonatomic, strong)NSString *text;
@property(nonatomic, strong)NSDate *update;
@property(nonatomic, unsafe_unretained)NSTimeInterval time;
@end

@interface CMUBroadcastSenderFeedModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString *refreshURL;
@property(nonatomic, strong)NSString *loadMoreURL;
@property(nonatomic, strong)NSMutableArray *broadcastSenderFeedList;
@end

//noti number
@interface CMUNotificationNumberModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int notificationNumber;
@end

@interface CMUNotificationModel : NSObject

@end
