//
//  CMUPhoneFacultyView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUPhoneFacultyView : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int phoneFacultyViewId;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *detail;
@property(nonatomic, strong)NSString *phone;
@property(nonatomic, strong)NSString *fax;
@property(nonatomic, strong)NSString *email;
@property(nonatomic, strong)NSString *fanpage;
@property(nonatomic, strong)NSString *web;
@property(nonatomic, strong)NSString *phoneNumber;
@property(nonatomic, strong)NSMutableArray *phoneSubViewList;//CMUPhoneFacultyView
@end
