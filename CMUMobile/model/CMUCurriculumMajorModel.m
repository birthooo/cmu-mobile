//
//  CMUCurriculumMajorModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUCurriculumMajorModel.h"
#import "CMUJSONUtils.h"
#import "global.h"

@interface CMUCurriculumMajorView()
@property(nonatomic, strong) NSString *curriculumId;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSString *year;
@property(nonatomic, unsafe_unretained) int term;
@end
@implementation CMUCurriculumMajorView
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        self.curriculumId = JSON_GET_OBJECT([dict objectForKey:@"id"]);
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        self.year = JSON_GET_OBJECT([dict objectForKey:@"year"]);
        
        NSNumber *term = JSON_GET_OBJECT([dict objectForKey:@"term"]);
        if(term)
        {
            self.term = [term intValue];
        }
    }
    return self;
}
@end

@interface CMUCurriculumMajorModel()
@property(nonatomic, strong)NSMutableArray *curriculumList;
@end
@implementation CMUCurriculumMajorModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *curriculumMajorViewList = JSON_GET_OBJECT([dict objectForKey:@"CurriculumMajorViewList"]);
        if(curriculumMajorViewList)
        {
            self.curriculumList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in curriculumMajorViewList)
            {
                CMUCurriculumMajorView *curriculum = [[CMUCurriculumMajorView alloc] initWithDictionary:dict];
                [self.curriculumList addObject:curriculum];
            }
        }
    }
    return self;
}
@end
