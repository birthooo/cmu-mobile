//
//  CMULoginModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/13/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    CMU_USER_TYPE_EMPLOYEE,
    CMU_USER_TYPE_STUDENT,
}CMU_USER_TYPE;

@interface CMUTicket : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong) NSString *accessToken;
//"expires_in": 1800,
@property(nonatomic, strong) NSString *userName;
//"issued": "28/9/2557 13:30:54",
//"expires": "28/9/2557 14:00:54"
- (CMU_USER_TYPE) getUserType;
@end



@interface CMULoginModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) BOOL success;
@property(nonatomic, strong) CMUTicket *ticket;
@end
