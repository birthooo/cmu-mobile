//
//  CMUOnlineModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineModel.h"
#import "global.h"
#import "CMUFormatUtils.h"


@interface CMUOnlineVideoType()
@property(nonatomic, unsafe_unretained) CMU_ONLINE_VIDEOL_TYPE videoTypeId;
@property(nonatomic, strong) NSString *title;
@end

@implementation CMUOnlineVideoType
- (id)initWithType:(CMU_ONLINE_VIDEOL_TYPE) videoTypeId title:(NSString *)title;
{
    self = [super init];
    if(self)
    {
        self.videoTypeId = videoTypeId;
        self.title = title;
    }
    return self;
}

+ (NSMutableArray *)allVideoType
{
    NSMutableArray *allVideoType = [[NSMutableArray alloc] init];
    [allVideoType addObject:[[CMUOnlineVideoType alloc] initWithType:CMU_ONLINE_VIDEO_TYPE_RECENTLY_ADDED title:@"Recently Added"]];
    [allVideoType addObject:[[CMUOnlineVideoType alloc] initWithType:CMU_ONLINE_VIDEO_TYPE_MOST_POPULAR title:@"Most Popular"]];
    [allVideoType addObject:[[CMUOnlineVideoType alloc] initWithType:CMU_ONLINE_VIDEO_TYPE_ALSO_CHECK_OUT title:@"Also Check Out"]];
    [allVideoType addObject:[[CMUOnlineVideoType alloc] initWithType:CMU_ONLINE_VIDEO_TYPE_ALL_VIDEO title:@"All VDO"]];
    return allVideoType;
}

+ (CMUOnlineVideoType *)getByVideoTypeId:(CMU_ONLINE_VIDEOL_TYPE) videoTypeId
{
    CMUOnlineVideoType *temp = [[CMUOnlineVideoType alloc] init];
    temp.videoTypeId = videoTypeId;
    NSInteger index = [[self allVideoType] indexOfObject:temp];
    if(index != NSNotFound)
    {
        return [[self allVideoType] objectAtIndex:index];
    }
    return nil;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUOnlineVideoType class]])
    {
        return self.videoTypeId == [other videoTypeId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.videoTypeId;
}
@end



@implementation CMUOnlineVideoFeed
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *vdoId = JSON_GET_OBJECT([dict objectForKey:@"vdoId"]);
        if(vdoId)
        {
            self.vdoId = [vdoId intValue];
        }
        
        NSNumber *vdoview = JSON_GET_OBJECT([dict objectForKey:@"vdoview"]);
        if(vdoview)
        {
            self.vdoview = [vdoview intValue];
        }
        
        NSNumber *vdolike = JSON_GET_OBJECT([dict objectForKey:@"vdolike"]);
        if(vdolike)
        {
            self.vdolike = [vdolike intValue];
        }
        
        NSNumber *vdopub = JSON_GET_OBJECT([dict objectForKey:@"vdopub"]);
        if(vdopub)
        {
            self.vdopub = [vdopub intValue];
        }
        
        self.vdoName = JSON_GET_OBJECT([dict objectForKey:@"vdoName"]);
        self.vdoDesc = JSON_GET_OBJECT([dict objectForKey:@"vdoDesc"]);
        self.linkPage = JSON_GET_OBJECT([dict objectForKey:@"linkPage"]);
        self.vdoDate = [CMUFormatUtils parseWebServiceDate:JSON_GET_OBJECT([dict objectForKey:@"vdoDate"])];
        self.vdoPic = JSON_GET_OBJECT([dict objectForKey:@"vdoPic"]);
        self.vdoPath = JSON_GET_OBJECT([dict objectForKey:@"vdoPath"]);
        self.vdotime = JSON_GET_OBJECT([dict objectForKey:@"vdotime"]);
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUOnlineVideoFeed class]])
    {
        return self.vdoId == [other vdoId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.vdoId;
}
@end

@implementation CMUOnlineVideoFeedModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *videoFeedList = JSON_GET_OBJECT([dict objectForKey:@"CuteVdoList"]);
        if(videoFeedList)
        {
            self.videoFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in videoFeedList)
            {
                CMUOnlineVideoFeed *videoFeed = [[CMUOnlineVideoFeed alloc] initWithDictionary:dict];
                [self.videoFeedList addObject:videoFeed];
            }
        }
        
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end


#pragma mark CMUOnlineItem
@implementation CMUOnlineCuteAjarnFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *cuteId = JSON_GET_OBJECT([dict objectForKey:@"cuteId"]);
        if(cuteId)
        {
            self.cuteId = [cuteId intValue];
        }
        self.cuteName = JSON_GET_OBJECT([dict objectForKey:@"cuteName"]);
    }
    return self;
}
@end


@implementation CMUOnlineCuteAjarnModel
//- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(id) array
{
    
    NSArray *cuteAjarnList = (NSArray *)array;
    if(cuteAjarnList)
    {
        self.cuteAjarnList = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in cuteAjarnList)
        {
            CMUOnlineCuteAjarnFeed *cuteAjarn = [[CMUOnlineCuteAjarnFeed alloc] initWithDictionary:dict];
            [self.cuteAjarnList addObject:cuteAjarn];
        }
    }
    return self;
}
@end


@implementation CMUOnlineCommentFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *commentId = JSON_GET_OBJECT([dict objectForKey:@"comment_id"]);
        if(commentId)
        {
            self.commentId = [commentId intValue];
        }
        self.commentText = JSON_GET_OBJECT([dict objectForKey:@"comment_text"]);
        
        NSNumber *vdoId = JSON_GET_OBJECT([dict objectForKey:@"vdo_id"]);
        if(vdoId)
        {
            self.vdoId = [vdoId intValue];
        }
        self.accId = JSON_GET_OBJECT([dict objectForKey:@"ac_id"]);
        self.commentDatetime = [CMUFormatUtils parseWebServiceDate:JSON_GET_OBJECT([dict objectForKey:@"comment_datetime"])];
        
        
    }
    return self;
}

- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUOnlineCommentFeed class]])
    {
        return self.commentId == [other commentId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.commentId;
}

@end

@implementation CMUOnlineCommentModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *commentFeedList = JSON_GET_OBJECT([dict objectForKey:@"CommentList"]);
        if(commentFeedList)
        {
            self.commentFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in commentFeedList)
            {
                CMUOnlineCommentFeed *commentFeed = [[CMUOnlineCommentFeed alloc] initWithDictionary:dict];
                [self.commentFeedList addObject:commentFeed];
            }
        }
        
        NSNumber *numberComment = JSON_GET_OBJECT([dict objectForKey:@"numberComment"]);
        if(numberComment)
        {
            self.numberComment = [numberComment intValue];
        }
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end

@implementation CMUOnlineModel

@end
