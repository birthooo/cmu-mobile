//
//  CMUModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/17/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsModel.h"
#import "CMUNewsFeed.h"
#import "CMUNewsForPost.h"

#import "CMUNewsCategory.h"
#import "CMUMenuItem.h"

#import "CMUPhoneVersionModel.h"
#import "CMUPhoneFacultyView.h"
#import "CMUPhoneFacultyViewModel.h"

//curriculum
#import "CMUFaculty.h"
#import "CMUCurriculumMajorModel.h"

//photo and video
#import "CMUPhotoAndVdoModel.h"

//channel
#import "CMUChanelFeedModel.h"

//event
#import "CMUEventDetailFeedModel.h"

//map
#import "CMUMapFeedModel.h"

//log in
#import "CMULoginModel.h"

//user info
#import "CMUUserInfoModel.h"

//notification
#import "CMUNotificationModel.h"

//sis
#import "CMUSISModel.h"

//cmu online
#import "CMUOnlineModel.h"

//cmu mis
#import "CMUMISModel.h"

//cmu contest
#import "CMUPhotoContestModel.h"

//cmu transit
#import "CMUTransit.h"