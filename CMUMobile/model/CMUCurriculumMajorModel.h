//
//  CMUCurriculumMajorModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/30/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUCurriculumMajorView : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, readonly) NSString *curriculumId;
@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *year;
@property(nonatomic, readonly) int term;
@end

@interface CMUCurriculumMajorModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, readonly)NSMutableArray *curriculumList;
@end
