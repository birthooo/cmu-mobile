//
//  CMUMISModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUMISModel.h"
#import "global.h"
#import "CMUFormatUtils.h"
#import "NSString+HTML.h"
#import "CMUStringUtils.h"
#import "global.h"
#import <UIKit/UIKit.h>

@interface CMUMISEdocCountNumber()

@end

@implementation CMUMISEdocCountNumber

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *nPublic = JSON_GET_OBJECT([dict objectForKey:@"nPublic"]);
        if(nPublic)
        {
            self.nPublic = [nPublic intValue];
        }
        
        NSNumber *nRecieve = JSON_GET_OBJECT([dict objectForKey:@"nRecieve"]);
        if(nRecieve)
        {
            self.nRecieve = [nRecieve intValue];
        }
        
        NSNumber *nStatusFollow = JSON_GET_OBJECT([dict objectForKey:@"nStatusFollow"]);
        if(nStatusFollow)
        {
            self.nStatusFollow = [nStatusFollow intValue];
        }
        
        NSNumber *nStatusSend = JSON_GET_OBJECT([dict objectForKey:@"nStatusSend"]);
        if(nStatusSend)
        {
            self.nStatusSend = [nStatusSend intValue];
        }
        
    }
    return self;
}
@end



@implementation CMUMISEdocFeed

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *IsOpened = JSON_GET_OBJECT([dict objectForKey:@"IsOpened"]);
        if(IsOpened)
        {
            self.IsOpened = [IsOpened boolValue];
        }
        
        self.docCode = JSON_GET_OBJECT([dict objectForKey:@"docCode"]);
        
        NSNumber *docID = JSON_GET_OBJECT([dict objectForKey:@"docID"]);
        if(docID)
        {
            self.docID = [docID intValue];
        }
        
        self.docSubID = JSON_GET_OBJECT([dict objectForKey:@"docSubID"]);
        
        NSNumber *docTypeID = JSON_GET_OBJECT([dict objectForKey:@"docTypeID"]);
        if(docTypeID)
        {
            self.docTypeID = [docTypeID intValue];
        }
        
        
        self.sendTime = [CMUFormatUtils parseWebServiceDate:JSON_GET_OBJECT([dict objectForKey:@"sendTime"])];
        
        self.textStaus = JSON_GET_OBJECT([dict objectForKey:@"textStaus"]);
        
        self.topic = JSON_GET_OBJECT([dict objectForKey:@"topic"]);
        
    }
    return self;
}

- (NSString *)htmlText
{
    return [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><font color=\"red\">%@</font>%@</body></html>",
            (IS_IPAD)?20:14,
            [CMUStringUtils isEmpty:self.textStaus]?@"":[NSString stringWithFormat:@"%@  &nbsp;", [self.textStaus kv_encodeHTMLCharacterEntities]],
            [CMUStringUtils isEmpty:self.topic]?@"":[self.topic kv_encodeHTMLCharacterEntities]];
    
}
@end

@implementation CMUMISEdocFeedModel
//- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(id) array
{
    
    NSArray *cuteAjarnList = (NSArray *)array;
    if(cuteAjarnList)
    {
        self.edocFeedList = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in cuteAjarnList)
        {
            CMUMISEdocFeed *edocFeed = [[CMUMISEdocFeed alloc] initWithDictionary:dict];
            [self.edocFeedList addObject:edocFeed];
        }
    }
    return self;
}

@end


@implementation CMUMISEdocOrderFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *order = JSON_GET_OBJECT([dict objectForKey:@"order"]);
        if(order)
        {
            self.order = [order intValue];
        }
        
        self.text = JSON_GET_OBJECT([dict objectForKey:@"text"]);
        self.toName = JSON_GET_OBJECT([dict objectForKey:@"toName"]);
        self.toOrgan = JSON_GET_OBJECT([dict objectForKey:@"toOrgan"]);
        self.toNote = JSON_GET_OBJECT([dict objectForKey:@"toNote"]);
        self.fromNote = JSON_GET_OBJECT([dict objectForKey:@"fromNote"]);
        self.fromOrgan = JSON_GET_OBJECT([dict objectForKey:@"fromOrgan"]);
        self.objective = JSON_GET_OBJECT([dict objectForKey:@"objective"]);
        self.fromName = JSON_GET_OBJECT([dict objectForKey:@"fromName"]);
    }
    return self;
}
- (NSString *)htmlFrom
{
    return [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>จาก &nbsp;</strong>%@<br>%@<br>หมายเหตุ%@</body></html>",
            (IS_IPAD)?17:14,
            [CMUStringUtils isEmpty:self.fromName]?@"":[self.fromName kv_encodeHTMLCharacterEntities],
            [CMUStringUtils isEmpty:self.fromOrgan]?@"":[self.fromOrgan kv_encodeHTMLCharacterEntities],
            [CMUStringUtils isEmpty:self.fromNote]?@"":[NSString stringWithFormat:@"<br>%@",[self.fromNote kv_encodeHTMLCharacterEntities]]  ];
    
}

- (NSString *)htmlTo
{
    return [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>ถึง &nbsp;</strong>%@<br>หมายเหตุ%@</body></html>",
            (IS_IPAD)?17:14,
            [CMUStringUtils isEmpty:self.toName]?@"":[self.toName kv_encodeHTMLCharacterEntities],
            [CMUStringUtils isEmpty:self.toNote]?@"":[NSString stringWithFormat:@"<br>%@",[self.toNote kv_encodeHTMLCharacterEntities]]  ];
}

- (NSString *)htmlObjective
{
    return [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>วัตถุประสงค์ &nbsp;</strong>%@</body></html>",
            (IS_IPAD)?17:14,
            [CMUStringUtils isEmpty:self.objective]?@"":[self.objective kv_encodeHTMLCharacterEntities]];
}

- (NSString *)htmlText
{
    return [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>ข้อความ &nbsp;</strong>%@</body></html>",
            (IS_IPAD)?17:14,
            [CMUStringUtils isEmpty:self.text]?@"":[self.text kv_encodeHTMLCharacterEntities]];
}
@end

@implementation CMUMISEdocOrderModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSArray *edocTabOderDetailList = JSON_GET_OBJECT([dict objectForKey:@"EdocTabOderDetailList"]);
        if(edocTabOderDetailList)
        {
            self.edocTabOderDetailList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in edocTabOderDetailList)
            {
                CMUMISEdocOrderFeed *edocOrderFeed = [[CMUMISEdocOrderFeed alloc] initWithDictionary:dict];
                [self.edocTabOderDetailList addObject:edocOrderFeed];
            }
        }
    }
    return self;
}

@end

@implementation CMUMISEdocDetail

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.lblTypeDocumentName = JSON_GET_OBJECT([dict objectForKey:@"lblTypeDocumentName"]);
        self.lblDocumentCode = JSON_GET_OBJECT([dict objectForKey:@"lblDocumentCode"]);
        self.lblPriorityLevel = JSON_GET_OBJECT([dict objectForKey:@"lblPriorityLevel"]);
        self.lblTitleHead = JSON_GET_OBJECT([dict objectForKey:@"lblTitleHead"]);
        self.lblSendDate = JSON_GET_OBJECT([dict objectForKey:@"lblSendDate"]);
        self.lblSecurityLevel = JSON_GET_OBJECT([dict objectForKey:@"lblSecurityLevel"]);
        self.lblCreateReceiveDate = JSON_GET_OBJECT([dict objectForKey:@"lblCreateReceiveDate"]);
        self.hdfTypeDocumentID = JSON_GET_OBJECT([dict objectForKey:@"hdfTypeDocumentID"]);
        self.docSubID= JSON_GET_OBJECT([dict objectForKey:@"docSubID"]);
        NSNumber *accept = JSON_GET_OBJECT([dict objectForKey:@"accept"]);
        if(accept)
        {
            self.accept = [accept boolValue];
        }
        
        self.detailModel = JSON_GET_OBJECT([dict objectForKey:@"detailModel"]);
        self.fileModel = JSON_GET_OBJECT([dict objectForKey:@"fileModel"]);
        self.orderModel = [[CMUMISEdocOrderModel alloc] initWithDictionary:JSON_GET_OBJECT([dict objectForKey:@"orderModel"])] ;
        
    }
    return self;
}
@end


/* Folder */
@implementation CMUMISEdocFolderFeed

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *folderID = JSON_GET_OBJECT([dict objectForKey:@"folderID"]);
        if(folderID)
        {
            self.folderID = [folderID intValue];
        }
        
        self.folderName = JSON_GET_OBJECT([dict objectForKey:@"folderName"]);
    }
    return self;
}
@end

@implementation CMUMISEdocFolderFeedModel
- (id)initWithDictionary:(id) array
{
    NSArray *edocFolderFeedList = (NSArray *)array;
    if(edocFolderFeedList)
    {
        self.edocFolderFeedList = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in edocFolderFeedList)
        {
            CMUMISEdocFolderFeed *edocFolderFeed = [[CMUMISEdocFolderFeed alloc] initWithDictionary:dict];
            [self.edocFolderFeedList addObject:edocFolderFeed];
        }
    }
    return self;
}
@end

/*Edoc SubList*/


@implementation CMUMISEdocFolderSubListFeedModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *nSend = JSON_GET_OBJECT([dict objectForKey:@"nSend"]);
        if(nSend)
        {
            self.nSend = [nSend intValue];
        }
        
        NSNumber *nOut = JSON_GET_OBJECT([dict objectForKey:@"nOut"]);
        if(nOut)
        {
            self.nOut = [nOut intValue];
        }
        
        NSArray *listSend = JSON_GET_OBJECT([dict objectForKey:@"listSend"]);
        if(listSend)
        {
            self.listSend = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in listSend)
            {
                CMUMISEdocFeed *edocFeed = [[CMUMISEdocFeed alloc] initWithDictionary:dict];
                [self.listSend addObject:edocFeed];
            }
        }
        
        NSArray *listOut = JSON_GET_OBJECT([dict objectForKey:@"listOut"]);
        if(listOut)
        {
            self.listOut = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in listOut)
            {
                CMUMISEdocFeed *edocFeed = [[CMUMISEdocFeed alloc] initWithDictionary:dict];
                [self.listOut addObject:edocFeed];
            }
        }
    }
    return self;
}

@end


@implementation CMUMISModel

@end
