//
//  CMUNewsFeed.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CMUNewsFeed : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int newsFeedId;
@property(nonatomic, strong) NSString* topic;
@property(nonatomic, strong) NSString* detail;
@property(nonatomic, strong) NSDate* update;
@property(nonatomic, strong) NSString* link;
@property(nonatomic, strong) NSString* imageLink;
@property(nonatomic, strong) NSString* nameType;
@property(nonatomic, unsafe_unretained) BOOL isFacebook;
@property(nonatomic, strong) NSString* facebookId;
@property(nonatomic, strong) NSString*facebookPostId;
@property(nonatomic, strong) NSDate* time;
@property(nonatomic, unsafe_unretained)int typeId;
@property(nonatomic, unsafe_unretained)BOOL isOpenBrowser;
@end
