//
//  CMUMenuItem.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMenuItem.h"
#import "global.h"
#import "CMUUserInfoModel.h"
#import "CMUWebServiceManager.h"
@interface CMUMenuItem ()
@property(nonatomic, unsafe_unretained) CMU_MENU_ITEM menuItemId;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) UIImage *iconImage;

@end
@implementation CMUMenuItem

- (id)initWithMenuId:(CMU_MENU_ITEM) menuItemId title:(NSString *)title iconImage:(UIImage *) iconImage;
{
    self = [super init];
    if(self)
    {
        self.menuItemId = menuItemId;
        self.title = title;
        self.iconImage = iconImage;
    }
    return self;
}

+ (NSMutableArray *)allMenuItems
{
    NSLog(@"allmenuitems loaded");
    NSMutableArray *allMenuItems = [[NSMutableArray alloc] init];
    CMUUser *user = [[[CMUWebServiceManager sharedInstance] userInfoModel] user];
    if([user.personTypeEN isEqual:@"Employee"]){
        //add edoc
        [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_EDOC title:@"E-Doc" iconImage:[UIImage imageNamed:@"cmu_noti_edoc_setting.png"]]];
    }
    
    //1.News
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_NEWS title:@"News" iconImage:[UIImage imageNamed:@"news.png"]]];
    
    //2. Calendar
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_EVENT title:@"Activity" iconImage:[UIImage imageNamed:@"event.png"]]];
    
    //3.CMU Channel
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CMU_CHANNEL title:@"CMU Channel" iconImage:[UIImage imageNamed:@"cmu_channel.png"]]];
    
    //4. Map
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_MAP title:@"Map" iconImage:[UIImage imageNamed:@"map.png"]]];
    
    //5. Curriculum
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CURRICULUM title:@"Curriculum" iconImage:[UIImage imageNamed:@"curriculum.png"]]];
    
    //6. Photo & VDO
        [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_PHOTO_VIDEO title:@"Photo & Video" iconImage:[UIImage imageNamed:@"photo_video.png"]]];
    
    //7. Information
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_INFORMATION title:@"Information" iconImage:[UIImage imageNamed:@"information.png"]]];
    
    //8. Phone/Emergency
     [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_PHONE title:@"Phone/Emergency" iconImage:[UIImage imageNamed:@"phone.png"]]];
    
    //9. Transit
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CMU_TRANSIT title:@"CMU Transit" iconImage:[UIImage imageNamed:@"cmu_transit.png"]]];
    
    //10. e-Learning
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CMU_ONLINE title:@"e-Learning" iconImage:[UIImage imageNamed:@"cmu_online.png"]]];
    
    //11. Jumbo Test
    
    //12. MyFlashCard
     [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_MYFLASHCARD title:@"MyFlashCard" iconImage:[UIImage imageNamed:@"cmu_myflashcard.png"]]];
    
    //13. My Echo
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_MY_ECHO title:@"MyEcho" iconImage:[UIImage imageNamed:@"myecho.png"]]];
    
    //14. ITSC
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_ITSC title:@"ITSC" iconImage:[UIImage imageNamed:@"itsc.png"]]];
    
    //15. e-Textbook
    
    //16. e-Research
     [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_E_RESEARCH title:@"e-Research" iconImage:[UIImage imageNamed:@"e_research.png"]]];
    
    //17. e-Theses
    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_E_THESES title:@"e-Theses" iconImage:[UIImage imageNamed:@"e_theses.png"]]];

    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CMU_ONLINE_ENGLISH title:@"CMUOnlineEnglish" iconImage:[UIImage imageNamed:@"cmu_onlineEnglish"]]];
    
    NSLog(@"allitem :%@",allMenuItems);
   
 
    //  Created by Satianpong Yodnin on 9/9/2558 BE.
    
   
    
    
   
        
    // [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_5DEC title:@"CMU 5decades" iconImage:[UIImage imageNamed:@"cmu_5dec.png"]]];
   // [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_LI_DIPLOMA title:@"LICMU Diploma" iconImage:[UIImage imageNamed:@"licmu_diploma.png"]]];
    //[allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_LI_CONVERSATION title:@"LICMU Conversation" iconImage:[UIImage imageNamed:@"licmu_conversation.png"]]];
    //[allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_LI_GRAMMAR title:@"LICMU Grammar" iconImage:[UIImage imageNamed:@"licmu_grammar.png"]]];
    //[allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_LI_KIDS title:@"LICMU Kids" iconImage:[UIImage imageNamed:@"licmu_kids.png"]]];
    //[allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_FEEDBACK title:@"Feedback" iconImage:[UIImage imageNamed:@"feedback180.png"]]];
    
    
    
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_PHOTO_VIDEO2 title:@"Photo & Video2" iconImage:[UIImage imageNamed:@"photo_video.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_INFORMATION2 title:@"Information2" iconImage:[UIImage imageNamed:@"information.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_PHONE2 title:@"Phone2" iconImage:[UIImage imageNamed:@"phone.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CMU_ONLINE2 title:@"CMU Online2" iconImage:[UIImage imageNamed:@"cmu_online.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_E_RESEARCH2 title:@"e-Research2" iconImage:[UIImage imageNamed:@"e_research.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_E_THESES2 title:@"e-Theses2" iconImage:[UIImage imageNamed:@"e_theses.png"]]];
//    [allMenuItems addObject:[[CMUMenuItem alloc] initWithMenuId:CMU_MENU_ITEM_CONNECT2 title:@"Connect2" iconImage:[UIImage imageNamed:@"connect.png"]]];
    return allMenuItems;
     
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUMenuItem class]])
    {
        return self.menuItemId == [other menuItemId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.menuItemId;
}

@end
