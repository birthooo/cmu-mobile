//
//  CMUNewsCategory.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    CMU_NEWS_CATEGORY_TYPE_ALL = 0,
    CMU_NEWS_CATEGORY_HOT = 1,
    CMU_NEWS_CATEGORY_JOB = 2,
    CMU_NEWS_CATEGORY_ORDER = 3,
    CMU_NEWS_CATEGORY_MIS = 4,
    CMU_NEWS_CATEGORY_CNOC = 5,
    CMU_NEWS_CATEGORY_REG = 6,
    CMU_NEWS_CATEGORY_LANGUAGE = 7,
    CMU_NEWS_CATEGORY_LIBRARY = 8,
    CMU_NEWS_CATEGORY_COOPERATIVE = 9,
    //CMU_NEWS_CATEGORY_ACADEMIC = 10,
    CMU_NEWS_CATEGORY_ACTIVITY = 11,
    CMU_NEWS_CATEGORY_ACTIVITY_STUDENT = 12,
    CMU_NEWS_CATEGORY_DIRECTOR = 13,
    CMU_NEWS_CATEGORY_COLLEGE = 14,
    CMU_NEWS_CATEGORY_DENT = 15,
    CMU_NEWS_CATEGORY_VITHED = 16,
    CMU_NEWS_CATEGORY_MAX = 17,
    
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    //CMU_NEWS_CATEGORY_TEST = 3
    
} CMU_NEWS_CATEGORY;

@interface CMUNewsCategory : NSObject
- (id)initWithCategory:(CMU_NEWS_CATEGORY) categoryId title:(NSString *)title;
+ (NSMutableArray *)allCategories;
+ (CMUNewsCategory *)getByCategoryId:(CMU_NEWS_CATEGORY) categoryId;
@property(nonatomic, readonly) CMU_NEWS_CATEGORY categoryId;
@property(nonatomic, readonly) NSString *title;
@end
