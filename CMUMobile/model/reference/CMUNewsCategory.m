//
//  CMUNewsCategory.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsCategory.h"

@interface CMUNewsCategory()
@property(nonatomic, unsafe_unretained) CMU_NEWS_CATEGORY categoryId;
@property(nonatomic, strong) NSString *title;
@end
@implementation CMUNewsCategory
- (id)initWithCategory:(CMU_NEWS_CATEGORY) categoryId title:(NSString *)title
{
    self = [super init];
    if(self)
    {
        self.categoryId = categoryId;
        self.title = title;
    }
    return self;
}

+ (NSMutableArray *)allCategories
{
      NSMutableArray *allCategories = [[NSMutableArray alloc] init];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_TYPE_ALL title:@"ข่าวทั้งหมด"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_HOT title:@"ข่าวเด่น"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_JOB title:@"ข่าวทุนการศึกษา&แนะแนวนักศึกษา"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_ORDER title:@"ITSC"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_MIS title:@"CMU MIS"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_CNOC title:@"CNOC ITSC"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_REG title:@"ประกาศสำนักทะเบียนและประมวลผล"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_LANGUAGE title:@"สถาบันภาษา มหาวิทยาลัยเชียงใหม่"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_LIBRARY title:@"สำนักหอสมุด"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_COOPERATIVE title:@"กองพัฒนานักศึกษา"]];
//    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_ACADEMIC title:@"สำนักบริการวิชาการ มช."]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_ACTIVITY title:@"ข่าวกิจกรรม"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_ACTIVITY_STUDENT title:@"ข่าวกิจกรรมนักศึกษา"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_DIRECTOR title:@"ข่าวผู้บริหาร"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_COLLEGE title:@"ข่าววิทยาลัยนานาชาติ"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_DENT title:@"คณะทันตแพทย์"]];
    [allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_VITHED title:@"กองวิเทศสัมพันธ์"]];
    
    //  Edited by Satianpong Yodnin on 9/9/2558 BE.
    //[allCategories addObject:[[CMUNewsCategory alloc] initWithCategory:CMU_NEWS_CATEGORY_TEST title:@"ทดสอบข่าว"]];
    
    return allCategories;
}

+ (CMUNewsCategory *)getByCategoryId:(CMU_NEWS_CATEGORY) categoryId
{
    CMUNewsCategory *temp = [[CMUNewsCategory alloc] init];
    temp.categoryId = categoryId;
    NSInteger index = [[self allCategories] indexOfObject:temp];
    if(index != NSNotFound)
    {
        return [[self allCategories] objectAtIndex:index];
    }
    return nil;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUNewsCategory class]])
    {
        return self.categoryId == [other categoryId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.categoryId;
}
@end
