//
//  CMUMenuItem.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    CMU_MENU_ITEM_EDOC = 0,
    CMU_MENU_ITEM_NEWS = 1,
    CMU_MENU_ITEM_EVENT = 2,
    CMU_MENU_ITEM_CMU_CHANNEL = 3,
    CMU_MENU_ITEM_MAP = 4,
    CMU_MENU_ITEM_CURRICULUM = 5,
    CMU_MENU_ITEM_INFORMATION = 6,
    CMU_MENU_ITEM_PHONE = 7,
    CMU_MENU_ITEM_CMU_TRANSIT = 8,
    CMU_MENU_ITEM_CMU_ONLINE = 9,
    CMU_MENU_ITEM_MYFLASHCARD = 10,
    CMU_MENU_ITEM_MY_ECHO = 11,
    CMU_MENU_ITEM_ITSC = 12,
    CMU_MENU_ITEM_E_RESEARCH = 13,
    CMU_MENU_ITEM_E_THESES = 14,
    CMU_MENU_ITEM_CMU_ONLINE_ENGLISH = 15,
    
    CMU_MENU_ITEM_PHOTO_VIDEO = 99
} CMU_MENU_ITEM;
    /*
    CMU_MENU_ITEM_CONNECT = 12,
   
   
   
    CMU_MENU_ITEM_LI_CONVERSATION = 13,
    CMU_MENU_ITEM_LI_DIPLOMA = 14,
    CMU_MENU_ITEM_LI_GRAMMAR = 15,
    CMU_MENU_ITEM_LI_KIDS = 16,
    CMU_MENU_ITEM_5DEC = 17,
    CMU_MENU_ITEM_FEEDBACK = 18,

    
    CMU_MENU_ITEM_PHONE2 = 20,
    CMU_MENU_ITEM_CMU_ONLINE2 = 21,
    CMU_MENU_ITEM_E_RESEARCH2 = 22,
    CMU_MENU_ITEM_E_THESES2 = 23,
    CMU_MENU_ITEM_CONNECT2 = 24,
       CMU_MENU_ITEM_PHOTO_VIDEO = 99,
    //  Created by Satianpong Yodnin on 9/9/258 BE.
*/
    



@interface CMUMenuItem : NSObject
- (id)initWithMenuId:(CMU_MENU_ITEM) menuItemId title:(NSString *)title iconImage:(UIImage *) iconImage;
+ (NSMutableArray *)allMenuItems;
@property(nonatomic, readonly) CMU_MENU_ITEM menuItemId;
@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) UIImage *iconImage;
@end
