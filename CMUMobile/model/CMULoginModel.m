//
//  CMULoginModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/13/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMULoginModel.h"
#import "global.h"

@implementation CMUTicket
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.accessToken = JSON_GET_OBJECT([dict objectForKey:@"access_token"]);
        self.userName = JSON_GET_OBJECT([dict objectForKey:@"userName"]);    
    }
    return self;
}

- (CMU_USER_TYPE) getUserType
{
    if([self.userName rangeOfString:@"."].location != NSNotFound)
    {
        return CMU_USER_TYPE_EMPLOYEE;
    }
    return CMU_USER_TYPE_STUDENT;
}
@end

@implementation CMULoginModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *success = JSON_GET_OBJECT([dict objectForKey:@"success"]);
        if(success)
        {
            self.success = [success boolValue];
            
            NSDictionary *ticket = JSON_GET_OBJECT([dict objectForKey:@"ticket"]);
            if(ticket)
            {
                self.ticket = [[CMUTicket alloc] initWithDictionary:ticket];
            }
        }
    }
    return self;
}
@end
