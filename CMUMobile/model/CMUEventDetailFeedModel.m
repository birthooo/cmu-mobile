//
//  CmuEventdetailFeedModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/13/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUEventDetailFeedModel.h"
#import "global.h"
#import "CMUFormatUtils.h"

/* CMUEventType */
static NSMutableArray *allEventType;

@interface CMUEventType()
@property(nonatomic, unsafe_unretained) CMU_EVENT_TYPE eventTypeId;
@property(nonatomic, strong) NSString *title;
@end

@implementation CMUEventType
- (id)initWithType:(CMU_EVENT_TYPE) eventTypeId title:(NSString *)title;
{
    self = [super init];
    if(self)
    {
        self.eventTypeId = eventTypeId;
        self.title = title;
    }
    return self;
}

+ (NSMutableArray *)allEventType
{
    if(allEventType == nil)
    {
        allEventType = [[NSMutableArray alloc] init];
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_HIGHLIGHT title:@"highlight"]];
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_HOLIDAY title:@"วันหยุด"]];
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_ACTIVITY_GENERAL title:@"กิจกรรมทั่วไป"]];
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_ACTIVITY_INSTITUTION title:@"กิจกรรม นศ จากหน่วยงาน"]];
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_ACTIVITY_STUDENT title:@"กิจกรรม นศ จากนักศึกษา"]];
        
        //  Created by Satianpong Yodnin on 9/9/2558 BE.
        [allEventType addObject:[[CMUEventType alloc] initWithType:CMU_EVENT_TYPE_ACTIVITY_ITSC title:@"กิจกรรม ITSC"]];
    }
    return allEventType;
}

+ (CMUEventType *)getByEventId:(CMU_EVENT_TYPE) eventTypeId
{
    CMUEventType *temp = [[CMUEventType alloc] init];
    temp.eventTypeId = eventTypeId;
    NSInteger index = [[self allEventType] indexOfObject:temp];
    if(index != NSNotFound)
    {
        return [[self allEventType] objectAtIndex:index];
    }
    return nil;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUEventType class]])
    {
        return self.eventTypeId == [other eventTypeId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.eventTypeId;
}
@end
/* CmuEventdetailFeed */
@implementation CMUEventDetailFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *eventdetailFeedId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(eventdetailFeedId)
        {
            self.eventdetailFeedId = [eventdetailFeedId intValue];
        }
        
        self.dayOfWeek = JSON_GET_OBJECT([dict objectForKey:@"dayOfWeek"]);
        self.numOfDay = JSON_GET_OBJECT([dict objectForKey:@"numOfDay"]);
        self.topic = JSON_GET_OBJECT([dict objectForKey:@"Toppic"]);
        
        NSString *startDateStr = JSON_GET_OBJECT([dict objectForKey:@"startDate"]);
        self.startDate = [CMUFormatUtils parseWebServiceDate:startDateStr];
        
        NSString *endtDateStr = JSON_GET_OBJECT([dict objectForKey:@"endtDate"]);
        self.endtDate = [CMUFormatUtils parseWebServiceDate:endtDateStr];
        self.linkURL = JSON_GET_OBJECT([dict objectForKey:@"LinkURL"]);
        //self.linkURL = @"http://www.google.co.th";
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUEventDetailFeed class]])
    {
        return self.eventdetailFeedId == [other eventdetailFeedId];
    }
    return NO;
}
@end


//@implementation CmuDateEvent
//@end

@implementation CMUEventDetailFeedModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *eventdetailFeedList = JSON_GET_OBJECT([dict objectForKey:@"CmuEventdetailFeed"]);
        if(eventdetailFeedList)
        {
            self.eventdetailFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in eventdetailFeedList)
            {
                CMUEventDetailFeed *channelFeed = [[CMUEventDetailFeed alloc] initWithDictionary:dict];
                [self.eventdetailFeedList addObject:channelFeed];
            }
        }
    }
    return self;
}
@end
