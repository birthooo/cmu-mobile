//
//  CMUSISModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/17/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISModel.h"
#import "global.h"


@implementation CMUSISEvalForm
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.subjectCode = JSON_GET_OBJECT([dict objectForKey:@"subjectCode"]);
        self.questionId = JSON_GET_OBJECT([dict objectForKey:@"questionid"]);
        self.question = JSON_GET_OBJECT([dict objectForKey:@"question"]);
        self.questionLevel = JSON_GET_OBJECT([dict objectForKey:@"questionLevel"]);
        NSNumber *questionType = JSON_GET_OBJECT([dict objectForKey:@"questionType"]);
        if(questionType)
        {
            self.questionType = [questionType intValue];
        }
        
        NSNumber *score = JSON_GET_OBJECT([dict objectForKey:@"score"]);
        if(score)
        {
            self.score = [score intValue];
        }
    }
    return self;
}

- (NSDictionary *)dictionary
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            JSON_PARSE_OBJECT(self.subjectCode),@"subjectCode",
            JSON_PARSE_OBJECT(self.questionId),@"questionid",
            JSON_PARSE_OBJECT(nil), @"question",
            JSON_PARSE_OBJECT(self.questionLevel),@"questionLevel",
            [NSNumber numberWithInt:self.questionType],@"questionType",
            [NSNumber numberWithInt:self.score], @"score",
            nil];
}

@end

@implementation CMUSISEvalFormModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *status = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(status)
        {
            self.status = [status boolValue];
        }
        
        NSArray *evaFormList = JSON_GET_OBJECT([dict objectForKey:@"evaFormView"]);
        if(evaFormList)
        {
            self.evaFormList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in evaFormList)
            {
                CMUSISEvalForm *evalForm = [[CMUSISEvalForm alloc] initWithDictionary:dict];
                [self.evaFormList addObject:evalForm];
            }
        }
        self.comment = JSON_GET_OBJECT([dict objectForKey:@"comment"]);
        
    }
    return self;
}
- (NSDictionary *)dictionary
{
    NSMutableArray *evalFormList = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.evaFormList.count; i++)
    {
        [evalFormList addObject:[[self.evaFormList objectAtIndex:i] dictionary]];
    }
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithBool:self.status],@"status",
            evalFormList, @"evaFormView",
            self.comment,@"comment",
            nil];
}
@end


@implementation CMUSISEvalSubject
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
    
        self.subjectCode = JSON_GET_OBJECT([dict objectForKey:@"subjectCode"]);
        self.subjectName = JSON_GET_OBJECT([dict objectForKey:@"subjectName"]);
        self.questType = JSON_GET_OBJECT([dict objectForKey:@"quest_type"]);
        self.year = JSON_GET_OBJECT([dict objectForKey:@"year"]);
        self.term = JSON_GET_OBJECT([dict objectForKey:@"term"]);
        self.sectionName = JSON_GET_OBJECT([dict objectForKey:@"section_Name"]);
        self.labName = JSON_GET_OBJECT([dict objectForKey:@"Lab_Name"]);
        
        NSNumber *sectionId = JSON_GET_OBJECT([dict objectForKey:@"section_id"]);
        if(sectionId)
        {
            self.sectionId = [sectionId intValue];
        }
        
        NSNumber *status = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(status)
        {
            self.status = [status boolValue];
        }
        
        NSNumber *isLockSubject = JSON_GET_OBJECT([dict objectForKey:@"isLock"]);
        if(isLockSubject)
        {
            self.isLockSubject = [isLockSubject boolValue];
        }
        
    }
    return self;
}
@end

@implementation CMUSISEvalTeacher
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        
        self.teacherName = JSON_GET_OBJECT([dict objectForKey:@"TeacherName"]);
        self.teacherId = JSON_GET_OBJECT([dict objectForKey:@"TeacherId"]);
        self.teacherImage = JSON_GET_OBJECT([dict objectForKey:@"TeacherImage"]);
        self.subjectCode = JSON_GET_OBJECT([dict objectForKey:@"subjectCode"]);
        self.subjectName = JSON_GET_OBJECT([dict objectForKey:@"subjectName"]);
        self.questType = JSON_GET_OBJECT([dict objectForKey:@"quest_type"]);
        self.year = JSON_GET_OBJECT([dict objectForKey:@"year"]);
        self.term = JSON_GET_OBJECT([dict objectForKey:@"term"]);
        NSNumber *sectionId = JSON_GET_OBJECT([dict objectForKey:@"section_id"]);
        if(sectionId)
        {
            self.sectionId = [sectionId intValue];
        }

        NSNumber *status = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(status)
        {
            self.status = [status intValue];
        }
        
        NSNumber *isLockTeacher = JSON_GET_OBJECT([dict objectForKey:@"isLock"]);
        if(isLockTeacher)
        {
            self.isLockTeacher = [isLockTeacher boolValue];
        }

    }
    return self;
}
@end

@implementation CMUSISFeedModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSArray *evaSubjectList = JSON_GET_OBJECT([dict objectForKey:@"EvaSubjectView"]);
        if(evaSubjectList)
        {
            self.evaSubjectList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in evaSubjectList)
            {
                CMUSISEvalSubject *evaSubject = [[CMUSISEvalSubject alloc] initWithDictionary:dict];
                [self.evaSubjectList addObject:evaSubject];
            }
        }
        
        NSArray *evaTeacherList = JSON_GET_OBJECT([dict objectForKey:@"EvaTeacherView"]);
        if(evaTeacherList)
        {
            self.evaTeacherList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in evaTeacherList)
            {
                CMUSISEvalTeacher *evaTeacher = [[CMUSISEvalTeacher alloc] initWithDictionary:dict];
                [self.evaTeacherList addObject:evaTeacher];
            }
        }
        
        NSNumber *numberSubject = JSON_GET_OBJECT([dict objectForKey:@"numberSubject"]);
        if(numberSubject)
        {
            self.numberSubject = [numberSubject intValue];
        }
        
        NSNumber *numberTeacher = JSON_GET_OBJECT([dict objectForKey:@"numberTeacher"]);
        if(numberTeacher)
        {
            self.numberTeacher = [numberTeacher intValue];
        }
        
        NSNumber *status = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(status)
        {
            self.status = [status boolValue];
        }
    }
    return self;
}
@end


//PCheck
@implementation CMUSISPCheckQuestion
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *questionId = JSON_GET_OBJECT([dict objectForKey:@"QuestionID"]);
        if(questionId)
        {
            self.questionId = [questionId intValue];
        }
        self.questionName = JSON_GET_OBJECT([dict objectForKey:@"QuestionName"]);
       
        NSNumber *check = JSON_GET_OBJECT([dict objectForKey:@"check"]);
        if(check)
        {
            self.check = [check boolValue];
        }
    }
    return self;
}

- (NSDictionary *)dictionary
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            JSON_PARSE_OBJECT(self.questionName),@"QuestionName",
            [NSNumber numberWithInt:self.questionId],@"QuestionID",
            nil];
}

@end

@implementation CMUSISPCheckModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.title = JSON_GET_OBJECT([dict objectForKey:@"Title"]);
        NSNumber *status = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(status)
        {
            self.status = [status boolValue];
        }
        self.link = JSON_GET_OBJECT([dict objectForKey:@"link"]);
        self.year = JSON_GET_OBJECT([dict objectForKey:@"year"]);
        self.term = JSON_GET_OBJECT([dict objectForKey:@"term"]);
        self.studentCode = JSON_GET_OBJECT([dict objectForKey:@"studentcode"]);
        self.token = JSON_GET_OBJECT([dict objectForKey:@"token"]);
        
        NSArray *questionViewList1 = JSON_GET_OBJECT([dict objectForKey:@"QuestionViewList1"]);
        if(questionViewList1)
        {
            self.questionViewList1 = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in questionViewList1)
            {
                CMUSISPCheckQuestion *pCheckQuestion = [[CMUSISPCheckQuestion alloc] initWithDictionary:dict];
                [self.questionViewList1 addObject:pCheckQuestion];
            }
        }
        
        NSArray *questionViewList2 = JSON_GET_OBJECT([dict objectForKey:@"QuestionViewList2"]);
        if(questionViewList2)
        {
            self.questionViewList2 = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in questionViewList2)
            {
                CMUSISPCheckQuestion *pCheckQuestion = [[CMUSISPCheckQuestion alloc] initWithDictionary:dict];
                [self.questionViewList2 addObject:pCheckQuestion];
            }
        }
    }
    return self;
}

- (NSDictionary *)dictionary
{
    NSMutableArray *questionViewList1 = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.questionViewList1.count; i++)
    {
        [questionViewList1 addObject:[[self.questionViewList1 objectAtIndex:i] dictionary]];
    }
    
    NSMutableArray *questionViewList2 = [[NSMutableArray alloc] init];
    for(int i = 0; i < self.questionViewList2.count; i++)
    {
        [questionViewList2 addObject:[[self.questionViewList2 objectAtIndex:i] dictionary]];
    }
    
    NSString *studentCode = self.studentCode;
//#ifdef DEBUG
//    studentCode = FAKE_STUDENT_CODE;
//#else
//    
//#endif
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithBool:self.status],@"status",
             JSON_PARSE_OBJECT(self.title),@"Title",
            JSON_PARSE_OBJECT(self.link),@"link",
            JSON_PARSE_OBJECT(self.year),@"year",
            JSON_PARSE_OBJECT(self.term),@"term",
            JSON_PARSE_OBJECT(studentCode),@"studentcode",
            JSON_PARSE_OBJECT(self.token),@"token",
            questionViewList1, @"QuestionViewList1",
            questionViewList2, @"QuestionViewList2",
            nil];
}

@end

@implementation CMUSISModel

@end
