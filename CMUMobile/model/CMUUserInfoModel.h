//
//  CMUUserInfoModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 10/14/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CMUUser : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong) NSString *personTypeTH;
@property(nonatomic, strong) NSString *personTypeEN;
@property(nonatomic, strong) NSString *prefixTH;
@property(nonatomic, strong) NSString *prefixEN;
@property(nonatomic, strong) NSString *firstNameTH;
@property(nonatomic, strong) NSString *firstNameEN;
@property(nonatomic, strong) NSString *lastNameTH;
@property(nonatomic, strong) NSString *lastNameEN;
@property(nonatomic, strong) NSString *citizenId;
@property(nonatomic, strong) NSString *image;

//employee
@property(nonatomic, strong) NSString *organizationCode;
@property(nonatomic, strong) NSString *organizationTH;
@property(nonatomic, strong) NSString *organizationEN;


//student
@property(nonatomic, strong) NSString *studentId;
@property(nonatomic, strong) NSString *level;
@property(nonatomic, strong) NSString *facultyCode;
@property(nonatomic, strong) NSString *facultyEN;
@property(nonatomic, strong) NSString *facultyTH;
@end

@interface CMUUserInfoModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) BOOL success;
@property(nonatomic, strong) CMUUser *user;
@end
