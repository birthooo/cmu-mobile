//
//  CMUOnlineModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    CMU_ONLINE_VIDEO_TYPE_RECENTLY_ADDED = 1,
    CMU_ONLINE_VIDEO_TYPE_MOST_POPULAR = 2,
    CMU_ONLINE_VIDEO_TYPE_ALSO_CHECK_OUT = 3,
    CMU_ONLINE_VIDEO_TYPE_ALL_VIDEO = 4
} CMU_ONLINE_VIDEOL_TYPE;


@interface CMUOnlineVideoType : NSObject
- (id)initWithType:(CMU_ONLINE_VIDEOL_TYPE) videoTypeId title:(NSString *)title;
+ (NSMutableArray *)allVideoType;
+ (CMUOnlineVideoType *)getByVideoTypeId:(CMU_ONLINE_VIDEOL_TYPE) videoTypeId;
@property(nonatomic, readonly) CMU_ONLINE_VIDEOL_TYPE videoTypeId;
@property(nonatomic, readonly) NSString *title;
@end


@interface CMUOnlineVideoFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) int vdoId;
@property(nonatomic, strong) NSString *vdoName;
@property(nonatomic, unsafe_unretained) int vdoview;
@property(nonatomic, strong) NSDate *vdoDate;
@property(nonatomic, strong) NSString *vdoPic;
@property(nonatomic, strong) NSString *vdoPath;
@property(nonatomic, strong) NSString *vdotime;
@property(nonatomic, unsafe_unretained) int vdolike;
@property(nonatomic, strong) NSString *vdoDesc;
@property(nonatomic, unsafe_unretained) int vdopub;
@property(nonatomic, strong) NSString *linkPage;
@end

@interface CMUOnlineVideoFeedModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong) NSMutableArray *videoFeedList;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@end


@interface CMUOnlineCuteAjarnFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) int cuteId;
@property(nonatomic, strong) NSString *cuteName;
@end


@interface CMUOnlineCuteAjarnModel : NSObject
//- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(id) array;
@property(nonatomic, strong) NSMutableArray *cuteAjarnList;
@end


@interface CMUOnlineCommentFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) int commentId;
@property(nonatomic, strong) NSString *commentText;
@property(nonatomic, unsafe_unretained) int vdoId;
@property(nonatomic, strong) NSString *accId;
@property(nonatomic, strong) NSDate *commentDatetime;
@end

@interface CMUOnlineCommentModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong) NSMutableArray *commentFeedList;
@property(nonatomic, unsafe_unretained) int numberComment;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@end


@interface CMUOnlineModel : NSObject

@end
