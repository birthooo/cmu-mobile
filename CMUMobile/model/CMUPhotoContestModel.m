//
//  CMUContestModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/20/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUPhotoContestModel.h"
#import "global.h"
#import "CMUFormatUtils.h"

@implementation CMUPhotoContestFeedDetail
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *photoContestFeedDetailId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(photoContestFeedDetailId)
        {
            self.photoContestFeedDetailId = [photoContestFeedDetailId intValue];
        }
        
        self.account = JSON_GET_OBJECT([dict objectForKey:@"Account"]);
        self.path = JSON_GET_OBJECT([dict objectForKey:@"Path"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        self.cropPath = JSON_GET_OBJECT([dict objectForKey:@"cropPath"]);
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        NSTimeInterval time = [JSON_GET_OBJECT([dict objectForKey:@"time"]) doubleValue];
        self.time = [NSDate dateWithTimeIntervalSince1970:time];
        
        NSNumber *nVote = JSON_GET_OBJECT([dict objectForKey:@"nVote"]);
        if(nVote)
        {
            self.nVote = [nVote intValue];
        }
        NSString *dtime= JSON_GET_OBJECT([dict objectForKey:@"dtime"]);
        self.dtime = [CMUFormatUtils parseWebServiceDate:dtime];
        
    }
    return self;
}
@end

@implementation CMUPhotoContestFeed
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *photoContestFeedId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(photoContestFeedId)
        {
            self.photoContestFeedId = [photoContestFeedId intValue];
        }
        
        self.account = JSON_GET_OBJECT([dict objectForKey:@"Account"]);
        self.path = JSON_GET_OBJECT([dict objectForKey:@"Path"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        self.cropPath = JSON_GET_OBJECT([dict objectForKey:@"cropPath"]);
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        NSTimeInterval time = [JSON_GET_OBJECT([dict objectForKey:@"time"]) doubleValue];
        self.time = [NSDate dateWithTimeIntervalSince1970:time];
        
        NSNumber *nVote = JSON_GET_OBJECT([dict objectForKey:@"nVote"]);
        if(nVote)
        {
            self.nVote = [nVote intValue];
        }
        NSString *dtime= JSON_GET_OBJECT([dict objectForKey:@"dtime"]);
        self.dtime = [CMUFormatUtils parseWebServiceDate:dtime];
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUPhotoContestFeed class]])
    {
        return self.photoContestFeedId == [other photoContestFeedId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.photoContestFeedId;
}
@end

@implementation CMUPhotoContestModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *newsFeeds = JSON_GET_OBJECT([dict objectForKey:@"PhotocontestViewList"]);
        if(newsFeeds)
        {
            self.photoFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in newsFeeds)
            {
                CMUPhotoContestFeed *photoContestFeed = [[CMUPhotoContestFeed alloc] initWithDictionary:dict];
                [self.photoFeedList addObject:photoContestFeed];
            }
        }
        
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end
