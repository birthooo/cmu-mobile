//
//  CMUMapFeedModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 9/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUMapFeedModel.h"
#import "global.h"

@implementation CMUMapVersionModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *versionNumber = JSON_GET_OBJECT([dict objectForKey:@"version"]);
        if(versionNumber)
        {
            self.version = [versionNumber intValue];
        }
        self.versionURL = JSON_GET_OBJECT([dict objectForKey:@"versionURL"]);
    }
    return self;
}
- (NSDictionary *)toDictionary
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithInt:self.version] forKey:@"version"];
    [dict setObject:self.versionURL forKey:@"versionURL"];
    return dict;
}
@end

//CMUMapFeed
@implementation CMUMapFeed
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *mapFeedId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(mapFeedId)
        {
            self.mapFeedId = [mapFeedId intValue];
        }
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        
        NSNumber *lat = JSON_GET_OBJECT([dict objectForKey:@"x"]);
        if(lat)
        {
            self.lat = [lat doubleValue];
        }
        
        NSNumber *lng = JSON_GET_OBJECT([dict objectForKey:@"y"]);
        if(lng)
        {
            self.lng = [lng doubleValue];
        }
        
        NSNumber *zoneId = JSON_GET_OBJECT([dict objectForKey:@"ZoneID"]);
        if(zoneId)
        {
            self.zoneId = [zoneId intValue];
        }
        
        
        NSArray *mapFeedList = JSON_GET_OBJECT([dict objectForKey:@"MapFeed"]);
        if(mapFeedList)
        {
            self.mapFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in mapFeedList)
            {
                CMUMapFeed *mapFeed = [[CMUMapFeed alloc] initWithDictionary:dict];
                [self.mapFeedList addObject:mapFeed];
            }
        }

    }
    return self;
}

- (id)initWithJumboMapDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"name"]);
        
        NSNumber *lat = JSON_GET_OBJECT([dict objectForKey:@"lat"]);
        if(lat)
        {
            self.lat = [lat doubleValue];
        }
        
        NSNumber *lng = JSON_GET_OBJECT([dict objectForKey:@"lng"]);
        if(lng)
        {
            self.lng = [lng doubleValue];
        }
        
        NSNumber *zoneId = JSON_GET_OBJECT([dict objectForKey:@"ZoneID"]);
        if(zoneId)
        {
            self.zoneId = [zoneId intValue];
        }
        
        NSString *statusStr = JSON_GET_OBJECT([dict objectForKey:@"status"]);
        if(statusStr)
        {
            if([statusStr isEqualToString:@"DW"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_DOWN;
            }
            else if([statusStr isEqualToString:@"FA"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_IDLE;
            }
            else if([statusStr isEqualToString:@"LO"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_LOW;
            }
            else if([statusStr isEqualToString:@"HI"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_HIGH;
            }
            else if([statusStr isEqualToString:@"VH"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_VERY_HIGH;
            }
            else if([statusStr isEqualToString:@"MT"])
            {
                self.status = CMU_JUMBO_MAP_STATUS_MAINTENANCE;
            }
        }
        
        NSString *totalStr = JSON_GET_OBJECT([dict objectForKey:@"total"]);
        @try {
            self.total = [totalStr intValue];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    }
    return self;
}

- (id)initWithBusTypeDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        self.name = JSON_GET_OBJECT([dict objectForKey:@"BusTypeName"]);
        self.busTypeName = self.name;
        
        NSNumber *busTypeID = JSON_GET_OBJECT([dict objectForKey:@"BusTypeID"]);
        if(busTypeID)
        {
            self.busTypeID = [busTypeID intValue];
            self.mapFeedId = 100000+ self.busTypeID;//prevent busTypeId to duplicate with another mapfeed
        }
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUMapFeed class]])
    {
        return self.mapFeedId == [other mapFeedId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.mapFeedId;
}

@end

@implementation CMUMapBusPoint
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *busPointId = JSON_GET_OBJECT([dict objectForKey:@"id"]);
        if(busPointId)
        {
            self.busPointId = [busPointId intValue];
        }
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"name"]);
        
        NSNumber *lat = JSON_GET_OBJECT([dict objectForKey:@"x"]);
        if(lat)
        {
            self.lat = [lat doubleValue];
        }
        
        NSNumber *lng = JSON_GET_OBJECT([dict objectForKey:@"y"]);
        if(lng)
        {
            self.lng = [lng doubleValue];
        }
        
    }
    return self;
}

@end

//CMUMapBusType
@implementation CMUMapBusTypeModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        self.colorCode = JSON_GET_OBJECT([dict objectForKey:@"colorcode"]);
        
        NSArray *mapBusPointViewList = JSON_GET_OBJECT([dict objectForKey:@"mapBusPointViewList"]);
        if(mapBusPointViewList)
        {
            self.mapBusPointViewList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in mapBusPointViewList)
            {
                CMUMapBusPoint *point = [[CMUMapBusPoint alloc] initWithDictionary:dict];
                [self.mapBusPointViewList addObject:point];
            }
        }
        
        NSArray *mapViewList = JSON_GET_OBJECT([dict objectForKey:@"mapViewList"]);
        if(mapViewList)
        {
            self.mapViewList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in mapViewList)
            {
                CMUMapFeed *mapFeed = [[CMUMapFeed alloc] initWithDictionary:dict];
                [self.mapViewList addObject:mapFeed];
            }
        }
        
    }
    return self;
}
@end

//CMUMapFeedModel
@implementation CMUMapFeedModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *mapFeedList = JSON_GET_OBJECT([dict objectForKey:@"MapFeed"]);
        if(mapFeedList)
        {
            self.mapFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in mapFeedList)
            {
                CMUMapFeed *mapFeed = [[CMUMapFeed alloc] initWithDictionary:dict];
                [self.mapFeedList addObject:mapFeed];
            }
        }
    }
    return self;
}

- (CMUMapFeed *)getByMapType:(CMU_MAP_GROUP) mapType
{
    CMUMapFeed *temp = [[CMUMapFeed alloc] init];
    temp.mapFeedId = mapType;
    NSUInteger index = [self.mapFeedList indexOfObject:temp];
    if(index != NSNotFound)
    {
        return [self.mapFeedList objectAtIndex:index];
    }
    return nil;
}
@end
