//
//  CmuEventdetailFeedModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/13/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    CMU_EVENT_TYPE_HIGHLIGHT = 0,
    CMU_EVENT_TYPE_HOLIDAY = 1,
    CMU_EVENT_TYPE_ACTIVITY_GENERAL = 2,
    CMU_EVENT_TYPE_ACTIVITY_INSTITUTION = 3,
    CMU_EVENT_TYPE_ACTIVITY_STUDENT = 4,
    
    //  Created by Satianpong Yodnin on 9/9/2558 BE.
    CMU_EVENT_TYPE_ACTIVITY_ITSC = 1,
    
} CMU_EVENT_TYPE;

@interface CMUEventType : NSObject
- (id)initWithType:(CMU_EVENT_TYPE) eventTypeId title:(NSString *)title;
+ (NSMutableArray *)allEventType;
+ (CMUEventType *)getByEventId:(CMU_EVENT_TYPE) eventTypeId;
@property(nonatomic, readonly) CMU_EVENT_TYPE eventTypeId;
@property(nonatomic, readonly) NSString *title;
@end


@interface CMUEventDetailFeed : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, assign) int eventdetailFeedId;
@property(nonatomic, strong) NSString *dayOfWeek;
@property(nonatomic, strong) NSString *numOfDay;
@property(nonatomic, strong) NSString *topic;
@property(nonatomic, strong) NSDate *startDate;
@property(nonatomic, strong) NSDate *endtDate;
@property(nonatomic, strong) NSString *linkURL;
@end

//display on ui
//@interface CmuDateEvent : NSObject
//@property(nonatomic, assign) int dateEventId;
//@property(nonatomic, strong) NSDate *date;
//@property(nonatomic, strong) NSString *url;
//@end

@interface CMUEventDetailFeedModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong) NSMutableArray *eventdetailFeedList;
@end
