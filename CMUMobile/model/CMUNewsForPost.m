//
//  CMUNewsForPost.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsForPost.h"
#import "CMUNewsCategory.h"
#import "global.h"

@implementation CMUNewsForPost
- (id)initFromDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.hotNews = [JSON_GET_OBJECT([dict valueForKey:@"hotNews"]) isEqualToString:@"1"]? YES:NO;
        
        self.jobNews = [JSON_GET_OBJECT([dict valueForKey:@"jobNews"]) isEqualToString:@"1"]? YES:NO;
        self.orderNews = [JSON_GET_OBJECT([dict valueForKey:@"orderNews"]) isEqualToString:@"1"]? YES:NO;
        self.MISNews = [JSON_GET_OBJECT([dict valueForKey:@"MISNews"]) isEqualToString:@"1"]? YES:NO;
        self.CNOCNews = [JSON_GET_OBJECT([dict valueForKey:@"CNOCNews"]) isEqualToString:@"1"]? YES:NO;
        self.regNews = [JSON_GET_OBJECT([dict valueForKey:@"regNews"]) isEqualToString:@"1"]? YES:NO;
        self.laguageNews = [JSON_GET_OBJECT([dict valueForKey:@"laguageNews"]) isEqualToString:@"1"]? YES:NO;
        self.cooperativeNews = [JSON_GET_OBJECT([dict valueForKey:@"cooperativeNews"]) isEqualToString:@"1"]? YES:NO;
        self.libraryNews = [JSON_GET_OBJECT([dict valueForKey:@"libraryNews"]) isEqualToString:@"1"]? YES:NO;
        self.academicNews = [JSON_GET_OBJECT([dict valueForKey:@"academicNews"]) isEqualToString:@"1"]? YES:NO;
        self.activityNews = [JSON_GET_OBJECT([dict valueForKey:@"activityNews"]) isEqualToString:@"1"]? YES:NO;
        self.activityStudentNews = [JSON_GET_OBJECT([dict valueForKey:@"activityStudentNews"]) isEqualToString:@"1"]? YES:NO;
        self.directorNews = [JSON_GET_OBJECT([dict valueForKey:@"directorNews"]) isEqualToString:@"1"]? YES:NO;
        self.collegeNews = [JSON_GET_OBJECT([dict valueForKey:@"collegeNews"]) isEqualToString:@"1"]? YES:NO;
        self.dentNews = [JSON_GET_OBJECT([dict valueForKey:@"dentNews"]) isEqualToString:@"1"]? YES:NO;
        self.vithedNews = [JSON_GET_OBJECT([dict valueForKey:@"vithedNews"]) isEqualToString:@"1"]? YES:NO;
         
    }
    return self;
}

- (NSDictionary *)toDictionary;
{
    NSDictionary *mappingDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          self.hotNews?@"1":@"0",@"hotNews",
                          self.jobNews?@"1":@"0",@"jobNews",
                          self.orderNews?@"1":@"0",@"orderNews",
                          self.MISNews?@"1":@"0",@"MISNews",
                          self.CNOCNews?@"1":@"0",@"CNOCNews",
                          self.regNews?@"1":@"0",@"regNews",
                          self.laguageNews?@"1":@"0",@"laguageNews",
                          self.libraryNews?@"1":@"0",@"libraryNews",
                          self.cooperativeNews?@"1":@"0",@"cooperativeNews",
                          self.academicNews?@"1":@"0",@"academicNews",
                          self.activityNews?@"1":@"0",@"activityNews",
                          self.activityStudentNews?@"1":@"0",@"activityStudentNews",
                          self.directorNews?@"1":@"0",@"directorNews",
                          self.collegeNews?@"1":@"0",@"collegeNews",
                          self.dentNews?@"1":@"0",@"dentNews",
                          self.vithedNews?@"1":@"0",@"vithedNews",
                          nil];
    return mappingDict;
}

- (NSString *)toWebServiceParams
{
    NSMutableArray *paramsArray = [[NSMutableArray alloc] init];
    if(self.hotNews)
       [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_HOT]];
    if(self.jobNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_JOB]];
    if(self.orderNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_ORDER]];
    if(self.MISNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_MIS]];
    if(self.CNOCNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_CNOC]];
    if(self.regNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_REG]];
    if(self.laguageNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_LANGUAGE]];
    if(self.libraryNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_LIBRARY]];
    if(self.cooperativeNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_COOPERATIVE]];
    //if(self.academicNews)
    //    [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_ACADEMIC]];
    if(self.activityNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_ACTIVITY]];
    if(self.activityStudentNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_ACTIVITY_STUDENT]];
    if(self.directorNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_DIRECTOR]];
    if(self.collegeNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_COLLEGE]];
    if(self.dentNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_DENT]];
    if(self.vithedNews)
        [paramsArray addObject:[NSString stringWithFormat:@"%i",CMU_NEWS_CATEGORY_VITHED]];
    return [paramsArray componentsJoinedByString:@","];
    
}

@end
