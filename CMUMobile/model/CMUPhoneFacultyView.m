//
//  CMUPhoneFacultyView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneFacultyView.h"
#import "global.h"

@implementation CMUPhoneFacultyView
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *phoneFacultyViewId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(phoneFacultyViewId)
        {
            self.phoneFacultyViewId = [phoneFacultyViewId intValue];
        }
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        self.phone = JSON_GET_OBJECT([dict objectForKey:@"Phone"]);
        self.fax = JSON_GET_OBJECT([dict objectForKey:@"Fax"]);
        self.email = JSON_GET_OBJECT([dict objectForKey:@"Email"]);
        self.fanpage = JSON_GET_OBJECT([dict objectForKey:@"Fanpage"]);
        self.web = JSON_GET_OBJECT([dict objectForKey:@"Web"]);
        self.phoneNumber = JSON_GET_OBJECT([dict objectForKey:@"PhoneNumber"]);
        
        NSArray *phoneSubViewList = JSON_GET_OBJECT([dict objectForKey:@"PhoneSubView"]);
        if(phoneSubViewList)
        {
            self.phoneSubViewList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in phoneSubViewList)
            {
                CMUPhoneFacultyView *newsFeed = [[CMUPhoneFacultyView alloc] initWithDictionary:dict];
                [self.phoneSubViewList addObject:newsFeed];
            }
        }
        
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUPhoneFacultyView class]])
    {
        return self.phoneFacultyViewId == [other phoneFacultyViewId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.phoneFacultyViewId;
}

@end
