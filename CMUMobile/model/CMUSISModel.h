//
//  CMUSISModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/17/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CMUSISEvalForm:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString *subjectCode;
@property(nonatomic, strong)NSString *questionId;
@property(nonatomic, strong)NSString *question;
@property(nonatomic, strong)NSString *questionLevel;
@property(nonatomic, unsafe_unretained)int questionType;
@property(nonatomic, unsafe_unretained)int score;
- (NSDictionary *)dictionary;
@end

@interface CMUSISEvalFormModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)BOOL status;
@property(nonatomic, strong)NSMutableArray *evaFormList;
@property(nonatomic, strong)NSString *comment;
- (NSDictionary *)dictionary;
@end


@interface CMUSISEvalSubject:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString *subjectCode;
@property(nonatomic, strong)NSString *subjectName;
@property(nonatomic, unsafe_unretained)BOOL status;
@property(nonatomic, unsafe_unretained)int sectionId;
@property(nonatomic, strong)NSString *sectionName;
@property(nonatomic, strong)NSString *labName;
@property(nonatomic, strong)NSString *questType;
@property(nonatomic, strong)NSString *year;
@property(nonatomic, strong)NSString *term;
@property(nonatomic, strong)NSDate *evaDate;
@property(nonatomic, unsafe_unretained)BOOL isLockSubject;
@end

@interface CMUSISEvalTeacher:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString *teacherName;
@property(nonatomic, strong)NSString *teacherId;
@property(nonatomic, strong)NSString *teacherImage;
@property(nonatomic, strong)NSString *subjectCode;
@property(nonatomic, strong)NSString *subjectName;
@property(nonatomic, unsafe_unretained)int sectionId;
@property(nonatomic, strong)NSString *questType;
@property(nonatomic, unsafe_unretained)BOOL status;
@property(nonatomic, strong)NSString *year;
@property(nonatomic, strong)NSString *term;
@property(nonatomic, strong)NSDate *evaDate;
@property(nonatomic, unsafe_unretained)BOOL isLockTeacher;
@end

@interface CMUSISFeedModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int numberSubject;
@property(nonatomic, unsafe_unretained)int numberTeacher;
@property(nonatomic, strong)NSMutableArray *evaSubjectList;
@property(nonatomic, strong)NSMutableArray *evaTeacherList;
@property(nonatomic, unsafe_unretained)BOOL status;
@end


//PCheck
@interface CMUSISPCheckQuestion:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int questionId;
@property(nonatomic, strong)NSString *questionName;
@property(nonatomic, unsafe_unretained)BOOL check;
- (NSDictionary *)dictionary;
@end

@interface CMUSISPCheckModel:NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString * title;
@property(nonatomic, unsafe_unretained)BOOL status;
@property(nonatomic, strong)NSString * link;
@property(nonatomic, strong)NSString * year;
@property(nonatomic, strong)NSString * term;
@property(nonatomic, strong)NSString * studentCode;
@property(nonatomic, strong)NSString * token;
@property(nonatomic, strong)NSMutableArray *questionViewList1;
@property(nonatomic, strong)NSMutableArray *questionViewList2;
- (NSDictionary *)dictionary;
@end

@interface CMUSISModel : NSObject

@end
