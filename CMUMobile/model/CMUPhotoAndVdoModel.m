//
//  CMUPhotoAndVdoModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/6/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhotoAndVdoModel.h"
#import "global.h"
#import "CMUFormatUtils.h"

@interface CMUPhotoView()
@property(nonatomic, unsafe_unretained) int photoViewId;
@property(nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSString* path;
@property(nonatomic, strong) NSString* detail;
@property(nonatomic, strong) NSString* cropPath;
@property(nonatomic, strong) NSDate* update;
@end

@implementation CMUPhotoView
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *photoViewId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(photoViewId)
        {
            self.photoViewId = [photoViewId intValue];
        }
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        self.path = JSON_GET_OBJECT([dict objectForKey:@"Path"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        self.cropPath = JSON_GET_OBJECT([dict objectForKey:@"cropPath"]);
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];

    }
    return self;
}
@end


@interface CMUVideoView()
@property(nonatomic, unsafe_unretained) int photoViewId;
@property(nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSString* link;
@property(nonatomic, strong) NSString* detail;
@property(nonatomic, strong) NSString* cropPath;
@property(nonatomic, strong) NSDate* update;
@end

@implementation CMUVideoView
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *photoViewId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(photoViewId)
        {
            self.photoViewId = [photoViewId intValue];
        }
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        self.link = JSON_GET_OBJECT([dict objectForKey:@"Link"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        self.cropPath = JSON_GET_OBJECT([dict objectForKey:@"cropPath"]);
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        
    }
    return self;
}
@end


@interface CMUPhotoAndVideoModel()
@property(nonatomic, strong) NSMutableArray *photoFeedList;
@property(nonatomic, strong) NSMutableArray *videoFeedList;
@end

@implementation CMUPhotoAndVideoModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *photoFeedList = JSON_GET_OBJECT([dict objectForKey:@"PhotoFeed"]);
        if(photoFeedList)
        {
            self.photoFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in photoFeedList)
            {
                CMUPhotoView *photoView = [[CMUPhotoView alloc] initWithDictionary:dict];
                [self.photoFeedList addObject:photoView];
            }
        }
        
        NSArray *videoFeedList = JSON_GET_OBJECT([dict objectForKey:@"vdoFeed"]);
        if(videoFeedList)
        {
            self.videoFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in videoFeedList)
            {
                CMUVideoView *videoView = [[CMUVideoView alloc] initWithDictionary:dict];
                [self.videoFeedList addObject:videoView];
            }
        }

    }
    return self;
}

@end
