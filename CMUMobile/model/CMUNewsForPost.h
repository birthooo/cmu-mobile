//
//  CMUNewsForPost.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUNewsForPost : NSObject
@property(nonatomic, unsafe_unretained)BOOL hotNews;
@property(nonatomic, unsafe_unretained)BOOL jobNews;
@property(nonatomic, unsafe_unretained)BOOL orderNews;
@property(nonatomic, unsafe_unretained)BOOL MISNews;
@property(nonatomic, unsafe_unretained)BOOL CNOCNews;
@property(nonatomic, unsafe_unretained)BOOL regNews;
@property(nonatomic, unsafe_unretained)BOOL laguageNews;
@property(nonatomic, unsafe_unretained)BOOL cooperativeNews;
@property(nonatomic, unsafe_unretained)BOOL libraryNews;
@property(nonatomic, unsafe_unretained)BOOL academicNews;
@property(nonatomic, unsafe_unretained)BOOL activityNews;
@property(nonatomic, unsafe_unretained)BOOL activityStudentNews;
@property(nonatomic, unsafe_unretained)BOOL directorNews;
@property(nonatomic, unsafe_unretained)BOOL collegeNews;
@property(nonatomic, unsafe_unretained)BOOL dentNews;
@property(nonatomic, unsafe_unretained)BOOL vithedNews;
- (id)initFromDictionary:(NSDictionary *)dict;
- (NSDictionary *)toDictionary;
- (NSString *)toWebServiceParams;
@end
