//
//  CMUFaculty.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/29/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUFaculty : NSObject
- (id)initWithFacultyId:(NSString *) facultyId nameThai:(NSString *)nameThai nameEng:(NSString *)nameEng;
@property(nonatomic, strong) NSString *facultyId;
@property(nonatomic, strong) NSString *nameThai;
@property(nonatomic, strong) NSString *nameEng;

+ (NSArray *)getAllFaculty;
@end
