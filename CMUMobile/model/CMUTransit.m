//
//  CMUTransit.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/2/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import "CMUTransit.h"
#import "global.h"

@implementation CMUTransitBusPosition

- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        self.no = JSON_GET_OBJECT([dict objectForKey:@"No"]);
        
        NSString *direction = JSON_GET_OBJECT([dict objectForKey:@"direction"]);
        if(direction)
        {
            self.direction = [direction intValue];
        }
        
        NSString *coordinate = JSON_GET_OBJECT([dict objectForKey:@"latlng"]);
        if(coordinate)
        {
            NSArray* myArray = [coordinate  componentsSeparatedByString:@","];
            NSString* lat = [myArray objectAtIndex:0];
            NSString* lng = [myArray objectAtIndex:1];
            self.coordinate =  CLLocationCoordinate2DMake([lat doubleValue], [lng doubleValue]);
        }
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUTransitBusPosition class]])
    {
        return [self.no isEqualToString:[other no]];
    }
    return NO;
}

- (NSUInteger)hash
{
    return [self.no hash];
}
@end

@implementation CMUTransitBusPositionModel
- (id)initWithArray:(NSArray *) array
{
    self = [super init];
    if(self)
    {
        self.busPositions = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in array)
        {
            CMUTransitBusPosition *busPosition = [[CMUTransitBusPosition alloc] initWithDictionary:dict];
            [self.busPositions addObject:busPosition];
        }
    }
    return self;
}
@end


@implementation CMUTransitBusStation
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        
        self.no = JSON_GET_OBJECT([dict objectForKey:@"No"]);
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        
        NSString *coordinate = JSON_GET_OBJECT([dict objectForKey:@"latlng"]);
        if(coordinate)
        {
            NSArray* myArray = [coordinate  componentsSeparatedByString:@","];
            NSString* lat = [myArray objectAtIndex:0];
            NSString* lng = [myArray objectAtIndex:1];
            self.coordinate =  CLLocationCoordinate2DMake([lat doubleValue], [lng doubleValue]);
        }
    }
    return self;
}



@end

@implementation  CMUTransitBusStationModel
- (id)initWithArray:(NSArray *) array 
{
    self = [super init];
    if(self)
    {
        self.busStations = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in array)
        {
            CMUTransitBusStation *busStation = [[CMUTransitBusStation alloc] initWithDictionary:dict];
            [self.busStations addObject:busStation];
        
        }
    }
    return self;
}

-(id)initWithBackGroundArray:(NSArray*)array
{
    self = [super init];
    if(self)
    {
        self.AllBusStations = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in array)
        {
            CMUTransitBusStation *busStation = [[CMUTransitBusStation alloc] initWithDictionary:dict];
            [self.AllBusStations addObject:busStation];
            
        }
    }
    return self;
}


@end

@interface CMUTransitCoordinate()
@property(nonatomic, unsafe_unretained) CLLocationCoordinate2D coordinate;
@end
@implementation CMUTransitCoordinate

- (id)initWithLat:(CLLocationDegrees)lat lng:(CLLocationDegrees) lng
{
    self = [super init];
    if(self)
    {
        self.coordinate = CLLocationCoordinate2DMake(lat, lng);
    }
    return self;
}

@end

@implementation CMUTransit
+(NSArray *)busGreenLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.805253268182916 lng:98.9547049999237],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805964195617467 lng:98.9534068107605],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80780243691488 lng:98.9545226097107],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807152453337167 lng:98.95574569702148],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8054970150703 lng:98.95547747612],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805222799797164 lng:98.95471572875977],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804379838936764 lng:98.95397543907166],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803861872939912 lng:98.95384669303894],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803739998356 lng:98.9533531665802],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804237652351304 lng:98.9523446559906],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802572029117105 lng:98.95146489143372],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801830618350564 lng:98.95126104354858],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80053060254631 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799768869870178 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799393080480037 lng:98.95199060440063],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79944386287909 lng:98.95333170890808],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796478145094976 lng:98.95332098007202],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796376578492055 lng:98.95407199859619],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79402021610297 lng:98.95438313484192],
                              nil];
    return lines;
}

+(NSArray *)busOrangeLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.799596209984312 lng:98.94757032394409],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799555584103064 lng:98.94913136959076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799459097595815 lng:98.94947469234467],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799108698762225 lng:98.94981801509857],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95034372806549],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796996133816553 lng:98.9503276348114],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79652892837344 lng:98.95317077636719],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796508615063892 lng:98.95347654819489],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796396891817558 lng:98.95408272743225],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795934763056383 lng:98.95723164081573],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795883979598674 lng:98.95927548408508],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796178523440144 lng:98.95928084850311],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796173445102415 lng:98.96018743515015],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796203915126494 lng:98.96067023277283],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798732907898124 lng:98.9604502916336],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79887002087819 lng:98.96033227443695],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798692281808513 lng:98.95798802375793],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798682125284575 lng:98.95753741264343],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79920518547033 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79950987997496 lng:98.95730137825012],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799565740574295 lng:98.9570277929306],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79958605351491 lng:98.9554613828659],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799555584103064 lng:98.95431876182556],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95325660705566],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79944386287909 lng:98.95193159580231],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79892080343505 lng:98.95142734050751],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798829394821666 lng:98.9509391784668],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95032227039337],
                             nil];
    return lines;
}

+(NSArray *)busRedLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95328342914581],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799428628160992 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79975871341121 lng:98.95194232463837],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800383334497752 lng:98.95140051841736],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800586462806866 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801338035418883 lng:98.95122349262238],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801891556345 lng:98.95125031471252],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802592341694886 lng:98.95148634910583],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803003670867163 lng:98.95069777965546],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8033032803721 lng:98.94961953163147],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8035317957376 lng:98.9492654800415],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804059918950507 lng:98.9490669965744],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80424273044571 lng:98.94915282726288],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80468960215338 lng:98.94913136959076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804872412964457 lng:98.94934058189392],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805182175274243 lng:98.95017743110657],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8050552235769 lng:98.95046710968018],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80426812091546 lng:98.95231783390045],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80383140430221 lng:98.9531546831131],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803765388901603 lng:98.95343363285065],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803912653990498 lng:98.95384669303894],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803699373475077 lng:98.95409345626831],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80345562398358 lng:98.95423829555511],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802764998506913 lng:98.95434021949768],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802343512448264 lng:98.95440459251404],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80209468283373 lng:98.95454406738281],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801779836671688 lng:98.95476937294006],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80129233177481 lng:98.95460844039917],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801139986204934 lng:98.95453333854675],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801089204317645 lng:98.95440459251404],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801089204317645 lng:98.95399689674377],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800967327725672 lng:98.95364820957184],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800022781145284 lng:98.95331561565399],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799504801737722 lng:98.95328342914581],
                             nil];
    return lines;
}

+(NSArray *)busBlueLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.798184454860955 lng:98.94833207130432],
                             [[CMUTransitCoordinate alloc] initWithLat:18.797686782965883 lng:98.94835352897644],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796985977190275 lng:98.9503276348114],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796498458408188 lng:98.95332098007202],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799474332311167 lng:98.95331025123596],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79942354992131 lng:98.95195841789246],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79981965215584 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800591541011457 lng:98.95135760307312],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801871243682644 lng:98.95125031471252],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802632966843067 lng:98.95150780677795],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80318140538361 lng:98.95014524459839],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803364217833245 lng:98.94950151443481],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803536873853314 lng:98.94925475120544],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804064997050283 lng:98.94907236099243],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804471244535414 lng:98.94853591918945],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80346578021945 lng:98.94818186759949],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802805623613427 lng:98.94833207130432],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802612654270195 lng:98.9484715461731],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80213530810203 lng:98.94860029220581],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801911869004904 lng:98.94854664802551],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801718898636807 lng:98.94861102104187],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801353269964153 lng:98.94911527633667],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801139986204934 lng:98.94972681999207],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801180611703714 lng:98.9500379562378],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800520446133298 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80008371979429 lng:98.95164728164673],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79973840049143 lng:98.9519476890564],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799413393441505 lng:98.95196914672852],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798875099134563 lng:98.9514434337616],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79877353397794 lng:98.95033836364746],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796975820563375 lng:98.9503276348114],
                             nil];
    return lines;
}

+(NSArray *)busVioletLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.801200924449425 lng:98.95712435245514],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80140912995166 lng:98.95653426647186],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800860685635282 lng:98.95603001117706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800342708806514 lng:98.9558207988739],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79958097527999 lng:98.95548820495605],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799570818809674 lng:98.95430266857147],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95326733589172],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796493380080115 lng:98.95330488681793],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796391813486405 lng:98.9540559053421],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79338541456159 lng:98.9545065164566],
                             [[CMUTransitCoordinate alloc] initWithLat:18.793558080817817 lng:98.95584762096405],
                             [[CMUTransitCoordinate alloc] initWithLat:18.793456512453236 lng:98.95604074001312],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79280647346909 lng:98.95608365535736],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79265919866305 lng:98.95618557929993],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792450982338135 lng:98.95696341991425],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792984218753112 lng:98.95690977573395],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792999454054435 lng:98.95716190338135],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792496688382727 lng:98.95715653896332],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79239511937789 lng:98.95721554756165],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792324021038038 lng:98.95740330219269],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792191981184402 lng:98.95807921886444],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792176745810004 lng:98.9585030078888],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792242765755752 lng:98.95882487297058],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795868744558373 lng:98.95887315273285],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795955076435163 lng:98.95723700523376],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79797624536959 lng:98.95730674266815],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798118437245243 lng:98.95737111568451],
                             [[CMUTransitCoordinate alloc] initWithLat:18.7983368031061 lng:98.95752131938934],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79868720354662 lng:98.95755350589752],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79920518547033 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799479410549306 lng:98.95732283592224],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799550505867213 lng:98.95709216594696],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799591131749693 lng:98.95550966262817],
                             nil];
    return lines;
}

+(NSArray *)busSixLine
{
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             [[CMUTransitCoordinate alloc] initWithLat:18.805273580437017 lng:98.95472645759583],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805943883446734 lng:98.95343899726868],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807807514901697 lng:98.9545226097107],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807157531343613 lng:98.95574569702148],
                             [[CMUTransitCoordinate alloc] initWithLat:18.806192707369185 lng:98.95564377307892],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8054970150703 lng:98.95545601844788],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805268502373718 lng:98.9547049999237],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804384917026884 lng:98.95399153232574],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803907575886132 lng:98.95381450653076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803567342544348 lng:98.9541631937027],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803272811633256 lng:98.95428657531738],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802764998506913 lng:98.95433485507965],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802307965383 lng:98.9544153213501],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801779836671688 lng:98.95476400852203],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80155131892712 lng:98.95466208457947],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80121108082137 lng:98.95456552505493],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8011298298287 lng:98.95453333854675],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801094282507073 lng:98.9544153213501],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801094282507073 lng:98.95402371883392],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801007953266144 lng:98.9536964893341],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800926702175396 lng:98.9536052942276],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80021575345816 lng:98.9533531665802],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800027859366875 lng:98.9532995223999],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500347 lng:98.95328879356384],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799575897044914 lng:98.95540237426758],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799565740574295 lng:98.95700633525848],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799484488787296 lng:98.95732820034027],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799220420208673 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79868720354662 lng:98.95755887031555],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798677047022373 lng:98.95799338817596],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95948469638824],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79882939482167 lng:98.95983874797821],
                             nil];
    return lines;
}
+(NSArray *)busAllLine{
    NSArray *lines;
    /*
    NSMutableArray *lines = [[NSMutableArray alloc] initWithObjects:
                             
                             [[CMUTransitCoordinate alloc] initWithLat:18.805273580437017 lng:98.95472645759583],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805943883446734 lng:98.95343899726868],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807807514901697 lng:98.9545226097107],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807157531343613 lng:98.95574569702148],
                             [[CMUTransitCoordinate alloc] initWithLat:18.806192707369185 lng:98.95564377307892],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8054970150703 lng:98.95545601844788],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805268502373718 lng:98.9547049999237],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804384917026884 lng:98.95399153232574],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803907575886132 lng:98.95381450653076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803567342544348 lng:98.9541631937027],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803272811633256 lng:98.95428657531738],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802764998506913 lng:98.95433485507965],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802307965383 lng:98.9544153213501],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801779836671688 lng:98.95476400852203],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80155131892712 lng:98.95466208457947],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80121108082137 lng:98.95456552505493],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8011298298287 lng:98.95453333854675],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801094282507073 lng:98.9544153213501],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801094282507073 lng:98.95402371883392],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801007953266144 lng:98.9536964893341],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800926702175396 lng:98.9536052942276],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80021575345816 lng:98.9533531665802],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800027859366875 lng:98.9532995223999],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500347 lng:98.95328879356384],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799575897044914 lng:98.95540237426758],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799565740574295 lng:98.95700633525848],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799484488787296 lng:98.95732820034027],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799220420208673 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79868720354662 lng:98.95755887031555],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798677047022373 lng:98.95799338817596],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95948469638824],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79882939482167 lng:98.95983874797821],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801200924449425 lng:98.95712435245514],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80140912995166 lng:98.95653426647186],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800860685635282 lng:98.95603001117706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800342708806514 lng:98.9558207988739],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79958097527999 lng:98.95548820495605],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799570818809674 lng:98.95430266857147],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95326733589172],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796493380080115 lng:98.95330488681793],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796391813486405 lng:98.9540559053421],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79338541456159 lng:98.9545065164566],
                             [[CMUTransitCoordinate alloc] initWithLat:18.793558080817817 lng:98.95584762096405],
                             [[CMUTransitCoordinate alloc] initWithLat:18.793456512453236 lng:98.95604074001312],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79280647346909 lng:98.95608365535736],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79265919866305 lng:98.95618557929993],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792450982338135 lng:98.95696341991425],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792984218753112 lng:98.95690977573395],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792999454054435 lng:98.95716190338135],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792496688382727 lng:98.95715653896332],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79239511937789 lng:98.95721554756165],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792324021038038 lng:98.95740330219269],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792191981184402 lng:98.95807921886444],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792176745810004 lng:98.9585030078888],
                             [[CMUTransitCoordinate alloc] initWithLat:18.792242765755752 lng:98.95882487297058],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795868744558373 lng:98.95887315273285],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795955076435163 lng:98.95723700523376],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79797624536959 lng:98.95730674266815],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798118437245243 lng:98.95737111568451],
                             [[CMUTransitCoordinate alloc] initWithLat:18.7983368031061 lng:98.95752131938934],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79868720354662 lng:98.95755350589752],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79920518547033 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799479410549306 lng:98.95732283592224],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799550505867213 lng:98.95709216594696],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799591131749693 lng:98.95550966262817],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798184454860955 lng:98.94833207130432],
                             [[CMUTransitCoordinate alloc] initWithLat:18.797686782965883 lng:98.94835352897644],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796985977190275 lng:98.9503276348114],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796498458408188 lng:98.95332098007202],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799474332311167 lng:98.95331025123596],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79942354992131 lng:98.95195841789246],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79981965215584 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800591541011457 lng:98.95135760307312],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801871243682644 lng:98.95125031471252],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802632966843067 lng:98.95150780677795],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80318140538361 lng:98.95014524459839],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803364217833245 lng:98.94950151443481],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803536873853314 lng:98.94925475120544],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804064997050283 lng:98.94907236099243],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804471244535414 lng:98.94853591918945],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80346578021945 lng:98.94818186759949],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802805623613427 lng:98.94833207130432],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802612654270195 lng:98.9484715461731],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80213530810203 lng:98.94860029220581],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801911869004904 lng:98.94854664802551],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801718898636807 lng:98.94861102104187],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801353269964153 lng:98.94911527633667],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801139986204934 lng:98.94972681999207],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801180611703714 lng:98.9500379562378],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800520446133298 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80008371979429 lng:98.95164728164673],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79973840049143 lng:98.9519476890564],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799413393441505 lng:98.95196914672852],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798875099134563 lng:98.9514434337616],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79877353397794 lng:98.95033836364746],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796975820563375 lng:98.9503276348114],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805253268182916 lng:98.9547049999237],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805964195617467 lng:98.9534068107605],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80780243691488 lng:98.9545226097107],
                             [[CMUTransitCoordinate alloc] initWithLat:18.807152453337167 lng:98.95574569702148],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8054970150703 lng:98.95547747612],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805222799797164 lng:98.95471572875977],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804379838936764 lng:98.95397543907166],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803861872939912 lng:98.95384669303894],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803739998356 lng:98.9533531665802],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804237652351304 lng:98.9523446559906],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802572029117105 lng:98.95146489143372],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801830618350564 lng:98.95126104354858],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80053060254631 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799768869870178 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799393080480037 lng:98.95199060440063],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79944386287909 lng:98.95333170890808],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796478145094976 lng:98.95332098007202],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796376578492055 lng:98.95407199859619],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79402021610297 lng:98.95438313484192],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799596209984312 lng:98.94757032394409],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799555584103064 lng:98.94913136959076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799459097595815 lng:98.94947469234467],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799108698762225 lng:98.94981801509857],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95034372806549],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796996133816553 lng:98.9503276348114],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79652892837344 lng:98.95317077636719],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796508615063892 lng:98.95347654819489],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796396891817558 lng:98.95408272743225],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795934763056383 lng:98.95723164081573],
                             [[CMUTransitCoordinate alloc] initWithLat:18.795883979598674 lng:98.95927548408508],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796178523440144 lng:98.95928084850311],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796173445102415 lng:98.96018743515015],
                             [[CMUTransitCoordinate alloc] initWithLat:18.796203915126494 lng:98.96067023277283],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798732907898124 lng:98.9604502916336],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79887002087819 lng:98.96033227443695],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798692281808513 lng:98.95798802375793],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798682125284575 lng:98.95753741264343],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79920518547033 lng:98.9574944972992],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79950987997496 lng:98.95730137825012],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799565740574295 lng:98.9570277929306],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79958605351491 lng:98.9554613828659],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799555584103064 lng:98.95431876182556],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95325660705566],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79944386287909 lng:98.95193159580231],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79892080343505 lng:98.95142734050751],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798829394821666 lng:98.9509391784668],
                             [[CMUTransitCoordinate alloc] initWithLat:18.798798925272827 lng:98.95032227039337],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799499723500343 lng:98.95328342914581],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799428628160992 lng:98.95193696022034],
                             [[CMUTransitCoordinate alloc] initWithLat:18.79975871341121 lng:98.95194232463837],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800383334497752 lng:98.95140051841736],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800586462806866 lng:98.95134687423706],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801338035418883 lng:98.95122349262238],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801891556345 lng:98.95125031471252],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802592341694886 lng:98.95148634910583],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803003670867163 lng:98.95069777965546],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8033032803721 lng:98.94961953163147],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8035317957376 lng:98.9492654800415],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804059918950507 lng:98.9490669965744],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80424273044571 lng:98.94915282726288],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80468960215338 lng:98.94913136959076],
                             [[CMUTransitCoordinate alloc] initWithLat:18.804872412964457 lng:98.94934058189392],
                             [[CMUTransitCoordinate alloc] initWithLat:18.805182175274243 lng:98.95017743110657],
                             [[CMUTransitCoordinate alloc] initWithLat:18.8050552235769 lng:98.95046710968018],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80426812091546 lng:98.95231783390045],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80383140430221 lng:98.9531546831131],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803765388901603 lng:98.95343363285065],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803912653990498 lng:98.95384669303894],
                             [[CMUTransitCoordinate alloc] initWithLat:18.803699373475077 lng:98.95409345626831],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80345562398358 lng:98.95423829555511],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802764998506913 lng:98.95434021949768],
                             [[CMUTransitCoordinate alloc] initWithLat:18.802343512448264 lng:98.95440459251404],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80209468283373 lng:98.95454406738281],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801779836671688 lng:98.95476937294006],
                             [[CMUTransitCoordinate alloc] initWithLat:18.80129233177481 lng:98.95460844039917],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801139986204934 lng:98.95453333854675],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801089204317645 lng:98.95440459251404],
                             [[CMUTransitCoordinate alloc] initWithLat:18.801089204317645 lng:98.95399689674377],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800967327725672 lng:98.95364820957184],
                             [[CMUTransitCoordinate alloc] initWithLat:18.800022781145284 lng:98.95331561565399],
                             [[CMUTransitCoordinate alloc] initWithLat:18.799504801737722 lng:98.95328342914581],
                            
                             nil];
     */
    return lines;
}
@end
