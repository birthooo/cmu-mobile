//
//  CMUNewsFeed.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsFeed.h"
#import "global.h"
#import "CMUFormatUtils.h"

@implementation CMUNewsFeed
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSNumber *newsFeedId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(newsFeedId)
        {
            self.newsFeedId = [newsFeedId intValue];
        }
        
        self.topic = JSON_GET_OBJECT([dict objectForKey:@"Toppic"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        
        //TODO fix this -> currently web service return +0000 timezone
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        
        self.link = JSON_GET_OBJECT([dict objectForKey:@"Link"]);
        self.imageLink = JSON_GET_OBJECT([dict objectForKey:@"ImageLink"]);
        //self.imageLink = @"http://www.mahabatt.com/batteryshop/image/cache/data/Dapter/acer/ACER%20Laptop%20Adapter%2019V%204.74A%205.5mm%20%201.7mm-500x500.jpg";
        self.nameType = JSON_GET_OBJECT([dict objectForKey:@"NameType"]);
        self.isFacebook = [JSON_GET_OBJECT([dict objectForKey:@"IsFacebook"]) boolValue];
        self.facebookId = JSON_GET_OBJECT([dict objectForKey:@"FacebookID"]);
        self.facebookPostId = JSON_GET_OBJECT([dict objectForKey:@"FacebookPostID"]);
        
        NSTimeInterval time = [JSON_GET_OBJECT([dict objectForKey:@"time"]) doubleValue];
        self.time = [NSDate dateWithTimeIntervalSince1970:time];
        
        self.typeId = [JSON_GET_OBJECT([dict objectForKey:@"TypeId"]) intValue];
        
        NSNumber *isOpenBrowser = JSON_GET_OBJECT([dict objectForKey:@"IsOpenBrowser"]);
        if(isOpenBrowser){
            self.isOpenBrowser = [isOpenBrowser boolValue];
        }
        
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUNewsFeed class]])
    {
        return self.newsFeedId == [other newsFeedId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.newsFeedId;
}
@end
