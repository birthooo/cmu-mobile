//
//  CMUNotificationModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 11/19/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNotificationModel.h"
#import "global.h"
#import "CMUFormatUtils.h"

@implementation CMUNotificationSettingsModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        self.newsList = JSON_GET_OBJECT([dict objectForKey:@"news"]);
        self.activityList = JSON_GET_OBJECT([dict objectForKey:@"activity"]);
        self.channelList = JSON_GET_OBJECT([dict objectForKey:@"channel"]);
    }
    return self;
}
@end


@implementation CMUNotificationFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *notiId = JSON_GET_OBJECT([dict objectForKey:@"id"]);
        if(notiId)
        {
            self.notiId =  [notiId intValue];
        }
        
        NSNumber *type = JSON_GET_OBJECT([dict objectForKey:@"type"]);
        if(type)
        {
            self.type =  [type intValue];
        }
        
        self.typeName = JSON_GET_OBJECT([dict objectForKey:@"TypeName"]);
        self.message = JSON_GET_OBJECT([dict objectForKey:@"message"]);
        
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        
        self.time = [JSON_GET_OBJECT([dict objectForKey:@"time"]) doubleValue];
        self.read = [JSON_GET_OBJECT([dict objectForKey:@"read"]) boolValue];
        
        NSNumber *itemId = JSON_GET_OBJECT([dict objectForKey:@"ItemId"]);
        if(itemId)
        {
            self.itemId =  [itemId intValue];
        }
        
        NSNumber *senderId = JSON_GET_OBJECT([dict objectForKey:@"senderId"]);
        if(senderId)
        {
            self.senderId =  [senderId intValue];
        }
        
        self.senderName = JSON_GET_OBJECT([dict objectForKey:@"senderName"]);
        self.senderImage = JSON_GET_OBJECT([dict objectForKey:@"senderImage"]);
        NSNumber *subItemId = [dict objectForKey:@"subItemId"];
        if(subItemId)
        {
            self.subItemId = [NSString stringWithFormat:@"%i", [subItemId intValue]];
        }
        
        
    }
    return self;
}
@end

@implementation CMUNotificationFeedModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSArray *notificationFeeds = JSON_GET_OBJECT([dict objectForKey:@"nofiticationFeed"]);
        if(notificationFeeds)
        {
            self.notificationFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in notificationFeeds)
            {
                CMUNotificationFeed *notiFeed = [[CMUNotificationFeed alloc] initWithDictionary:dict];
                [self.notificationFeedList addObject:notiFeed];
            }
        }
        
        NSNumber *notificationNumber = JSON_GET_OBJECT([dict objectForKey:@"nofiticationNumber"]);
        if(notificationNumber)
        {
            self.notificationNumber = [notificationNumber intValue];
        }
    }
    return self;
}
@end

#pragma mark Broadcast
@implementation CMUBroadcastFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *broadcastId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(broadcastId)
        {
            self.broadcastId =  [broadcastId intValue];
        }
        
        self.name = JSON_GET_OBJECT([dict objectForKey:@"Name"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"detail"]);
        self.image = JSON_GET_OBJECT([dict objectForKey:@"Image"]);
        
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"Update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        NSNumber *unread = JSON_GET_OBJECT([dict objectForKey:@"unread"]);
        if(unread)
        {
            self.unread =  [unread intValue];
        }
    }
    return self;
}
@end

@implementation CMUBroadcastFeedModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSArray *broadcastFeedList = JSON_GET_OBJECT([dict objectForKey:@"BroadcastModel"]);
        if(broadcastFeedList)
        {
            self.broadcastFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in broadcastFeedList)
            {
                CMUBroadcastFeed *broadcastFeed = [[CMUBroadcastFeed alloc] initWithDictionary:dict];
                [self.broadcastFeedList addObject:broadcastFeed];
            }
        }
    }
    return self;
}
@end

//broadcast by sender
@implementation CMUBroadcastSenderFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *broadcastSenderId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(broadcastSenderId)
        {
            self.broadcastSenderId =  [broadcastSenderId intValue];
        }
        
        NSNumber *senderId = JSON_GET_OBJECT([dict objectForKey:@"sendid"]);
        if(senderId)
        {
            self.senderId =  [senderId intValue];
        }
        
        self.senderName = JSON_GET_OBJECT([dict objectForKey:@"sendName"]);
        self.text = JSON_GET_OBJECT([dict objectForKey:@"text"]);

        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"Update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        self.time = [JSON_GET_OBJECT([dict objectForKey:@"time"]) doubleValue];
    }
    return self;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUBroadcastSenderFeed class]])
    {
        return self.broadcastSenderId == [other broadcastSenderId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.broadcastSenderId;
}

@end

@implementation CMUBroadcastSenderFeedModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSArray *broadcastSenderFeedList = JSON_GET_OBJECT([dict objectForKey:@"broadcastSenderModel"]);
        if(broadcastSenderFeedList)
        {
            self.broadcastSenderFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in broadcastSenderFeedList)
            {
                CMUBroadcastSenderFeed *broadcastFeed = [[CMUBroadcastSenderFeed alloc] initWithDictionary:dict];
                [self.broadcastSenderFeedList addObject:broadcastFeed];
            }
        }
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end

//notification
@implementation CMUNotificationNumberModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *notificationNumber = JSON_GET_OBJECT([dict objectForKey:@"nofiticationNumber"]);
        if(notificationNumber)
        {
            self.notificationNumber = [notificationNumber intValue];
        }
    }
    return self;
}
@end

@implementation CMUNotificationModel

@end
