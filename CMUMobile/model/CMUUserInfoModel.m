//
//  CMUUserInfoModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 10/14/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUUserInfoModel.h"
#import "global.h"

@implementation CMUUser
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSDictionary *personType = JSON_GET_OBJECT([dict objectForKey:@"personType"]);
        self.personTypeTH = JSON_GET_OBJECT([personType objectForKey:@"th_TH"]);
        self.personTypeEN = JSON_GET_OBJECT([personType objectForKey:@"en_US"]);
        
        NSDictionary *prefix = JSON_GET_OBJECT([dict objectForKey:@"prefix"]);
        self.prefixTH = JSON_GET_OBJECT([prefix objectForKey:@"th_TH"]);
        self.prefixEN = JSON_GET_OBJECT([prefix objectForKey:@"en_US"]);
        
        NSDictionary *firstName = JSON_GET_OBJECT([dict objectForKey:@"firstName"]);
        self.firstNameTH = JSON_GET_OBJECT([firstName objectForKey:@"th_TH"]);
        self.firstNameEN = JSON_GET_OBJECT([firstName objectForKey:@"en_US"]);
        
        NSDictionary *lastName = JSON_GET_OBJECT([dict objectForKey:@"lastName"]);
        self.lastNameTH = JSON_GET_OBJECT([lastName objectForKey:@"th_TH"]);
        self.lastNameEN = JSON_GET_OBJECT([lastName objectForKey:@"en_US"]);
        
        self.citizenId = JSON_GET_OBJECT([dict objectForKey:@"citizen_id"]);
        
        self.image = JSON_GET_OBJECT([dict objectForKey:@"image"]);
        
        //employee
        NSDictionary *organization = JSON_GET_OBJECT([dict objectForKey:@"organization"]);
        self.organizationCode = JSON_GET_OBJECT([organization objectForKey:@"code"]);
        self.organizationTH = JSON_GET_OBJECT([organization objectForKey:@"th_TH"]);
        self.organizationEN = JSON_GET_OBJECT([organization objectForKey:@"en_US"]);
        
        //student
        self.studentId  = JSON_GET_OBJECT([dict objectForKey:@"id"]);
        
        self.level = JSON_GET_OBJECT([dict objectForKey:@"level"]);
        
        NSDictionary *faculty = JSON_GET_OBJECT([dict objectForKey:@"faculty"]);
        self.facultyCode = JSON_GET_OBJECT([faculty objectForKey:@"code"]);
        self.facultyTH = JSON_GET_OBJECT([faculty objectForKey:@"th_TH"]);
        self.facultyEN = JSON_GET_OBJECT([faculty objectForKey:@"en_US"]);
    }
    return self;
}
@end

@implementation CMUUserInfoModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *success = JSON_GET_OBJECT([dict objectForKey:@"success"]);
        if(success)
        {
            self.success = [success boolValue];
            
            NSDictionary *employee = JSON_GET_OBJECT([dict objectForKey:@"employee"]);
            if(employee)
            {
                self.user = [[CMUUser alloc] initWithDictionary:employee];
            }
            else
            {
                NSDictionary *student = JSON_GET_OBJECT([dict objectForKey:@"student"]);
                if(student)
                {
                    self.user = [[CMUUser alloc] initWithDictionary:student];
                }
            }
        }
    }
    return self;
}
@end
