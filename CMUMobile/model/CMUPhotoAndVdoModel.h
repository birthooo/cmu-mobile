//
//  CMUPhotoAndVdoModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/6/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CMUPhotoView : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, readonly) int photoViewId;
@property(nonatomic, readonly) NSString* name;
@property(nonatomic, readonly) NSString* path;
@property(nonatomic, readonly) NSString* detail;
@property(nonatomic, readonly) NSString* cropPath;
@property(nonatomic, readonly) NSDate* update;
@end

@interface CMUVideoView : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, readonly) int photoViewId;
@property(nonatomic, readonly) NSString* name;
@property(nonatomic, readonly) NSString* link;
@property(nonatomic, readonly) NSString* detail;
@property(nonatomic, readonly) NSString* cropPath;
@property(nonatomic, readonly) NSDate* update;
@end

@interface CMUPhotoAndVideoModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, readonly) NSMutableArray *photoFeedList;
@property(nonatomic, readonly) NSMutableArray *videoFeedList;
@end
