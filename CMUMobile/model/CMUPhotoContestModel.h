//
//  CMUContestModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/20/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUPhotoContestFeedDetail : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int photoContestFeedDetailId;
@property(nonatomic, strong) NSString* account;
@property(nonatomic, strong) NSString* path;
@property(nonatomic, strong) NSString* detail;
@property(nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSString* cropPath;
@property(nonatomic, strong) NSDate* time;
@property(nonatomic, strong) NSDate* dtime;
@property(nonatomic, unsafe_unretained) int nVote;

@end

@interface CMUPhotoContestFeed : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int photoContestFeedId;
@property(nonatomic, strong) NSString* account;
@property(nonatomic, strong) NSString* path;
@property(nonatomic, strong) NSString* detail;
@property(nonatomic, strong) NSString* name;
@property(nonatomic, strong) NSString* cropPath;
@property(nonatomic, strong) NSDate* time;
@property(nonatomic, strong) NSDate* dtime;
@property(nonatomic, unsafe_unretained) int nVote;
@end

@interface CMUPhotoContestModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong)NSMutableArray *photoFeedList; //CMUPhotoContestFeed
@property(nonatomic, strong)NSString *refreshURL;
@property(nonatomic, strong)NSString *loadMoreURL;
@end

