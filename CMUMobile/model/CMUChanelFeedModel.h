//
//  CmuChanelFeedModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/27/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    CMU_CHANNEL_TYPE_ALL = 0,
    CMU_CHANNEL_TYPE_AROUND_CMU = 1,
    CMU_CHANNEL_TYPE_CMU_KNOWLEDGE = 2,
    CMU_CHANNEL_TYPE_TECH_BY_ITSC = 4,
    CMU_CHANNEL_TYPE_DEK_MOR = 5,
    CMU_CHANNEL_TYPE_HOT_VIEW = 6
} CMU_CHANNEL_TYPE;

@interface CMUChannelType : NSObject
- (id)initWithType:(CMU_CHANNEL_TYPE) channelTypeId title:(NSString *)title;
+ (NSMutableArray *)allChannelType;
+ (CMUChannelType *)getByChannelId:(CMU_CHANNEL_TYPE) channelTypeId;
@property(nonatomic, readonly) CMU_CHANNEL_TYPE channelTypeId;
@property(nonatomic, readonly) NSString *title;
@end

//decare in JSON
@interface CMUChanelCommentView : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@end

@interface CMUChanelFeed : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int channelFeedId;
@property(nonatomic, strong) NSString *topic;
@property(nonatomic, strong) NSString *detail;
@property(nonatomic, strong) NSDate *update;
@property(nonatomic, strong) NSString *link;
@property(nonatomic, unsafe_unretained) int numberLike;
@property(nonatomic, strong) NSString *time;
@property(nonatomic, unsafe_unretained) int numberComment;
@property(nonatomic, unsafe_unretained) BOOL isLike;
@property(nonatomic, strong) NSString *imageLink;
@property(nonatomic, unsafe_unretained) int numberView;
@property(nonatomic, unsafe_unretained) BOOL isYoutube;
@property(nonatomic, strong) NSString *youtubelink;

@end

@interface CMUChanelFeedModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSMutableArray *chanelFeedList;
@end

@interface CMUChannelCommentFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained) int commentId;
@property(nonatomic, strong) NSString *commentText;
@property(nonatomic, unsafe_unretained) int vdoId;
@property(nonatomic, strong) NSString *accId;
@property(nonatomic, strong) NSDate *commentDatetime;
@end

@interface CMUChannelCommentModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong) NSMutableArray *commentFeedList;
@property(nonatomic, unsafe_unretained) int numberComment;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@end

