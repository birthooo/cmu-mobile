//
//  CMUNewsModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMUNewsModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong)NSMutableArray *newsFeedList; //CMUNewsFeed
@property(nonatomic, strong)NSString *refreshURL;
@property(nonatomic, strong)NSString *loadMoreURL;
@end
