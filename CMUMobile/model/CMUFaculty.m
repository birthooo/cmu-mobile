//
//  CMUFaculty.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/29/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUFaculty.h"

@implementation CMUFaculty
- (id)initWithFacultyId:(NSString *) facultyId nameThai:(NSString *)nameThai nameEng:(NSString *)nameEng
{
    self = [super init];
    if(self)
    {
        self.facultyId = facultyId;
        self.nameThai = nameThai;
        self.nameEng = nameEng;
    }
    return self;
}

+ (NSArray *)getAllFaculty
{
    NSArray *allFaculty = [[NSArray alloc] initWithObjects:
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000001" nameThai:@"คณะมนุษยศาสตร์" nameEng:@"Faculty of Humanities"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000002" nameThai:@"คณะศึกษาศาสตร์" nameEng:@"Faculty of Education"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000003" nameThai:@"คณะวิจิตรศิลป์" nameEng:@"Faculty of Fine Arts"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000004" nameThai:@"คณะสังคมศาสตร์" nameEng:@"Faculty of Social Sciences"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000005" nameThai:@"คณะวิทยาศาสตร์" nameEng:@"Faculty of Science"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000006" nameThai:@"คณะวิศวกรรมศาสตร์" nameEng:@"Faculty of Engineering"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000007" nameThai:@"คณะแพทยศาสตร์" nameEng:@"Faculty of Medicine"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000008" nameThai:@"คณะเกษตรศาสตร์" nameEng:@"Faculty of Agriculture"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000009" nameThai:@"คณะทันตแพทยศาสตร์" nameEng:@"Faculty of Dentistry"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000010" nameThai:@"คณะเภสัชศาสตร์" nameEng:@"Faculty of Pharmacy"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000011" nameThai:@"คณะเทคนิคการแพทย์" nameEng:@"Faculty of Associated Medical Science"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000012" nameThai:@"คณะพยาบาลศาสตร์" nameEng:@"Faculty of Nurse"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000013" nameThai:@"คณะอุตสาหกรรมเกษตร" nameEng:@"Faculty of Agro-Industry"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000014" nameThai:@"คณะสัตวแพทยศาสตร์" nameEng:@"Faculty of Veterinary Medical"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000015" nameThai:@"คณะบริหารธุรกิจ" nameEng:@"Faculty of Business Administration"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000016" nameThai:@"คณะเศรษฐศาสตร์" nameEng:@"Faculty of Economics"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000017" nameThai:@"คณะสถาปัตยกรรมศาสตร์" nameEng:@"Faculty of Architecture"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000018" nameThai:@"คณะการสื่อสารมวลชน" nameEng:@"Faculty of Masscommunity"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000019" nameThai:@"คณะรัฐศาสตร์และรัฐประศาสนศาสตร์" nameEng:@"Faculty of Political Science and Public Administration"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000020" nameThai:@"คณะนิติศาสตร์" nameEng:@"Faculty of Law"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000021" nameThai:@"วิทยาลัยศิลปะ สื่อ และเทคโนโลยี" nameEng:@"College of Art, Media and Technology"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000030" nameThai:@"บัณฑิตวิทยาลัย" nameEng:@"The Graduate School"],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000031" nameThai:@"บัณฑิตศึกษาสถาน" nameEng:@""],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000053" nameThai:@"บัณฑิตสมทบ" nameEng:@""],
                           [[CMUFaculty alloc] initWithFacultyId:@"0000000038" nameThai:@"สถานวิทยาการหลังการเก็บเกี่ยว" nameEng:@""],
                           nil];
    return allFaculty;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUFaculty class]])
    {
        return [self.facultyId isEqualToString:[other facultyId]];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.facultyId.hash;
}
@end
