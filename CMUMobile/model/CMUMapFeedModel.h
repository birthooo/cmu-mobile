//
//  CMUMapFeedModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 9/22/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
typedef enum
{
    CMU_MAP_GROUP_NONE = -1,
    CMU_MAP_GROUP_WIFI = 0,
    CMU_MAP_GROUP_FOOD_CENTER = 1,
    CMU_MAP_GROUP_ARENA = 2,
    CMU_MAP_GROUP_DORM = 3,
    CMU_MAP_GROUP_ATM = 4,
    CMU_MAP_GROUP_BUILDING = 5,
    CMU_MAP_GROUP_TERMINAL = 6,
}
CMU_MAP_GROUP;

typedef enum
{
    CMU_JUMBO_MAP_STATUS_DOWN = 0,
    CMU_JUMBO_MAP_STATUS_IDLE = 1,
    CMU_JUMBO_MAP_STATUS_LOW = 2,
    CMU_JUMBO_MAP_STATUS_HIGH = 3,
    CMU_JUMBO_MAP_STATUS_VERY_HIGH = 4,
    CMU_JUMBO_MAP_STATUS_MAINTENANCE = 5
}
CMU_JUMBO_MAP_STATUS;

@interface CMUMapVersionModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int version;
@property(nonatomic, strong)NSString *versionURL;
- (NSDictionary *)toDictionary;
@end

@interface CMUMapFeed : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
- (id)initWithJumboMapDictionary:(NSDictionary *) dict;
- (id)initWithBusTypeDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int mapFeedId;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, unsafe_unretained)CLLocationDegrees lat;
@property(nonatomic, unsafe_unretained)CLLocationDegrees lng;
@property(nonatomic, unsafe_unretained)int zoneId;
@property(nonatomic, strong)NSMutableArray *mapFeedList;
//Jumbo Map
@property(nonatomic, unsafe_unretained)CMU_JUMBO_MAP_STATUS status;
@property(nonatomic, unsafe_unretained)int total;

//bus type
@property(nonatomic, unsafe_unretained)int busTypeID;
@property(nonatomic, strong)NSString *busTypeName;
@property(nonatomic, strong)NSMutableArray *busMapFeedList;
@end

@interface CMUMapBusPoint : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, unsafe_unretained) int busPointId;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, unsafe_unretained)CLLocationDegrees lat;
@property(nonatomic, unsafe_unretained)CLLocationDegrees lng;
@end


@interface CMUMapBusTypeModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong)NSMutableArray *mapBusPointViewList;
@property(nonatomic, strong)NSMutableArray *mapViewList;
@property(nonatomic, strong)NSString *colorCode;
@end

@interface CMUMapFeedModel : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong)NSMutableArray *mapFeedList;
- (CMUMapFeed *)getByMapType:(CMU_MAP_GROUP) mapType;
@end

