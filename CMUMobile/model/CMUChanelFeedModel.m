//
//  CmuChanelFeedModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/27/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUChanelFeedModel.h"
#import "global.h"
#import "CMUFormatUtils.h"


@interface CMUChannelType()
@property(nonatomic, unsafe_unretained) CMU_CHANNEL_TYPE channelTypeId;
@property(nonatomic, strong) NSString *title;
@end

@implementation CMUChannelType
- (id)initWithType:(CMU_CHANNEL_TYPE) channelTypeId title:(NSString *)title;
{
    self = [super init];
    if(self)
    {
        self.channelTypeId = channelTypeId;
        self.title = title;
    }
    return self;
}

+ (NSMutableArray *)allChannelType
{
    NSMutableArray *allChannelType = [[NSMutableArray alloc] init];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_ALL title:@"ทั้งหมด"]];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_AROUND_CMU title:@"Around CMU"]];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_CMU_KNOWLEDGE title:@"CMU Knowledge"]];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_TECH_BY_ITSC title:@"Tecnology Update by ITSC"]];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_DEK_MOR title:@"Dek Mor"]];
    [allChannelType addObject:[[CMUChannelType alloc] initWithType:CMU_CHANNEL_TYPE_HOT_VIEW title:@"Hot View"]];
    return allChannelType;
}

+ (CMUChannelType *)getByChannelId:(CMU_CHANNEL_TYPE) channelTypeId
{
    CMUChannelType *temp = [[CMUChannelType alloc] init];
    temp.channelTypeId = channelTypeId;
    NSInteger index = [[self allChannelType] indexOfObject:temp];
    if(index != NSNotFound)
    {
        return [[self allChannelType] objectAtIndex:index];
    }
    return nil;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUChannelType class]])
    {
        return self.channelTypeId == [other channelTypeId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.channelTypeId;
}
@end

/* CmuChanelCommentView */
@implementation CMUChanelCommentView
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
    }
    return self;
}
@end

/* CmuChanelFeed */
@implementation CMUChanelFeed

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *channelFeedId = JSON_GET_OBJECT([dict objectForKey:@"Id"]);
        if(channelFeedId)
        {
            self.channelFeedId = [channelFeedId intValue];
        }
        
        self.topic = JSON_GET_OBJECT([dict objectForKey:@"Toppic"]);
        self.detail = JSON_GET_OBJECT([dict objectForKey:@"Detail"]);
        
        NSString *updateDateStr = JSON_GET_OBJECT([dict objectForKey:@"update"]);
        self.update = [CMUFormatUtils parseWebServiceDate:updateDateStr];
        
        self.link = JSON_GET_OBJECT([dict objectForKey:@"Link"]);
       
        NSNumber *numberLike = JSON_GET_OBJECT([dict objectForKey:@"NumberLike"]);
        if(numberLike)
        {
            self.numberLike = [numberLike intValue];
        }
        
        self.time= JSON_GET_OBJECT([dict objectForKey:@"time"]);
        
        NSNumber *numberComment = JSON_GET_OBJECT([dict objectForKey:@"NumberComment"]);
        if(numberComment)
        {
            self.numberComment = [numberComment intValue];
        }
        
        NSNumber *isLike = JSON_GET_OBJECT([dict objectForKey:@"IsLike"]);
        if(isLike)
        {
            self.isLike = [isLike boolValue];
        }
        
        self.imageLink = JSON_GET_OBJECT([dict objectForKey:@"ImageLink"]);
        //TODO remove this
        //self.imageLink = @"http://ibssthailand.com/test_channel.png";
        
        NSNumber *numberView = JSON_GET_OBJECT([dict objectForKey:@"NumberView"]);
        if(numberView)
        {
            self.numberView = [numberView intValue];
        }
        
        NSNumber *isYoutube = JSON_GET_OBJECT([dict objectForKey:@"isYoutube"]);
        if(isYoutube)
        {
            self.isYoutube = [isYoutube boolValue];
        }
        
        self.youtubelink= JSON_GET_OBJECT([dict objectForKey:@"Youtubelink"]);
        
    }
    return self;
}

- (void)setNumberLike:(int)numberLike
{
    _numberLike = numberLike < 0? 0: numberLike;
}

- (void)setNumberComment:(int)numberComment
{
    _numberComment = numberComment < 0 ? 0 : numberComment;
}

#pragma mark NSObject
- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUChanelFeed class]])
    {
        return self.channelFeedId == [other channelFeedId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.channelFeedId;
}
@end

/* CmuChanelFeedModel */
@implementation CMUChanelFeedModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *chanelFeed = JSON_GET_OBJECT([dict objectForKey:@"CmuChanelFeed"]);
        if(chanelFeed)
        {
            self.chanelFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in chanelFeed)
            {
                CMUChanelFeed *channelFeed = [[CMUChanelFeed alloc] initWithDictionary:dict];
                [self.chanelFeedList addObject:channelFeed];
            }
        }
        
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end

@implementation CMUChannelCommentFeed
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        NSNumber *commentId = JSON_GET_OBJECT([dict objectForKey:@"comment_id"]);
        if(commentId)
        {
            self.commentId = [commentId intValue];
        }
        self.commentText = JSON_GET_OBJECT([dict objectForKey:@"comment_text"]);
        
        NSNumber *vdoId = JSON_GET_OBJECT([dict objectForKey:@"vdo_id"]);
        if(vdoId)
        {
            self.vdoId = [vdoId intValue];
        }
        self.accId = JSON_GET_OBJECT([dict objectForKey:@"ac_id"]);
        self.commentDatetime = [CMUFormatUtils parseWebServiceDate:JSON_GET_OBJECT([dict objectForKey:@"comment_datetime"])];
        
        
    }
    return self;
}

- (BOOL)isEqual:(id)other
{
    if(self == other)
    {
        return YES;
    }
    else if([other isKindOfClass:[CMUChannelCommentFeed class]])
    {
        return self.commentId == [other commentId];
    }
    return NO;
}

- (NSUInteger)hash
{
    return self.commentId;
}

@end

@implementation CMUChannelCommentModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *commentFeedList = JSON_GET_OBJECT([dict objectForKey:@"CommentList"]);
        if(commentFeedList)
        {
            self.commentFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in commentFeedList)
            {
                CMUChannelCommentFeed *commentFeed = [[CMUChannelCommentFeed alloc] initWithDictionary:dict];
                [self.commentFeedList addObject:commentFeed];
            }
        }
        
        NSNumber *numberComment = JSON_GET_OBJECT([dict objectForKey:@"numberComment"]);
        if(numberComment)
        {
            self.numberComment = [numberComment intValue];
        }
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end
