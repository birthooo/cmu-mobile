//
//  CMUPhoneFacultyViewFeed.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/7/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneFacultyViewModel.h"
#import "CMUPhoneFacultyView.h"
#import "global.h"

@implementation CMUPhoneFacultyViewModel
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
//        NSNumber *versionNumber = JSON_GET_OBJECT([dict objectForKey:@"version"]);
//        if(versionNumber)
//        {
//            self.version = [versionNumber intValue];
//        }
        NSArray *phoneFacultyViewList = JSON_GET_OBJECT([dict objectForKey:@"phoneFacultyViewFeed"]);
        if(phoneFacultyViewList)
        {
            self.phoneFacultyViewList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in phoneFacultyViewList)
            {
                CMUPhoneFacultyView *phoneFacultyView = [[CMUPhoneFacultyView alloc] initWithDictionary:dict];
                [self.phoneFacultyViewList addObject:phoneFacultyView];
            }
        }
        
    }
    return self;
}

@end
