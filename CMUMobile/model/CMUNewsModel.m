//
//  CMUNewsModel.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNewsModel.h"
#import "CMUNewsFeed.h"
#import "global.h"

@implementation CMUNewsModel
- (id)initWithDictionary:(NSDictionary *) dict
{
    self = [super init];
    if(self)
    {
        NSArray *newsFeeds = JSON_GET_OBJECT([dict objectForKey:@"NewsFeeds"]);
        if(newsFeeds)
        {
            self.newsFeedList = [[NSMutableArray alloc] init];
            for(NSDictionary *dict in newsFeeds)
            {
                CMUNewsFeed *newsFeed = [[CMUNewsFeed alloc] initWithDictionary:dict];
                [self.newsFeedList addObject:newsFeed];
            }
        }
        
        self.refreshURL = JSON_GET_OBJECT([dict objectForKey:@"previous"]);
        self.loadMoreURL = JSON_GET_OBJECT([dict objectForKey:@"next"]);
    }
    return self;
}
@end
