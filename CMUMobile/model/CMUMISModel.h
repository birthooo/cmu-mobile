//
//  CMUMISModel.h
//  CMUMobile
//
//  Created by Nikorn lansa on 5/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CMUMISEdocCountNumber : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int nPublic;
@property(nonatomic, unsafe_unretained)int nRecieve;
@property(nonatomic, unsafe_unretained)int nStatusFollow;
@property(nonatomic, unsafe_unretained)int nStatusSend;
@end

@interface CMUMISEdocFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)BOOL IsOpened;
@property(nonatomic, strong)NSString *docCode;
@property(nonatomic, unsafe_unretained)int docID;
@property(nonatomic, strong)NSString *docSubID;
@property(nonatomic, unsafe_unretained)int docTypeID;
@property(nonatomic, strong)NSDate *sendTime;
@property(nonatomic, strong)NSString *textStaus;
@property(nonatomic, strong)NSString *topic;

//UI Helper
@property(nonatomic, unsafe_unretained)BOOL uiSelected;
//ui helper
- (NSString *)htmlText;
@end

@interface CMUMISEdocFeedModel : NSObject
//- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(id) array;
@property(nonatomic, strong) NSMutableArray *edocFeedList;
@end

//Edoc detail

@interface CMUMISEdocOrderFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int order;
@property(nonatomic, strong)NSString *text;
@property(nonatomic, strong)NSString *toName;
@property(nonatomic, strong)NSString *toOrgan;
@property(nonatomic, strong)NSString *toNote;
@property(nonatomic, strong)NSString *fromNote;
@property(nonatomic, strong)NSString *fromOrgan;
@property(nonatomic, strong)NSString *objective;
@property(nonatomic, strong)NSString *fromName;

//ui helper
- (NSString *)htmlFrom;
- (NSString *)htmlTo;
- (NSString *)htmlObjective;
- (NSString *)htmlText;
@end

@interface CMUMISEdocOrderModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSMutableArray *edocTabOderDetailList;
@end

@interface CMUMISEdocDetail : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, strong)NSString *lblTypeDocumentName;
@property(nonatomic, strong)NSString *lblDocumentCode;
@property(nonatomic, strong)NSString *lblPriorityLevel;
@property(nonatomic, strong)NSString *lblTitleHead;
@property(nonatomic, strong)NSString *lblSendDate;
@property(nonatomic, strong)NSString *lblSecurityLevel;
@property(nonatomic, strong)NSString *lblCreateReceiveDate;
@property(nonatomic, strong)NSString *hdfTypeDocumentID;
@property(nonatomic, strong)NSString *docSubID;
@property(nonatomic, unsafe_unretained)BOOL accept;

@property(nonatomic, strong)NSDictionary *detailModel;
@property(nonatomic, strong)NSDictionary *fileModel;
@property(nonatomic, strong)CMUMISEdocOrderModel *orderModel;
@end



//Folder
@interface CMUMISEdocFolderFeed : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int folderID;
@property(nonatomic, strong)NSString *folderName;
@end

@interface CMUMISEdocFolderFeedModel : NSObject
//- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(id) array;
@property(nonatomic, strong) NSMutableArray *edocFolderFeedList;
@end

@interface CMUMISEdocFolderSubListFeedModel : NSObject
- (id)initWithDictionary:(NSDictionary *)dict;
@property(nonatomic, unsafe_unretained)int nSend;
@property(nonatomic, unsafe_unretained)int nOut;
@property(nonatomic, strong) NSMutableArray *listSend;
@property(nonatomic, strong) NSMutableArray *listOut;
@end

@interface CMUMISModel : NSObject

@end
