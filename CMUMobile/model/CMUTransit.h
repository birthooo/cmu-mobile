//
//  CMUTransit.h
//  CMUMobile
//
//  Created by Nikorn lansa on 11/2/2558 BE.
//  Copyright © 2558 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CMUTransitBusPosition : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong) NSString *no;
@property(nonatomic, unsafe_unretained) CLLocationCoordinate2D coordinate;
@property(nonatomic, unsafe_unretained) int direction;
@end

@interface CMUTransitBusPositionModel : NSObject
- (id)initWithArray:(NSArray *) array;
@property(nonatomic, strong) NSMutableArray *busPositions;
@end


@interface CMUTransitBusStation : NSObject
- (id)initWithDictionary:(NSDictionary *) dict;
@property(nonatomic, strong) NSString *no;
@property(nonatomic, strong) NSString *name;
@property(nonatomic, unsafe_unretained) CLLocationCoordinate2D coordinate;
@end

@interface CMUTransitBusStationModel : NSObject
- (id)initWithArray:(NSArray *) array;
-(id)initWithBackGroundArray:(NSArray*)array;
@property(nonatomic, strong) NSMutableArray *busStations;
@property(nonatomic, strong) NSMutableArray *AllBusStations;
@end


@interface CMUTransitCoordinate : NSObject
- (id)initWithLat:(CLLocationDegrees)lat lng:(CLLocationDegrees) lng;
@property(nonatomic, readonly) CLLocationCoordinate2D coordinate;
@end


@interface CMUTransit : NSObject
+(NSArray *)busGreenLine;
+(NSArray *)busOrangeLine;
+(NSArray *)busRedLine;
+(NSArray *)busBlueLine;
+(NSArray *)busVioletLine;
+(NSArray *)busSixLine;
+(NSArray *)busAllLine;
@end
