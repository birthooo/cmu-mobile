//
//  inboxDetailViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 10/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "inboxDetailViewController.h"

@interface inboxDetailViewController ()
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIView *container1;
@property (strong, nonatomic) IBOutlet UIView *container2;
@property (strong, nonatomic) IBOutlet UIView *container3;
@property (strong, nonatomic) IBOutlet UIImageView *statusImage;
@property (strong, nonatomic) IBOutlet UIButton *acceptButton;
@property (strong, nonatomic) IBOutlet UIButton *replyButton;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;
@property (strong, nonatomic) IBOutlet UITableView *detailTable;
@property (strong, nonatomic) IBOutlet UITableView *documentTable;
@property (strong, nonatomic) IBOutlet UITableView *orderTable;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *priorityLabel;
@property (strong, nonatomic) IBOutlet UILabel *securityLevelLabel;
@property (strong, nonatomic) IBOutlet UILabel *subCategoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *documentCodeLabel;


//detail
@property (strong, nonatomic) IBOutlet UILabel *sendDate;
@property (strong, nonatomic) IBOutlet UIImageView *acceptStatusImage;
@property (strong, nonatomic) IBOutlet UILabel *receiveDate;

@end

@implementation inboxDetailViewController
{
    EdocData *edoc;
    CMUTicket *ticket;
    NSArray *topicName;
    NSArray *listData;
    
    id isAccept;
}
- (void)viewDidLoad {
    edoc = [EdocData getInstance];
    [super viewDidLoad];
    ticket = [[CMUWebServiceManager sharedInstance] ticket];
    // [self getEdocDetail:edoc.ID docsubid:edoc.SubID cmuaccount:ticket.userName];
    
        
    
   
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
      [self getEdocDetail:edoc.ID docsubid:edoc.SubID cmuaccount:ticket.userName];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            topicName = [[[edoc.Detail valueForKey:@"data"]valueForKey:@"detailModel"]valueForKey:@"topicName"];
            listData = [[[edoc.Detail valueForKey:@"data"]valueForKey:@"detailModel"]valueForKey:@"listdata"];
            [self detail];
            [self setLabel];
            self.container1.hidden = NO;
            self.container2.hidden = YES;
            self.container3.hidden = YES;
            [self.detailTable reloadData];
            [self.documentTable reloadData];
            [self.orderTable reloadData];
            
        });
    });
    

}

-(void)setLabel{
    [self.categoryLabel.layer setCornerRadius:5.0f];
    [self.categoryLabel.layer setMasksToBounds:YES];
    [self.categoryLabel setTextColor:[UIColor whiteColor]];
    isAccept = [[edoc.Detail valueForKey:@"data"]valueForKey:@"accept"];
    if([isAccept isEqual:@1]){
       self.acceptStatusImage.image = [UIImage imageNamed:@"mis_check.png"];
        self.acceptButton.enabled = NO;
       [self.acceptButton setBackgroundColor:STATUS_BUTTON_COLOR];
    }
    else{
        self.acceptStatusImage.image = nil;
        [self.acceptButton setBackgroundColor:LIGHT_BLUE_COLOR];
    }
    self.categoryLabel.backgroundColor = [UIColor colorWithRed:0.38 green:0.67 blue:0.80 alpha:1.0];
    [self.priorityLabel.layer setCornerRadius:5.0f];
    
    if([[[edoc.Detail valueForKey:@"data"]valueForKey:@"lblPriorityLevel"]isEqual:@""]){
        self.priorityLabel.text = nil;
    }
    else{
        self.priorityLabel.layer.masksToBounds = YES;
        self.priorityLabel.text = [NSString stringWithFormat:@" %@ ",[[edoc.Detail  valueForKey:@"data"]valueForKey:@"lblPriorityLevel"]];
        self.priorityLabel.textColor = [UIColor whiteColor];
        self.priorityLabel.backgroundColor = [UIColor redColor];
    }
    //
    if([[[edoc.Detail  valueForKey:@"data"]valueForKey:@"lblSecurityLevel"]isEqual:@""]){
        self.securityLevelLabel.text = nil;
    }
    else{
        self.securityLevelLabel.layer.masksToBounds = YES;
        self.securityLevelLabel.text = [NSString stringWithFormat:@" %@ ",[[edoc.Detail  valueForKey:@"data"]valueForKey:@"lblSecurityLevel"]];
        self.securityLevelLabel.textColor = [UIColor whiteColor];
        self.securityLevelLabel.backgroundColor = [UIColor blueColor];
    }
    //
    [self.securityLevelLabel.layer setCornerRadius:5.0f];
    [self.priorityLabel sizeToFit];
    [self.securityLevelLabel sizeToFit];
}
-(void)detail{
    self.sendDate.text = [[edoc.Detail valueForKey:@"data"] valueForKey:@"lblSendDate"];
    self.receiveDate.text = [[edoc.Detail valueForKey:@"data"]valueForKey:@"lblCreateReceiveDate"];
    self.subCategoryLabel.text = [[edoc.Detail valueForKey:@"data"]valueForKey:@"lblTitleHead"];
    self.documentCodeLabel.text = [[edoc.Detail valueForKey:@"data"]valueForKey:@"lblDocumentCode"];
}
- (IBAction)backAction:(id)sender {
     [self.navigationController popViewControllerAnimated: YES];
}
- (IBAction)segmentAction:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        self.container1.hidden = NO;
        self.container2.hidden = YES;
        self.container3.hidden = YES;
    }
    else if (selectedSegment ==1){
        self.container1.hidden = YES;
        self.container2.hidden = NO;
        self.container3.hidden = YES;
    }
    else{
        self.container1.hidden = YES;
        self.container2.hidden = YES;
        self.container3.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getEdocDetail:(NSString*)docID docsubid:(NSString*)docsubid cmuaccount:(NSString*)cmuaccount{
    //global.edocDetail = [[NSMutableDictionary alloc]init];
    edoc.OrderDetailList = [[NSMutableDictionary alloc]init];
    NSString *postStr = [NSString stringWithFormat:EDOC_GET_EDOC_DETAIL,docID,docsubid,cmuaccount];
    
    
    NSDictionary *dictionaryData = [self POST:postStr];
    edoc.Detail = [[NSMutableDictionary alloc]initWithDictionary:dictionaryData];
    NSLog(@"detail::%@",edoc.Detail);
     edoc.OrderDetailList = [[dictionaryData valueForKey:@"data"]valueForKey:@"orderModel"];
    
}
-(id)POST:(NSString*)postStr{
    NSData *postData = [postStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:postStr]];
    
    //set Header
    [request setHTTPMethod:@"POST"];
    [request setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"edoc_token"] forHTTPHeaderField:@"token"];
    NSLog(@"access_token :: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"access_token"]);
    
    [request setHTTPBody:postData];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSData *objectData = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionaryData = [NSJSONSerialization JSONObjectWithData:objectData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    NSLog(@"postStr :%@",postStr);
    NSLog(@"postReturn :%@",dictionaryData);
    return dictionaryData;
}
- (IBAction)sendAction:(id)sender {
}
- (IBAction)replyAction:(id)sender {
}
- (IBAction)acceptAction:(id)sender {
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView==self.detailTable){
        return 1;
    }
    else if(tableView == self.documentTable){
        return 1;
    }
    else return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView==self.detailTable){
        return [topicName count];
    }
    else if(tableView == self.documentTable){
        return 1;
    }
    else return [[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]count];;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView==self.detailTable){
    DetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
    NSLog(@"topicname:%@",topicName);
    cell.detailLabel.text = [NSString stringWithFormat:@"%@ :%@",[topicName objectAtIndex:indexPath.row],[listData objectAtIndex:indexPath.row]];
        return  cell;
    }
    else if(tableView==self.documentTable){
        documentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"documentCell" forIndexPath:indexPath];
        cell.fileNameLabel.text = [NSString stringWithFormat:@"%@",[[[[[edoc.Detail valueForKey:@"data"]valueForKey:@"fileModel"]valueForKey:@"fileList"]objectAtIndex:indexPath.row]valueForKey:@"name"]];
        cell.fileNumberLabel.text = [NSString stringWithFormat:@"%d",indexPath.row+1];
        return cell;
    }
    else{
        OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
        NSString *htmlStrings= [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>จาก &nbsp;</strong>%@<br>%@<br>หมายเหตุ<br>%@</body></html>",
                                14, //Font Size
                                [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"fromName"],
                                [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"fromOrgan"],
                                [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"fromNote"]];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlStrings dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        
        NSString *htmlStrings2= [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>ถึง &nbsp;</strong>%@<br>หมายเหตุ%@</body></html>",
                                 14, //Font Size
                                 [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"toName"],
                                 [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"toNote"]];
        
        NSAttributedString *attributedString2 = [[NSAttributedString alloc]
                                                 initWithData: [htmlStrings2 dataUsingEncoding:NSUnicodeStringEncoding]
                                                 options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                 error: nil
                                                 ];
        
        NSString *htmlStrings3= [NSString stringWithFormat:@"  <html><body style=\"font-size:%ipx\"><strong>วัตถุประสงค์ &nbsp;</strong>%@</body></html>",
                                 14, //Font Size
                                 [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"objective"]];
        
        NSAttributedString *attributedString3 = [[NSAttributedString alloc]
                                                 initWithData: [htmlStrings3 dataUsingEncoding:NSUnicodeStringEncoding]
                                                 options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                 error: nil
                                                 ];
        
        NSString *htmlStrings4= [NSString stringWithFormat:@"<html><body style=\"font-size:%ipx\"><strong>ข้อความ &nbsp;</strong>%@</body></html>",
                                 14, //Font Size
                                 [[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"text"]];
        
        NSAttributedString *attributedString4 = [[NSAttributedString alloc]
                                                 initWithData: [htmlStrings4 dataUsingEncoding:NSUnicodeStringEncoding]
                                                 options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                 error: nil
                                                 ];
        
        
        
        
        
        
        cell.nameLabel.attributedText = attributedString;
        cell.toLabel.attributedText = attributedString2;
        cell.objectiveLabel.attributedText = attributedString3;
        cell.messageLabel.attributedText = attributedString4;
        cell.numberLabel.text = [NSString stringWithFormat:@"%@",[[[edoc.OrderDetailList valueForKey:@"EdocTabOderDetailList"]objectAtIndex:indexPath.row]valueForKey:@"order"]];
        [cell.messageLabel sizeToFit];
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==self.orderTable){
        return 300;
    }
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(tableView==self.documentTable){
        
      edoc.linkFile = [[[[[edoc.Detail valueForKey:@"data"]valueForKey:@"fileModel"]valueForKey:@"fileList"]objectAtIndex:indexPath.row]valueForKey:@"linkFile"];
        
        
    }
    
}
- (IBAction)statusAction:(id)sender {
    NSDictionary *data = [self updateAcceptStatus:edoc.ID docSubID:edoc.SubID cmuaccount:ticket.userName];
    if(![[data valueForKey:@"status"]isEqual:@"error"]){
        self.acceptButton.backgroundColor = STATUS_BUTTON_COLOR;
        self.acceptStatusImage.image = [UIImage imageNamed:@"mis_check.png"];
        self.acceptButton.enabled = NO;
        
    }
}
-(id)updateAcceptStatus:(NSString*)docID docSubID:(NSString*)docSubID cmuaccount:(NSString*)cmuaccount{
    NSString *postStr = [NSString stringWithFormat:EDOC_UPDATE_ACCEPT_STATUS,docID,docSubID,cmuaccount];
    return [self POST:postStr];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    
}

@end
