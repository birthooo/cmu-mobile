//
//  documentWebView.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 16/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "EdocData.h"
@interface documentWebView : CMUNavBarViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property NSString *urlString;
@end
