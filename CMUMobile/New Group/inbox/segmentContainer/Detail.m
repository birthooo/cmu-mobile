//
//  Detail.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 15/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "Detail.h"
#import "DetailCell.h"
@interface Detail ()

@end

@implementation Detail
{
    NSArray *topicName;
    NSArray *listData;
    EdocData *edoc;
    IBOutlet UITableView *detailTable;
}
-(void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    edoc = [EdocData getInstance];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        topicName = [[[edoc.Detail valueForKey:@"data"]valueForKey:@"detailModel"]valueForKey:@"topicName"];
        listData = [[[edoc.Detail valueForKey:@"data"]valueForKey:@"detailModel"]valueForKey:@"listdata"];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            NSLog(@"edocdetail ::: %@",edoc.Detail);
            
            NSLog(@"listdata :%@",listData);
            
        });
    });
   
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
    cell.detailLabel.text = [NSString stringWithFormat:@"%@ :%@",[topicName objectAtIndex:indexPath.row],[listData objectAtIndex:indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
}

@end
