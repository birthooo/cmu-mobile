//
//  documentWebView.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 16/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "documentWebView.h"

@interface documentWebView ()
@property EdocData *edoc;
@end

@implementation documentWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edoc = [EdocData getInstance];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            self.urlString = [self.edoc.linkFile stringByReplacingOccurrencesOfString:@"\\"
                                                                           withString:@"/"];
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
        });
    });
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
