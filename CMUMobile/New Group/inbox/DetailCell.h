//
//  DetailCell.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 15/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;

@end
