//
//  documentCell.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 15/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface documentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *documentNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *fileNumberLabel;

@end
