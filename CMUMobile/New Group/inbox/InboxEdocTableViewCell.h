//
//  InboxEdocTableViewCell.h
//  CMUMobile_DEV
//
//  Created by Tharadol-ITSC on 8/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InboxEdocTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *docCodeLabel;
@property (strong, nonatomic) IBOutlet UILabel *topicLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;


@end
