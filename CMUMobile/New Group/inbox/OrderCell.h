//
//  OrderCell.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 15/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *toLabel;
@property (strong, nonatomic) IBOutlet UILabel *objectiveLabel;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *numberLabel;

@end
