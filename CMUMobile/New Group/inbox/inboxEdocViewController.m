//
//  inboxEdocViewController.m
//  CMUMobile_DEV
//
//  Created by Tharadol-ITSC on 8/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "inboxEdocViewController.h"
#import "InboxEdocTableViewCell.h"
#import "CMUWebServiceManager.h"
@interface inboxEdocViewController ()
@property (strong, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *inboxTable;




@end

@implementation inboxEdocViewController
{
    EdocData *edoc;
    BOOL isAccept;
    UIRefreshControl *refreshControl;
    CMUTicket *ticket;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    edoc = [EdocData getInstance];
    ticket = [[CMUWebServiceManager sharedInstance] ticket];
    refreshControl = [[UIRefreshControl alloc]init];
    [self.inboxTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    NSString *strURL = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/api/EdocNew/GetEdocRecieveTab?cmuaccount=%@&key=null",ticket.userName];
    strURL = [strURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    edoc.Data = [[NSMutableDictionary alloc]init];
    edoc.Data = [self POST:strURL];
    NSLog(@"edddddd :%@",edoc.Data);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[edoc.Data valueForKey:@"data"]count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InboxEdocTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inboxEdoc" forIndexPath:indexPath];
    
    if(![[[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"topic"]isKindOfClass:[NSNull class]]){
        cell.topicLabel.text = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"topic"];
    }else{
        cell.topicLabel.text = @"";
    }
    //
    
    NSString*dateString = [[[[edoc.Data objectForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"sendTime"]substringToIndex:10];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *newDateString = [dateFormat stringFromDate:date];
    
    
    
    cell.timeLabel.text = newDateString;//[NSString stringWithFormat:@"%@",date];
    cell.statusLabel.text = [[[edoc.Data objectForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"textStaus"];
    
    //
    if(![[[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"docCode"]isKindOfClass:[NSNull class]]){
        cell.docCodeLabel.text = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"docCode"];
    }else{
        cell.docCodeLabel.text = @"";
    }
    
    if(![[[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"textStaus"]isKindOfClass:[NSNull class]]){
        cell.statusLabel.text = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"textStaus"];
    }else{
        cell.statusLabel.text = @"";
    }
    
    if(![[[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"sendTime"]isKindOfClass:[NSNull class]]){
        cell.timeLabel.text = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"sendTime"];
    }else{
        cell.timeLabel.text = @"";
    }
    //
    if([[[[edoc.Data objectForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"IsOpened"]isEqual:@0]){
        [cell.docCodeLabel setTextColor:DOC_CODE_LABEL_COLOR];
        //[cell.textStatusLabel setTextColor:[UIColor redColor]];
        [cell.topicLabel setTextColor:[UIColor blackColor]];
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.nextButton setImage:[UIImage imageNamed:@"cmu_next_icon_purple.png"] forState:UIControlStateNormal];
        
    }
    else{
        [cell.docCodeLabel setTextColor:[UIColor darkGrayColor]];
       // [cell.textStatusLabel setTextColor:[UIColor darkGrayColor]];
        [cell.topicLabel setTextColor:[UIColor darkGrayColor]];
        [cell setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [cell.nextButton setImage:[UIImage imageNamed:@"cell_disabled.png"] forState:UIControlStateNormal];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
   
    //edocData.Detail = [[edocData.Data valueForKey:@"data"]objectAtIndex:indexPath.row];
    edoc.ID = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"docID"];
    edoc.SubID = [[[edoc.Data valueForKey:@"data"]objectAtIndex:indexPath.row]valueForKey:@"docSubID"];
    

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = (NSIndexPath *)sender;
    if([segue.identifier isEqual:@"detailSegue"]){
       
        
    }
}

-(id)POST:(NSString*)postStr{
    NSData *postData = [postStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:postStr]];
    
    //set Header
    [request setHTTPMethod:@"POST"];
    [request setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"edoc_token"] forHTTPHeaderField:@"token"];
    NSLog(@"access_token :: %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"edoc_token"]);
    
    [request setHTTPBody:postData];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSData *objectData = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionaryData = [NSJSONSerialization JSONObjectWithData:objectData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    NSLog(@"postStr :%@",postStr);
    NSLog(@"postReturn :%@",dictionaryData);
    return dictionaryData;
}
- (void)refreshTable {
    //TODO: refresh your data
    [refreshControl endRefreshing];
    [self.inboxTable reloadData];
}
-(void)getEdocDetail:(NSString*)docID docsubid:(NSString*)docsubid cmuaccount:(NSString*)cmuaccount{
    edoc.OrderDetailList = [[NSMutableDictionary alloc]init];
    NSString *postStr = [NSString stringWithFormat:EDOC_GET_EDOC_DETAIL,docID,docsubid,cmuaccount];
    NSDictionary *dictionaryData = [self POST:postStr];
    edoc.Detail = [[NSMutableDictionary alloc]initWithDictionary:dictionaryData];
    NSLog(@"detail::%@",edoc.Detail);
    edoc.OrderDetailList = [[dictionaryData valueForKey:@"data"]valueForKey:@"orderModel"];
    
}

@end
