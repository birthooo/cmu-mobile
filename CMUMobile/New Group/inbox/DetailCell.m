//
//  DetailCell.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 15/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "DetailCell.h"

@implementation DetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
