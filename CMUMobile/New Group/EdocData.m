//
//  EdocData.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 10/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "EdocData.h"

@implementation EdocData
static EdocData *instance = nil;

+(EdocData*)getInstance{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [EdocData new];
        }
    }
    return instance;
}
@end
