//
//  EdocData.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 10/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#define EDOC_GET_EDOC_DETAIL @"https://mobileapi.cmu.ac.th/api/EdocNew/getEdocDetail?docID=%@&docsubid=%@&cmuaccount=%@"
#define STATUS_BUTTON_COLOR [UIColor colorWithRed:0.38 green:0.67 blue:0.80 alpha:1.0]
#define PURPLE_COLOR [UIColor colorWithRed:0.35f green:0.16f blue:0.64f alpha:1.0]
#define LIGHT_PURPLE_COLOR [UIColor colorWithRed:0.61 green:0.53 blue:0.80 alpha:1.0]
#define CMU_LOGIN_URL @"https://account.cmu.ac.th/v3/api/validateUser"
#define CMU_PURPLE_COLOR [UIColor colorWithRed:148.0/255.0f green:113.0/255.0f blue:210.0/255.0f alpha:1.0]

#define HOME_COLOR [UIColor colorWithRed:0.02 green:0.46 blue:0.81 alpha:1.0]
#define BLUE_BUTTON_COLOR [UIColor colorWithRed:0.38 green:0.67 blue:0.80 alpha:1.0]
#define DOC_CODE_LABEL_COLOR [UIColor colorWithRed:1.00 green:0.63 blue:0.00 alpha:1.0]
#define LIGHT_BLUE_COLOR [UIColor colorWithRed:0.13 green:0.59 blue:0.95 alpha:1.0]
#define SEARCH_BG_COLOR [UIColor colorWithRed:0.61 green:0.82 blue:0.99 alpha:1.0]



#define EDOC_GET_EDOC_DETAIL @"https://mobileapi.cmu.ac.th/api/EdocNew/getEdocDetail?docID=%@&docsubid=%@&cmuaccount=%@"
#define EDOC_UPDATE_ACCEPT_STATUS @"https://mobileapi.cmu.ac.th/api/EdocNew/updateAcceptStatus?docID=%@&docSubID=%@&cmuaccount=%@"
#define EDOC_EDIT_IS_OPEN @"https://mobileapi.cmu.ac.th/api/EdocNew/editIsOpen?cmuaccount=%@&docSubID=%@"
#define EDOC_REPLY_EDOC @"https://mobileapi.cmu.ac.th/api/EdocNew/replyEdoc?docId=%@&docSubID=%@&cmuaccount=%@&comment=%@"
#define EDOC_GET_EDOC_COUNT_NUMBER @"https://mobileapi.cmu.ac.th/api/EdocNew/getEdocCountNumber?cmuaccount=%@"
#define EDOC_DELETE_EDOC_RECIEVE @"https://mobileapi.cmu.ac.th/api/EdocNew/delteEdocRecive?cmuaccount=%@"
#define EDOC_GET_SEND_TAB @"https://mobileapi.cmu.ac.th/api/EdocNew/getSendTab?cmuaccount=%@&key=%@"

@interface EdocData : NSObject

@property NSMutableDictionary *selectedEdocData;

@property NSMutableDictionary *Data;
@property NSString *ID;
@property NSString *SubID;
@property NSMutableDictionary *Detail;
@property NSMutableDictionary *OrderDetailList;
@property NSMutableDictionary *selectedEdoc;
@property NSString *linkFile;
+(EdocData *)getInstance;




@end

