//
//  NewEdocViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 1/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import "NewEdocViewController.h"
#import "InboxEdocTableViewCell.h"
#import "CMUWebServiceManager.h"
@interface NewEdocViewController ()
@property (strong, nonatomic) IBOutlet UIView *container1;
@property (strong, nonatomic) IBOutlet UIView *container2;
@property (strong, nonatomic) IBOutlet UIView *container3;
@property (strong, nonatomic) IBOutlet UITabBar *tabbar;
@property (strong, nonatomic) IBOutlet UITabBarItem *barItem1;
@property (strong, nonatomic) IBOutlet UITabBarItem *barItem2;
@property (strong, nonatomic) IBOutlet UITabBarItem *barItem3;
@property(nonatomic, strong) CMUTicket *ticket;
@end

@implementation NewEdocViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.container1.hidden = NO;
    self.container2.hidden = YES;
    self.container3.hidden = YES;
    
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if([item.title isEqual:@"เอกสารเข้า"]){
        NSLog(@"เอกสารเข้า");
        self.container1.hidden = NO;
        self.container2.hidden = YES;
        self.container3.hidden = YES;
    }
    else if ([item.title isEqual:@"เอกสารเผยแพร่"]){
        NSLog(@"เอกสารเผยแพร่");
        self.container1.hidden = YES;
        self.container2.hidden = NO;
        self.container3.hidden = YES;
    }
    else{
        NSLog(@"เอกสารส่ง");
        self.container1.hidden = YES;
        self.container2.hidden = YES;
        self.container3.hidden = NO;
    }
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
