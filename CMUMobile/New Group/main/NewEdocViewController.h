//
//  NewEdocViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 1/11/2560 BE.
//  Copyright © 2560 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"

@interface NewEdocViewController : CMUNavBarViewController<UITabBarDelegate>

@end
