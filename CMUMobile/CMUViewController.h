//
//  CMUViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/9/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "global.h"

//  Edited by Satianpong Yodnin on 9/9/2558 BE.
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface CMUViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIScrollView *scrollViewBase;
@property(nonatomic, weak) IBOutlet UIView *contentViewBase;

//- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer;
- (void)updatePresentState;
- (void)showHUDLoading;
- (void)dismisHUD;
- (void)alert:(NSString *)message;

//login/logout notification
- (void)loginChangedNotification:(NSNotification *) notification;
@end
