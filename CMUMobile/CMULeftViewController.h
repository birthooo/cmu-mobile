//
//  CMULeftViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/16/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUNewsTypePopupView.h"
@interface CMULeftViewController : CMUNavBarViewController<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic, unsafe_unretained)id<CMUNewsTypePopupViewDelegate> delegate;
@end
