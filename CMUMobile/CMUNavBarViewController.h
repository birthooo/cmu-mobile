//
//  CMUBaseViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUViewController.h"
#import "CMULoginViewController.h"
#import "BBBadgeBarButtonItem.h"

@interface CMUNavBarViewController : CMUViewController<CMULoginViewControllerDelegate, UIActionSheetDelegate>
@property(strong, nonatomic)UIBarButtonItem *cmuButtonItem;
@property(strong, nonatomic)UIButton *notiButton;


@property(strong, nonatomic)UIBarButtonItem *menuButtonItem;
@property(strong, nonatomic)BBBadgeBarButtonItem *notiButtonItem;
@property(strong, nonatomic)UIButton *loginButton;
@property(strong, nonatomic)UIBarButtonItem *loginButtonItem;
@property(strong, nonatomic)UIBarButtonItem *separatorButtonItem;

- (void)btnLeftViewClicked:(id) sender;
- (void)btnHomeClicked:(id) sender;
- (void)btnNotiClicked:(id) sender;
- (void)btnLoginClicked:(id) sender;
- (void)updatePresentState;

//login logout
- (void)doLogin;
- (void)doLogout;
- (void)loginViewControllerDidLogin:(CMULoginViewController *) loginViewController;
- (void)loginViewControllerDidCancel:(CMULoginViewController *) loginViewController;
@end
