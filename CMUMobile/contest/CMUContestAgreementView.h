//
//  CMUContestAgreementView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CMUContestAgreementView;
@protocol CMUContestAgreementViewDelegate <NSObject>
- (void)contestAgreementDidAgree:(CMUContestAgreementView *) view;
@end

@interface CMUContestAgreementView : UIView
@property(nonatomic, weak)IBOutlet UIWebView *webview;
@property(nonatomic, weak)id<CMUContestAgreementViewDelegate> delegate;
@end
