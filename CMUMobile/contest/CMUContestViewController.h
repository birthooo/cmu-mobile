//
//  CMUContestViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUContestGalleryViewController.h"
#import "CMUContestGalleryDetailViewController.h"
#import "CMUContestShotViewController.h"
#import "CMUContestShotStep2ViewController.h"
#import "CMUContestHomeGalleryViewController.h"
#import "CMUContestHomeViewController.h"
#import "CMUContestWebViewController.h"
#import "CMULoginViewController.h"
#import "CMUContestAgreementView.h"
#import "CMUITSCInformationViewController.h"

@interface CMUContestViewController : CMUNavBarViewController<UIPageViewControllerDelegate, CMULoginViewControllerDelegate, CMUContestShotViewControllerDelegate, CMUContestShotStep2ViewControllerDelegate, CMUContestGalleryViewControllerDelegate, CMUContestHomeGalleryViewControllerDelegate, CMUContestGalleryDetailViewControllerDelegate,CMUContestAgreementViewDelegate>

@end
