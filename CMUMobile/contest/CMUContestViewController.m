//
//  CMUContestViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUSocial.h"

typedef enum
{
    CMU_CONTEST_TAB_NONE = 0,
    CMU_CONTEST_TAB_GALLERY = 1,
    CMU_CONTEST_TAB_SHOT = 2,
    CMU_CONTEST_TAB_HOME = 3,
    CMU_CONTEST_TAB_AWARD = 4,
    CMU_CONTEST_TAB_ACHEIVEMENT = 5
} CMU_CONTEST_TAB;

@interface CMUContestViewController ()
@property(nonatomic, unsafe_unretained) CMU_CONTEST_TAB currentTab;
@property(nonatomic, strong)UIViewController *currenViewController;
@property(nonatomic, weak) IBOutlet UIView *pageControllerContainer;
@property(nonatomic, strong) UIPageViewController *pageController;
@property(nonatomic, weak) IBOutlet UIButton *btnGallery;
@property(nonatomic, weak) IBOutlet UIButton *btnShot;
@property(nonatomic, weak) IBOutlet UIButton *btnHome;
@property(nonatomic, weak) IBOutlet UIButton *btnAward;
@property(nonatomic, weak) IBOutlet UIButton *btnAcheivement;
@property(nonatomic, weak) IBOutlet CMUContestAgreementView *agreementView;

@property(nonatomic, strong) CMUContestGalleryViewController *galleryVC;
@property(nonatomic, strong) CMUContestHomeGalleryViewController *homeGalleryVC;
@property(nonatomic, unsafe_unretained)CMU_CONTEST_TAB penddingGoTab;
@end

#define KEY_CMU_PHOTO_CONTEST @"KEY_CMU_PHOTO_CONTEST"

@implementation CMUContestViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        self.currentTab = CMU_CONTEST_TAB_GALLERY;
        self.penddingGoTab = CMU_CONTEST_TAB_NONE;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self updatePresentState];
    
    //user agreement
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *data = [defaults objectForKey:KEY_CMU_PHOTO_CONTEST];
    BOOL showAgreement = YES;
    if(data)
    {
        showAgreement = !data.boolValue;
    }
    if(showAgreement){
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:CMU_PHOTO_CONTEST_AGREEMENT]];
        self.agreementView.webview.scalesPageToFit = YES;
        [self.agreementView.webview loadRequest:request];
        
        self.agreementView.delegate = self;
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             self.agreementView.alpha = 1.0f;
         }completion:^(BOOL finished){
             
         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(!self.pageController)
    {
        self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        self.pageController.dataSource = self;
        self.pageController.delegate = self;

        [[self.pageController view] setFrame:[self.pageControllerContainer bounds]];
        [self addChildViewController:self.pageController];
        [[self pageControllerContainer] addSubview:[self.pageController view]];
        [self.pageController didMoveToParentViewController:self];
        self.currentTab = CMU_CONTEST_TAB_GALLERY;
        [self updatePresentState];
    }
}

- (void)updatePresentState{
    [super updatePresentState];
    
    //tab color
    UIColor *selectedColor = [UIColor colorWithRed:148/255.0 green:84/255.0 blue:243/255.0 alpha:1.0];
    UIColor *unselectedColor = [UIColor colorWithRed:27/255.0 green:20/255.0 blue:30/255.0 alpha:1.0];
    [self.btnGallery setBackgroundColor:self.currentTab == CMU_CONTEST_TAB_GALLERY?selectedColor:unselectedColor];
    [self.btnShot setBackgroundColor:self.currentTab == CMU_CONTEST_TAB_SHOT?selectedColor:unselectedColor];
    [self.btnHome setBackgroundColor:self.currentTab == CMU_CONTEST_TAB_HOME?selectedColor:unselectedColor];
    [self.btnAward setBackgroundColor:self.currentTab == CMU_CONTEST_TAB_AWARD?selectedColor:unselectedColor];
    [self.btnAcheivement setBackgroundColor:self.currentTab == CMU_CONTEST_TAB_ACHEIVEMENT?selectedColor:unselectedColor];
}

- (IBAction)onTabBtnTap:(id)sender{
    if(sender == self.btnGallery){
        if(self.currentTab != CMU_CONTEST_TAB_GALLERY){
            self.currentTab = CMU_CONTEST_TAB_GALLERY;
            self.penddingGoTab = CMU_CONTEST_TAB_NONE;
        }
    }
    
    if(sender == self.btnShot){
        if([[CMUWebServiceManager sharedInstance] isLogin]){
            if(self.currentTab != CMU_CONTEST_TAB_SHOT){
                self.currentTab = CMU_CONTEST_TAB_SHOT;
                self.penddingGoTab = CMU_CONTEST_TAB_NONE;
            }
        }else{
            self.penddingGoTab = CMU_CONTEST_TAB_SHOT;
            CMULoginViewController *loginViewController = [[CMULoginViewController alloc] init];
            loginViewController.delegate = self;
            [self.navigationController presentViewController:loginViewController animated:YES completion:nil];

        }
    }

    if(sender == self.btnHome){
        if([[CMUWebServiceManager sharedInstance] isLogin]){
            if(self.currentTab != CMU_CONTEST_TAB_HOME){
                self.currentTab = CMU_CONTEST_TAB_HOME;
                self.penddingGoTab = CMU_CONTEST_TAB_NONE;
            }
        }else{
            self.penddingGoTab = CMU_CONTEST_TAB_HOME;
            CMULoginViewController *loginViewController = [[CMULoginViewController alloc] init];
            loginViewController.delegate = self;
            [self.navigationController presentViewController:loginViewController animated:YES completion:nil];

        }
    }

    if(sender == self.btnAward){
        if(self.currentTab != CMU_CONTEST_TAB_AWARD){
            self.currentTab = CMU_CONTEST_TAB_AWARD;
            self.penddingGoTab = CMU_CONTEST_TAB_NONE;
        }
    }

    if(sender == self.btnAcheivement){
        if(self.currentTab != CMU_CONTEST_TAB_ACHEIVEMENT){
            self.currentTab = CMU_CONTEST_TAB_ACHEIVEMENT;
            self.penddingGoTab = CMU_CONTEST_TAB_NONE;
        }
    }
}

- (void)setCurrentTab:(CMU_CONTEST_TAB)currentTab{

    _currentTab = currentTab;
    
    if(self.currentTab == CMU_CONTEST_TAB_GALLERY){
        self.galleryVC = [[CMUContestGalleryViewController alloc] init];
        self.galleryVC.delegate = self;
        self.currenViewController = self.galleryVC;
    }else if(self.currentTab == CMU_CONTEST_TAB_SHOT){
        self.currenViewController = [[CMUContestShotViewController alloc] init];
        ((CMUContestShotViewController*)self.currenViewController).delegate = self;
    }else if(self.currentTab == CMU_CONTEST_TAB_HOME){
        self.homeGalleryVC = [[CMUContestHomeGalleryViewController alloc] init];
        self.homeGalleryVC.delegate = self;
        self.currenViewController = self.homeGalleryVC;
    }else if(self.currentTab == CMU_CONTEST_TAB_AWARD){
        self.currenViewController = [[CMUContestWebViewController alloc] init];
        ((CMUContestWebViewController*)self.currenViewController).url = CMU_PHOTO_PROMOTE_ITSC;
    }else if(self.currentTab == CMU_CONTEST_TAB_ACHEIVEMENT){
        self.currenViewController = [[CMUITSCInformationViewController alloc] init];
    }
    
    [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self updatePresentState];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -overide
- (void)loginChangedNotification:(NSNotification *) notification
{
    [super loginChangedNotification:notification];
    if(![[CMUWebServiceManager sharedInstance] isLogin]){
        if(self.currentTab == CMU_CONTEST_TAB_SHOT || self.currentTab == CMU_CONTEST_TAB_HOME){
            self.currentTab = CMU_CONTEST_TAB_GALLERY;
        }
    }
}


#pragma mark -CMULoginViewControllerDelegate;

- (void)loginViewControllerDidLogin:(CMULoginViewController *) loginViewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:CMU_LOGIN_CHANGED_NOTIFICATION object:self];
    if(self.penddingGoTab != CMU_CONTEST_TAB_NONE){
        self.currentTab = self.penddingGoTab;
        self.penddingGoTab = CMU_CONTEST_TAB_NONE;
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)loginViewControllerDidCancel:(CMULoginViewController *) loginViewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark -CMUContestShotViewControllerDelegate
- (void) contestShotViewController:(CMUContestShotViewController *) vc didPickImage:(UIImage *) image
{
    CMUContestShotStep2ViewController *step2VC = [[CMUContestShotStep2ViewController alloc] init];
    step2VC.image = image;
    step2VC.delegate = self;
    [self.pageController setViewControllers:[NSArray arrayWithObject:step2VC] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark -CMUContestShotStep2ViewControllerDelegate
- (void) contestShotStep2ViewControllerDidCancel:(CMUContestShotStep2ViewController *) vc
{
    _currentTab = CMU_CONTEST_TAB_SHOT;
    self.currenViewController = [[CMUContestShotViewController alloc] init];
    ((CMUContestShotViewController*)self.currenViewController).delegate = self;
    [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];

    [self updatePresentState];
}

- (void) contestShotStep2ViewController:(CMUContestShotStep2ViewController *) vc didUploadCompletedWithResultPhotoId:(int) photoId
{
    _currentTab = CMU_CONTEST_TAB_HOME;
    self.homeGalleryVC = [[CMUContestHomeGalleryViewController alloc] init];
    self.homeGalleryVC.delegate = self;
    self.currenViewController = self.homeGalleryVC;
    [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self updatePresentState];
}

#pragma mark CMUContestGalleryViewControllerDelegate
- (void) contestGalleryViewController:(CMUContestGalleryViewController*) vc didSelectPhotoId:(int) photoId{
    CMUContestGalleryDetailViewController *detailVC = [[CMUContestGalleryDetailViewController alloc] init];
    detailVC.photoId = photoId;
    detailVC.delegate = self;
    detailVC.vcToBack = self.galleryVC;
    [self.pageController setViewControllers:[NSArray arrayWithObject:detailVC] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark CMUContestHomeGalleryViewControllerDelegate
- (void) contestHomeGalleryViewController:(CMUContestHomeGalleryViewController*) vc didSelectPhotoId:(int) photoId
{
    CMUContestGalleryDetailViewController *detailVC = [[CMUContestGalleryDetailViewController alloc] init];
    detailVC.photoId = photoId;
    detailVC.delegate = self;
    detailVC.vcToBack = self.homeGalleryVC;
    [self.pageController setViewControllers:[NSArray arrayWithObject:detailVC] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark CMUContestGalleryDetailViewControllerDelegate
- (void) contestGalleryDetailViewControllerDidBack:(CMUContestGalleryDetailViewController*) vc
{
    if(vc.vcToBack == self.galleryVC){
        _currentTab = CMU_CONTEST_TAB_GALLERY;
        self.currenViewController = self.galleryVC;
    }else if(vc.vcToBack == self.homeGalleryVC){
        _currentTab = CMU_CONTEST_TAB_HOME;
        self.currenViewController = self.homeGalleryVC;
    }
    
    [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    
    [self updatePresentState];
}

- (void) contestGalleryDetailViewControllerDidDelete:(CMUContestGalleryDetailViewController*) vc
{
    if(vc.vcToBack == self.galleryVC){
        _currentTab = CMU_CONTEST_TAB_GALLERY;
        self.galleryVC = [[CMUContestGalleryViewController alloc] init];
        self.galleryVC.delegate = self;
        self.currenViewController = self.galleryVC;
        
    }else if(vc.vcToBack == self.homeGalleryVC){
        _currentTab = CMU_CONTEST_TAB_HOME;
        self.homeGalleryVC = [[CMUContestHomeGalleryViewController alloc] init];
        self.homeGalleryVC.delegate = self;
        self.currenViewController = self.homeGalleryVC;
        
    }
    
    [self.pageController setViewControllers:[NSArray arrayWithObject:self.currenViewController] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    
    [self updatePresentState];
}

#pragma mark CMUContestAgreementViewDelegate
- (void)contestAgreementDidAgree:(CMUContestAgreementView *) view
{
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         self.agreementView.alpha = 0.0f;
     }completion:^(BOOL finished){
         
     }];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:KEY_CMU_PHOTO_CONTEST];
    [defaults synchronize];
}

@end
