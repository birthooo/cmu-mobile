//
//  CMUContestAgreementView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/2/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestAgreementView.h"

@interface CMUContestAgreementView()
@property(nonatomic, weak)IBOutlet UIView *containerView;
@property(nonatomic, weak)IBOutlet UIView *bottomContainerView;

@end

@implementation CMUContestAgreementView
- (void)customInit
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUContestAgreementView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    
    contentView.frame = self.bounds;
    
    self.backgroundColor = [UIColor clearColor];
    self.containerView.layer.cornerRadius = 10.0f;
    self.containerView.layer.masksToBounds = YES;
    self.bottomContainerView.layer.cornerRadius = 10.0f;
    self.bottomContainerView.layer.masksToBounds = YES;
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (IBAction)agreeClicked:(id)sender
{
    if(_delegate &&[ _delegate respondsToSelector:@selector(contestAgreementDidAgree: )])
    {
        [_delegate contestAgreementDidAgree:self];
    }
    
}

@end
