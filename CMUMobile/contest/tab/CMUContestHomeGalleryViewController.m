//
//  CMUContestHomeGalleryViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestHomeGalleryViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUStringUtils.h"
#import "CMUContestGalleryCell.h"
#import "GMGridViewLayoutStrategies.h"

@interface CMUContestHomeGalleryViewController ()
//@property(nonatomic, strong) ODRefreshControl *refreshControl;
@property(nonatomic, strong)IBOutlet GMGridView *gridView;

@property(nonatomic, unsafe_unretained)BOOL resetFeedList;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSArray *feedList;

@property(nonatomic, unsafe_unretained) BOOL loadingMore;
//no footer view support
//@property(nonatomic, strong) UIActivityIndicatorView *loadMoreSpinner;

@end

@implementation CMUContestHomeGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //pull refresh
//    self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.gridView];
//    self.gridView.alwaysBounceVertical = YES;
//    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
//    self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    self.loadMoreSpinner.frame = CGRectMake(0, 0, self.gridView.frame.size.width, 44);
    
    //grid view
    NSInteger spacing = 0;
    _gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _gridView.backgroundColor = [UIColor clearColor];
    
    _gridView.minimumPressDuration = 0.5;
    _gridView.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutVertical];
    //_gridView.pagingEnabled = YES;
    _gridView.showsHorizontalScrollIndicator = NO;
    _gridView.showsVerticalScrollIndicator = NO;
    //_gridView.style = GMGridViewStyleSwap;
    _gridView.style = GMGridViewStylePush;
    _gridView.itemSpacing = 0;
    _gridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gridView.centerGrid = NO;
    _gridView.actionDelegate = self;
    _gridView.dataSource = self;
    _gridView.mainSuperView = self.view;
    
    //scrollview delegate
    _gridView.delegate = self;
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] photoContestGetHome:self success:^(id result) {
        CMUPhotoContestModel *photoContestModel = (CMUPhotoContestModel *)result;
        if(![CMUStringUtils isEmpty:photoContestModel.refreshURL])
        {
            self.refreshURL = photoContestModel.refreshURL;
        }
        if(![CMUStringUtils isEmpty:photoContestModel.loadMoreURL])
        {
            self.loadMoreURL = photoContestModel.loadMoreURL;
        }
        [self populateFeedList:photoContestModel reload:YES];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self dismisHUD];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updatePresentState
{
    [super updatePresentState];
}

- (IBAction)bannerTab:(id)sender
{
    CMUWebViewController *vc = [[CMUWebViewController alloc] init];
    vc.url = CMU_PHOTO_CONTEST_DETAIL;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)populateFeedList:(CMUPhotoContestModel *) photoContestModel reload:(BOOL)reload
{
    //check to reset feed list
    BOOL scrollToTop = self.resetFeedList;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    if(!self.resetFeedList)
    {
        [tempFeedList addObjectsFromArray:self.feedList];
    }
    self.resetFeedList = NO;
    
    //add Photo feed to feed list
    
    NSMutableArray *newFeedList = [[NSMutableArray alloc] init];
    for(CMUPhotoContestFeed *newFeed in photoContestModel.photoFeedList)
    {
        if(![tempFeedList containsObject:newFeed])
        {
            [tempFeedList addObject:newFeed];
            [newFeedList addObject:newFeed];
        }
    }
    
    //perform sort
    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        CMUPhotoContestFeed *first = (CMUPhotoContestFeed*)a;
        CMUPhotoContestFeed *second = (CMUPhotoContestFeed*)b;
        return [second.time compare:first.time];
    }];
    
    self.feedList = sortedFeedList;
    
    if(reload)
    {
        [self.gridView reloadData];
    }
    else
    {
        for(int i=0; i < self.feedList.count; i++){
            CMUPhotoContestFeed *item = [self.feedList objectAtIndex:i];
            if([newFeedList containsObject:item]){
                [self.gridView insertObjectAtIndex:i withAnimation:GMGridViewItemAnimationFade];
            }
        }
    }
    //
    if(scrollToTop)
    {
        [self.gridView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark GMGridViewDataSource

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return self.feedList.count;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    CGFloat cellWidth = gridView.frame.size.width / 3.0;
    return CGSizeMake(cellWidth, cellWidth);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CMUContestGalleryCell *cell = (CMUContestGalleryCell*)[gridView dequeueReusableCell];
    
    if (!cell)
    {
        CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
        cell = [[CMUContestGalleryCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }
    CMUPhotoContestFeed *photoContestFeed = [self.feedList objectAtIndex:index];
    cell.photoContestFeed = photoContestFeed;
    
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark GMGridViewActionDelegate
- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    CMUPhotoContestFeed *photoContestFeed = [self.feedList objectAtIndex:position];
    if(_delegate && [_delegate respondsToSelector:@selector(contestHomeGalleryViewController:didSelectPhotoId:)]){
        [_delegate contestHomeGalleryViewController:self didSelectPhotoId:photoContestFeed.photoContestFeedId];
    }
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    
}

//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
//    CGPoint offset = aScrollView.contentOffset;
//    CGRect bounds = aScrollView.bounds;
//    CGSize size = aScrollView.contentSize;
//    UIEdgeInsets inset = aScrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
//    
//    float reload_distance = 10;
//    if(y > h + reload_distance) {
//        if(self.loadMoreURL)
//        {
//            if(self.loadingMore)
//            {
//                return;
//            }
//            self.loadingMore = YES;
//            [self updatePresentState];
//            [[CMUWebServiceManager sharedInstance] photoContestGetPhotoByURL:self url:self.loadMoreURL success:^(id result) {
//                CMUPhotoContestModel *photoContestModel = (CMUPhotoContestModel *)result;
//                if(![CMUStringUtils isEmpty:photoContestModel.refreshURL])
//                {
//                    self.refreshURL = photoContestModel.refreshURL;
//                }
//                if(![CMUStringUtils isEmpty:photoContestModel.loadMoreURL])
//                {
//                    self.loadMoreURL = photoContestModel.loadMoreURL;
//                }
//                [self populateFeedList:photoContestModel reload:NO];
//                self.loadingMore = NO;
//                [self dismisHUD];
//            } failure:^(NSError *error) {
//                self.loadingMore = NO;
//                [self updatePresentState];
//            }];
//        }
//    }
//}


#pragma mark Dropview
//- (void)dropViewDidBeginRefreshing:(id) sender
//{
//    if(self.refreshURL)
//    {
//        [[CMUWebServiceManager sharedInstance] photoContestGetPhotoByURL:self url:self.refreshURL success:^(id result) {
//            CMUPhotoContestModel *photoContestModel = (CMUPhotoContestModel *)result;
//            if(![CMUStringUtils isEmpty:photoContestModel.refreshURL])
//            {
//                self.refreshURL = photoContestModel.refreshURL;
//            }
//            [self populateFeedList:photoContestModel reload:YES];
//            [self.refreshControl endRefreshing];
//        } failure:^(NSError *error) {
//            [self.refreshControl endRefreshing];
//        }];
//    }
//    else
//    {
//        [self.refreshControl endRefreshing];
//    }
//}

@end
