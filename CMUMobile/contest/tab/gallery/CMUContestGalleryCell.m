//
//  CMUContestGalleryCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/27/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestGalleryCell.h"
#import "UIImageView+WebCache.h"

@interface CMUContestGalleryCell()
@property(nonatomic, strong)IBOutlet UIImageView *imageView;
@end

@implementation CMUContestGalleryCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUContestGalleryCell" owner:self options:nil];
        UIView *contentView = [nibObjects objectAtIndex:0];
        self.contentView = contentView;
    }
    return self;
}

-(void)setPhotoContestFeed:(CMUPhotoContestFeed *)photoContestFeed
{
    _photoContestFeed = photoContestFeed;
    [self.imageView setImageWithURL: [NSURL URLWithString: self.photoContestFeed.cropPath]];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
