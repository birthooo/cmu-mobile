//
//  CMUContestGalleryDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/27/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestGalleryDetailViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUPhotoContestModel.h"
#import "CMUUIUtils.h"
#import "UIImageView+WebCache.h"
#import "CMUSocial.h"
#import "CMUFormatUtils.h"
#import "CMUStringUtils.h"
#import "global.h"
#import "UIAlertView+Blocks.h"

@interface CMUContestGalleryDetailViewController ()
@property(nonatomic, weak)IBOutlet UILabel *lblHeader;
@property(nonatomic, weak)IBOutlet UIImageView *avatarImageView;
@property(nonatomic, weak)IBOutlet UILabel *lblName;
@property(nonatomic, weak)IBOutlet UIImageView *imageView;
@property(nonatomic, weak)IBOutlet UILabel *lblCaption;
@property(nonatomic, weak)IBOutlet UIButton *btnLike;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *btnLikeWidth;
@property(nonatomic, weak)IBOutlet UIButton *btnShareFacebook;
@property(nonatomic, weak)IBOutlet UIButton *btnReport;
@property(nonatomic, weak)IBOutlet UIButton *btnDelete;
@property(nonatomic, weak)IBOutlet UILabel *lblLikeCount;
@property(nonatomic, weak)IBOutlet UILabel *lblDate;

@property(nonatomic, strong) CMUPhotoContestFeedDetail *detail;
@property(nonatomic, unsafe_unretained)BOOL isLike;

@property(nonatomic, weak)IBOutlet CMUDialogView *replyView;
@property(nonatomic, weak)IBOutlet NSLayoutConstraint *replyViewBottom;
@end

@implementation CMUContestGalleryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.contentViewBase.alpha = 0.0;
    [[CMUWebServiceManager sharedInstance] photoContestGetPhotoDetail:self photoID:self.photoId success:^(id result) {
        self.detail = (CMUPhotoContestFeedDetail *) result;
        [self loadFromModel];
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             self.contentViewBase.alpha = 1.0f;
         }completion:^(BOOL finished){
             
         }];
        
        [self.view setNeedsLayout];
        [self updatePresentState];
    } failure:^(NSError *error) {
        [self alert:CMU_ERROR_READ_DATA];
    }];
    
    BOOL isLogin = [[CMUWebServiceManager sharedInstance] isLogin];
    if(isLogin)
    {
        [[CMUWebServiceManager sharedInstance] photoContestIsLike:self photoID:self.photoId success:^(id result) {
            self.isLike = [result boolValue];
            [self updatePresentState];
        } failure:^(NSError *error) {
            
        }];
    }
    
    [self.replyView.btnOK setTitle:@"แจ้งภาพ" forState:UIControlStateNormal];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //corrected size scrollview content size
    CGFloat height = 0;
    UIFont *font = nil;
    if(IS_IPHONE){
        height = 500 - 17 - 320;//nib label height, nib image height
        font = [UIFont systemFontOfSize:14.0]; //according to iphone nib
    }else{
        height = 530 - 24 - 320;//nib label height, nib image height
        font = [UIFont systemFontOfSize:20.0]; //according to iphone nib
    }
    
    height += self.view.frame.size.width; //image
    
    CGFloat labelWidth = self.view.frame.size.width - (8 + 8); // autolayput constraint (8,8
    
    CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:self.detail.detail withFont:font];
    height+= size.height;
    //}
    CGSize contentViewBaseSize = self.scrollViewBase.frame.size;
    contentViewBaseSize.height =  height;
    self.contentViewBase.frame = CGRectMake(0, 0, contentViewBaseSize.width, contentViewBaseSize.height);
    [self.scrollViewBase setContentSize:contentViewBaseSize];
    [self.contentViewBase layoutSubviews];
}

- (void)loadFromModel{
    
    //load detail
    self.lblName.text = self.detail.name;
    [self.imageView setImageWithURL: [NSURL URLWithString: self.detail.path]];
    self.lblCaption.text = self.detail.detail;
    self.lblLikeCount.text = [NSString stringWithFormat:@"%i likes", self.detail.nVote];
    self.lblDate.text = [CMUFormatUtils formatDateMediumStyle:self.detail.dtime locale:[NSLocale currentLocale]];
}

- (void)updatePresentState
{
    [super updatePresentState];
    BOOL isLogin = [[CMUWebServiceManager sharedInstance] isLogin];
    self.btnLike.hidden = !isLogin;
    self.btnLikeWidth.constant = self.btnLike.hidden? 0: (IS_IPAD?100:80);
    
    CMUWebServiceManager *ws = [CMUWebServiceManager sharedInstance];
    BOOL isMyPhoto = NO;
    if(self.detail){
        NSString *username = ws.ticket.userName;
        NSString *fullUserName = [NSString stringWithFormat:@"%@@cmu.ac.th", username];
        isMyPhoto = [[fullUserName lowercaseString] isEqualToString:self.detail.account];
    }
    
    self.btnReport.hidden = !isLogin || isMyPhoto;
    self.btnDelete.hidden = !isMyPhoto;
    
    [self.btnLike setTitleColor:self.isLike?[UIColor colorWithRed:200/255.0 green:0/255.0 blue:8/255.0 alpha:1.0]:[UIColor colorWithRed:172/255.0 green:172/255.0 blue:172/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    [self.btnLike setImage:self.isLike?[UIImage imageNamed:@"contest_like_selected.png"]:[UIImage imageNamed:@"contest_like_unselected.png"] forState:UIControlStateNormal];
}
- (void) keyboardWillHide:(NSNotification *)notification
{
    self.replyViewBottom.constant = 0;
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.replyViewBottom.constant = keyboardSize.height - (IS_IPAD? 60 : 40); //tabbar
}

- (IBAction)backClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(contestGalleryDetailViewControllerDidBack:)]){
        [_delegate contestGalleryDetailViewControllerDidBack:self];
    }
}

- (IBAction)btnFacebookClicked:(id)sender
{
    NSString *photoUrl = [NSString stringWithFormat:CMU_PHOTO_CONTEST_SHARE_FB_URL, self.detail.photoContestFeedDetailId];
    [CMUSocial shareFacebookWithTitle:@"Digital Life : Digital University@CMU" url:photoUrl desc:self.detail.detail pictureUrl:self.detail.path];
}

- (IBAction)btnLikeClicked:(id)sender
{
    if(!self.isLike)
    {
        [self.btnLike setTransform:CGAffineTransformMakeScale(0.8, 0.8)];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations: ^{
                             [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                             [self.btnLike setAlpha:0.7];
                         }
                         completion: ^(BOOL finished) {
                             [UIView animateWithDuration:0.2
                                                   delay:0.1
                                                 options: UIViewAnimationOptionCurveEaseIn
                                              animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                                  [self.btnLike setAlpha:1.0]; }
                                              completion: ^(BOOL finished) {
                                              }];
                         }];
        
        [UIView animateWithDuration:0.2
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseOut
                         animations: ^{
                             [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                             [self.btnLike setAlpha:0.7];
                         }
                         completion: ^(BOOL finished) {
                             [UIView animateWithDuration:0.2
                                                   delay:0.1
                                                 options: UIViewAnimationOptionCurveEaseIn
                                              animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                                  [self.btnLike setAlpha:1.0]; }
                                              completion: ^(BOOL finished) {
                                              }];
                         }];
        
        self.detail.nVote++;
        self.lblLikeCount.text = [NSString stringWithFormat:@"%i likes", self.detail.nVote];
        self.isLike = YES;
        [self updatePresentState];
        [[CMUWebServiceManager sharedInstance] photoContestAddLike:self photoID:self.detail.photoContestFeedDetailId success:^(id result) {
            
        } failure:^(NSError *error) {
            
        }];
    }
    
}

- (IBAction)btnReportClicked:(id)sender
{
    __weak CMUContestGalleryDetailViewController *weakSelf = self;
    weakSelf.replyView.delegate = self;
    weakSelf.replyView.textView.text = @"";
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         weakSelf.replyView.alpha = 1.0f;
     }completion:^(BOOL finished){
         [weakSelf.replyView.textView becomeFirstResponder];
     }];
}

- (IBAction)btnDeleteClicked:(id)sender
{
    __weak typeof(self) weakSelf = self;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:CMU_CONFIRM_DELETE_DATA
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [self showHUDLoading];
            [[CMUWebServiceManager sharedInstance] photoContestDelete:weakSelf photoId:self.detail.photoContestFeedDetailId success:^(id result) {
                [weakSelf dismisHUD];
                
                if(_delegate && [_delegate respondsToSelector:@selector(contestGalleryDetailViewControllerDidDelete:)]){
                    [_delegate contestGalleryDetailViewControllerDidDelete:weakSelf];
                }
                
            } failure:^(NSError *error) {
                [weakSelf dismisHUD];
                [weakSelf alert:CMU_ERROR_DELETE_DATA];
            }];
        }
    };
    [alert show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -CMUMISEdocReplyViewDelegate
- (void)cmuDialogView:(CMUDialogView *)view didOK:(NSString *) text
{
    if([CMUStringUtils isEmpty:text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                        message:@"กรุณากรอกข้อความการแจ้งภาพไม่เหมาะสม"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == alertView.firstOtherButtonIndex) {
                
            } else if (buttonIndex == alertView.cancelButtonIndex) {
                
            }
        };
        [alert show];
        
        return;
    }
    [self.view endEditing:YES];
    
    __weak CMUContestGalleryDetailViewController *weakSelf = self;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] photoContestReport:self photoId:self.detail.photoContestFeedDetailId comment:text success:^(id result) {
        [weakSelf dismisHUD];
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             weakSelf.replyView.alpha = 0.0f;
         }completion:^(BOOL finished){
             
         }];
        [weakSelf alert:CMU_SUCCESS_SAVE_DATA];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [weakSelf dismisHUD];
        [weakSelf alert:CMU_ERROR_SAVE_DATA];
    }];
    
}

- (void)cmuDialogViewDidCancel
{
    [self.view endEditing:YES];
    __weak CMUContestGalleryDetailViewController *weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         weakSelf.replyView.alpha = 0.0f;
     }completion:^(BOOL finished){
         
     }];
}


@end
