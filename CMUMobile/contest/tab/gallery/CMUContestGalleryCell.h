//
//  CMUContestGalleryCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/27/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMGridViewCell.h"
#import "CMUPhotoContestModel.h"

@interface CMUContestGalleryCell  : GMGridViewCell
@property(nonatomic, strong)CMUPhotoContestFeed *photoContestFeed;
@end
