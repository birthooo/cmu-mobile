//
//  CMUContestGalleryDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/27/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "CMUDialogView.h"

@class CMUContestGalleryDetailViewController;
@protocol CMUContestGalleryDetailViewControllerDelegate <NSObject>
- (void) contestGalleryDetailViewControllerDidBack:(CMUContestGalleryDetailViewController*) vc;
- (void) contestGalleryDetailViewControllerDidDelete:(CMUContestGalleryDetailViewController*) vc;
@end

@interface CMUContestGalleryDetailViewController : CMUViewController<CMUDialogViewDelegate>
@property(nonatomic, weak)id<CMUContestGalleryDetailViewControllerDelegate> delegate;
@property(nonatomic, weak)UIViewController *vcToBack;
@property(nonatomic, unsafe_unretained)int photoId;
@end
