//
//  CMUContestShotViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestShotViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUContestShotStep2ViewController.h"

@interface CMUContestShotViewController ()

@end

@implementation CMUContestShotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pickImageFromCamera:(id)sender{
    if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        return;
    }
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickController.delegate = self;
    imagePickController.modalPresentationStyle = UIModalPresentationFullScreen;
    imagePickController.allowsEditing = YES;
    [self presentViewController:imagePickController animated:YES completion:^{
        
    }];
}

- (IBAction)pickImageFromPhotoLibrary:(id)sender{
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickController.delegate = self;
    imagePickController.modalPresentationStyle = UIModalPresentationFullScreen;
    imagePickController.allowsEditing = YES;
    [self presentViewController:imagePickController animated:YES completion:^{
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - When finish shoot

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    //if(!img) img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    //self.imageView.image =[info objectForKey:UIImagePickerControllerEditedImage];
    [self dismissViewControllerAnimated:YES completion:^{
        
        if(_delegate && [_delegate respondsToSelector:@selector(contestShotViewController:didPickImage:)])
        {
            [_delegate contestShotViewController:self didPickImage:[info objectForKey:UIImagePickerControllerEditedImage]];
        }
     
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
}

@end
