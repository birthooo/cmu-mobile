//
//  CMUContestWebViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUViewController.h"

@interface CMUContestWebViewController : CMUViewController<UIWebViewDelegate>
@property(nonatomic, strong) NSString *url;
@end
