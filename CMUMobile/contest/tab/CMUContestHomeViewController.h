//
//  CMUContestHomeViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"

@interface CMUContestHomeViewController : CMUViewController
@property(nonatomic, unsafe_unretained)BOOL shareFacebookPending;
@end
