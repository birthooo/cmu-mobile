//
//  CMUContestShotViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUViewController.h"

@class CMUContestShotViewController;
@protocol CMUContestShotViewControllerDelegate <NSObject>
- (void) contestShotViewController:(CMUContestShotViewController *) vc didPickImage:(UIImage *) image;
@end

@interface CMUContestShotViewController : CMUViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property(nonatomic, weak)id<CMUContestShotViewControllerDelegate> delegate;
@end
