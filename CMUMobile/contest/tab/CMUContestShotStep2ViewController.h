//
//  CMUContestShotStep2ViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/26/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"

@class CMUContestShotStep2ViewController;
@protocol CMUContestShotStep2ViewControllerDelegate <NSObject>
- (void) contestShotStep2ViewControllerDidCancel:(CMUContestShotStep2ViewController *) vc;
- (void) contestShotStep2ViewController:(CMUContestShotStep2ViewController *) vc didUploadCompletedWithResultPhotoId:(int) photoId;
@end

@interface CMUContestShotStep2ViewController : CMUViewController
@property(nonatomic, strong)UIImage *image;
@property(nonatomic, weak)id<CMUContestShotStep2ViewControllerDelegate> delegate;
@end
