//
//  CMUContestShotStep2ViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/26/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestShotStep2ViewController.h"
#import "CMUWebServiceManager.h"
#import "UIPlaceHolderTextView.h"
#import "UIAlertView+Blocks.h"

@interface CMUContestShotStep2ViewController ()
@property(nonatomic, weak)IBOutlet UIImageView *imageView;
@property(nonatomic, weak)IBOutlet UIPlaceHolderTextView *textView;
@end

@implementation CMUContestShotStep2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textView.placeholder = @"Enter detail about this photo....";
    self.imageView.image = self.image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGFloat minTextAreaHeight = 90;//fit for iphone 5
    if(IS_IPAD){
        minTextAreaHeight = 62; //fit to ipad
    }
    
    CGFloat minHeight = 0;
    minHeight += self.imageView.frame.size.height;
    minHeight += minTextAreaHeight;
    
    CGSize contentViewBaseSize = self.scrollViewBase.frame.size;
    contentViewBaseSize.height =   contentViewBaseSize.height< minHeight? minHeight:contentViewBaseSize.height;
    self.contentViewBase.frame = CGRectMake(0, 0, contentViewBaseSize.width, contentViewBaseSize.height);
    [self.scrollViewBase setContentSize:contentViewBaseSize];
    [self.contentViewBase layoutSubviews];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    //overide not adjust scrollview content size
}

- (void)keyboardWasShown:(NSNotification *)notification
{
     //overide not adjust scrollview content size
}

- (IBAction)cancelClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(contestShotStep2ViewControllerDidCancel:)])
    {
        [_delegate contestShotStep2ViewControllerDidCancel:self];
    }
}

- (IBAction)uploadClicked:(id)sender
{
    UIImage *image = self.imageView.image;
    if(!image){
        return;
    }
    
    NSString *comment = self.textView.text;
    __weak typeof(self) weakSelf = self;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                    message:CMU_CONFIRM_SAVE_DATA
                                                   delegate:self
                                          cancelButtonTitle:@"CANCEL"
                                          otherButtonTitles:@"OK", nil];
    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            [self showHUDLoading];
            
            [[CMUWebServiceManager sharedInstance] photoContestCanUpload:weakSelf success:^(id result) {
                NSNumber *canUpload = (NSNumber *)result;
                if(![canUpload boolValue]){
                    [weakSelf dismisHUD];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME
                                                                    message:@"ไม่สามารถอัพโหลดรูปภาพเกิน 5 รูปได้"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tapBlock = ^(UIAlertView *alertView, NSInteger buttonIndex) {
                        if (buttonIndex == alertView.firstOtherButtonIndex) {
                            
                        } else if (buttonIndex == alertView.cancelButtonIndex) {
                            
                        }
                    };
                    [alert show];
                    
                    return;
                }
                
                
                [[CMUWebServiceManager sharedInstance] photoContestSave:self image:image success:^(id result) {
                    NSString *filename = [result objectForKey:@"Name"];
                    if(filename){
                        [[CMUWebServiceManager sharedInstance] photoContestInsertPhoto:weakSelf filename:filename comment:comment success:^(id result) {
                            [weakSelf dismisHUD];
                            if(_delegate && [_delegate respondsToSelector:@selector(contestShotStep2ViewController:didUploadCompletedWithResultPhotoId:)])
                            {
                                [_delegate contestShotStep2ViewController:self didUploadCompletedWithResultPhotoId:[result intValue]];
                            }
                        } failure:^(NSError *error) {
                            [weakSelf dismisHUD];
                            [weakSelf alert:CMU_ERROR_SAVE_DATA];
                        }];
                    }else{
                        [weakSelf dismisHUD];
                        [weakSelf alert:CMU_ERROR_SAVE_DATA];
                    }
                    
                } failure:^(NSError *error){
                    [weakSelf dismisHUD];
                    [weakSelf alert:CMU_ERROR_SAVE_DATA];
                }];
                
                
            } failure:^(NSError *error) {
                [weakSelf dismisHUD];
                [weakSelf alert:CMU_ERROR_READ_DATA];
                return ;
            }];
            
            
        }
    };
    [alert show];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
