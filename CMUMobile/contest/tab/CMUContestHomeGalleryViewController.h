//
//  CMUContestHomeGalleryViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUViewController.h"
#import "GMGridView.h"
#import "ODRefreshControl.h"
#import "CMUPhotoContestModel.h"
#import "GMGridView.h"
#import "CMUWebViewController.h"

@class CMUContestHomeGalleryViewController;
@protocol CMUContestHomeGalleryViewControllerDelegate <NSObject>
- (void) contestHomeGalleryViewController:(CMUContestHomeGalleryViewController*) vc didSelectPhotoId:(int) photoId;
@end

@interface CMUContestHomeGalleryViewController : CMUViewController<GMGridViewDataSource, GMGridViewActionDelegate, UIScrollViewDelegate>
@property(nonatomic, weak)id<CMUContestHomeGalleryViewControllerDelegate> delegate;
@end
