//
//  CMUContestHomeViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/19/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUContestHomeViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUPhotoContestModel.h"
#import "CMUUIUtils.h"
#import "UIImageView+WebCache.h"
#import "CMUSocial.h"
#import "CMUFormatUtils.h"

@interface CMUContestHomeViewController ()
@property(nonatomic, weak)IBOutlet UIImageView *avatarImageView;
@property(nonatomic, weak)IBOutlet UILabel *lblName;
@property(nonatomic, weak)IBOutlet UIImageView *imageView;
@property(nonatomic, weak)IBOutlet UILabel *lblCaption;
@property(nonatomic, weak)IBOutlet UIButton *btnLike;
@property(nonatomic, weak)IBOutlet UIButton *btnShareFacebook;
@property(nonatomic, weak)IBOutlet UILabel *lblLikeCount;
@property(nonatomic, weak)IBOutlet UILabel *lblDate;

@property(nonatomic, strong)CMUPhotoContestFeedDetail *detail;
@property(nonatomic, unsafe_unretained)BOOL isLike;
@end

@implementation CMUContestHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.contentViewBase.alpha = 0.0;
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] photoContestGetHome:self success:^(id result) {
        [self dismisHUD];
        self.detail = (CMUPhotoContestFeedDetail *)result;
        [self loadFromModel];
        [UIView animateWithDuration:0.3 animations:^(void)
         {
             self.contentViewBase.alpha = 1.0f;
         }completion:^(BOOL finished){
             
         }];
        
        [self.view setNeedsLayout];
        if(self.shareFacebookPending){
            [self btnFacebookClicked:nil];
        }
        
        [[CMUWebServiceManager sharedInstance] photoContestIsLike:self photoID:self.detail.photoContestFeedDetailId success:^(id result) {
            self.isLike = [result boolValue];
            [self updatePresentState];
        } failure:^(NSError *error) {
            
        }];
        
    } failure:^(NSError *error) {
        [self dismisHUD];
    }];
    
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //corrected size scrollview content size
    CGFloat height = 0;
    UIFont *font = nil;
    if(IS_IPHONE){
        height = 470 - 17 - 320;//nib label height, nib image height
        font = [UIFont systemFontOfSize:14.0]; //according to iphone nib
    }else{
        height = 500 - 24 - 320;//nib label height, nib image height
        font = [UIFont systemFontOfSize:20.0]; //according to iphone nib
    }
    
    height += self.view.frame.size.width; //image
    
    CGFloat labelWidth = self.view.frame.size.width - (8 + 8); // autolayput constraint (8,8
    
    CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:self.detail.detail withFont:font];
    height+= size.height;
    //}
    CGSize contentViewBaseSize = self.scrollViewBase.frame.size;
    contentViewBaseSize.height =  height;
    self.contentViewBase.frame = CGRectMake(0, 0, contentViewBaseSize.width, contentViewBaseSize.height);
    [self.scrollViewBase setContentSize:contentViewBaseSize];
    [self.contentViewBase layoutSubviews];
    
}

- (void)loadFromModel{
    
    //load detail
    self.lblName.text = self.detail.name;
    [self.imageView setImageWithURL: [NSURL URLWithString: self.detail.path]];
    self.lblCaption.text = self.detail.detail;
    self.lblLikeCount.text = [NSString stringWithFormat:@"%i likes", self.detail.nVote];
    self.lblDate.text = [CMUFormatUtils formatDateMediumStyle:self.detail.dtime locale:[NSLocale currentLocale]];
}

- (void)updatePresentState
{
    [super updatePresentState];
    BOOL isLogin = [[CMUWebServiceManager sharedInstance] isLogin];
    self.btnLike.hidden = !isLogin;
    
    [self.btnLike setTitleColor:self.isLike?[UIColor colorWithRed:200/255.0 green:0/255.0 blue:8/255.0 alpha:1.0]:[UIColor colorWithRed:172/255.0 green:172/255.0 blue:172/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    [self.btnLike setImage:self.isLike?[UIImage imageNamed:@"contest_like_selected.png"]:[UIImage imageNamed:@"contest_like_unselected.png"] forState:UIControlStateNormal];
}

- (IBAction)btnFacebookClicked:(id)sender
{
    NSString *photoUrl = [NSString stringWithFormat:CMU_PHOTO_CONTEST_SHARE_FB_URL, self.detail.photoContestFeedDetailId];
    [CMUSocial shareFacebookWithTitle:@"Digital Life : Digital University@CMU" url:photoUrl desc:self.detail.detail pictureUrl:self.detail.path];
}

- (IBAction)btnLikeClicked:(id)sender
{
    if(!self.isLike)
    {
        [self.btnLike setTransform:CGAffineTransformMakeScale(0.8, 0.8)];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations: ^{
                             [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                             [self.btnLike setAlpha:0.7];
                         }
                         completion: ^(BOOL finished) {
                             [UIView animateWithDuration:0.2
                                                   delay:0.1
                                                 options: UIViewAnimationOptionCurveEaseIn
                                              animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                                  [self.btnLike setAlpha:1.0]; }
                                              completion: ^(BOOL finished) {
                                              }];
                         }];
        
        [UIView animateWithDuration:0.2
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseOut
                         animations: ^{
                             [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                             [self.btnLike setAlpha:0.7];
                         }
                         completion: ^(BOOL finished) {
                             [UIView animateWithDuration:0.2
                                                   delay:0.1
                                                 options: UIViewAnimationOptionCurveEaseIn
                                              animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                                  [self.btnLike setAlpha:1.0]; }
                                              completion: ^(BOOL finished) {
                                              }];
                         }];
        
        self.detail.nVote++;
        self.lblLikeCount.text = [NSString stringWithFormat:@"%i likes", self.detail.nVote];
        self.isLike = YES;
        [self updatePresentState];
        [[CMUWebServiceManager sharedInstance] photoContestAddLike:self photoID:self.detail.photoContestFeedDetailId success:^(id result) {
            
        } failure:^(NSError *error) {
            
        }];
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
