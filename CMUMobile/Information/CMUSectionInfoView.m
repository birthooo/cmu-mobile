//
//  CMUSectionInfoView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/14/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUSectionInfoView.h"
#import "UIColor+CMU.h"
@implementation CMUSectionInfo
- (id)initWith:(NSString *) title imageName:(NSString *)imageName showBar:(BOOL) isShowBar htmlFile:(NSString *)htmlFile
{
    self = [super init];
    if(self)
    {
        self.title = title;
        self.imageName = imageName;
        self.isShowBar = isShowBar;
        self.htmlFile = htmlFile;
    }
    return self;
}
@end

@interface CMUSectionInfoView()
@property(nonatomic, weak)IBOutlet UIImageView *imvBackground;
@property(nonatomic, weak)IBOutlet UIView *viewBar;
@property(nonatomic, weak)IBOutlet UILabel *lblTitle;

-(IBAction)contentTapped:(id)sender;
@end
@implementation CMUSectionInfoView

- (void)customInit
{
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUSectionInfoView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    self.clipsToBounds = YES;
    [self addSubview:contentView];
    self.backgroundColor = [UIColor clearColor];
    contentView.backgroundColor = [UIColor clearColor];
    
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
//                                                          attribute:NSLayoutAttributeTop
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self
//                                                          attribute:NSLayoutAttributeTop
//                                                         multiplier:1.0
//                                                           constant:0.0]];
//    
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
//                                                     attribute:NSLayoutAttributeBottom
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeBottom
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
//                                                     attribute:NSLayoutAttributeLeading
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeLeading
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    [self addConstraint:[NSLayoutConstraint constraintWithItem:contentView
//                                                     attribute:NSLayoutAttributeTrailing
//                                                     relatedBy:NSLayoutRelationEqual
//                                                        toItem:self
//                                                     attribute:NSLayoutAttributeTrailing
//                                                    multiplier:1.0
//                                                      constant:0.0]];
//    [self setNeedsUpdateConstraints];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void)setSectionInfo:(CMUSectionInfo *)sectionInfo
{
    _sectionInfo = sectionInfo;
    self.imvBackground.image = [UIImage imageNamed:_sectionInfo.imageName];
    self.viewBar.backgroundColor = _sectionInfo.isShowBar?[UIColor cmuPurpleTrasparentColor]:[UIColor clearColor];
    self.lblTitle.text = _sectionInfo.title;
}

- (void)contentTapped:(id)sender
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(sectionInfoViewDidClicked:)])
    {
        [self.delegate sectionInfoViewDidClicked:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
