//
//  CMUInformationDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUInformationDetailViewController.h"

@interface CMUInformationDetailViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, weak) IBOutlet UILabel *lblWebpageTitle;
@property(nonatomic, assign)BOOL firstLoad;

- (IBAction)back:(id)sender;
@end

@implementation CMUInformationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _firstLoad = YES;
    self.lblWebpageTitle.text = self.webTitle;
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(infomationDetailViewControllerBackButtonClicked:)])
    {
        [_delegate infomationDetailViewControllerBackButtonClicked:self];
    }
}

#pragma mark UIWebviewDelegate
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if(_firstLoad)
    {
        _firstLoad = NO;
        //like android
        //[self showHUDLoading];
    }
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
