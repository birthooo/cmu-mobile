//
//  CMUSectionInfoView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/14/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUSectionInfo : NSObject
- (id)initWith:(NSString *) title imageName:(NSString *)imageName showBar:(BOOL) isShowBar htmlFile:(NSString*) htmlFile;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *imageName;
@property(nonatomic, unsafe_unretained) BOOL isShowBar;
@property(nonatomic, strong) NSString *htmlFile;
@end

@class CMUSectionInfoView;
@protocol CMUSectionInfoViewDelegate<NSObject>
- (void)sectionInfoViewDidClicked:(CMUSectionInfoView* )sectionInfoView;
@end

@interface CMUSectionInfoView : UIView
@property(nonatomic, unsafe_unretained) id<CMUSectionInfoViewDelegate> delegate;
@property(nonatomic, strong) CMUSectionInfo* sectionInfo;
@end
