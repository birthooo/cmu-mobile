//
//  CMUInformationDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 7/23/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@class CMUInformationDetailViewController;
@protocol CMUInformationDetailViewControllerDelegate <NSObject>
- (void)infomationDetailViewControllerBackButtonClicked:(CMUInformationDetailViewController *) infomationDetailViewController;
@end

@interface CMUInformationDetailViewController : CMUNavBarViewController<UIWebViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUInformationDetailViewControllerDelegate> delegate;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) NSString *webTitle;
@end
