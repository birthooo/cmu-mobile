//
//  CMUOnlineViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUOnlineHeaderView.h"
#import "CMUOnlineTableViewCell.h"
#import "CMUOnlineVideoListViewController.h"
@class CMUOnlineViewController;
@protocol CMUOnlineViewControllerDelegate <NSObject>
-(void)cmuOnlineViewControllerDidBack:(CMUOnlineViewController *)vc;
@end

@interface CMUOnlineViewController : CMUNavBarViewController<CMUOnlineHeaderViewDelegate, CMUOnlineVideoListViewControllerDelegate>
@property(nonatomic, unsafe_unretained)id<CMUOnlineViewControllerDelegate> delegate;
@end
