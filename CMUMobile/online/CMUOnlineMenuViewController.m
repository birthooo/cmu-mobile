//
//  CMUOnlineMenuViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineMenuViewController.h"

@interface CMUOnlineMenuViewController ()
@property(nonatomic, weak)IBOutlet UIButton *btnStreaming;
- (IBAction)btnStreamingClicked:(id)sender;
- (IBAction)btnOnlineClicked:(id)sender;
@end

@implementation CMUOnlineMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.btnStreaming.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnStreaming.titleLabel.numberOfLines = 0;
    // you probably want to center it
    //self.btnStreaming.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
    [self.btnStreaming setTitle: @"CMU STREAMING MEDIA" forState: UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btnStreamingClicked:(id)sender
{
    CMUOnlineViewController *viewController = [[CMUOnlineViewController alloc] init];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)btnOnlineClicked:(id)sender
{
    NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id633359593?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

#pragma mark - CMUOnlineViewController
-(void)cmuOnlineViewControllerDidBack:(CMUOnlineViewController *)vc
{
    [self.navigationController popToViewController:self animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
