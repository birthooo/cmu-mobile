//
//  CMUOnlineVideoDetailViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/3/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineVideoDetailViewController.h"
#import "CMUStringUtils.h"
#import <MediaPlayer/MediaPlayer.h>
#import "NSString+URLEncoder.h"
#import "CMUWebServiceManager.h"
#import "UIImageView+WebCache.h"
#import "CMUImageSizeCached.h"
#import "CMUFormatUtils.h"
#import "CMUSocial.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "NSString+URLEncoder.h"
#import "CMUOnlineModel.h"
#import "ODRefreshControl.h"
#import "CMUUIUtils.h"
#import "CMUOnlineVideoDetailCommentTableViewCell.h"
#import "CMUStringUtils.h"


static NSString *onlineVideoDetailCommentTableViewCell = @"CMUOnlineVideoDetailCommentTableViewCell";

@interface CMUOnlineVideoDetailViewController ()
@property(nonatomic, strong) IBOutlet ODRefreshControl *refreshControl;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) UIActivityIndicatorView *loadMoreSpinner;

@property(nonatomic, strong) IBOutlet UITableViewCell *videoDetailTableViewCell;
@property(nonatomic, strong) IBOutlet UITableViewCell *addCommentTableViewCell;


@property(nonatomic, weak) IBOutlet UIView *navView;
@property(nonatomic, weak) IBOutlet UILabel *navLabel;
@property(nonatomic, weak)IBOutlet UIImageView* thumbnailImageView;
@property(nonatomic, weak)IBOutlet UILabel *lblView;
@property(nonatomic, weak)IBOutlet UILabel *lblDuration;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoLike;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoName;

@property(nonatomic, weak)IBOutlet UILabel *lblDate;
@property(nonatomic, weak)IBOutlet UILabel*lblDescription;
@property(nonatomic, weak)IBOutlet UILabel *lblNumComment;
@property(nonatomic, weak)IBOutlet UITextField *txtAddComment;

@property(nonatomic, weak)IBOutlet NSLayoutConstraint *imageHeighConstraint;

@property(nonatomic, weak)IBOutlet UIButton *btnLike;

@property(nonatomic, unsafe_unretained) BOOL loadingMore;
@property(nonatomic, unsafe_unretained)BOOL resetFeedList;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSArray *feedList;
@property(nonatomic, unsafe_unretained)int numComment;
@property(nonatomic, unsafe_unretained)BOOL scrollToLastestComment;

@property(nonatomic, unsafe_unretained)int calculatedImageHeight;

@property(nonatomic, unsafe_unretained)int keyBoardHeigh;

-(IBAction) backTapped:(id) sender;
-(IBAction) thumnailTapped:(id) sender;
-(IBAction) btnFacebookClicked:(id) sender;
-(IBAction) btnTwitterClicked:(id) sender;
-(IBAction) btnLikeClicked:(id) sender;

@property (strong, nonatomic) MPMoviePlayerController *videoPlayer;

@end

@implementation CMUOnlineVideoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(IS_IPHONE)
    {
        self.calculatedImageHeight = 180;
    }
    else
    {
        self.calculatedImageHeight = 432;
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUOnlineVideoDetailCommentTableViewCell" bundle:nil] forCellReuseIdentifier:onlineVideoDetailCommentTableViewCell];
    
    self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
    
    //pull refresh
    self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
    [self showHUDLoading];
    [self getVideoComment];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // Touch background and hide keyboard
    tapGesture.cancelsTouchesInView = NO;
    [self.tableView addGestureRecognizer:tapGesture];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)getVideoComment
{
    [[CMUWebServiceManager sharedInstance]  getVideoComment:self videoId:self.videoFeed.vdoId  success:^(id result) {
        CMUOnlineCommentModel *commentModel = (CMUOnlineCommentModel *)result;
        self.refreshURL = [CMUStringUtils isEmpty:commentModel.refreshURL]? nil: commentModel.refreshURL;
        self.loadMoreURL = [CMUStringUtils isEmpty:commentModel.loadMoreURL]? nil: commentModel.loadMoreURL;
        self.numComment = commentModel.numberComment;
        [self populateFeedList:result];
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        [self populateFeedList:nil];
        [self updatePresentState];
        [self dismisHUD];
    }];
}

-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
    [self.tableView setContentOffset:scrollPoint animated:YES];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _keyBoardHeigh = keyboardSize.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, _keyBoardHeigh, 0.0);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:YES];
    //[self adjustScrollView];
}

- (void)dropViewDidBeginRefreshing:(id) sender
{
    if(self.refreshURL)
    {
        [[CMUWebServiceManager sharedInstance] getVideoCommentByURL:self url:self.refreshURL success:^(id result) {
            CMUOnlineCommentModel *onlineCommentModel = (CMUOnlineCommentModel *)result;
            if(![CMUStringUtils isEmpty:onlineCommentModel.refreshURL])
            {
                self.refreshURL = onlineCommentModel.refreshURL;
            }
            self.numComment = onlineCommentModel.numberComment;
            [self populateFeedList:onlineCommentModel];
            [self.refreshControl endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControl endRefreshing];
        }];
    }
    else
    {
        [self.refreshControl endRefreshing];
    }
}

- (void)populateFeedList:(CMUOnlineCommentModel *) onlineCommentModel
{
    //check to reset feed list
    BOOL scrollToTop = self.resetFeedList;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    if(!self.resetFeedList)
    {
        [tempFeedList addObjectsFromArray:self.feedList];
    }
    self.resetFeedList = NO;
    
    //add new feed to feed list
    for(CMUOnlineCommentFeed *commentFeed in onlineCommentModel.commentFeedList)
    {
        if(![tempFeedList containsObject:commentFeed])
        {
            [tempFeedList addObject:commentFeed];
        }
    }
    
    //perform sort
    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        CMUOnlineCommentFeed *first = (CMUOnlineCommentFeed*)a;
        CMUOnlineCommentFeed *second = (CMUOnlineCommentFeed*)b;
        return [second.commentDatetime compare:first.commentDatetime];
    }];
    
    self.feedList = sortedFeedList;
    
    [self.tableView reloadData];
    if(scrollToTop)
    {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    else if (self.scrollToLastestComment)
    {
        self.scrollToLastestComment = false;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

//overide
- (void)loginChangedNotification:(NSNotification *) notification
{
    [super loginChangedNotification:notification];
    //need to get new feed for current user
    self.resetFeedList = YES;
    [self showHUDLoading];
    [self getVideoComment];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)setVideoFeed:(CMUOnlineVideoFeed *)videoFeed
//{
//    _videoFeed = videoFeed;
//    
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


-(void) backTapped:(id) sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuOnlineVideoDetailViewControllerDidBack:)])
    {
        [self hideKeyboard];
        [_delegate cmuOnlineVideoDetailViewControllerDidBack:self];
    }
}

- (void)openVideoFeed:(CMUOnlineVideoFeed *) videoFeed
{
    if(![CMUStringUtils isEmpty:videoFeed.vdoPath])
    {
        NSURL *videoURL = [NSURL URLWithString:[self.videoFeed.vdoPath urlFixSpace]];
        
        _videoPlayer =  [[MPMoviePlayerController alloc]initWithContentURL:videoURL];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_videoPlayer];
        _videoPlayer.controlStyle = MPMovieControlStyleDefault;
        _videoPlayer.shouldAutoplay = YES;
        [_videoPlayer prepareToPlay];
        [self.view addSubview:_videoPlayer.view];
        [_videoPlayer setFullscreen:YES animated:YES];
        [_videoPlayer stop];
        [_videoPlayer play];
    }
}

-(void) thumnailTapped:(id) sender
{
    [self openVideoFeed:self.videoFeed];
    
    [[CMUWebServiceManager sharedInstance] addCMUOnlineVideoView:self videoId:self.videoFeed.vdoId success:^(id result) {
        self.videoFeed.vdoview ++;
        self.lblView.text = [NSString stringWithFormat:@"%i",self.videoFeed.vdoview];
        DebugLog(@"");
    } failure:^(NSError *error) {
        DebugLog(@"");
    }];
    
}


- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}

-(void) btnFacebookClicked:(id) sender
{
    //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
    
#ifdef USE_FACEBOOK_SDK_SHARE
    [CMUSocial shareFacebookWithTitle:self.videoFeed.vdoName url:[self.videoFeed.linkPage urlFixSpace] desc:self.videoFeed.vdoDesc pictureUrl:[self.videoFeed.vdoPic urlFixSpace]];
#else
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                NSLog(@"Post Sucessful");
                break;
                
            default:
                break;
        }
    }];
    
    [controller setInitialText:channelFeed.topic];
    [controller addURL:[NSURL URLWithString:channelFeed.link]];
    //TODO adding icon?
    //[controller addImage:[UIImage imageNamed:@"myImage.png"]];
    [self presentViewController:controller animated:YES completion:nil];
#endif
    
}

-(void) btnTwitterClicked:(id) sender
{
    //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                NSLog(@"Post Sucessful");
                break;
                
            default:
                break;
        }
    }];
    
    [controller setInitialText:self.videoFeed.vdoName];
    [controller addURL:[NSURL URLWithString:[self.videoFeed.linkPage urlFixSpace]]];
    //TODO adding icon?
    //[controller addImage:[UIImage imageNamed:@"myImage.png"]];
    [self presentViewController:controller animated:YES completion:Nil];
    
    // }
}

-(void) btnLikeClicked:(id) sender
{
    [self.btnLike setTransform:CGAffineTransformMakeScale(0.8, 0.8)];
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations: ^{
                         [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                         [self.btnLike setAlpha:0.7];
                     }
                     completion: ^(BOOL finished) {
                         [UIView animateWithDuration:0.2
                                               delay:0.1
                                             options: UIViewAnimationOptionCurveEaseIn
                                          animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                              [self.btnLike setAlpha:1.0]; }
                                          completion: ^(BOOL finished) {
                                          }];
                     }];
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseOut
                     animations: ^{
                         [self.btnLike setTransform:CGAffineTransformMakeScale(1.8, 1.8)];
                         [self.btnLike setAlpha:0.7];
                     }
                     completion: ^(BOOL finished) {
                         [UIView animateWithDuration:0.2
                                               delay:0.1
                                             options: UIViewAnimationOptionCurveEaseIn
                                          animations: ^{ [self.btnLike setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                              [self.btnLike setAlpha:1.0]; }
                                          completion: ^(BOOL finished) {
                                          }];
                     }];

    self.videoFeed.vdolike++;
    self.lblVdoLike.text = [NSString stringWithFormat:@"%i",self.videoFeed.vdolike];
    [[CMUWebServiceManager sharedInstance] addCMUOnlineVideoLike:self videoId:self.videoFeed.vdoId success:^(id result) {
        DebugLog(@"");
    } failure:^(NSError *error) {
        DebugLog(@"");
    }];
}

- (void)updatePresentState
{
    [super updatePresentState];
    if(self.loadingMore && self.tableView.tableFooterView == nil)
    {
        self.tableView.tableFooterView = self.loadMoreSpinner;
        [self.loadMoreSpinner startAnimating];
    }
    else
    {
        self.tableView.tableFooterView = nil;
        [self.loadMoreSpinner stopAnimating];
    }
    
}

#pragma mark section table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numOfCell = (int)self.feedList.count;
    numOfCell += 1;//detail cell
    numOfCell += [[CMUWebServiceManager sharedInstance] isLogin]? 1 : 0;
    return numOfCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = self.tableView.frame.size.width;
    if(indexPath.row == 0)//detail cell
    {
        
        CGFloat height = 0;
        
        if(IS_IPHONE)
        {
            height = 480 - (180 + 134);// 180 = image, 134 = description
            
            CGFloat labelWidth = cellWidth - (5 + 5); // autolayput constraint (5,5
            UIFont *font = [UIFont boldSystemFontOfSize:14.0]; //according to iphone nib
            CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:self.videoFeed.vdoDesc withFont:font];
            height += size.height;

            NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:self.videoFeed.vdoPic];
            if(cachedSizeValue)
            {
                CGSize originalSize = [cachedSizeValue CGSizeValue];
                CGFloat imageWidth = self.view.frame.size.width < self.view.frame.size.height? self.view.frame.size.width: self.view.frame.size.height;
//                CGFloat imageWidth = cellWidth; // autolayput constraint (7,7 border, content view)
                @try {
                    self.calculatedImageHeight = imageWidth * originalSize.height / originalSize.width;
                }
                @catch (NSException *exception) {
                }
                @finally {
                }
                height += self.calculatedImageHeight;
            }
            else
            {
                height += 180;
            }
           
        }
        else
        {
            height = 480 - (180 + 102);// 180 = image, 102 = description
            
            CGFloat labelWidth = cellWidth - (5 + 5); // autolayput constraint (5,5
            UIFont *font = [UIFont boldSystemFontOfSize:20.0]; //according to iphone nib
            CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:self.videoFeed.vdoDesc withFont:font];
            height += size.height;
            
            NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:self.videoFeed.vdoPic];
            if(cachedSizeValue)
            {
                CGSize originalSize = [cachedSizeValue CGSizeValue];
                CGFloat imageWidth = self.view.frame.size.width < self.view.frame.size.height? self.view.frame.size.width: self.view.frame.size.height;
                //                CGFloat imageWidth = cellWidth; // autolayput constraint (7,7 border, content view)
                @try {
                    self.calculatedImageHeight = imageWidth * originalSize.height / originalSize.width;
                }
                @catch (NSException *exception) {
                }
                @finally {
                }
                height += self.calculatedImageHeight;
            }
            else
            {
                height += 432;
            }

        }
        return height;
    }
    
    int startCommentIndexPath = 1;
    
    if([[CMUWebServiceManager sharedInstance] isLogin])
    {
        if(indexPath.row == 1)
        {
            if(IS_IPHONE)
            {
                return 46;
            }
            else
            {
                return 60;
            }
        }
        startCommentIndexPath ++;
    }
    
    //comment cell
    CMUOnlineCommentFeed *commentFeed = [self.feedList objectAtIndex:indexPath.row -  startCommentIndexPath];
    
    CGFloat height = 0;
    if(IS_IPHONE)
    {
        height = 44 - 17;//17 = label nib height
        CGFloat labelWidth = cellWidth - (8 + 8); // autolayput constraint (8,8
        UIFont *font = [UIFont boldSystemFontOfSize:14.0]; //according to iphone nib
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:commentFeed.commentText withFont:font];
        height += size.height;
        height = height < 44? 44: height;
    }
    else
    {
        height = 60 - 24;//17 = label nib height
        CGFloat labelWidth = cellWidth - (8 + 8); // autolayput constraint (8,8
        UIFont *font = [UIFont boldSystemFontOfSize:20.0]; //according to iphone nib
        CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:commentFeed.commentText withFont:font];
        height += size.height;
        height = height < 60? 60: height;
    }
    return height;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0)//detail cell
    {
        self.navLabel.text = self.videoFeed.vdoName;
        self.lblView.text = [NSString stringWithFormat:@"%i",self.videoFeed.vdoview];
        self.lblDuration.text = self.videoFeed.vdotime;
        self.lblVdoLike.text = [NSString stringWithFormat:@"%i",self.videoFeed.vdolike];
        self.lblVdoName.text = self.videoFeed.vdoName;
        
        self.lblDate.text =  [CMUFormatUtils formatDateVideoFeedStyle:self.videoFeed.vdoDate locale:[NSLocale currentLocale]];
        self.lblDescription.text = self.videoFeed.vdoDesc;
        self.lblNumComment.text = [NSString stringWithFormat:@"COMMENTS : %i",self.numComment];
        self.imageHeighConstraint.constant = self.calculatedImageHeight;
        [self.thumbnailImageView setImageWithURL:[NSURL URLWithString:[self.videoFeed.vdoPic urlFixSpace]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if(image && error == nil)
            {
                
                NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:self.videoFeed.vdoPic];
                
                CGSize originalImageSize = image.size;
                
                if(cachedSizeValue == nil)
                {
                    
                    [[CMUImageSizeCached sharedInstance] setImageSizeCached:originalImageSize forURL:self.videoFeed.vdoPic];
                    [tableView beginUpdates];
                    [tableView reloadRowsAtIndexPaths:@[indexPath]
                                     withRowAnimation:UITableViewRowAnimationFade];
                    [tableView endUpdates];
                }
            }
        }];
        return self.videoDetailTableViewCell;
        
    }
    
    int startCommentIndexPath = 1;
    if([[CMUWebServiceManager sharedInstance] isLogin])
    {
        if(indexPath.row == 1)
        {
            return self.addCommentTableViewCell;
        }
        startCommentIndexPath ++;
    }

    CMUOnlineCommentFeed *commentFeed = [self.feedList objectAtIndex:indexPath.row -  startCommentIndexPath];
    CMUOnlineVideoDetailCommentTableViewCell *cell =  (CMUOnlineVideoDetailCommentTableViewCell*)[tableView dequeueReusableCellWithIdentifier:onlineVideoDetailCommentTableViewCell forIndexPath:indexPath];
    cell.onlineCommentFeed = commentFeed;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if(self.loadMoreURL)
        {
            if(self.loadingMore)
            {
                return;
            }
            self.loadingMore = YES;
            [self updatePresentState];
            [[CMUWebServiceManager sharedInstance] getVideoCommentByURL:self url:self.loadMoreURL success:^(id result) {
                CMUOnlineCommentModel *onlineCommentModel = (CMUOnlineCommentModel *)result;
                self.loadMoreURL = [CMUStringUtils isEmpty:onlineCommentModel.loadMoreURL]? nil: onlineCommentModel.loadMoreURL;
                self.numComment = onlineCommentModel.numberComment;
                [self populateFeedList:result];
                self.loadingMore = NO;
                [self updatePresentState];
            } failure:^(NSError *error) {
                self.loadingMore = NO;
                [self updatePresentState];
            }];
        }
    }
}

#pragma UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSString *text = [CMUStringUtils trim: self.txtAddComment.text];
    if(![CMUStringUtils isEmpty:text])
    {
        [self showHUDLoading];
        __weak typeof(self) weakSelf = self;
        [[CMUWebServiceManager sharedInstance] addVideoComment:self videoId:self.videoFeed.vdoId comment:text success:^(id result) {
            self.txtAddComment.text = nil;
            [self.txtAddComment resignFirstResponder];
            weakSelf.scrollToLastestComment = YES;
            [weakSelf getVideoComment];
        } failure:^(NSError *error) {
            [weakSelf dismisHUD];
        }];
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
