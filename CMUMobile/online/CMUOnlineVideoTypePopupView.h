//
//  CMUOnlineVideoTypePopupView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUOnlineModel.h"

@class CMUOnlineVideoTypePopupView;

@protocol CMUOnlineVideoTypePopupViewDelegate<NSObject>
- (void)cmuOnlineVideoTypePopupView:(CMUOnlineVideoTypePopupView *) cmuOnlineVideoTypePopupView didSelectedVideoType:(CMUOnlineVideoType *) videoType;
@end

@interface CMUOnlineVideoTypePopupView : UIView<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUOnlineVideoTypePopupViewDelegate> delegate;
@end
