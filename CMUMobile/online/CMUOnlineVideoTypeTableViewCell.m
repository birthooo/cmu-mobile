//
//  CMUOnlineVideoTypeTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUOnlineVideoTypeTableViewCell.h"
#import "UIColor+CMU.h"
#import "global.h"


@interface CMUOnlineVideoTypeTableViewCell()
@property(nonatomic, weak) IBOutlet UILabel *lblVideoTypeTitle;
@end

@implementation CMUOnlineVideoTypeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
    if(IS_IPAD)
    {
        self.lblVideoTypeTitle.font = [UIFont systemFontOfSize:20.0];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setVideoType:(CMUOnlineVideoType *)videoType
{
    _videoType = videoType;
    self.lblVideoTypeTitle.text = self.videoType.title;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblVideoTypeTitle.text = nil;
}


@end
