//
//  CMUOnlineVideoDetailViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/3/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUOnlineModel.h"

@class CMUOnlineVideoDetailViewController;

@protocol CMUOnlineVideoDetailViewControllerDelegate <NSObject>
- (void)cmuOnlineVideoDetailViewControllerDidBack:(CMUOnlineVideoDetailViewController *) vc;
@end

@interface CMUOnlineVideoDetailViewController : CMUNavBarViewController<UITextFieldDelegate>
@property(nonatomic, weak)id<CMUOnlineVideoDetailViewControllerDelegate> delegate;
@property(nonatomic, strong)CMUOnlineVideoFeed *videoFeed;
@end
