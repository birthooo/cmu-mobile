//
//  CMUOnlineMenuViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUOnlineViewController.h"

@interface CMUOnlineMenuViewController : CMUNavBarViewController<CMUOnlineViewControllerDelegate>

@end
