//
//  CMUOnlineVideoListTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUOnlineModel.h"

@interface CMUOnlineVideoListTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUOnlineVideoFeed *onlineVideoFeed;
@property(nonatomic, weak)IBOutlet UIImageView* thumbnailImageView;
@end
