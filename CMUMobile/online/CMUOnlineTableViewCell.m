//
//  CMUOnlineTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUOnlineTableViewCell.h"
#import "UIColor+CMU.h"

@implementation CMUOnlineTableViewCell

- (void)awakeFromNib {
    // Initialization code
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor cmuPurpleColor];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self updateUI:selected];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self updateUI:highlighted];
}

- (void)updateUI:(BOOL)selected
{
    self.imgAccesoryView.image = selected?[UIImage imageNamed:@"cell_accessory_selected.png"]:[UIImage imageNamed:@"cell_accessory_normal.png"];
    self.lblTitle.textColor = selected?[UIColor whiteColor]:[UIColor cmuTableViewCellTextColor];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblTitle.text = nil;
}
@end
