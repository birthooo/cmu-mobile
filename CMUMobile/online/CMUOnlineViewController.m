//
//  CMUOnlineViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/15/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineViewController.h"
#import "CMUWebServiceManager.h"

static NSString *cellIdentifier = @"CMUOnlineTableViewCell";

@interface CMUOnlineViewController ()
@property (nonatomic, strong) IBOutlet CMUOnlineHeaderView *headerView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *headerModelList;
@property(nonatomic, strong) NSMutableArray *onlineItemlList;

@property(nonatomic, unsafe_unretained) int headerInit;

- (IBAction)backClicked:(id)sender;
@end

@implementation CMUOnlineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUOnlineTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self initViewByUserPermission];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)initViewByUserPermission
{
    CMUWebServiceManager *webServiceManager = [CMUWebServiceManager sharedInstance];
    
    //sample for load header by authentication type
    CMUOnlineHeaderModel *cuteAjarnModel = [[CMUOnlineHeaderModel alloc] init];
    cuteAjarnModel.cmuOnlineType = CMUOnlineTypeCuteAjarn;
    cuteAjarnModel.title = @"CMU CuteAjarn";
    cuteAjarnModel.image = [UIImage imageNamed:@"online_cute_ajarn_logo.png"];
    
//    CMUOnlineHeaderModel *smartClassroom = [[CMUOnlineHeaderModel alloc] init];
//    smartClassroom.videoType = CMUOnlineVideoTypeCuteAjarn;
//    smartClassroom.title = @"CMU CuteAjarn";
//    smartClassroom.image = [UIImage imageNamed:@"online_cute_ajarn_logo.png"];
    
    //test like android
    CMUOnlineHeaderModel *test1 = [[CMUOnlineHeaderModel alloc] init];
    test1.cmuOnlineType = CMUOnlineTypeCuteAjarn;
    test1.title = @"CMU CuteAjarn test1";
    test1.image = [UIImage imageNamed:@"online_cute_ajarn_logo.png"];
    
    CMUOnlineHeaderModel *test2 = [[CMUOnlineHeaderModel alloc] init];
    test2.cmuOnlineType = CMUOnlineTypeCuteAjarn;
    test2.title = @"CMU CuteAjarn test2";
    test2.image = [UIImage imageNamed:@"online_cute_ajarn_logo.png"];
    
    //test for
    if([webServiceManager isLogin])
    {
//        self.headerModelList = [[NSMutableArray alloc] initWithObjects:cuteAjarnModel,test1, test2, nil];
        self.headerModelList = [[NSMutableArray alloc] initWithObjects:cuteAjarnModel, nil];
        
    }
    else
    {
//        self.headerModelList = [[NSMutableArray alloc] initWithObjects:cuteAjarnModel,test1, test2, nil];
        self.headerModelList = [[NSMutableArray alloc] initWithObjects:cuteAjarnModel, nil];
    }
    
    [self showHUDLoading];
    [webServiceManager getCuteajarn:self success:^(id result) {
        CMUOnlineCuteAjarnModel *cuteAjarnModel = (CMUOnlineCuteAjarnModel *)result;
        [self dismisHUD];
        self.onlineItemlList = cuteAjarnModel.cuteAjarnList;
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        [self dismisHUD];
        //CMUSISPCheckModel *sisPcheckModel
    }];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(!self.headerInit)
    {
        self.headerView.headerModelList = self.headerModelList;
        self.headerInit = YES;
    }
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.onlineItemlList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUOnlineTableViewCell *cell = (CMUOnlineTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CMUOnlineCuteAjarnFeed *item = [self.onlineItemlList objectAtIndex:indexPath.row];
    cell.lblTitle.text = item.cuteName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUOnlineVideoListViewController *cmuOnlineVideoListViewController = [[CMUOnlineVideoListViewController alloc] init];
    cmuOnlineVideoListViewController.delegate = self;
    cmuOnlineVideoListViewController.cmuOnlineCuteAjarnFeed = [self.onlineItemlList objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:cmuOnlineVideoListViewController animated:YES];
    [self.tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

- (void)backClicked:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuOnlineViewControllerDidBack:)])
    {
        [_delegate cmuOnlineViewControllerDidBack:self];
    }
}

#pragma mark - CMUOnlineHeaderViewDelegate
- (void) onlineHeaderViewDelegate:(CMUOnlineHeaderView *) onlineHeaderView
                      didSelected:(int) index
{
    
}


#pragma mark - CMUOnlineVideoListViewControlleDelegate
- (void)cmuOnlineVideoListViewControllerDidBack:(CMUOnlineVideoListViewController *) vc
{
    [self.navigationController popToViewController:self animated:YES];
}


@end
