//
//  CMUOnlineHeaderView.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/22/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
    CMUOnlineTypeCuteAjarn = 1,
    CMUOnlineTypeSmartClassroom = 2,
    CMUOnlineTypeSmartClassroomXXX = 3
} CMUOnlineType;

@interface CMUOnlineHeaderModel : NSObject
@property(nonatomic, unsafe_unretained) CMUOnlineType cmuOnlineType;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) UIImage *image;
@end

@class CMUOnlineHeaderView;
@protocol CMUOnlineHeaderViewDelegate <NSObject>
- (void) onlineHeaderViewDelegate:(CMUOnlineHeaderView *) onlineHeaderView
              didSelected:(int) index;
@end

@interface CMUOnlineHeaderView : UIView<UIScrollViewDelegate>
@property(nonatomic, strong)  NSArray *headerModelList;
@property(nonatomic, unsafe_unretained)  int selectedIndex;
@end
