//
//  CMUOnlineVideoTypePopupView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUOnlineVideoTypePopupView.h"
#import "CMUOnlineVideoTypeTableViewCell.h"

static NSString *cellIdentifier = @"CMUOnlineVideoTypeTableViewCell";

@interface CMUOnlineVideoTypePopupView()
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *videoTypeList;
@end

@implementation CMUOnlineVideoTypePopupView

- (void)customInit
{
    self.videoTypeList= [[NSMutableArray alloc] initWithArray:[CMUOnlineVideoType allVideoType]];
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUOnlineVideoTypePopupView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUOnlineVideoTypeTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

#pragma mark table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.videoTypeList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUOnlineVideoTypeTableViewCell *cell = (CMUOnlineVideoTypeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.videoType = [self.videoTypeList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuOnlineVideoTypePopupView:didSelectedVideoType:)])
    {
        CMUOnlineVideoType *selectedVideoType = [self.videoTypeList objectAtIndex:indexPath.row];
        [_delegate cmuOnlineVideoTypePopupView:self didSelectedVideoType:selectedVideoType];
    }
}

//snap to cell
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    *targetContentOffset = CGPointMake(0, 44 * round((*targetContentOffset).y /44));
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
