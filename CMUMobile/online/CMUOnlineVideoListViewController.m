//
//  CMUOnlineVideoListViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 8/27/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUOnlineVideoListViewController.h"
#import "ODRefreshControl.h"
#import "CMUWebServiceManager.h"
#import "CMUSettings.h"
#import "CMUModel.h"
#import "CMUStringUtils.h"
#import <Twitter/Twitter.h>
#import "CMUUtils.h"
#import <Accounts/Accounts.h>
#import "CMUNewsSettingPopupView.h"
#import "PopoverView.h"
#import "CMUOnlineVideoTypePopupView.h"
#import "CMUUIUtils.h"
#import "CMUStringUtils.h"
#import "CMUWebViewController.h"
#import "UIImageView+WebCache.h"
#import "CMUImageSizeCached.h"
#import "UIColor+CMU.h"
#import "CMUOnlineVideoListTableViewCell.h"
#import "CMUSocial.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "NSString+URLEncoder.h"

static NSString *cellIdentifierIPhone = @"CMUOnlineVideoListTableViewCell";

@interface CMUOnlineVideoListViewController ()
@property(nonatomic, weak) IBOutlet UIView *titleView;
@property(nonatomic, weak) IBOutlet UILabel *titleLabel;
@property(nonatomic, weak) IBOutlet UIView *topView;
@property(nonatomic, weak) IBOutlet UILabel *lblSelectedVideoType;
@property(nonatomic, weak) IBOutlet UIButton *btnVideoType;
@property(nonatomic, weak) IBOutlet UIButton *btnSearch;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) IBOutlet ODRefreshControl *refreshControl;
@property(nonatomic, strong) CMUOnlineVideoTypePopupView *videoTypeView;
@property(nonatomic, strong) PopoverView *popoverView;

@property(nonatomic, unsafe_unretained) BOOL loadingMore;
@property(nonatomic, strong) UIActivityIndicatorView *loadMoreSpinner;

-(IBAction) backTapped:(id) sender;
-(IBAction) lblSelectedVideoTypeTapped:(id) sender;
-(IBAction) btnVideoTypeClicked:(id) sender;
-(IBAction) btnSearchClicked:(id) sender;

@property(nonatomic, strong) IBOutlet UIView *searchView;
@property(nonatomic, strong) IBOutlet UISearchBar *searchBar;

//fetch model
@property(nonatomic, unsafe_unretained)BOOL resetFeedList;
@property(nonatomic, unsafe_unretained)CMU_ONLINE_VIDEOL_TYPE videoType;
//@property(nonatomic, strong) NSString *searchKeyword;
@property(nonatomic, strong) NSString *refreshURL;
@property(nonatomic, strong) NSString *loadMoreURL;
@property(nonatomic, strong) NSArray *feedList;

@end

@implementation CMUOnlineVideoListViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.videoType = CMU_ONLINE_VIDEO_TYPE_RECENTLY_ADDED;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.titleLabel.text = self.cmuOnlineCuteAjarnFeed.cuteName;
    self.lblSelectedVideoType.textColor = [UIColor cmuBlackColor];
    
    self.videoTypeView = [[CMUOnlineVideoTypePopupView alloc] initWithFrame:IS_IPAD? CGRectMake(0, 0, 350, 176): CGRectMake(0, 0, 280, 176)];
    self.videoTypeView.delegate = self;
    
    //tableview
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUOnlineVideoListTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifierIPhone];
    
    
    //pull refresh
    self.refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [self.refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
    [self showHUDLoading];
    [self getVideoListByCriteria];
    
    self.loadMoreSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.loadMoreSpinner.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 44);
    
    [self updatePresentState];
}

//overide
- (void)updatePresentState
{
    [super updatePresentState];
    if(self.loadingMore && self.tableView.tableFooterView == nil)
    {
        self.tableView.tableFooterView = self.loadMoreSpinner;
        [self.loadMoreSpinner startAnimating];
    }
    else
    {
        self.tableView.tableFooterView = nil;
        [self.loadMoreSpinner stopAnimating];
    }
    
}

//overide
- (void)loginChangedNotification:(NSNotification *) notification
{
    [super loginChangedNotification:notification];
    //need to get new feed for current user
    self.resetFeedList = YES;
    [self showHUDLoading];
    [self getVideoListByCriteria];
}

- (void)getVideoListByCriteria
{
    NSString *searchText = self.searchBar.text;
    if([CMUStringUtils isEmpty:searchText])
    {
        [[CMUWebServiceManager sharedInstance] getCuteajarnByType:self cuteId:self.cmuOnlineCuteAjarnFeed.cuteId mode:self.videoType success:^(id result) {
            CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = (CMUOnlineVideoFeedModel *)result;
            self.refreshURL = [CMUStringUtils isEmpty:cmuOnlineVideoFeedModel.refreshURL]? nil: cmuOnlineVideoFeedModel.refreshURL;
            self.loadMoreURL = [CMUStringUtils isEmpty:cmuOnlineVideoFeedModel.loadMoreURL]? nil: cmuOnlineVideoFeedModel.loadMoreURL;
            [self populateFeedList:result];
            [self updatePresentState];
            [self dismisHUD];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];
    }
    else
    {
        [[CMUWebServiceManager sharedInstance] getSearchCuteajarn:self cuteId:self.cmuOnlineCuteAjarnFeed.cuteId key:searchText success:^(id result) {
            CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = (CMUOnlineVideoFeedModel *)result;
            self.refreshURL = [CMUStringUtils isEmpty:cmuOnlineVideoFeedModel.refreshURL]? nil: cmuOnlineVideoFeedModel.refreshURL;
            self.loadMoreURL = [CMUStringUtils isEmpty:cmuOnlineVideoFeedModel.loadMoreURL]? nil: cmuOnlineVideoFeedModel.loadMoreURL;
            [self populateFeedList:result];
            [self updatePresentState];
            [self dismisHUD];
        } failure:^(NSError *error) {
            [self dismisHUD];
        }];

    }
    
}


- (void)populateFeedList:(CMUOnlineVideoFeedModel *) videoFeedModel
{
    //check to reset feed list
    BOOL scrollToTop = self.resetFeedList;
    NSMutableArray *tempFeedList = [[NSMutableArray alloc] init];
    if(!self.resetFeedList)
    {
        [tempFeedList addObjectsFromArray:self.feedList];
    }
    self.resetFeedList = NO;
    
    //add new feed to feed list
    for(CMUOnlineVideoFeed *videoFeed in videoFeedModel.videoFeedList)
    {
        if(![tempFeedList containsObject:videoFeed])
        {
            [tempFeedList addObject:videoFeed];
        }
    }
    
    //perform sort
    NSArray *sortedFeedList = [tempFeedList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        CMUOnlineVideoFeed *first = (CMUOnlineVideoFeed*)a;
        CMUOnlineVideoFeed *second = (CMUOnlineVideoFeed*)b;
        return second.vdoId > first.vdoId;
    }];
    
    self.feedList = sortedFeedList;
    
    [self.tableView reloadData];
    if(scrollToTop)
    {
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}

- (void)setShowSearchView:(BOOL) show animated:(BOOL) animated
{
    self.searchView.hidden = !show;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dropViewDidBeginRefreshing:(id) sender
{
    if(self.refreshURL)
    {
        [[CMUWebServiceManager sharedInstance] getCuteajarnByURL:self url:self.refreshURL success:^(id result) {
            CMUOnlineVideoFeedModel *cmuOnlineVideoFeedModel = (CMUOnlineVideoFeedModel *)result;
            if(![CMUStringUtils isEmpty:cmuOnlineVideoFeedModel.refreshURL])
            {
                self.refreshURL = cmuOnlineVideoFeedModel.refreshURL;
            }
            [self populateFeedList:cmuOnlineVideoFeedModel];
            [self.refreshControl endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControl endRefreshing];
        }];
    }
    else
    {
        [self.refreshControl endRefreshing];
    }
}

-(void) backTapped:(id) sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(cmuOnlineVideoListViewControllerDidBack:)])
    {
        [_delegate cmuOnlineVideoListViewControllerDidBack:self];
    }
}

-(void) lblSelectedVideoTypeTapped:(id) sender
{
    [self btnVideoTypeClicked:sender];
}

-(void) btnVideoTypeClicked:(id) sender
{

    CGRect videoTypeButtonFrame = [self.view convertRect:self.btnVideoType.frame fromView:self.topView];
    CGPoint pointToShow = CGPointMake(videoTypeButtonFrame.origin.x + videoTypeButtonFrame.size.width / 2, videoTypeButtonFrame.origin.y + videoTypeButtonFrame.size.height - 5);
    self.popoverView = [[PopoverView alloc] initWithFrame:CGRectZero];
    self.popoverView.delegate = self;
    [self.popoverView showAtPoint:pointToShow inView:self.view withContentView:self.videoTypeView];

}

-(void) btnSearchClicked:(id) sender
{
    [self setShowSearchView:YES animated:YES];
    [self.searchBar becomeFirstResponder];
}


#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUOnlineVideoFeed *videoFeed =[self.feedList objectAtIndex:indexPath.row];
    BOOL hasImage =![CMUStringUtils isEmpty:videoFeed.vdoPic];
    
    CGFloat cellWidth = self.tableView.frame.size.width;
    CGFloat height = 0;
    
    
    if(IS_IPHONE)
    {
        height = 50;//  50 bottombar view nib;
    }
    else
    {
        height = 70;//  70 bottombar view nib;
    }
    
    if(hasImage)
    {
        NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:videoFeed.vdoPic];
        if(cachedSizeValue)
        {
            CGSize originalSize = [cachedSizeValue CGSizeValue];
            CGFloat imageWidth = cellWidth ;
            CGFloat imageHeight = 0;
            @try {
                imageHeight = imageWidth * originalSize.height / originalSize.width;
            }
            @catch (NSException *exception) {
            }
            @finally {
            }
            
            height += imageHeight;
        }
        else
        {
            if(IS_IPHONE)
            {
                height += 180;//default image ratio size
            }
            else
            {
                height += 432;//default image ratio size
            }
        }
    }
    else
    {
        if(IS_IPHONE)
        {
            height += 180;//default image ratio size
        }
        else
        {
            height += 432;//default image ratio size
        }
    }

    
    return height + 1;//separetor
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUOnlineVideoListTableViewCell *cell = nil;
    CMUOnlineVideoFeed *videoFeed = [self.feedList objectAtIndex:indexPath.row];

    cell = (CMUOnlineVideoListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifierIPhone forIndexPath:indexPath];

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.onlineVideoFeed = videoFeed;
    BOOL hasImage =![CMUStringUtils isEmpty:videoFeed.vdoPic];
    if(hasImage)
    {
        //__weak typeof(self) weakSelf = self;
        [cell.thumbnailImageView setImageWithURL:[NSURL URLWithString:[videoFeed.vdoPic urlFixSpace]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if(image && error == nil)
            {
                NSValue *cachedSizeValue = [[CMUImageSizeCached sharedInstance] getImageSizeCachedForURL:videoFeed.vdoPic];
                
                if(cachedSizeValue == nil)
                {
                    CGSize originalImageSize = image.size;
                    cachedSizeValue = [NSValue valueWithCGSize:originalImageSize];
                    [[CMUImageSizeCached sharedInstance] setImageSizeCached:originalImageSize forURL:videoFeed.vdoPic];
                    //[tableView beginUpdates];
                    [tableView reloadRowsAtIndexPaths:@[indexPath]
                                     withRowAnimation:UITableViewRowAnimationNone];
                    //[tableView endUpdates];
                }
            }
        }];
    }
    
    cell.thumbnailImageView.hidden = !hasImage;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUOnlineVideoFeed *videoFeed = [self.feedList objectAtIndex:indexPath.row];
    CMUOnlineVideoDetailViewController *vc = [[CMUOnlineVideoDetailViewController alloc] init];
    vc.delegate = self;
    vc.videoFeed = videoFeed;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if(self.loadMoreURL)
        {
            if(self.loadingMore)
            {
                return;
            }
            self.loadingMore = YES;
            [self updatePresentState];
            [[CMUWebServiceManager sharedInstance] getCuteajarnByURL:self url:self.loadMoreURL success:^(id result) {
                CMUOnlineVideoFeedModel *videoFeedModel = (CMUOnlineVideoFeedModel *)result;
                self.loadMoreURL = [CMUStringUtils isEmpty:videoFeedModel.loadMoreURL]? nil: videoFeedModel.loadMoreURL;
                [self populateFeedList:result];
                self.loadingMore = NO;
                [self updatePresentState];
            } failure:^(NSError *error) {
                self.loadingMore = NO;
                [self updatePresentState];
            }];
        }
    }
}


#pragma mark -PopoverViewDelegate
//Delegate receives this call once the popover has begun the dismissal animation
- (void)popoverViewDidDismiss:(PopoverView *)popoverView
{
    [[CMUSettings sharedInstance] saveNewsSettings];
}

//Delegate receives this call as soon as the item has been selected
- (void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    
}

#pragma mark -CMUOnlineVideoDetailViewControllerDelegate
- (void)cmuOnlineVideoDetailViewControllerDidBack:(CMUOnlineVideoDetailViewController *) vc
{
    [self.tableView reloadData];
    [self.navigationController popToViewController:self animated:YES];
}

#pragma mark -CMUNewsTypePopupViewDelegate
- (void)cmuOnlineVideoTypePopupView:(CMUOnlineVideoTypePopupView *)onlineVideoTypePopupView didSelectedVideoType:(CMUOnlineVideoType *)videoType
{
    [self.popoverView dismiss];
    self.videoType = videoType.videoTypeId;
    self.lblSelectedVideoType.text = videoType.title;
    self.resetFeedList = YES;
    self.searchBar.text = nil;
    [self showHUDLoading];
    [self getVideoListByCriteria];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    self.resetFeedList = YES;
    [self setShowSearchView:NO animated:YES];
    [self showHUDLoading];
    [self getVideoListByCriteria];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self setShowSearchView:NO animated:YES];
    
}
@end
