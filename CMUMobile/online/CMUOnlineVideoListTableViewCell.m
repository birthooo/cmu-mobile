//
//  CMUOnlineVideoListTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineVideoListTableViewCell.h"

@interface CMUOnlineVideoListTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel *lblView;
@property(nonatomic, weak)IBOutlet UILabel *lblDuration;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoLike;
@property(nonatomic, weak)IBOutlet UILabel *lblVdoName;

@end

@implementation CMUOnlineVideoListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setOnlineVideoFeed:(CMUOnlineVideoFeed *)onlineVideoFeed
{
    _onlineVideoFeed = onlineVideoFeed;
    self.lblVdoName.text = self.onlineVideoFeed.vdoName;
    self.lblView.text = [NSString stringWithFormat:@"%i",self.onlineVideoFeed.vdoview];
    self.lblVdoLike.text = [NSString stringWithFormat:@"%i",self.onlineVideoFeed.vdolike];
    self.lblDuration.text = self.onlineVideoFeed.vdotime;
}

@end
