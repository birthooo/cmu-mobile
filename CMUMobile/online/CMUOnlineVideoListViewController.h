//
//  CMUOnlineVideoListViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 8/27/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUOnlineVideoTypePopupView.h"
#import "CMUOnlineVideoListTableViewCell.h"
#import "PopoverView.h"
#import "CMUOnlineModel.h"
#import "CMUOnlineVideoDetailViewController.h"

@class CMUOnlineVideoListViewController;

@protocol CMUOnlineVideoListViewControllerDelegate <NSObject>
- (void)cmuOnlineVideoListViewControllerDidBack:(CMUOnlineVideoListViewController *) vc;
@end

@interface CMUOnlineVideoListViewController : CMUNavBarViewController<PopoverViewDelegate, CMUOnlineVideoTypePopupViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CMUOnlineVideoDetailViewControllerDelegate>
@property(nonatomic, weak)id<CMUOnlineVideoListViewControllerDelegate> delegate;
@property(nonatomic, strong)CMUOnlineCuteAjarnFeed * cmuOnlineCuteAjarnFeed;

@end
