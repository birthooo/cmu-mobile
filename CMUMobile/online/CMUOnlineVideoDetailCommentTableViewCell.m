//
//  CMUOnlineVideoDetailCommentTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/20/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineVideoDetailCommentTableViewCell.h"

@interface CMUOnlineVideoDetailCommentTableViewCell()
@property(nonatomic, weak)IBOutlet UILabel *lblName;
@property(nonatomic, weak)IBOutlet UILabel *lblComment;
@end

@implementation CMUOnlineVideoDetailCommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setOnlineCommentFeed:(CMUOnlineCommentFeed *)onlineCommentFeed
{
    _onlineCommentFeed = onlineCommentFeed;
    self.lblName.text = _onlineCommentFeed.accId;
    self.lblComment.text = _onlineCommentFeed.commentText;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblName.text = nil;
    self.lblComment.text = nil;
}

@end
