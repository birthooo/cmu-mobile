//
//  CMUFacultyTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 2/24/2558 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUOnlineTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UILabel* lblTitle;
@property(nonatomic, weak) IBOutlet UIImageView *imgAccesoryView;
@end
