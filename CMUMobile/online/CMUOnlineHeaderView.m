//
//  CMUOnlineHeaderView.m
//  CMUMobile
//
//  Created by Nikorn lansa on 2/22/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUOnlineHeaderView.h"

@implementation CMUOnlineHeaderModel

@end

#define SLIDE_ANIMATION_DURATION 0.3
@interface CMUOnlineHeaderView()
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property(nonatomic, strong) IBOutlet UIImageView *imvPrevious;
@property(nonatomic, strong) IBOutlet UIImageView *imvNext;
@property(nonatomic, strong) IBOutlet UILabel *lblTitle;
@end

@implementation CMUOnlineHeaderView
- (void)customInit
{
    // Initialization code
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CMUOnlineHeaderView" owner:self options:nil];
    UIView *contentView = [nibObjects objectAtIndex:0];
    [self addSubview:contentView];
    self.userInteractionEnabled = YES;
    CGRect c = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    contentView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.scrollView.delegate = self;
    [self.scrollView setShowsHorizontalScrollIndicator:false];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setBounces:NO];
    self.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
        [self updatePresentState];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
        [self updatePresentState];
    }
    return self;
}

- (void)setHeaderModelList:(NSArray *)headerModelList
{
    _headerModelList = headerModelList;
    [self initByModel];
    [self setSelectedIndex:0];
    [self updatePresentState];
}


- (void)setSelectedIndex:(int)selectedIndex
{
    [self doMovePageIndex:selectedIndex animate:NO];
}

#pragma mark event
- (IBAction)previousPress:(id)sender
{
    if([self canMovePreviousPage])
    {
        [self doMovePreviousPage:YES];
    }
}

- (IBAction)nextPress:(id)sender
{
    if([self canMoveNextPage])
    {
        [self doMoveNextPage:YES];
    }
}


- (void)initByModel
{
    
    CGSize pageSize = self.scrollView.frame.size;
    pageSize.width = self.frame.size.width;//don't know why scroll size incorrect
    pageSize.height = self.frame.size.height - self.lblTitle.frame.size.height;
    //reset view and model
    
    for(UIView *view in self.scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    //self.pageCount = (int)self.headerModelList.count;
    //self.pageCount = self.pageCount == 0? 1 : self.pageCount;
    
    self.scrollView.contentSize = CGSizeMake(self.headerModelList.count * pageSize.width, pageSize.height);
    
    for(int i = 0; i < self.headerModelList.count; i++)
    {
        CMUOnlineHeaderModel *model = [self.headerModelList objectAtIndex:i];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:model.image];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.frame = CGRectMake(i*pageSize.width, 0, pageSize.width, pageSize.height);
        [self.scrollView addSubview:imageView];
    }
    
    self.pageControl.numberOfPages = self.headerModelList.count;
    self.pageControl.hidden = self.pageControl.numberOfPages <= 1;
}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    CGSize pageSize = self.scrollView.frame.size;
//    pageSize.width = self.frame.size.width;
//    for(int i = 0; i < self.headerModelList.count; i++)
//    {
//        CMUOnlineHeaderModel *model = [self.headerModelList objectAtIndex:i];
//        UIImageView *imageView = [[UIImageView alloc] initWithImage:model.image];
//        imageView.contentMode = UIViewContentModeScaleToFill;
//        imageView.frame = CGRectMake(i*pageSize.width, 0, pageSize.width, pageSize.height);
//        [self.scrollView addSubview:imageView];
//    }
//}


- (int)currentPage
{
    CGFloat width = self.scrollView.frame.size.width;
    NSInteger pageIndex = (self.scrollView.contentOffset.x + (0.5f * width)) / width;
    return (int)pageIndex;
}

- (BOOL)canMoveNextPage
{
    return [self currentPage] < ((int)self.headerModelList.count - 1);
}

- (void)doMoveNextPage:(BOOL) animated
{
    [self doMovePageIndex:[self currentPage] + 1 animate:animated];
}


- (BOOL)canMovePreviousPage
{
    return [self currentPage] > 0;
}

- (void)doMovePreviousPage:(BOOL) animated
{
    [self doMovePageIndex:[self currentPage] + -1 animate:animated];
}

- (void)doMovePageIndex:(int)pageIndex animate:(BOOL) animated
{
    CGPoint contentOffset = CGPointMake(self.scrollView.frame.size.width * pageIndex, 0);
    CGFloat duration = 0.0;
    if(animated)
    {
        duration = SLIDE_ANIMATION_DURATION;
    }
    
    [UIView animateWithDuration:duration animations:^{
        self.scrollView.contentOffset = contentOffset;
    }];
}

- (void)updatePresentState
{
    int currentPage = [self currentPage];
    self.pageControl.currentPage = currentPage;
    self.imvPrevious.hidden = ![self canMovePreviousPage];
    self.imvNext.hidden = ![self canMoveNextPage];
    
    if(self.headerModelList.count > 0)
    {
        CMUOnlineHeaderModel *model = [self.headerModelList objectAtIndex:currentPage];
        self.lblTitle.text = model.title;
    }
    else
    {
        self.lblTitle.text = nil;
    }
    
}


#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //DebugLog(@"");
    [self updatePresentState];
}
// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
}

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
}
// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    //DebugLog(@"");
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    //DebugLog(@"");
}   // called on finger up as we are moving
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //DebugLog(@"");
}      // called when scroll view grinds to a halt

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    //DebugLog(@"");
} // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating



@end
