//
//  CMUNavBarViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 6/12/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUAppDelegate.h"
#import "global.h"
#import "CMUFormatUtils.h"
#import "CMUSettings.h"

#import "OAuthLoginViewController.h"

@interface CMUNavBarViewController ()

@end

@implementation CMUNavBarViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(notificationUpdatedNotification:)
                                                     name:CMU_NOTIFICATION_NUMBER_UPDATE_NOTIFICATION
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableButton)
                                                 name:@"disableButton"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableButton)
                                                 name:@"enableButton"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doLogin)
                                                 name:@"btnLoginAction"
                                               object:nil];
    
    // Do any additional setup after loading the view.
    //bar buttom item
    UIImage *menuIcon = [UIImage imageNamed:@"title_bar_menu_icon.png"];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:menuIcon forState:UIControlStateNormal];
    [menuButton setImage:menuIcon forState:UIControlStateHighlighted];
    menuButton.frame = CGRectMake(0, 0, 22, 22);
    //[menuButton sizeToFit];
    [menuButton addTarget:self action:@selector(btnLeftViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.menuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];

    UIImage *cmuIcon = [UIImage imageNamed:@"titlebar_cmu_icon.png"];
    UIButton *cmuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cmuButton setImage:cmuIcon forState:UIControlStateNormal];
    [cmuButton setImage:cmuIcon forState:UIControlStateHighlighted];
    cmuButton.frame = CGRectMake(0, 0, 60, 22);
    //[cmuButton sizeToFit];
    [cmuButton addTarget:self action:@selector(btnHomeClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.cmuButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cmuButton];

    UIImage *notiIcon = [UIImage imageNamed:@"titlebar_noti_icon.png"];
    UIButton *notiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notiButton setImage:notiIcon forState:UIControlStateNormal];
    [notiButton setImage:notiIcon forState:UIControlStateHighlighted];
    notiButton.frame = CGRectMake(0, 0, 40, 22);
    //[notiButton sizeToFit];
    [notiButton addTarget:self action:@selector(btnNotiClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.notiButtonItem = [[BBBadgeBarButtonItem alloc] initWithCustomView:notiButton];
    self.notiButtonItem.badgeBGColor = [UIColor redColor];
    self.notiButtonItem.badgeTextColor = [UIColor whiteColor];
    self.notiButtonItem.badgeOriginX = 22;
    self.notiButtonItem.badgeOriginY = -7;
    self.notiButtonItem.shouldHideBadgeAtZero = YES;
    self.notiButtonItem.shouldAnimateBadge = YES;
    self.notiButton = notiButton;
    
    //UIImage *loginIcon = [UIImage imageNamed:@"titlebar_login_icon.png"];
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    self.loginButton.contentEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
    self.loginButton.frame = CGRectMake(0, 0, 50, 22);
    self.loginButton.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    //[loginButton sizeToFit];
    [self.loginButton addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.loginButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.loginButton];
    
    UIImage *separatorIcon = [UIImage imageNamed:@"titlebar_separator.png"];
    //UIImageView *separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 2, 22)];
    //separatorImageView.image = separatorIcon;
    
    UIButton *separaterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [separaterButton setImage:separatorIcon forState:UIControlStateNormal];
    [separaterButton setImage:separatorIcon forState:UIControlStateHighlighted];
    separaterButton.frame = CGRectMake(0, 0, 5, 22);
    //[separaterButton sizeToFit];
    
    self.separatorButtonItem = [[UIBarButtonItem alloc] initWithCustomView:separaterButton];

    [self updatePresentState];
    
   

}
-(void)enableButton{

    NSLog(@"YES NOW LOGIN");
    self.loginButtonItem.enabled = YES;
    self.cmuButtonItem.enabled = YES;
    self.menuButtonItem.enabled = YES;
    self.notiButtonItem.enabled = YES;

    
}
-(void)disableButton{
    NSLog(@"LOG OUT COMPLETE");
    self.loginButtonItem.enabled = NO;
    self.cmuButtonItem.enabled = NO;
    self.menuButtonItem.enabled = NO;
    self.notiButtonItem.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) notificationUpdatedNotification:(NSNotification *) notification
{
    [self updateBadgeView];
}
                                  
- (void)updateBadgeView
{
    int notificationNumber = [[CMUWebServiceManager sharedInstance] notificationNumberModel].notificationNumber;
    self.notiButtonItem.badgeValue = [CMUFormatUtils formatBadge:notificationNumber];
}

- (void)updatePresentState
{
    [super updatePresentState];
    
    [self updateBadgeView];
    
    CMUWebServiceManager *webServiceManager =  [CMUWebServiceManager sharedInstance];
    BOOL showMenuButton = [webServiceManager isLogin];
    BOOL showCMUButton = YES;
    BOOL showNotiButton = YES;
    //BOOL showLoginButton = YES;
    
    NSMutableArray *leftMenus = [[NSMutableArray alloc] init];
    
//#ifndef PROD
    if(showMenuButton && self.menuButtonItem)
    {
        [leftMenus addObject:self.menuButtonItem];
    }
//#endif
    if(showCMUButton && self.cmuButtonItem)
    {
        [leftMenus addObject:self.cmuButtonItem];
    }
    
    NSMutableArray *rightMenus = [[NSMutableArray alloc] init];
    
    //if(showLoginButton)
    //{
//#ifndef PROD
    if(self.loginButtonItem)
    [rightMenus addObject:self.loginButtonItem];
    
    if(self.loginButtonItem)
    [rightMenus addObject:self.separatorButtonItem];
    
    if(![webServiceManager isLogin])
    {
        [self.loginButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    }
    else
    {
        [self.loginButton setTitle:@"LOG OUT" forState:UIControlStateNormal];
    }
    [self.loginButton sizeToFit];
    NSLog(@"%@",NSStringFromCGRect(self.loginButton.frame));
    //![webServiceManager isLogin]
    //}
//#endif
    if(showNotiButton && self.notiButtonItem)
    {
        [rightMenus addObject:self.notiButtonItem];
    }
 
    
    self.navigationItem.leftBarButtonItems = leftMenus;
    self.navigationItem.rightBarButtonItems = rightMenus;
}

- (void)btnLeftViewClicked:(id) sender
{
    [AppDelegate goLeftView];
}

- (void)btnHomeClicked:(id) sender
{
    [AppDelegate goHome:NO];
}

- (void)btnNotiClicked:(id) sender
{
    [AppDelegate goNotification:NotiTabNotification];
}

#pragma mark Login / Logout
- (void)btnLoginClicked:(id) sender
{
    if(![[CMUWebServiceManager sharedInstance] isLogin])
    {
        [self doLogin];
    }
    else
    {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                            delegate:self
                                                   cancelButtonTitle:@"CANCEL"
                                              destructiveButtonTitle:@"LOG OUT"
                                                   otherButtonTitles:nil];
        sheet.tag = 1;
        [sheet showInView:self.view];
    }
}

- (void)doLogin
{
    CMULoginViewController *loginViewController = [[CMULoginViewController alloc] init];
    loginViewController.delegate = self;
    [self.navigationController presentViewController:loginViewController animated:YES completion:nil];

//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OAuthLogin" bundle: nil];
//    UIViewController *lvc = [storyboard instantiateViewControllerWithIdentifier:@"OAuthLogin"];
//    [self presentViewController:lvc animated:YES completion:nil];

    
}

- (void)doLogout
{
    //[[NSUserDefaults standardUserDefaults]setBool:1 forKey:@"isLogout"];
    [self  showHUDLoading];
    [[CMUWebServiceManager sharedInstance] logout:self success:^(id result) {
        [self dismisHUD];
        CMUSettings *share = [[CMUSettings sharedInstance]init];
        [share loadMenuItems];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:CMU_LOGIN_CHANGED_NOTIFICATION object:self];

        } failure:^(NSError *error) {
            [self dismisHUD];
            CMUSettings *share = [[CMUSettings sharedInstance]init];
            [share loadMenuItems];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goMIS" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goHome" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:CMU_LOGIN_CHANGED_NOTIFICATION object:self];
            
         

    }];
}

#pragma mark CMULoginViewControllerDelegate
- (void)loginViewControllerDidLogin:(CMULoginViewController *) loginViewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:CMU_LOGIN_CHANGED_NOTIFICATION object:self];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}

- (void)loginViewControllerDidCancel:(CMULoginViewController *) loginViewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag==1){
    if (buttonIndex == 0) { // logout
        [self doLogout];
    }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
