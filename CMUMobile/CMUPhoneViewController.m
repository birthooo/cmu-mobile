//
//  CMUPhoneViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 7/3/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUPhoneViewController.h"
#import "CMUFileUtils.h"
#import "global.h"
#import "CMUPhoneFacultyTableViewCell.h"
#import "CMUAppDelegate.h"
#define CMU_DEFAULT_PHONE_CONTENT_DIR @"phone"
static NSString *cellIdentifier = @"CMUPhoneFacultyTableViewCell";

@interface CMUPhoneViewController ()
@property(nonatomic, strong) CMUPhoneVersionController *versionController;
@property(nonatomic, strong) NSMutableArray *facultyList;//CMUPhoneFacultyView
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@end

@implementation CMUPhoneViewController
- (IBAction)backButtonAction:(id)sender {
    [AppDelegate goPhone];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSString *absoluteContentPath = [[CMUFileUtils pathForDocumentFolder] stringByAppendingPathComponent:CMU_DEFAULT_PHONE_CONTENT_DIR];
        NSError *error;
        
        self.versionController = [[CMUPhoneVersionController alloc] initWithContentPath:absoluteContentPath error:&error];
        self.versionController.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.facultyList = self.versionController.currentPhoneModel.phoneFacultyViewList;
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUPhoneFacultyTableViewCell" bundle:nil] forCellReuseIdentifier:cellIdentifier];
    [self.versionController startCheckVersion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.facultyList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 70;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUPhoneFacultyTableViewCell *cell = nil;
    cell = (CMUPhoneFacultyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.phoneFacultyView = [self.facultyList objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //like android
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CMUPhoneFacultyViewController *viewController = [[CMUPhoneFacultyViewController alloc] init];
    viewController.phoneFacultyView = [self.facultyList objectAtIndex:indexPath.row];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - CMUPhoneFacultyViewControllerDelegate
- (void)phoneFacultyViewControllerBackButtonClicked:(CMUPhoneFacultyViewController *) phoneFacultyViewController
{
    [self.navigationController popToViewController:self animated:YES];
}

#pragma mark - CMUPhoneVersionControllerDelegate
- (void)phoneVersionControllerDidStartedCheckNewVersion:(CMUPhoneVersionController *)versionController
{
    [self showHUDLoading];
    DebugLog(@"");
}

- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didFinishedCheckNewVersion:(CMUPhoneVersionModel *) newVersion//nil = no update
                         error:(NSError*) error
{
    DebugLog(@"");
    if(newVersion == nil || error)
    {
        [self dismisHUD];
    }
}

- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didStartedDownloadVersion:(CMUPhoneVersionModel *) newVersion
{
    DebugLog(@"");
}

- (void)phoneVersionController:(CMUPhoneVersionController *)versionController didFinishedDownloadVersion:(CMUPhoneVersionModel *) serverVersion
         phoneFacultyViewModel:(CMUPhoneFacultyViewModel *) phoneModel
                         error:(NSError *) error
{
    DebugLog(@"");
    [self dismisHUD];
    self.facultyList = self.versionController.currentPhoneModel.phoneFacultyViewList;
    [self.tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
