//
//  CMUSISAdviserViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/16/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISAdviserViewController.h"
#import "CMUWebServiceManager.h"

@interface CMUSISAdviserViewController ()

@end

@implementation CMUSISAdviserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CMUUser *user = [[[CMUWebServiceManager sharedInstance] userInfoModel] user];
    CMUTicket *userticket = [[CMUWebServiceManager sharedInstance] ticket];
    NSString *stringGET;
    stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/Advisor/Advisor?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:stringGET]];
    [self.myWebView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
