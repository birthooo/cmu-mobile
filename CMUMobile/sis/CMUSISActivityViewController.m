//
//  CMUSISActivityViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/16/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISActivityViewController.h"
#import "CMUSISActivityTableViewCell.h"

@interface CMUSISActivityViewController ()
{
    NSArray *yearArray;
    NSArray *activityNameArray;
    NSArray *startDateArray;
    NSArray *endDateArray;
    NSArray *numberArray;
    NSArray *signedArray;
    NSArray *availableArray;
    NSArray *joinedArray;
  
}
@end

@implementation CMUSISActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    yearArray = @[@"2557",@"2558",@"2559",@"2560"];
    activityNameArray = @[@"สอนกอล์ฟเบื้องต้น",@"Putting Challenge 2014",@"เสวนาอ่านวรรณกรรมด้วยหัวใจ",@"ค่ายพัฒนาผู้นำสภานักศึกษา มหาวิทยาลัยเชียงใหม่"];
    startDateArray = @[@"1 มกราคม 2557",@"2 กุมภาพันธ์ 2558",@"3 มีนาคม 2559",@"4 เมษายน 2560"];
    endDateArray = @[@"1 มกราคม 2557",@"2 กุมภาพันธ์ 2558",@"3 มีนาคม 2559",@"4 เมษายน 2560"];
    numberArray = @[@"1",@"2",@"3",@"4"];
    signedArray = @[@"292",@"122",@"47",@"25"];
    availableArray = @[@"0",@"1",@"10",@"17"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 4;
}



 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
CMUSISActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityCell" forIndexPath:indexPath];
     cell.yearLabel.text = [yearArray objectAtIndex:indexPath.row];
     cell.activityNameLabel.text = [activityNameArray objectAtIndex:indexPath.row];
     cell.startDateLabel.text = [startDateArray objectAtIndex:indexPath.row];
     cell.endDateLabel.text = [endDateArray objectAtIndex:indexPath.row];
     cell.numberLabel.text = [numberArray objectAtIndex:indexPath.row];
     cell.signedLabel.text = [signedArray objectAtIndex:indexPath.row];
     cell.availableLabel.text = [availableArray objectAtIndex:indexPath.row];
 // Configure the cell...
 
 return cell;
 }

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
