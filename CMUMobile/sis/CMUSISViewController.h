//
//  CMUSISViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/9/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUSISEvaluateViewController.h"

typedef enum {
    SISTabSubject,
    SISTabTeacher
}SISTab;

@interface CMUSISViewController : CMUNavBarViewController<UITableViewDataSource, UITableViewDelegate, CMUSISEvaluateViewControllerDelegate>
@property(nonatomic, assign)SISTab selectedTab;
- (void)refreshData:(BOOL) scrollToTop;
@end
