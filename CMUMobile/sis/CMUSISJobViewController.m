//
//  CMUSISJobViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/9/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISJobViewController.h"

@interface CMUSISJobViewController ()
{
    NSArray *menu;
}
@end

@implementation CMUSISJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableview sizeToFit];
    menu = @[@"แจ้งความประสงค์สมัครงาน",@"ตำแหน่งงานที่เปิดรับสมัคร",@"ผลการสมัครงาน",];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menu.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   CMUSISJobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"jobCell" forIndexPath:indexPath];
    cell.TitleLabel.text = [menu objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    CMUSISJobTableViewCell *selectedCell= [tableView cellForRowAtIndexPath:indexPath];
    CMUSISJobDetailViewController *vc = [[CMUSISJobDetailViewController alloc]init];
    vc.selectedTitle = selectedCell.TitleLabel.text;
   // vc.selectedURL = [url objectAtIndex:indexPath.row];
   // NSLog(@"url : %@",[url objectAtIndex:indexPath.row]);

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CMUSISJobDetailViewController *vc = [segue destinationViewController];
    CMUSISJobTableViewCell *cell = (CMUSISJobTableViewCell *) sender;
    NSIndexPath *indexPath = [self.tableview indexPathForCell:cell];
    if([segue.identifier isEqual:@"jobSegue"]){
        vc.selectedTitle = [menu objectAtIndex:indexPath.row];
    }
    
    
}

@end
