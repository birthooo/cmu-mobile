//
//  CMUSISJobTableViewCell.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/10/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUSISJobTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;

@end
