//
//  CMUSISStep3ViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 3/11/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMUSISStep3ViewController : CMUNavBarViewController<UIWebViewDelegate>
@property(nonatomic, strong) NSString *url;
@end
