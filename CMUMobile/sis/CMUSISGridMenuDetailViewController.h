//
//  CMUSISGridMenuDetailViewController.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 10/13/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMUSISGridMenuDetailViewController : CMUNavBarViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString *selectedTitle;
@property NSString *selectedWebView;
@end
