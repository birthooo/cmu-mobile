//
//  CMUSISGridMenuDetailViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 10/13/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISGridMenuDetailViewController.h"
#import "CMUUserInfoModel.h"
#import "CMUModel.h"
#import "CMUWebServiceManager.h"
#import "UIColor+CMU.h"
#import "CMUAppDelegate.h"
@interface CMUSISGridMenuDetailViewController ()

@end

@implementation CMUSISGridMenuDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.selectedTitle;
    self.webView.scrollView.bounces = NO;
    NSURL *url = [NSURL URLWithString:self.selectedWebView];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    self.webView.delegate  = self;
    [self.webView loadRequest:urlRequest];

  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)BackButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
