//
//  ActivityViewController.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 11/29/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
@interface ActivityViewController : CMUNavBarViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
