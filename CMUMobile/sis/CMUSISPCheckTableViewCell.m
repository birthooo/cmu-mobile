//
//  SISPCheckTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/28/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISPCheckTableViewCell.h"
@interface CMUSISPCheckTableViewCell()
@property(nonatomic, weak) IBOutlet UIImageView *imvCheck;
@property(nonatomic, weak) IBOutlet UILabel *lblQuestionName;
@end

@implementation CMUSISPCheckTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)setPCheckQuestion:(CMUSISPCheckQuestion *)pCheckQuestion
{
    _pCheckQuestion = pCheckQuestion;
    [self loadFromModel];
}

- (void)loadFromModel
{
    self.imvCheck.image = self.pCheckQuestion.check? [UIImage imageNamed:@"sis_pcheck_checked"]:[UIImage imageNamed:@"sis_pcheck_unchecked"];
    self.lblQuestionName.text = [NSString stringWithFormat:@"%i. %@", self.pCheckQuestion.questionId, self.pCheckQuestion.questionName];
    self.lblQuestionName.font = self.pCheckQuestion.check?[UIFont boldSystemFontOfSize:16.0]:[UIFont systemFontOfSize:16.0];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imvCheck.image = nil;
    self.lblQuestionName.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
