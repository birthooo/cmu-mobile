//
//  SISPCheckViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/28/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISPCheckViewController.h"
#import "CMUSISPCheckTableViewCell.h"
#import "CMUUIUtils.h"
#import "CMUSISModel.h"
#import "CMUWebServiceManager.h"
#import "CMUSISStep3ViewController.h"
typedef enum {
    PCheckStep1 = 1,
    PCheckStep2 = 2,
} CMUPCheckStep;

static NSString *pCheckTableViewCell = @"CMUSISPCheckTableViewCell";

@interface CMUSISPCheckViewController ()
@property(nonatomic, unsafe_unretained)CMUPCheckStep pCheckStep;
@property(nonatomic, strong)IBOutlet UILabel *lblTitle;
@property(nonatomic, strong)IBOutlet UILabel *lblDetail;
@property(nonatomic, strong)IBOutlet UITableView *tableView;
@property(nonatomic, strong)IBOutlet UITableViewCell *footerCell;
@property(nonatomic, strong)IBOutlet UIButton *btnSubmit;

@property(nonatomic, strong)NSMutableArray *step2Questions;
- (IBAction)submitTapped:(id)sender;
@end

@implementation CMUSISPCheckViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        _pCheckStep = PCheckStep1;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUSISPCheckTableViewCell" bundle:nil] forCellReuseIdentifier:pCheckTableViewCell];
    self.btnSubmit.layer.cornerRadius = 5.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updatePresentState
{
    [super updatePresentState];
    self.lblTitle.text = self.pCheckModel.title;
    if(self.pCheckStep == PCheckStep1)
    {
        self.lblDetail.text = @"ตอนที่ 1 \nเลือกข้อที่คิดว่าเป็นปัญหาของท่าน";
        [self.btnSubmit setTitle:@"Step 2" forState:UIControlStateNormal];
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        self.lblDetail.text = @"ตอนที่ 2 \nเลือกข้อที่ท่านรู้สึกกังวลใจมากที่สุด";
        [self.btnSubmit setTitle:@"Step 3" forState:UIControlStateNormal];
    }
}

#pragma mark section table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.pCheckStep == PCheckStep1)
    {
        return self.pCheckModel.questionViewList1.count + 1;// +1 = submit cell
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        return self.step2Questions.count + 1;// +1 = submit cell
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(self.pCheckStep == PCheckStep1)
    {
        if(indexPath.row == self.pCheckModel.questionViewList1.count)
        {
            return 80;
        }
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        if(indexPath.row == self.step2Questions.count)
        {
            return 80;
        }
    }
    
    
    CMUSISPCheckQuestion *  model = nil;
    if(self.pCheckStep == PCheckStep1)
    {
        model = [self.pCheckModel.questionViewList1 objectAtIndex:indexPath.row];
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        model = [self.step2Questions objectAtIndex:indexPath.row];
    }
    
    
    CGFloat cellWidth = self.tableView.frame.size.width;
    
    CGFloat labelWidth = cellWidth - (320 - 248); //248 lebel width
    
    UIFont *font = nil;
    if(model.check)
    {
        font = [UIFont boldSystemFontOfSize:16.0]; //according to iphone nib
    }
    else
    {
        font = [UIFont systemFontOfSize:16.0]; //according to iphone nib
    }
    
    
    CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:[NSString stringWithFormat:@"%i. %@", model.questionId, model.questionName] withFont:font];
    
    CGFloat height = size.height + (5 + 5);//20 fit label height 5 = pading
    height = height  < 52? 52:height; // 52 nib height
    return height + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(self.pCheckStep == PCheckStep1)
    {
        if(indexPath.row == self.pCheckModel.questionViewList1.count)
        {
            return self.footerCell;
        }
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        if(indexPath.row == self.step2Questions.count)
        {
            return self.footerCell;
        }
    }
    
    
    CMUSISPCheckTableViewCell *cell = (CMUSISPCheckTableViewCell*)[tableView dequeueReusableCellWithIdentifier:pCheckTableViewCell forIndexPath:indexPath];
    if(self.pCheckStep == PCheckStep1)
    {
        cell.pCheckQuestion = [self.pCheckModel.questionViewList1 objectAtIndex:indexPath.row];
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        cell.pCheckQuestion = [self.step2Questions objectAtIndex:indexPath.row];
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUSISPCheckQuestion *  model = nil;
    if(self.pCheckStep == PCheckStep1)
    {
        model = [self.pCheckModel.questionViewList1 objectAtIndex:indexPath.row];
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        model = [self.step2Questions objectAtIndex:indexPath.row];
    }
    
    model.check = !model.check;
    NSArray* rowsToReload = [NSArray arrayWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

- (void)submitTapped:(id)sender
{
    if(self.pCheckStep == PCheckStep1)
    {
        self.pCheckStep = PCheckStep2;
        
        self.step2Questions = [[NSMutableArray alloc] init];
        for(int i = 0; i < self.pCheckModel.questionViewList1.count; i++)
        {
            CMUSISPCheckQuestion *question = [self.pCheckModel.questionViewList1 objectAtIndex:i];
            if(question.check)
            {
                question.check = false;
                [self.step2Questions addObject:question];
            }
        }
        
        [self.tableView reloadData];
        [self updatePresentState];
    }
    else if(self.pCheckStep == PCheckStep2)
    {
        NSMutableArray *list1 = self.step2Questions;
        NSMutableArray *list2  = [[NSMutableArray alloc] init];
        for(int i = 0; i < self.step2Questions.count; i++)
        {
            CMUSISPCheckQuestion *question = [self.step2Questions objectAtIndex:i];
            if(question.check)
            {
                question.check = false;
                [list2 addObject:question];
            }
        }
        
        self.pCheckModel.questionViewList1 = list1;
        self.pCheckModel.questionViewList2 = list2;
        
        [[CMUWebServiceManager sharedInstance] getPCheckPart3Question:self pCheckModel:self.pCheckModel success:^(id result) {
            DebugLog(@"");
            CMUSISStep3ViewController *sisStep3ViewController = [[CMUSISStep3ViewController alloc] init];
            sisStep3ViewController.url = JSON_GET_OBJECT([result objectForKey:@"Text"]);
            [self.navigationController pushViewController:sisStep3ViewController animated:NO];
        } failure:^(NSError *error) {
            DebugLog(@"");
            [self alert:CMU_ERROR_SAVE_DATA];
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
