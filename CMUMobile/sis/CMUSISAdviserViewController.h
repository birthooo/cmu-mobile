//
//  CMUSISAdviserViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/16/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMUSISAdviserViewController : CMUNavBarViewController
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end
