//
//  CMUSISProfileViewController.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/8/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISProfileViewController.h"

@interface CMUSISProfileViewController (){
    NSArray *menu;
    NSArray *url;
    __weak IBOutlet UITableView *tableview;
}

@end

@implementation CMUSISProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menu = @[@"ข้อมูลส่วนบุคคล",
             @"ข้อมูลด้านการศึกษา",
             @"e-Resume",
             @"ข้อมูลองค์การนักศึกษา",
             @"ข้อมูลกิจกรรมนักศึกษา",
             @"ข้อมูลทุนการศึกษา",
             @"ข้อมูลวินัยนักศึกษา",
             @"ข้อมูลเกียรติประวัติ",
             @"ข้อมูลการทำงานพิเศษ",
             @"ข้อมูลด้านยานพาหนะ",];
    
    url = @[@"",
            @"",
            @"",
            @"",
            @"",
            @"",
            @"",
            @"",
            @"",
            @"",
            ];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menu.count;
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     CMUSISProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileCell" forIndexPath:indexPath];
     cell.nameLabel.text = [menu objectAtIndex:indexPath.row];
     return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CMUSISProfileTableViewCell *selectedCell= [tableView cellForRowAtIndexPath:indexPath];
    CMUSISProfileDetail *vc = [[CMUSISProfileDetail alloc]init];
    vc.selectedLabel = selectedCell.nameLabel.text;
    vc.selectedURL = [url objectAtIndex:indexPath.row];
    NSLog(@"url : %@",[url objectAtIndex:indexPath.row]);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CMUSISProfileDetail *vc = [segue destinationViewController];
    CMUSISProfileTableViewCell *cell = (CMUSISProfileTableViewCell *) sender;
    NSIndexPath *indexPath = [tableview indexPathForCell:cell];
    if([segue.identifier isEqual:@"testSegue"]){
        vc.selectedLabel = [menu objectAtIndex:indexPath.row];
    }
    

}

@end
