//
//  SISPCheckViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/28/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUSISModel.h"

@interface CMUSISPCheckViewController : CMUNavBarViewController
@property(nonatomic, strong) CMUSISPCheckModel *pCheckModel;
@end
