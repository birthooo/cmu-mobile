//
//  CMUSISProfileDetail.m
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/9/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISProfileDetail.h"

@interface CMUSISProfileDetail ()


@end

@implementation CMUSISProfileDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.selectedLabel;
    [self.detailWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.selectedURL]]];
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
