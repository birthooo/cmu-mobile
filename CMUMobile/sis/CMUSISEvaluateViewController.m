//
//  CMUSISEvaluateViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISEvaluateViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUModel.h"
#import "CMUSISEvaluateTableViewCell.h"
#import "CMUUIUtils.h"
#import <QuartzCore/QuartzCore.h>

static NSString *evaluateTableViewCell = @"CMUSISEvaluateTableViewCell";

@interface CMUSISEvaluateViewController ()
@property(nonatomic, strong)IBOutlet UILabel *lblTitle;
@property(nonatomic, strong)IBOutlet UILabel *lblDetail;
@property(nonatomic, strong)IBOutlet UITableView *tableView;
@property(nonatomic, strong)IBOutlet UITableViewCell *footerCell;
@property(nonatomic, strong)IBOutlet UILabel *footerTitle;
@property(nonatomic, strong)IBOutlet UITextView *commentTextView;
@property(nonatomic, strong)IBOutlet UIButton *btnSubmit;

@property(nonatomic, strong)NSString *year;
@property(nonatomic, strong)NSString *term;
@property(nonatomic, strong)NSString *subjectCode;
@property(nonatomic, unsafe_unretained)int sectionId;
@property(nonatomic, strong)NSString *teacherId;
@property(nonatomic, strong)NSString *questType;

@property(nonatomic, strong)CMUSISEvalFormModel *evaFormModel;

- (IBAction)backTapped:(id)sender;
- (IBAction)submitTapped:(id)sender;
@end

@implementation CMUSISEvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CMUSISEvaluateTableViewCell" bundle:nil] forCellReuseIdentifier:evaluateTableViewCell];
    
    self.commentTextView.layer.borderWidth = 1.0f;
    self.commentTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.commentTextView.layer.cornerRadius = 5.0f;
    
    self.btnSubmit.layer.cornerRadius = 5.0f;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    [tap setNumberOfTouchesRequired:1];
    [tap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tap];
    
    if([self.evaModel isKindOfClass:[CMUSISEvalSubject class]])
    {
        CMUSISEvalSubject *model = (CMUSISEvalSubject *)self.evaModel;
        self.year = model.year;
        self.term = model.term;
        self.subjectCode = model.subjectCode;
        self.sectionId = model.sectionId;
        self.teacherId = nil;
        self.questType = model.questType;
        
        self.lblTitle.text = model.subjectCode;
        self.lblDetail.text = model.subjectName;
        
    }
    else if([self.evaModel isKindOfClass:[CMUSISEvalTeacher class]])
    {
        CMUSISEvalTeacher *model = (CMUSISEvalTeacher *)self.evaModel;
        self.year = model.year;
        self.term = model.term;
        self.subjectCode = model.subjectCode;
        self.sectionId = model.sectionId;
        self.teacherId = model.teacherId;
        self.questType = model.questType;
        
        self.lblTitle.text = model.teacherName;
        self.lblDetail.text = [NSString stringWithFormat:@"%@ %@", model.subjectCode, model.subjectName];
    }
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getEvaSubjectForm:self year:self.year term:self.term subjectCode:self.subjectCode sectionId:self.sectionId teacherId:self.teacherId questType:self.questType success:^(id result) {
        self.evaFormModel = (CMUSISEvalFormModel *)result;
        [self.tableView reloadData];
        [self dismisHUD];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [self dismisHUD];
        [self alert:CMU_ERROR_READ_DATA];
    }];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backTapped:(id)sender
{
    if(_delegate && [_delegate respondsToSelector:@selector(evaluateViewControllerBackButtonClicked:)])
    {
        [_delegate evaluateViewControllerBackButtonClicked:self];
    }
}

- (void)viewTapped:(id) sender
{
    [self.view endEditing:YES];
}

- (void)submitTapped:(id)sender
{
    NSString *errorMessage = nil;
    for(int i = 0; i < self.evaFormModel.evaFormList.count; i++)
    {
        CMUSISEvalForm *evalForm = [self.evaFormModel.evaFormList objectAtIndex:i];
        if(evalForm.score == 0)
        {
            errorMessage = @"กรุณาตอบให้ครบทุกข้อ";
        }
    }
    
    if(errorMessage)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:CMU_APP_NAME message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    self.evaFormModel.comment = self.commentTextView.text;
    
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] saveEvaSubjectForm:self subjectCode:self.subjectCode sectionId:self.sectionId teacherId:self.teacherId evaFormModel:self.evaFormModel success:^(id result) {
        [self dismisHUD];
        if(_delegate && [_delegate respondsToSelector:@selector(evaluateViewControllerDidFinishedSaveForm:)])
        {
            [_delegate evaluateViewControllerDidFinishedSaveForm:self];
        }
    } failure:^(NSError *error) {
        [self dismisHUD];
        [self alert:CMU_ERROR_SAVE_DATA];
    }];
}

#pragma mark section table view
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.evaFormModel.evaFormList.count == 0)
        return 0;
    return self.evaFormModel.evaFormList.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.evaFormModel.evaFormList.count)
    {
        return 211;
    }
    
    CMUSISEvalForm *evalForm = [self.evaFormModel.evaFormList objectAtIndex:indexPath.row];
    CGFloat cellWidth = self.tableView.frame.size.width;
    CGFloat height = 120 - 20;//20 fit label height
    CGFloat labelWidth = cellWidth - (8 + 8);
    
    UIFont *font = [UIFont systemFontOfSize:16.0]; //according to iphone nib
    if(IS_IPAD)
    {
        font = [UIFont systemFontOfSize:20.0];
    }
    CGSize size = [CMUUIUtils sizeThatFit:labelWidth withText:[NSString stringWithFormat:@"%i. %@", (int)indexPath.row + 1, evalForm.question] withFont:font];
    height += size.height;
    return height + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == self.evaFormModel.evaFormList.count)
    {
        self.footerTitle.text = [NSString stringWithFormat:@"%i. ข้อเสนอแนะเพิ่มเติม", self.evaFormModel.evaFormList.count + 1];
        
        return self.footerCell;
    }
    
    CMUSISEvaluateTableViewCell *cell = (CMUSISEvaluateTableViewCell*)[tableView dequeueReusableCellWithIdentifier:evaluateTableViewCell forIndexPath:indexPath];
    CMUSISEvalForm *evalForm = [self.evaFormModel.evaFormList objectAtIndex:indexPath.row];
    cell.evalForm = evalForm;
    cell.lblTitle.text = [NSString stringWithFormat:@"%i. %@", indexPath.row + 1, evalForm.question];
    return cell;
    
}

#pragma KeyBoard Observer
- (void) keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
//    CGPoint scrollPoint = CGPointMake(0.0, 0.0);
//    [self.tableView setContentOffset:scrollPoint animated:YES];
    
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat _keyBoardHeigh = keyboardSize.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, _keyBoardHeigh, 0.0);
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    //scroll to last cell
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.evaFormModel.evaFormList.count inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
