//
//  CMUSISTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/9/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUSISTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UIImageView *imvIcon;
@property(nonatomic, weak) IBOutlet UILabel *lblTitle;
@property(nonatomic, weak) IBOutlet UILabel *lblDetail;
@property(nonatomic, weak) IBOutlet UILabel *lblDate;
@property(nonatomic, weak) IBOutlet UIImageView *imvAccessory;
@property(nonatomic, weak) IBOutlet UILabel *lblSec;
@property (weak, nonatomic) IBOutlet UILabel *lblLock;
@end
