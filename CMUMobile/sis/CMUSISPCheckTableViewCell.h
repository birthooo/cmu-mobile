//
//  SISPCheckTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/28/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUSISModel.h"

@interface CMUSISPCheckTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUSISPCheckQuestion *pCheckQuestion;
@end
