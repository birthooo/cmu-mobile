//
//  CMUSISEvaluateTableViewCell.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/21/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUSISModel.h"

@interface CMUSISEvaluateTableViewCell : UITableViewCell
@property(nonatomic, strong) CMUSISEvalForm *evalForm;
@property(nonatomic, weak) IBOutlet UILabel *lblTitle;
@end
