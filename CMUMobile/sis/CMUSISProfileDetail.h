//
//  CMUSISProfileDetail.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/9/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMUSISProfileDetail : CMUNavBarViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *detailWebView;
@property NSString *selectedLabel;
@property NSString *selectedURL;
@end
