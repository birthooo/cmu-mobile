//
//  CMUSISGridMenuViewController.h
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 10/12/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUUserInfoModel.h"
@interface CMUSISGridMenuViewController : CMUNavBarViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *educationView;
@property (weak, nonatomic) IBOutlet UIView *stdOrganizationView;
@property (weak, nonatomic) IBOutlet UIView *stdActivityView;
@property (weak, nonatomic) IBOutlet UIView *stdResearchView;
@property (weak, nonatomic) IBOutlet UIView *stdHonorView;
@property (weak, nonatomic) IBOutlet UIView *stdWorkView;
@property (weak, nonatomic) IBOutlet UIView *stdVehicleView;
@property(nonatomic, strong) CMUUserInfoModel *userInfoModel;
@end
