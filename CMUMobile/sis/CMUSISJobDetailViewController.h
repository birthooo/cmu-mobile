//
//  CMUSISJobDetailViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/10/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"

@interface CMUSISJobDetailViewController : CMUNavBarViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSString *selectedTitle;
@end
