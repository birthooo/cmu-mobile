//
//  CMUSISEvaluateTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/21/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISEvaluateTableViewCell.h"
@interface CMUSISEvaluateTableViewCell()

@property(nonatomic, weak) IBOutlet UIImageView *imvStar1;
//@property(nonatomic, weak) IBOutlet UILabel *lblStar1;
@property(nonatomic, weak) IBOutlet UIImageView *imvStar2;
//@property(nonatomic, weak) IBOutlet UILabel *lblStar2;
@property(nonatomic, weak) IBOutlet UIImageView *imvStar3;
//@property(nonatomic, weak) IBOutlet UILabel *lblStar3;
@property(nonatomic, weak) IBOutlet UIImageView *imvStar4;
//@property(nonatomic, weak) IBOutlet UILabel *lblStar4;
@property(nonatomic, weak) IBOutlet UIImageView *imvStar5;
//@property(nonatomic, weak) IBOutlet UILabel *lblStar5;
@end

@implementation CMUSISEvaluateTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starTapped:)];
    [tap1 setNumberOfTouchesRequired:1];
    [tap1 setNumberOfTapsRequired:1];
    [self.imvStar1 addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starTapped:)];
    [tap2 setNumberOfTouchesRequired:1];
    [tap2 setNumberOfTapsRequired:1];
    [self.imvStar2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starTapped:)];
    [tap3 setNumberOfTouchesRequired:1];
    [tap3 setNumberOfTapsRequired:1];
    [self.imvStar3 addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starTapped:)];
    [tap4 setNumberOfTouchesRequired:1];
    [tap4 setNumberOfTapsRequired:1];
    [self.imvStar4 addGestureRecognizer:tap4];
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(starTapped:)];
    [tap5 setNumberOfTouchesRequired:1];
    [tap5 setNumberOfTapsRequired:1];
    [self.imvStar5 addGestureRecognizer:tap5];
}

- (void)setEvalForm:(CMUSISEvalForm *)evalForm
{
    _evalForm = evalForm;
    [self loadFromModel];
}

- (void)loadFromModel
{
    UIImage *starHeightlighted = [UIImage imageNamed:@"sis_star_hilighted.png"];
    UIImage *starNormal = [UIImage imageNamed:@"sis_star_normal.png"];
    self.imvStar1.image = self.evalForm.score >=1?starHeightlighted:starNormal;
    self.imvStar2.image = self.evalForm.score >=2?starHeightlighted:starNormal;
    self.imvStar3.image = self.evalForm.score >=3?starHeightlighted:starNormal;
    self.imvStar4.image = self.evalForm.score >=4?starHeightlighted:starNormal;
    self.imvStar5.image = self.evalForm.score >=5?starHeightlighted:starNormal;
}

- (IBAction)starTapped:(id) sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *)sender;
    UIView *source = gesture.view;
    if(source == self.imvStar1) self.evalForm.score = 1;
    if(source == self.imvStar2) self.evalForm.score = 2;
    if(source == self.imvStar3) self.evalForm.score = 3;
    if(source == self.imvStar4) self.evalForm.score = 4;
    if(source == self.imvStar5) self.evalForm.score = 5;
    [self loadFromModel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.lblTitle.text = nil;
    self.imvStar1.image = nil;
    self.imvStar2.image = nil;
    self.imvStar3.image = nil;
    self.imvStar4.image = nil;
    self.imvStar5.image = nil;
}

@end
