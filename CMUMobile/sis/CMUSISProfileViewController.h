//
//  CMUSISProfileViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/8/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"
#import "CMUSISProfileTableViewCell.h"
#import "CMUSISProfileDetail.h"
@interface CMUSISProfileViewController : CMUNavBarViewController<UITableViewDataSource, UITableViewDelegate>

@end
