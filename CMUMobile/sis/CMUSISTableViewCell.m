//
//  CMUSISTableViewCell.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/9/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation CMUSISTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.imvIcon sd_cancelCurrentImageLoad];
    self.imvIcon.image = nil;
    self.lblTitle.text = nil;
    self.lblDetail.text = nil;
    self.lblDate.text = nil;
    self.lblSec.text = nil;
}

@end
