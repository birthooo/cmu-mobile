//
//  CMUSISViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 1/9/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISViewController.h"
#import "CMUWebServiceManager.h"
#import "CMUSISTableViewCell.h"
#import "CMUFormatUtils.h"
#import "CMUStringUtils.h"
#import "UIImageView+WebCache.h"
#import "UIColor+CMU.h"

static NSString *sisTableViewCell = @"CMUSISTableViewCell";

@interface CMUSISViewController ()
//Tab
@property(nonatomic, weak)IBOutlet UISegmentedControl *segmentController;

//eval subject
@property(nonatomic, weak)IBOutlet UIView *evaSubjectView;
@property(nonatomic, weak)IBOutlet UITableView *evaSubjectTableView;
@property(nonatomic, weak)IBOutlet UILabel *lblEvaSubjectNumber;

//eval teacher
@property(nonatomic, weak)IBOutlet UIView *evaTeacherView;
@property(nonatomic, weak)IBOutlet UITableView *evaTeacherTableView;
@property(nonatomic, weak)IBOutlet UILabel *lblEvaTeacherNumber;

//Model subject and teacher
@property(nonatomic, strong)CMUSISFeedModel *sisFeedModel;

@end

@implementation CMUSISViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        _selectedTab = SISTabSubject;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(IS_IPAD)
    {
        UIFont *font = [UIFont systemFontOfSize:24.0f];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                               forKey:NSFontAttributeName];
        [self.segmentController setTitleTextAttributes:attributes
                                        forState:UIControlStateNormal];
    }
    
    
    [self.evaSubjectTableView registerNib:[UINib nibWithNibName:@"CMUSISTableViewCell" bundle:nil] forCellReuseIdentifier:sisTableViewCell];
    [self.evaTeacherTableView registerNib:[UINib nibWithNibName:@"CMUSISTableViewCell" bundle:nil] forCellReuseIdentifier:sisTableViewCell];
    
    self.lblEvaSubjectNumber.layer.cornerRadius = 5;
    self.lblEvaSubjectNumber.layer.masksToBounds = YES;
    self.lblEvaTeacherNumber.layer.cornerRadius = 5;
    self.lblEvaTeacherNumber.layer.masksToBounds = YES;
    
    [self refreshData:NO];
}

- (IBAction)onSegmentSelect:(id) sender
{
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        _selectedTab = SISTabSubject;
        [self.evaSubjectTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    else
    {
       _selectedTab = SISTabTeacher;
        [self.evaTeacherTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    [self updatePresentState];
}


- (void)updatePresentState
{
    [super updatePresentState];
    
    //tab selection
    if(_selectedTab == SISTabSubject)
        self.segmentController.selectedSegmentIndex = 0;
    if(_selectedTab == SISTabTeacher)
        self.segmentController.selectedSegmentIndex = 1;
    //view display
    self.evaSubjectView.hidden = _selectedTab != SISTabSubject;
    self.evaTeacherView.hidden = _selectedTab != SISTabTeacher;
    
    self.lblEvaSubjectNumber.text = [CMUFormatUtils formatBadge: self.sisFeedModel.numberSubject];
    self.lblEvaTeacherNumber.text = [CMUFormatUtils formatBadge: self.sisFeedModel.numberTeacher];
    self.lblEvaSubjectNumber.hidden = self.sisFeedModel.numberSubject <= 0;
    self.lblEvaTeacherNumber.hidden = self.sisFeedModel.numberTeacher <= 0;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)refreshData:(BOOL) scrollToTop
{
    [self showHUDLoading];
    [[CMUWebServiceManager sharedInstance] getEvaSubject:self success:^(id result) {
        self.sisFeedModel = (CMUSISFeedModel *)result;
        [self.evaSubjectTableView reloadData];
        [self.evaTeacherTableView reloadData];
        if(scrollToTop)
        {
            [self.evaSubjectTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
            [self.evaTeacherTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        }
        [self updatePresentState];
        [self dismisHUD];
    } failure:^(NSError *error) {
        DebugLog(@"%@", error);
        [self dismisHUD];
        [self alert:CMU_ERROR_READ_DATA];
    }];
}


#pragma mark section table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.evaSubjectTableView)
    {
        return self.sisFeedModel.evaSubjectList.count;
    }
    else if(tableView == self.evaTeacherTableView)
    {
        return self.sisFeedModel.evaTeacherList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CMUSISTableViewCell *cell = (CMUSISTableViewCell*)[tableView dequeueReusableCellWithIdentifier:sisTableViewCell forIndexPath:indexPath];
    if(tableView == self.evaSubjectTableView)
    {
        CMUSISEvalSubject *model = [self.sisFeedModel.evaSubjectList objectAtIndex:indexPath.row];
        
        //cell.imvIcon;
        cell.imvIcon.image = [UIImage imageNamed:@"noti_cmu_logo.png"];
        cell.lblTitle.text = model.subjectCode;
        cell.lblDetail.text = model.subjectName;
        cell.lblDate.text = [CMUFormatUtils formatDateNotificationFeedStyle:model.evaDate locale:[NSLocale currentLocale]];
        
        bool completed = model.status;
        cell.lblDate.hidden = !completed;
        cell.imvAccessory.hidden = completed;
        cell.lblTitle.textColor = completed?[UIColor cmuMediumBlackColor]:[UIColor cmuYellowColor];
        cell.lblDetail.textColor = completed?[UIColor cmuLightBlackColor]:[UIColor blackColor];
        cell.contentView.backgroundColor = completed?[UIColor colorWithRed:241.0/255 green:241.0/255 blue:241.0/255 alpha:1.0]:[UIColor whiteColor];
        cell.lblSec.text = [NSString stringWithFormat:@"sec %@ : lab %@",model.sectionName, model.labName];
        cell.lblSec.hidden = NO;
        cell.lblLock.hidden = !model.isLockSubject;
        NSLog(@"isLockSubject:%d",model.isLockSubject);
    }
    else if(tableView == self.evaTeacherTableView)
    {
        CMUSISEvalTeacher *model = [self.sisFeedModel.evaTeacherList objectAtIndex:indexPath.row];

        //cell.imvIcon;
        if([CMUStringUtils isEmpty:model.teacherImage])
        {
            cell.imvIcon.image = [UIImage imageNamed:@"sis_teacher_logo.png"];
        }
        else
        {
            [cell.imvIcon sd_setImageWithURL: [NSURL URLWithString: model.teacherImage]];
        }
        cell.lblTitle.text = model.teacherName;
        cell.lblDetail.text = [NSString stringWithFormat:@"%@ %@", model.subjectCode, model.subjectName];
        
        cell.lblDate.text = [CMUFormatUtils formatDateNotificationFeedStyle:model.evaDate locale:[NSLocale currentLocale]];
        
        bool completed = model.status;
        cell.lblDate.hidden = !completed;
        cell.imvAccessory.hidden = completed;
        cell.lblTitle.textColor = completed?[UIColor cmuMediumBlackColor]:[UIColor cmuYellowColor];
        cell.lblDetail.textColor = completed?[UIColor cmuLightBlackColor]:[UIColor blackColor];
        cell.contentView.backgroundColor = completed?[UIColor colorWithRed:241.0/255 green:241.0/255 blue:241.0/255 alpha:1.0]:[UIColor whiteColor];
        cell.lblSec.hidden = YES;
        cell.lblLock.hidden = !model.isLockTeacher;
        NSLog(@"isLockTeacher:%d",model.isLockTeacher);
    }
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id  evaModel = nil;
    if(tableView == self.evaSubjectTableView)
    {
        CMUSISEvalSubject *evaSubject = [self.sisFeedModel.evaSubjectList objectAtIndex:indexPath.row];
        if(evaSubject.status == true)
        {
            return;
        }
        evaModel = evaSubject;
    }
    else
    {
        CMUSISEvalTeacher *evaTeacher = [self.sisFeedModel.evaTeacherList objectAtIndex:indexPath.row];
        if(evaTeacher.status == true)
        {
            return;
        }
        evaModel = evaTeacher;
        
    }
    
    
    CMUSISEvaluateViewController *evaViewController = [[CMUSISEvaluateViewController alloc] init];
    evaViewController.evaModel = evaModel;
    evaViewController.delegate = self;
    
    [self.navigationController pushViewController:evaViewController animated:YES];
}


#pragma mark CMUBroadcastSenderDetailViewControllerDelegate
- (void)evaluateViewControllerBackButtonClicked:(CMUSISEvaluateViewController *) evaluateViewController
{
    [self refreshData:NO];
    [self.navigationController popToViewController:self animated:YES];
}

- (void)evaluateViewControllerDidFinishedSaveForm:(CMUSISEvaluateViewController *) evaluateViewController
{
    [self refreshData:NO];
    [self.navigationController popToViewController:self animated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
