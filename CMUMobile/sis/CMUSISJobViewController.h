//
//  CMUSISJobViewController.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/9/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUSISJobTableViewCell.h"
#import "CMUSISJobDetailViewController.h"
@interface CMUSISJobViewController : CMUNavBarViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
