//
//  CMUSISGridMenuViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 10/12/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISGridMenuViewController.h"
#import "CMUSISGridMenuDetailViewController.h"
#import "CMUModel.h"
#import "CMUWebServiceManager.h"
#import "UIColor+CMU.h"
#import "CMUAppDelegate.h"
@interface CMUSISGridMenuViewController ()

@end

@implementation CMUSISGridMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(myEvent)];
    [self.stdVehicleView addGestureRecognizer:singleFingerTap];
    
    [self showFlipAnimation];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)myEvent{
     NSLog(@"TOUCH");
   // [self goToViewcontroller:@"TEST" storyboardID:@"CMUSISGridMenu"];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.stdVehicleView]) {
       
        return NO;
    }
    return YES;
}
- (void)showFlipAnimation
{
    [self performSelector:@selector(flipAnimation:) withObject:self.infoView afterDelay:0.1f];
    [self performSelector:@selector(flipAnimation:) withObject:self.educationView afterDelay:0.2f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdActivityView afterDelay:0.3f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdOrganizationView afterDelay:0.4f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdResearchView afterDelay:0.5f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdWorkView afterDelay:0.6f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdHonorView afterDelay:0.7f];
    [self performSelector:@selector(flipAnimation:) withObject:self.stdVehicleView afterDelay:0.8f];
   // [self performSelector:@selector(flipAnimation:) withObject:self.stdActivityView afterDelay:0.9f];
   // [self performSelector:@selector(flipAnimation:) withObject:self.stdOrganizationView afterDelay:1.0f];
 
}

-(void)flipAnimation:(UIView *) view{
    [UIView transitionWithView:view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft//|UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        view.alpha = 1.0;
                    } completion:^(BOOL finished) {
                    }];
}

#pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
      CMUSISGridMenuDetailViewController *vc = [segue destinationViewController];
     vc.selectedWebView = @"https://mobileapi.cmu.ac.th/Registration/profile";
     CMUUser *user = [[[CMUWebServiceManager sharedInstance] userInfoModel] user];
     CMUTicket *userticket = [[CMUWebServiceManager sharedInstance] ticket];
     NSString *stringGET;
     if ([[segue identifier] isEqualToString:@"vehicle"])
     {
         vc.selectedTitle = @"ข้อมูลด้านยานพาหนะ";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentVehicle/VehicleInfo?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"work"]){
         vc.selectedTitle = @"ข้อมูลการทำงานพิเศษ";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentPartTimeJob/PartTimeJob?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"honor"]){
         vc.selectedTitle = @"ข้อมูลเกียรติประวัติ";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentPrestige/Prestige?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     //
     if([[segue identifier]isEqualToString:@"scholarship"]){
         vc.selectedTitle = @"ข้อมูลทุนการศึกษา";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentScholarship/Scholarship?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"education"]){
         vc.selectedTitle = @"ข้อมูลการศึกษา";
         //https://mobileapi.cmu.ac.th/WebviewCurriculumPlan/IndexForStudent?StudentCode
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/WebviewCurriculumPlan/IndexForStudent?StudentCode=%@",user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"organization"]){
         vc.selectedTitle = @"ข้อมูลองค์การศึกษา";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentProfileAffairs/StudentProfileAffairs?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"vinai"]){
         vc.selectedTitle = @"ข้อมูลวินัยนักศึกษา";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentDiscipline/Discipline?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"profile"]){
         vc.selectedTitle = @"ข้อมูลส่วนบุคคล";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/Registration/profile?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     if([[segue identifier]isEqualToString:@"activity"]){
         vc.selectedTitle = @"ข้อมูลกิจกรรมนักศึกษา";
         stringGET = [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentActivity/Activity?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
         vc.selectedWebView = stringGET;
     }
     
}
 
-(void)goToViewcontroller:(NSString *)storyboardName storyboardID:(NSString*)StoryboardID{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:StoryboardID];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
