//
//  CMUSISEvaluateViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 1/12/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMUNavBarViewController.h"

@class CMUSISEvaluateViewController;
@protocol CMUSISEvaluateViewControllerDelegate <NSObject>
- (void)evaluateViewControllerBackButtonClicked:(CMUSISEvaluateViewController *) evaluateViewController;
- (void)evaluateViewControllerDidFinishedSaveForm:(CMUSISEvaluateViewController *) evaluateViewController;
@end

@interface CMUSISEvaluateViewController : CMUNavBarViewController<UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, unsafe_unretained)id<CMUSISEvaluateViewControllerDelegate> delegate;
@property(nonatomic, strong) id evaModel;
@end
