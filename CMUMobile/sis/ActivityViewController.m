//
//  ActivityViewController.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 11/29/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "ActivityViewController.h"
#import "CMUWebServiceManager.h"
@interface ActivityViewController ()

@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.scrollView.bounces = NO;
    CMUUser *user = [[[CMUWebServiceManager sharedInstance] userInfoModel] user];
    CMUTicket *userticket = [[CMUWebServiceManager sharedInstance] ticket];
    NSString *stringURL= [NSString stringWithFormat:@"https://mobileapi.cmu.ac.th/StudentActivity/getActivityForjoint?token=%@&account=%@&studentCode=%@",userticket.accessToken,userticket.userName,user.studentId];
    NSLog(@"stringURL :%@",stringURL);
    //NSString *stringURL= @"https://www.facebook.com";
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:stringURL]];
    [self.webView loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
