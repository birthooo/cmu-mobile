//
//  CMUSISSummaryViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/11/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISSummaryViewController.h"
#import "SVProgressHUD.h"

@interface CMUSISSummaryViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, assign)BOOL firstLoad;
@end

@implementation CMUSISSummaryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _firstLoad = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismisHUD];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebviewDelegate
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if(_firstLoad)
    {
        _firstLoad = NO;
        [SVProgressHUD showWithStatus:CMU_HUD_LOADING maskType:SVProgressHUDMaskTypeNone];
    }
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}

@end
