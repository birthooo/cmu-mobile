//
//  CMUSISActivityTableViewCell.m
//  CMUMobile
//
//  Created by Tharadol Chanyutthana on 11/1/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import "CMUSISActivityTableViewCell.h"

@implementation CMUSISActivityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.joinButton.layer.cornerRadius = 10;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)joinActivityAction:(id)sender {
    NSLog(@"pressed me");
        if(self.joinButton.selected){
            self.joinButton.selected = NO;
            [self.joinButton setTitle:@"ลงชื่อเข้าร่วม" forState:UIControlStateNormal];
        }
        else{   [self.joinButton setTitle:@"ลงชื่อเข้าร่วมแล้ว" forState:UIControlStateSelected];
            self.joinButton.selected = YES;
        }
    
}

@end
