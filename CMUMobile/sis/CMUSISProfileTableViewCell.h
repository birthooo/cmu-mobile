//
//  CMUSISProfileTableViewCell.h
//  CMUMobile
//
//  Created by Tharadol-ITSC on 8/9/2559 BE.
//  Copyright © 2559 IBSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMUSISProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
