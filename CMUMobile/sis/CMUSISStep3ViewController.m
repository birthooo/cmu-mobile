//
//  CMUSISStep3ViewController.m
//  CMUMobile
//
//  Created by Nikorn lansa on 3/11/2558 BE.
//  Copyright (c) 2558 IBSS. All rights reserved.
//

#import "CMUSISStep3ViewController.h"
#import "SVProgressHUD.h"
#import "CMUStringUtils.h"

@interface CMUSISStep3ViewController ()
@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, assign)BOOL firstLoad;
@end

@implementation CMUSISStep3ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _firstLoad = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    self.webView.scalesPageToFit = YES;
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismisHUD];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebviewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    NSString *urlStr = [url absoluteString];
    if([CMUStringUtils containsString:urlStr substring:@"google"] || [CMUStringUtils containsString:urlStr substring:@"error"])
    {
        DebugLog(@"summit error with url%@", urlStr);
    }
    else
    {
        return YES;
    }
    return NO;
}

- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if(_firstLoad)
    {
        _firstLoad = NO;
        [SVProgressHUD showWithStatus:CMU_HUD_LOADING maskType:SVProgressHUDMaskTypeNone];
    }
    
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [self dismisHUD];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self dismisHUD];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
