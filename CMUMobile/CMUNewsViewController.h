//
//  CMUNewsViewController.h
//  CMUMobile
//
//  Created by Nikorn lansa on 6/21/2557 BE.
//  Copyright (c) 2557 IBSS. All rights reserved.
//

#import "CMUNavBarViewController.h"
#import "CMUNewsTableViewCell.h"
#import "PopoverView.h"
#import "CMUNewsTypePopupView.h"

typedef enum {
    NEWS_VIEW_MODE_LIST = 0,
    NEWS_VIEW_MODE_DETAIL = 1,
    
} NewsViewMode;


@interface CMUNewsViewController : CMUNavBarViewController<CMUNewsTableViewCellDelegate, PopoverViewDelegate, CMUNewsTypePopupViewDelegate, UITableViewDelegate, UITableViewDataSource>

//reuse controller for news detail (from notification)
@property(nonatomic, unsafe_unretained) NewsViewMode newsViewMode;
@property(nonatomic, unsafe_unretained) int newsDetailItemId;
@property NSString *num;
@end
